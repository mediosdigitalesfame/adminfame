<!-- DataTables -->

<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/datatables/dataTables.bootstrap4.css"> 



<!-- Content Wrapper. Contains page content -->

<div class="content-wrapper">

  <section class="content">

    <!-- For Messages -->

    <?php $this->load->view('admin/includes/_messages.php') ?>

    <div class="card">

      <div class="card-header">

        <div class="d-inline-block">

          <h3 class="card-title"><i class="fa fa-list"></i>&nbsp; Grafico de Leads de Postventa</h3>

        </div>

        <div class="d-inline-block float-right">

          <?php if($this->rbac->check_operation_permission('kk')): ?>

            <a href="<?= base_url('admin/guides/add'); ?>" class="btn btn-success"><i class="fa fa-plus"></i> <?= trans('add_new_guide') ?></a>

          <?php endif; ?>  

          <?php if($this->rbac->check_operation_permission('kk')): ?>  

            <a href="<?= base_url('admin/guides/create_guides_pdf') ?>" class="btn btn-success"><?= trans('export_as_pdf') ?></a>

          <?php endif; ?>

          <?php if($this->rbac->check_operation_permission('kk')): ?>  

            <a href="<?= base_url('admin/guides/export_csv') ?>" class="btn btn-success"><?= trans('export_as_csv') ?></a>

          <?php endif; ?>

        </div>

      </div>

    </div>



    <!--<div class="card">

      <div class="card-body table-responsive"> 

        <?php echo form_open("/",'id="model_search"') ?>

        <div class="row">

          <div class="col-md-4">

            <label><?= trans('model_type') ?></label><hr style="margin:5px 0px;" />

            <input checked="checked" onchange="user_filter()" name="user_search_type" value="" type="radio"  /> <?= trans('all') ?>&nbsp;&nbsp;&nbsp;

            <input onchange="model_filter()" name="model_search_type" value="1" type="radio"  /> <?= trans('active') ?>&nbsp;&nbsp;&nbsp;

            <input onchange="model_filter()" name="model_search_type" value="0" type="radio"  /> <?= trans('inactive') ?>

          </div>

          <div class="col-md-3">

            <label>Date From:</label><input name="user_search_from" type="text" class="form-control form-control-inline input-medium datepicker" id="" />

          </div>

          <div class="col-md-3"> 

            <label>Date To:</label><input name="user_search_to" type="text" class="form-control form-control-inline input-medium datepicker" id="" /> 

          </div>

          <div class="col-md-2"> 

            <button type="button" style="margin-top:20px;" onclick="user_filter()" class="btn btn-info">Submit</button>

            <a href="<?= base_url('admin/guides/advance_search'); ?>" class="btn btn-danger" style="margin-top:20px;">

              <i class="fa fa-repeat"></i>

            </a>

          </div>

        </div>

        <?php echo form_close(); ?>

      </div>

    </div>-->  





    <div class="card">

      <div class="card-body table-responsive">

  

<?php
 
  switch ($idagen) {
    case 1:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/71064d66-308e-4d98-95c5-5a74e8b6db22/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
    case 2:
        echo "No Hay";
        break;
    case 3:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/6661dcdd-56d6-4c08-a0ab-18bcbb5edefa/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 4:
        echo '<iframe width="100%" height="1284" src="https://datastudio.google.com/embed/reporting/b13b2d21-7cd0-4157-8f93-197aa016da4d/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 5:
        echo '<iframe width="100%" height="788" src="https://datastudio.google.com/embed/reporting/45afa4ac-fec4-4707-baeb-4ba062d7fa7a/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 6:
        echo "No Hay";
        break;
        case 7:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/4748fade-6f1e-4b09-8f20-f4f5d9ce5d44/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 8:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/c07ded48-5044-4ed6-ae51-8bac0d8a7871/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 9:
        echo '<iframe width="100%" height="1300" src="https://datastudio.google.com/embed/reporting/1f3c681a-c6a9-4586-9380-773546f627fd/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 10:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/db34359f-21d1-4ad0-aed7-40b0232025dc/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 11:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/a3d51417-80ca-401c-8264-f3ce2e8c05ed/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 12:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/613fa54e-236a-4426-bd8c-bce5717d7357/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 13:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/73d4e919-c6bd-4790-83a5-8c7b8ff04a9b/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 14:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/06e4f44a-31d2-40e0-aafb-b9a20cb4e10d/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 15:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/33b6bc22-06ed-4fca-9445-6ddaed28f603/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 16:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/a0cd63ca-60c4-459b-9533-a4dbbb8153e6/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 17:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/d67cbb0f-2ca6-4033-9daf-e56e17773201/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 18:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/ad4334ff-59cd-4159-935b-032ad4234b14/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 19:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/28c08b49-f808-42f6-bd3c-fdf02852f647/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 20:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/19db757a-8121-4318-aa85-d6e57caff707/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 21:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/2ef2d910-b2a8-4ceb-91b3-b6e9cb685a1a/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 22:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/ee3b01d9-0a54-4f1d-91d8-ce712ae51c29/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 23:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/0cb6a2e7-c8d5-4986-b6fe-f013b2a26910/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 24:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/82c2f669-3f7e-4091-8325-d2986971f781/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 25:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/d4998998-6212-4455-8cef-81d551c5775b/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 26:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/2146c145-f915-4b62-a464-93f2d8ef0981/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 27:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/922ade3b-784d-4315-97e6-4ab83372b4f8/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 28:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/88421fc1-a779-454b-8f6f-6f0ee7d3b050/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 29:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/0fceb0e5-60c1-46f2-8686-5154bbfc0dee/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 30:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/83cdc2b4-09b0-4e60-bdda-7bd317a1930e/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
         case 31:
        echo 'No Tiene';
        break;
         case 32:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/2d567b98-ea3e-4432-8d18-e55d27e39dec/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
         case 33:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/eca4b268-a1bc-42fc-8047-08082859a3e6/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
         case 34:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/ae917fff-5156-4c56-b1d5-2a3a4082e8ba/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
         case 35:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/42554d04-ed61-4584-9ba3-60c99f84fed3/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
         case 36:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/54c6d18d-7b75-440c-af86-db41a82744f6/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
         case 37:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/aa751c7b-950d-4168-924b-3b94a8cae648/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
         case 38:
        echo "No Tiene";
        break;
         case 39:
        echo 'No Tiene';
        break;
         case 40:
        echo "No Tiene";
        break;
         case 41:
        echo "No Tiene";
        break;
         case 42:
        echo "No Tiene";
        break;

        case 91:
        echo '<iframe width="100%" height="1742" src="https://datastudio.google.com/embed/reporting/bb4102ce-8a0d-48b1-8688-b7de8549e058/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 92:
        echo '<iframe width="100%" height="1742" src="https://datastudio.google.com/embed/reporting/4ab1e2ab-f73b-41dc-b440-6c23b63cd988/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 93:
        echo '<iframe width="100%" height="1742" src="https://datastudio.google.com/embed/reporting/137d5078-7886-4761-846b-991e89734617/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 94:
        echo '<iframe width="100%" height="1742" src="https://datastudio.google.com/embed/reporting/e258ac3e-521b-4250-a083-1231ceb8ec7b/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 95:
        echo '<iframe width="100%" height="1742" src="https://datastudio.google.com/embed/reporting/a9159701-1d04-4d4d-959d-c119697830a6/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 96:
        echo '<iframe width="100%" height="1742" src="https://datastudio.google.com/embed/reporting/4e4c1c30-2ae4-459a-a745-4c24914681e6/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;

        case 100:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/71064d66-308e-4d98-95c5-5a74e8b6db22/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;




}
?>



       

 

        <!--<table id="na_datatable" class="table table-bordered table-striped" width="100%">

          <thead>

            <tr>

              <th>#<?= trans('id') ?></th>

              <th>Nombre</th>

              

              <th><?= trans('status') ?></th>

              <th width="100" class="text-right"><?= trans('action') ?></th>

            </tr>

          </thead>

        </table>-->

      </div>

    </div>














  </section>  

</div>



<!-- bootstrap datepicker -->

<!-- datepicker -->

<script src="<?= base_url() ?>assets/plugins/datepicker/bootstrap-datepicker.js"></script>

<script>

  $('.datepicker').datepicker({

    autoclose: true

  });

</script>

<!-- DataTables -->

<script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.js"></script>

<script src="<?= base_url() ?>assets/plugins/datatables/dataTables.bootstrap4.js"></script>



<script>

  //---------------------------------------------------

  var table = $('#na_datatable').DataTable( {

    "processing": true,

    "serverSide": false,

    "ajax": "<?=base_url('admin/leads/datatable_json')?>",

    "order": [[0,'desc']],

    "columnDefs": [

    { "targets": 0, "name": "id", 'searchable':true, 'orderable':true},

    { "targets": 1, "name": "name", 'searchable':true, 'orderable':true},

     

    { "targets": 2, "name": "Action", 'searchable':false, 'orderable':false,'width':'100px'}

    ]

  });

</script>



<script>

  //---------------------------------------------------

  function model_filter()

  {

    var _form = $("#model_search").serialize();

    $.ajax({

      data: _form,

      type: 'post',

      url: '<?php echo base_url();?>admin/guides/search',

      async: true,

      success: function(output){

        table.ajax.reload( null, false );

      }

    });

  }

</script>



<script type="text/javascript">

  $("body").on("change",".tgl_checkbox",function(){

    console.log('checked');

    $.post('<?=base_url("admin/guides/change_status")?>',

    {

      '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>',

      id : $(this).data('id'),

      status : $(this).is(':checked') == true?1:0

    },

    <?php if($this->rbac->check_operation_permission('add')): ?>

      function(data){$.notify("El Status cambió con éxito", "success");}

      <?php else: ?>

        function(data){$.notify("No Tienes Permisos", "warn");}

      <?php endif; ?>

      );

  });

</script>





