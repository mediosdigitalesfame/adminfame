<!-- DataTables -->

<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/datatables/dataTables.bootstrap4.css"> 



<!-- Content Wrapper. Contains page content -->

<div class="content-wrapper">

  <section class="content">

    <!-- For Messages -->

    <?php $this->load->view('admin/includes/_messages.php') ?>

    <div class="card">

      <div class="card-header">

        <div class="d-inline-block">

          <h3 class="card-title"><i class="fa fa-list"></i>&nbsp; Grafico Global de Leads</h3>

        </div>

        <div class="d-inline-block float-right">

          <?php if($this->rbac->check_operation_permission('kk')): ?>

            <a href="<?= base_url('admin/guides/add'); ?>" class="btn btn-success"><i class="fa fa-plus"></i> <?= trans('add_new_guide') ?></a>

          <?php endif; ?>  

          <?php if($this->rbac->check_operation_permission('kk')): ?>  

            <a href="<?= base_url('admin/guides/create_guides_pdf') ?>" class="btn btn-success"><?= trans('export_as_pdf') ?></a>

          <?php endif; ?>

          <?php if($this->rbac->check_operation_permission('kk')): ?>  

            <a href="<?= base_url('admin/guides/export_csv') ?>" class="btn btn-success"><?= trans('export_as_csv') ?></a>

          <?php endif; ?>

        </div>

      </div>

    </div>



    <!--<div class="card">

      <div class="card-body table-responsive"> 

        <?php echo form_open("/",'id="model_search"') ?>

        <div class="row">

          <div class="col-md-4">

            <label><?= trans('model_type') ?></label><hr style="margin:5px 0px;" />

            <input checked="checked" onchange="user_filter()" name="user_search_type" value="" type="radio"  /> <?= trans('all') ?>&nbsp;&nbsp;&nbsp;

            <input onchange="model_filter()" name="model_search_type" value="1" type="radio"  /> <?= trans('active') ?>&nbsp;&nbsp;&nbsp;

            <input onchange="model_filter()" name="model_search_type" value="0" type="radio"  /> <?= trans('inactive') ?>

          </div>

          <div class="col-md-3">

            <label>Date From:</label><input name="user_search_from" type="text" class="form-control form-control-inline input-medium datepicker" id="" />

          </div>

          <div class="col-md-3"> 

            <label>Date To:</label><input name="user_search_to" type="text" class="form-control form-control-inline input-medium datepicker" id="" /> 

          </div>

          <div class="col-md-2"> 

            <button type="button" style="margin-top:20px;" onclick="user_filter()" class="btn btn-info">Submit</button>

            <a href="<?= base_url('admin/guides/advance_search'); ?>" class="btn btn-danger" style="margin-top:20px;">

              <i class="fa fa-repeat"></i>

            </a>

          </div>

        </div>

        <?php echo form_close(); ?>

      </div>

    </div>-->  





    <div class="card">

      <div class="card-body table-responsive">

  

<?php
 
  switch ($idagen) {
    case 1:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/8979267c-7040-4ffe-81cf-30318cb0181c/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
    case 2:
        echo "No Tiene";
        break;
    case 3:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/f583ee19-2994-4100-9ca6-94fc954de240/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 4:
        echo '<iframe width="100%" height="1284" src="https://datastudio.google.com/embed/reporting/b07bce0a-8560-4f49-9836-998ecc3c707d/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 5:
        echo '<iframe width="100%" height="788" src="https://datastudio.google.com/embed/reporting/fd58881e-fa93-463f-be48-10823b104ff5/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 6:
        echo "No Tiene";
        break;
        case 7:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/f254fe31-a479-4108-9d0c-d3bc15f6acb4/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 8:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/a7a8e4bd-4ad0-4754-9419-3ff6453f703b/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 9:
        echo '<iframe width="100%" height="1300" src="https://datastudio.google.com/embed/reporting/e6168f27-738b-4703-bbb9-9d03453a3b83/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 10:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/a2563079-7db5-45fa-bb9d-3ae12b78ee98/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 11:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/66741370-d476-45a1-85a9-ec8f68deecc9/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 12:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/0ce81597-f0c1-41a6-b272-bc4d814586b2/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 13:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/a9aebd29-4d29-4afc-be67-277f8da68bb6/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 14:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/7cb4bf13-5845-47c7-ba9a-fd08318e7197/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 15:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/27076110-801b-474c-acd6-7657676e2d9d/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 16:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/bc38e611-c234-4ee6-84d9-cc27aabe4912/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 17:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/2fea4d56-85e5-4dab-a6f8-840e0123f498/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 18:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/99a3ac5b-5049-49b5-abc9-e0304d0bf15e/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 19:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/87ce7553-d16f-4625-bb99-6e45f162c220/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 20:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/c7144d10-b63c-4667-b83f-e26316449971/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 21:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/af3c79e6-3d97-4565-8df3-e53b00ca77bb/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 22:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/9b6e9156-8e04-4eea-a9d4-9a9f8caf2a67/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 23:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/e152666c-2d38-4724-969d-9e5f490a9770/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 24:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/cf7910c2-a939-4bc1-a4b4-4ecbd83e1b79/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 25:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/341b3b5e-27bb-46c0-8454-ada616046112/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 26:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/80646ce5-e0f3-4a15-8c2d-31edc9795928/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 27:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/79baef11-5035-455c-84a3-30c88f2f3cfb/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 28:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/a6b6a704-adea-405c-a205-ece7b6b21100/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 29:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/4613ca27-6806-45ca-83e3-f9ec1e7ba801/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 30:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/46f73b0d-c0ef-48df-aa53-0cb3151393a8/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
         case 31:
        echo 'No Tiene';
        break;
         case 32:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/09ad266d-2b9d-46fe-8252-15c3177ec4a6/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
         case 33:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/7f1f9477-773d-438c-a2da-2f6efd2c3fc3/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
         case 34:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/ae5755d9-61f3-4851-9989-16b1d9a9bbc5/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
         case 35:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/f0275dcc-1f60-42ee-8159-975291b8a760/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
         case 36:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/7a70abf1-46a5-4892-ab84-299e8d3ce75b/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
         case 37:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/580b8b90-7235-49ef-b647-baaa540a6353/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
         case 38:
        echo "No Tiene";
        break;
         case 39:
        echo 'No Tiene';
        break;
         case 40:
        echo "No Tiene";
        break;
         case 41:
        echo "No Tiene";
        break;
         case 42:
        echo "No Tiene";
        break;

        case 91:
        echo '<iframe width="100%" height="1742" src="https://datastudio.google.com/embed/reporting/5ce1be1c-f1d3-408d-b1b9-9b53546f1179/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 92:
        echo '<iframe width="100%" height="1742" src="https://datastudio.google.com/embed/reporting/3f7b8659-1c87-41fa-9f41-05be92e67524/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 93:
        echo '<iframe width="100%" height="1742" src="https://datastudio.google.com/embed/reporting/6232ee7a-e470-4cfd-9bed-24b5c5b09d26/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 94:
        echo '<iframe width="100%" height="1742" src="https://datastudio.google.com/embed/reporting/6970c770-c447-41b5-8f88-c4895cbdb43e/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 95:
        echo '<iframe width="100%" height="1742" src="https://datastudio.google.com/embed/reporting/ec8e358d-e65e-4d83-b4b3-9d4416239bc3/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 96:
        echo '<iframe width="100%" height="1742" src="https://datastudio.google.com/embed/reporting/096952e7-9a44-410b-b5e9-d92c88f53d0e/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;

        case 100:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/8979267c-7040-4ffe-81cf-30318cb0181c/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;




}
?>



       

 

        <!--<table id="na_datatable" class="table table-bordered table-striped" width="100%">

          <thead>

            <tr>

              <th>#<?= trans('id') ?></th>

              <th>Nombre</th>

              

              <th><?= trans('status') ?></th>

              <th width="100" class="text-right"><?= trans('action') ?></th>

            </tr>

          </thead>

        </table>-->

      </div>

    </div>














  </section>  

</div>



<!-- bootstrap datepicker -->

<!-- datepicker -->

<script src="<?= base_url() ?>assets/plugins/datepicker/bootstrap-datepicker.js"></script>

<script>

  $('.datepicker').datepicker({

    autoclose: true

  });

</script>

<!-- DataTables -->

<script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.js"></script>

<script src="<?= base_url() ?>assets/plugins/datatables/dataTables.bootstrap4.js"></script>



<script>

  //---------------------------------------------------

  var table = $('#na_datatable').DataTable( {

    "processing": true,

    "serverSide": false,

    "ajax": "<?=base_url('admin/leads/datatable_json')?>",

    "order": [[0,'desc']],

    "columnDefs": [

    { "targets": 0, "name": "id", 'searchable':true, 'orderable':true},

    { "targets": 1, "name": "name", 'searchable':true, 'orderable':true},

     

    { "targets": 2, "name": "Action", 'searchable':false, 'orderable':false,'width':'100px'}

    ]

  });

</script>



<script>

  //---------------------------------------------------

  function model_filter()

  {

    var _form = $("#model_search").serialize();

    $.ajax({

      data: _form,

      type: 'post',

      url: '<?php echo base_url();?>admin/guides/search',

      async: true,

      success: function(output){

        table.ajax.reload( null, false );

      }

    });

  }

</script>



<script type="text/javascript">

  $("body").on("change",".tgl_checkbox",function(){

    console.log('checked');

    $.post('<?=base_url("admin/guides/change_status")?>',

    {

      '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>',

      id : $(this).data('id'),

      status : $(this).is(':checked') == true?1:0

    },

    <?php if($this->rbac->check_operation_permission('add')): ?>

      function(data){$.notify("El Status cambió con éxito", "success");}

      <?php else: ?>

        function(data){$.notify("No Tienes Permisos", "warn");}

      <?php endif; ?>

      );

  });

</script>





