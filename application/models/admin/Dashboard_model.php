<?php
	class Dashboard_model extends CI_Model
	{
		public function get_all_users(){
			return $this->db->count_all('ci_users');
		}

		public function get_all_brands(){
			return $this->db->count_all('brands');
		}

		public function get_all_models(){
			return $this->db->count_all('models');
		}

		public function get_all_business(){
			return $this->db->count_all('business');
		}

		public function get_all_agencys(){
			return $this->db->count_all('agencys');
		}

		public function get_all_guides(){
			return $this->db->count_all('guides');
		}

		public function get_all_takes(){
			return $this->db->count_all('seminuevostomados');
		}

		public function get_active_users(){
			$this->db->where('is_active', 1);
			return $this->db->count_all_results('ci_users');
		}

		public function get_active_guides(){
			$this->db->where('status', 1);
			return $this->db->count_all_results('guides');
		}

		public function get_active_takes(){
			$this->db->where('status', 1);
			return $this->db->count_all_results('seminuevostomados');
		}


		public function get_libre_checks(){
			$this->db->where('status', 0);
			return $this->db->count_all_results('checklist');
		}
		public function get_pendiente_checks(){
			$this->db->where('status', 1);
			return $this->db->count_all_results('checklist');
		}
		public function get_proceso_checks(){
			$this->db->where('status', 2);
			return $this->db->count_all_results('checklist');
		}
		public function get_terminado_checks(){
			$this->db->where('status', 3);
			return $this->db->count_all_results('checklist');
		}



		public function get_take_guides(){
			$this->db->where('statustoma', 1);
			return $this->db->count_all_results('guides');
		}

		public function get_take_checks(){
			$this->db->where('statuscheck', 1);
			return $this->db->count_all_results('seminuevostomados');
		}



		public function get_deactive_users(){
			$this->db->where('is_active', 0);
			return $this->db->count_all_results('ci_users');
		}

		public function get_deactive_guides(){
			$this->db->where('status', 0);
			return $this->db->count_all_results('guides');
		}

		public function get_deactive_takes(){
			$this->db->where('status', 0);
			return $this->db->count_all_results('seminuevostomados');
		}

	}

?>
