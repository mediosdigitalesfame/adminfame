<?php

$html = '
<table border="1" style="width:100%">
<thead>
<tr class="headerrow">
<th>ID</th>
<th>1</th>
<th>2</th>
<th>3</th>
<th>4</th>
<th>5</th>
<th>6</th>
<th>7</th>
<th>8</th>
<th>9</th>
<th>10</th>
<th>11</th>
<th>12</th>
<th>13</th>
<th>14</th>
<th>15</th>
<th>16</th>
<th>17</th>
<th>18</th>
<th>19</th>
<th>20</th>
<th>21</th>
<th>22</th>
<th>23</th>
<th>24</th>
<th>25</th>
<th>26</th>
<th>27</th>
<th>28</th>
<th>29</th>
<th>30</th>
<th>31</th>
<th>32</th>
<th>33</th>
</tr>
</thead>
<tbody>';

foreach($all_checks as $row):

	$html .= ' <tr> <td colspan="40">Creado:'.$row['created_at'].' Por:'.$row['firstname'].' '.$row['lastname'].' Folio:'.$row['folio'].'</td> </tr>';
	$html .= '<tr> ';
	$html .= '<td>'.$row['id'].'</td>';

	if($row['solicitudcfdi']==1){$html .= '<td> <i style="color:#15A615;">OK</i> </td>';}else{$html .= '<td> <i style="color:#CC0000;">--</i> </td>';}
	if($row['cambioderol']==1){$html .= '<td> <i style="color:#15A615;">OK</i> </td>';}else{$html .= '<td> <i style="color:#CC0000;">--</i> </td>';}
	if($row['cedulafiscal']==1){$html .= '<td> <i style="color:#15A615;">OK</i> </td>';}else{$html .= '<td> <i style="color:#CC0000;">--</i> </td>';}
	if($row['cfdisinactividad']==1){$html .= '<td> <i style="color:#15A615;">OK</i> </td>';}else{$html .= '<td> <i style="color:#CC0000;">--</i> </td>';}
	if($row['xml']==1){$html .= '<td> <i style="color:#15A615;">OK</i> </td>';}else{$html .= '<td> <i style="color:#CC0000;">--</i> </td>';}
	if($row['rfc']==1){$html .= '<td> <i style="color:#15A615;">OK</i> </td>';}else{$html .= '<td> <i style="color:#CC0000;">--</i> </td>';}
	if($row['validacioncfdi']==1){$html .= '<td> <i style="color:#15A615;">OK</i> </td>';}else{$html .= '<td> <i style="color:#CC0000;">--</i> </td>';}
	if($row['verificacioncfdi']==1){$html .= '<td> <i style="color:#15A615;">OK</i> </td>';}else{$html .= '<td> <i style="color:#CC0000;">--</i> </td>';}
	if($row['autenticaciondoctos']==1){$html .= '<td> <i style="color:#15A615;">OK</i> </td>';}else{$html .= '<td> <i style="color:#CC0000;">--</i> </td>';}
	if($row['reportetransunion']==1){$html .= '<td> <i style="color:#15A615;">OK</i> </td>';}else{$html .= '<td> <i style="color:#CC0000;">--</i> </td>';}
	if($row['noretencion']==1){$html .= '<td> <i style="color:#15A615;">OK</i> </td>';}else{$html .= '<td> <i style="color:#CC0000;">--</i> </td>';}
	if($row['secuenciafacturas']==1){$html .= '<td> <i style="color:#15A615;">OK</i> </td>';}else{$html .= '<td> <i style="color:#CC0000;">--</i> </td>';}
	if($row['identificacionoficial']==1){$html .= '<td> <i style="color:#15A615;">OK</i> </td>';}else{$html .= '<td> <i style="color:#CC0000;">--</i> </td>';}
	if($row['curp']==1){$html .= '<td> <i style="color:#15A615;">OK</i> </td>';}else{$html .= '<td> <i style="color:#CC0000;">--</i> </td>';}
	if($row['comprobante']==1){$html .= '<td> <i style="color:#15A615;">OK</i> </td>';}else{$html .= '<td> <i style="color:#CC0000;">--</i> </td>';}
	if($row['bajaplacas']==1){$html .= '<td> <i style="color:#15A615;">OK</i> </td>';}else{$html .= '<td> <i style="color:#CC0000;">--</i> </td>';}
	if($row['tenencias']==1){$html .= '<td> <i style="color:#15A615;">OK</i> </td>';}else{$html .= '<td> <i style="color:#CC0000;">--</i> </td>';}
	if($row['tarjetacirculacion']==1){$html .= '<td> <i style="color:#15A615;">OK</i> </td>';}else{$html .= '<td> <i style="color:#CC0000;">--</i> </td>';}
	if($row['responsivacompraventa']==1){$html .= '<td> <i style="color:#15A615;">OK</i> </td>';}else{$html .= '<td> <i style="color:#CC0000;">--</i> </td>';}
	if($row['avisoprivacidad']==1){$html .= '<td> <i style="color:#15A615;">OK</i> </td>';}else{$html .= '<td> <i style="color:#CC0000;">--</i> </td>';}
	if($row['checklistrevision']==1){$html .= '<td> <i style="color:#15A615;">OK</i> </td>';}else{$html .= '<td> <i style="color:#CC0000;">--</i> </td>';}
	if($row['evaluacionmecanica']==1){$html .= '<td> <i style="color:#15A615;">OK</i> </td>';}else{$html .= '<td> <i style="color:#CC0000;">--</i> </td>';}
	if($row['guiaautometrica']==1){$html .= '<td> <i style="color:#15A615;">OK</i> </td>';}else{$html .= '<td> <i style="color:#CC0000;">--</i> </td>';}
	if($row['polizaderecepcion']==1){$html .= '<td> <i style="color:#15A615;">OK</i> </td>';}else{$html .= '<td> <i style="color:#CC0000;">--</i> </td>';}
	if($row['cuentasporpagar']==1){$html .= '<td> <i style="color:#15A615;">OK</i> </td>';}else{$html .= '<td> <i style="color:#CC0000;">--</i> </td>';}
	if($row['formatodedacion']==1){$html .= '<td> <i style="color:#15A615;">OK</i> </td>';}else{$html .= '<td> <i style="color:#CC0000;">--</i> </td>';}
	if($row['reportederobo']==1){$html .= '<td> <i style="color:#15A615;">OK</i> </td>';}else{$html .= '<td> <i style="color:#CC0000;">--</i> </td>';}
	if($row['chequedeproveedor']==1){$html .= '<td> <i style="color:#15A615;">OK</i> </td>';}else{$html .= '<td> <i style="color:#CC0000;">--</i> </td>';}
	if($row['solicitudcheque']==1){$html .= '<td> <i style="color:#15A615;">OK</i> </td>';}else{$html .= '<td> <i style="color:#CC0000;">--</i> </td>';}
	if($row['edocuenta']==1){$html .= '<td> <i style="color:#15A615;">OK</i> </td>';}else{$html .= '<td> <i style="color:#CC0000;">--</i> </td>';}
	if($row['calculoretencion']==1){$html .= '<td> <i style="color:#15A615;">OK</i> </td>';}else{$html .= '<td> <i style="color:#CC0000;">--</i> </td>';}
	if($row['autorizaciontoma']==1){$html .= '<td> <i style="color:#15A615;">OK</i> </td>';}else{$html .= '<td> <i style="color:#CC0000;">--</i> </td>';}

	$html .= '</tr>';

endforeach;

$html .='</tbody> </table>';
 

tcpdf();
$obj_pdf = new TCPDF('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
header('Content-type: application/pdf');
$obj_pdf->SetCreator(PDF_CREATOR);
$title = "Reporte de Checklist Registrados";
$obj_pdf->SetTitle($title);
$obj_pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $title, PDF_HEADER_STRING);
$obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$obj_pdf->SetDefaultMonospacedFont('helvetica');
$obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$obj_pdf->SetFont('helvetica', '', 9);
$obj_pdf->setFontSubsetting(false);
$obj_pdf->AddPage();
ob_start();
    // podemos tener cualquier parte de la vista aquí como HTML, PHP, etc.
$content = ob_get_contents();
ob_end_clean();
$obj_pdf->writeHTML($content, true, false, true, false, '');
$obj_pdf->writeHTML($html, true, false, true, false, '');


$filename = 'models_list1';

$obj_pdf->Output($filename . '.pdf', 'D');




?>