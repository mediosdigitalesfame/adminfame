<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Guides extends MY_Controller 

{

	public function __construct(){



		parent::__construct();

		$this->load->helper('form');

		$this->load->helper('functions');

		auth_check(); // comprobar la autenticación de inicio de sesión

		$this->rbac->check_module_access();



		$this->load->model('admin/guide_model', 'guide_model');

		$this->load->model('admin/Activity_model', 'activity_model');

	}

	//-----------------------------------------------------------

	//  vista principal

	//-------------------------------------------------------

	public function index(){

		$data['title'] = 'Lista de ';

		$this->load->view('admin/includes/_header');

		$this->load->view('admin/guides/guide_list');

		$this->load->view('admin/includes/_footer');

	}



	//-----------------------------------------------------------

	//  vista principal autorizadas

	//-------------------------------------------------------

	public function index_autorizadas(){

		$data['title'] = 'Lista de ';

		$this->load->view('admin/includes/_header');

		$this->load->view('admin/guides/guide_list_aut');

		$this->load->view('admin/includes/_footer');

	}

	//-----------------------------------------------------------

	//  vista principal pendientes

	//-------------------------------------------------------

	public function index_pendientes(){

		$data['title'] = 'Lista de ';

		$this->load->view('admin/includes/_header');

		$this->load->view('admin/guides/guide_list_pen');

		$this->load->view('admin/includes/_footer');

	}

	//-----------------------------------------------------------

	//  vista principal pendientes

	//-------------------------------------------------------

	public function index_tomadas(){

		$data['title'] = 'Lista de ';

		$this->load->view('admin/includes/_header');

		$this->load->view('admin/guides/guide_list_tom');

		$this->load->view('admin/includes/_footer');

	}

	//-----------------------------------------------------------

	//  tabla para listado principal

	//-----------------------------------------------------

	public function datatable_json(){	



		$usr=$this->session->userdata('is_supper');

		$idusr=$this->session->userdata('admin_id');



		if($usr>0){

			$records['data'] = $this->guide_model->get_all_guides();

		}else{

			$records['data'] = $this->guide_model->get_all_guides_peruser($idusr);

		}



		$data = array();

		$i=0;

		foreach ($records['data']   as $row) 

		{  

			$status = ($row['status'] == 1)? 'checked': '';

			$status2 = ($row['status2'] == 1)? 'checked': '';


			if($row['status'] == 1){

				$acciones='
				<a title="Exportar a PDF" class="update btn btn-sm btn-success" href="'.base_url('admin/guides/create_guide_pdf/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>
				<a title="Formato de Autorización" class="update btn btn-sm btn-success" href="'.base_url('admin/guides/create_autori_pdf/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>
				<a title="View" class="view btn btn-sm btn-info" href="'.base_url('admin/guides/view/'.$row['id']).'"> <i class="fa fa-eye"></i></a>
				<a title="Edit" class="disabled update btn btn-sm btn-warning" href="'.base_url('admin/guides/edit/'.$row['id']).'"> <i class="fa fa-pencil-square-o"></i></a>
				<a title="Delete" class="disabled delete btn btn-sm btn-danger" href='.base_url("admin/guides/delete/".$row['id']).' title="Delete" onclick="return confirm(\'Do you want to delete ?\')"> <i class="fa fa-trash-o"></i></a>
				';

			}else{

				$acciones='
				<a title="Exportar a PDF" class="update btn btn-sm btn-success" href="'.base_url('admin/guides/create_guide_pdf/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>
				<a title="Formato de Autorización" class="update btn btn-sm btn-success" href="'.base_url('admin/guides/create_autori_pdf/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>
				<a title="View" class="view btn btn-sm btn-info" href="'.base_url('admin/guides/view/'.$row['id']).'"> <i class="fa fa-eye"></i></a>
				<a title="Edit" class="update btn btn-sm btn-warning" href="'.base_url('admin/guides/edit/'.$row['id']).'"> <i class="fa fa-pencil-square-o"></i></a>
				<a title="Delete" class="delete btn btn-sm btn-danger" href='.base_url("admin/guides/delete/".$row['id']).' title="Delete" onclick="return confirm(\'Do you want to delete ?\')"> <i class="fa fa-trash-o"></i></a>
				';

			}

			$foliouno = $row['id'];
            $foliocero = str_pad($foliouno, 9, "0", STR_PAD_LEFT);



			$data[]= array(

				++$i,

				$row['agname'],

				$row['folio']. "-" .$foliocero,

				$row['km'],

				date("d-m-Y", strtotime($row['created_at'])),

				'<input class="tgl_checkbox tgl-ios"data-id="'.$row['id'].'"id="cb_'.$row['id'].'"type="checkbox"	'.$status.'><label for="cb_'.$row['id'].'"></label>',

				'<input class="tgl_checkbox2 tgl-ios"data-id="'.$row['id'].'"id="cb2_'.$row['id'].'"type="checkbox"	'.$status2.'><label for="cb2_'.$row['id'].'"></label>',



				$acciones	 		

			);

		}

		$records['data']=$data;

		echo json_encode($records);						   

	}



	//-----------------------------------------------------------

	//  tabla para listado principal

	//-----------------------------------------------------

	public function datatable_json_autorizadas(){	



		$usr=$this->session->userdata('is_supper');

		$idusr=$this->session->userdata('admin_id');



		if($usr>0){

			$records['data'] = $this->guide_model->get_all_guides_autorizadas();

		}else{

			$records['data'] = $this->guide_model->get_all_guides_peruser_autorizadas($idusr);

		}



		$data = array();

		$i=0;

		foreach ($records['data']   as $row) 

		{  

			$status = ($row['status'] == 1)? 'checked': '';

			if($row['status'] == 1){

				$acciones='<a title="View" class="view btn btn-sm btn-info" href="'.base_url('admin/guides/view/'.$row['id']).'"> <i class="fa fa-eye"></i></a>

				<a title="Edit" class="disabled update btn btn-sm btn-warning" href="'.base_url('admin/guides/edit/'.$row['id']).'"> <i class="fa fa-pencil-square-o"></i></a>

				<a title="Delete" class="disabled delete btn btn-sm btn-danger" href='.base_url("admin/guides/delete/".$row['id']).' title="Delete" onclick="return confirm(\'Do you want to delete ?\')"> <i class="fa fa-trash-o"></i></a>';

			}else{

				$acciones='<a title="View" class="view btn btn-sm btn-info" href="'.base_url('admin/guides/view/'.$row['id']).'"> <i class="fa fa-eye"></i></a>

				<a title="Edit" class="update btn btn-sm btn-warning" href="'.base_url('admin/guides/edit/'.$row['id']).'"> <i class="fa fa-pencil-square-o"></i></a>

				<a title="Delete" class="delete btn btn-sm btn-danger" href='.base_url("admin/guides/delete/".$row['id']).' title="Delete" onclick="return confirm(\'Do you want to delete ?\')"> <i class="fa fa-trash-o"></i></a>';

			}



			$data[]= array(

				++$i,

				$row['agname'],

				$row['folio'],

				$row['km'],

				date("d-m-Y", strtotime($row['created_at'])),

				'<input class="tgl_checkbox tgl-ios"data-id="'.$row['id'].'"id="cb_'.$row['id'].'"type="checkbox"	'.$status.'><label for="cb_'.$row['id'].'"></label>',

				$acciones



			);

		}

		$records['data']=$data;

		echo json_encode($records);						   

	}



	//-----------------------------------------------------------

	//  tabla para listado principal

	//-----------------------------------------------------

	public function datatable_json_pendientes(){	



		$usr=$this->session->userdata('is_supper');

		$idusr=$this->session->userdata('admin_id');



		if($usr>0){

			$records['data'] = $this->guide_model->get_all_guides_pendientes();

		}else{

			$records['data'] = $this->guide_model->get_all_guides_peruser_pendientes($idusr);

		}



		$data = array();

		$i=0;

		foreach ($records['data']   as $row) 

		{  

			$status = ($row['status'] == 1)? 'checked': '';

			if($row['status'] == 1){

				$acciones='<a title="View" class="view btn btn-sm btn-info" href="'.base_url('admin/guides/view/'.$row['id']).'"> <i class="fa fa-eye"></i></a>

				<a title="Edit" class="disabled update btn btn-sm btn-warning" href="'.base_url('admin/guides/edit/'.$row['id']).'"> <i class="fa fa-pencil-square-o"></i></a>

				<a title="Delete" class="disabled delete btn btn-sm btn-danger" href='.base_url("admin/guides/delete/".$row['id']).' title="Delete" onclick="return confirm(\'Do you want to delete ?\')"> <i class="fa fa-trash-o"></i></a>';

			}else{

				$acciones='<a title="View" class="view btn btn-sm btn-info" href="'.base_url('admin/guides/view/'.$row['id']).'"> <i class="fa fa-eye"></i></a>

				<a title="Edit" class="update btn btn-sm btn-warning" href="'.base_url('admin/guides/edit/'.$row['id']).'"> <i class="fa fa-pencil-square-o"></i></a>

				<a title="Delete" class="delete btn btn-sm btn-danger" href='.base_url("admin/guides/delete/".$row['id']).' title="Delete" onclick="return confirm(\'Do you want to delete ?\')"> <i class="fa fa-trash-o"></i></a>';

			}



			$data[]= array(

				++$i,

				$row['agname'],

				$row['folio'],

				$row['km'],

				date("d-m-Y", strtotime($row['created_at'])),

				'<input class="tgl_checkbox tgl-ios"data-id="'.$row['id'].'"id="cb_'.$row['id'].'"type="checkbox"	'.$status.'><label for="cb_'.$row['id'].'"></label>',	

				$acciones	 		

			);

		}

		$records['data']=$data;

		echo json_encode($records);						   

	}



	//-----------------------------------------------------------

	//  tabla para listado principal

	//-----------------------------------------------------

	public function datatable_json_tomadas(){	



		$usr=$this->session->userdata('is_supper');

		$idusr=$this->session->userdata('admin_id');



		if($usr>0){

			$records['data'] = $this->guide_model->get_all_guides_tomadas();

		}else{

			$records['data'] = $this->guide_model->get_all_guides_peruser_tomadas($idusr);

		}



		$data = array();

		$i=0;

		foreach ($records['data']   as $row) 

		{  

			$status = ($row['status'] == 1)? 'checked': '';

			if($row['status'] == 1){

				$acciones='<a title="View" class="view btn btn-sm btn-info" href="'.base_url('admin/guides/view/'.$row['id']).'"> <i class="fa fa-eye"></i></a>

				<a title="Edit" class="disabled update btn btn-sm btn-warning" href="'.base_url('admin/guides/edit/'.$row['id']).'"> <i class="fa fa-pencil-square-o"></i></a>

				<a title="Delete" class="disabled delete btn btn-sm btn-danger" href='.base_url("admin/guides/delete/".$row['id']).' title="Delete" onclick="return confirm(\'Do you want to delete ?\')"> <i class="fa fa-trash-o"></i></a>';

			}else{

				$acciones='<a title="View" class="view btn btn-sm btn-info" href="'.base_url('admin/guides/view/'.$row['id']).'"> <i class="fa fa-eye"></i></a>

				<a title="Edit" class="update btn btn-sm btn-warning" href="'.base_url('admin/guides/edit/'.$row['id']).'"> <i class="fa fa-pencil-square-o"></i></a>

				<a title="Delete" class="delete btn btn-sm btn-danger" href='.base_url("admin/guides/delete/".$row['id']).' title="Delete" onclick="return confirm(\'Do you want to delete ?\')"> <i class="fa fa-trash-o"></i></a>';

			}



			$data[]= array(

				++$i,

				$row['agname'],

				$row['folio'],

				$row['km'],

				date("d-m-Y", strtotime($row['created_at'])),

				'<input class="tgl_checkbox tgl-ios"data-id="'.$row['id'].'"id="cb_'.$row['id'].'"type="checkbox"	'.$status.'><label for="cb_'.$row['id'].'"></label>',	

				$acciones 		

			);

		}

		$records['data']=$data;

		echo json_encode($records);						   

	}

	//-----------------------------------------------------------

	//  Cambiar Status Guia

	//-------------------------------------------------------

	function change_status()

	{   

		$this->rbac->check_operation_access('change_status'); //comprobar el permiso de operación

		$this->guide_model->change_status();

	}

		//-----------------------------------------------------------

	//  Cambiar Status Guia

	//-------------------------------------------------------

	function change_status2()

	{   

		$this->rbac->check_operation_access('change_status2'); //comprobar el permiso de operación

		$this->guide_model->change_status2();

	}

	//-----------------------------------------------------------

	//  Agregar Guia

	//-------------------------------------------------------

	public function add(){

		

		$this->rbac->check_operation_access(); //comprobar el permiso de operación



		$data['brands']=$this->guide_model->get_all_simple_brands();

		$data['business']=$this->guide_model->get_all_simple_business();

		$data['admins']=$this->guide_model->get_all_simple_admins();

		$data['agencys']=$this->guide_model->get_all_simple_agencys();

		$data['utilitys']=$this->guide_model->get_all_simple_utilitys();

		$data['years']=$this->guide_model->get_all_simple_years();

		$data['models']=$this->guide_model->get_all_simple_models();

		$data['tguias']=$this->guide_model->get_all_simple_tguias();

		$data['regis'] = $this->guide_model->get_all_simple_regimenes();



		if($this->input->post('submit')){



			$this->form_validation->set_rules('folio', 'Folio', 'trim|required');

			$this->form_validation->set_rules('pdudlv7', '% de Utilidad', 'trim|required');

			$this->form_validation->set_rules('anio', 'Año', 'trim|required');

			$this->form_validation->set_rules('modelo', 'Modelo', 'trim|required');

			$this->form_validation->set_rules('version', 'Version', 'trim|required');

			$this->form_validation->set_rules('equipamiento', 'Equipamiento', 'trim|required');

			$this->form_validation->set_rules('premio', 'Premio/Castigo', 'trim|required');

			$this->form_validation->set_rules('tipoguia', 'Version de Guía', 'trim|required');

			$this->form_validation->set_rules('vin', trans('vin'), 'trim|required|min_length[10]');

			$this->form_validation->set_rules('km', trans('km'), 'trim|required');

			$this->form_validation->set_rules('agencia', 'Agencia', 'trim|required');

			$this->form_validation->set_rules('marca', 'Marca', 'trim|required');

			$this->form_validation->set_rules('valor', 'Valor del Coche', 'trim|required');

			$this->form_validation->set_rules('reacondicionamiento', 'reacondicionamiento', 'trim|required');

			$this->form_validation->set_rules('ventaautometrica', 'Venta Guía', 'trim|required');

			$this->form_validation->set_rules('compraautometrica', 'Compra Guía', 'trim|required');


			//$this->form_validation->set_message("folio","El Folio es Requerido para continuar")


			if ($this->form_validation->run() == FALSE) {

				$data = array('errors' => validation_errors());

				$this->session->set_flashdata('form_data', $this->input->post());

				$this->session->set_flashdata('errors', $data['errors']);

				redirect(base_url('admin/guides/add'),'refresh');

			}

			else{

				if(!empty($this->input->post('siniestros'))){$siniestros = $this->input->post('siniestros', true);}else{$siniestros = 0;}

				$s2 = $this->input->post('prebasestado2');
				$s3 = $this->input->post('prebasestado3');
				$s4 = $this->input->post('prebasestado4');
				$ss = $s2+$s3+$s4;

				if( $ss == 3){$dostatus = 1;}else{$dostatus = 0;}

				$data = array(

					'siniestros' => $siniestros,

					'valor' => $this->input->post('valor', true),

					'reacondicionamiento' => $this->input->post('reacondicionamiento', true),

					'premio' => $this->input->post('premio', true),

					'equipamiento' => $this->input->post('equipamiento', true),

					'compraautometrica' => $this->input->post('compraautometrica', true),

					'ventaautometrica' => $this->input->post('ventaautometrica', true),

					'km' => $this->input->post('km', true),

					'vin' => $this->input->post('vin', true),

					'folio' => $this->input->post('folio', true),

					'pdvapsr1' => $this->input->post('pdvapsr1a', true),

					'pdlttfac2' => $this->input->post('pdlttfac2a', true),

					'crpdltsi3' => $this->input->post('crpdltsi3a', true),

					'pdvapsi4' => $this->input->post('pdvapsi4a', true),

					'pdvapciyr5' => $this->input->post('pdvapciyr5a', true),

					'udlv6' => $this->input->post('udlv6a', true),

					'pdudlv7' => $this->input->post('pdudlv7', true),

					'pmdvap8' => $this->input->post('pmdvap8a', true),

					'cccr9' => $this->input->post('cccr9a', true),

					'pdvap10' => $this->input->post('pdvap10a', true),

					'ucidlu11' => $this->input->post('ucidlu11a', true),

					'idlu12' => $this->input->post('idlu12a', true),

					'fdume13' => $this->input->post('fdume13a', true),

					'models_id' => 1,

					'users_id' => $this->session->userdata('admin_id'),

					'tipoguias_id' => $this->input->post('tipoguia', true),

					'agencys_id' => $this->input->post('agencia', true),

					'status' => 0,

					'status2' => $dostatus,

					'anio_id' => $this->input->post('anio', true),

					'modelotxt' => $this->input->post('modelo', true),

					'versiontxt' => $this->input->post('version', true),

					'brands_id' => $this->input->post('marca', true),

					'created_at' => date('Y-m-d h:m:s'),

					'regimenes_id' => $this->input->post('faregimen', true),
					'proveecliente' => $this->input->post('fanombre', true),
					'color' => $this->input->post('facolorext', true),
                    'edo' => $this->input->post('faedoemisor', true),
                    'placas' => $this->input->post('faplacasbaja', true),
                    'telefono' => $this->input->post('fatel', true),
                    'observaciones' => $this->input->post('faobservaciones', true),

                    //'congrales' => $this->input->post('facgenerales', true),
					//'rdiagnostico' => $this->input->post('fardiagnostico', true),
                    //'rtransunion' => $this->input->post('fartrans', true),
                    //'sitfiscal' => $this->input->post('fasfiscal', true),
                    


				);


				$path="assets/img/guides/costo/";
				$path2="assets/img/guides/km/";
				$path3="assets/img/guides/portada/";

				$pathA="uploads/docautoris/condicionesgenerales/";
				$pathB="uploads/docautoris/reportediagnostico/";
				$pathC="uploads/docautoris/reportetransunion/";
				$pathD="uploads/docautoris/situacionfiscal/";


				if(!empty($_FILES['image']['name']))
				{
					$result = $this->functions->file_insert($path, 'image', 'pdf', '9097152');
					if($result['status'] == 1){
						$data['image'] = $path.$result['msg'];
					}
					else{
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/guides/'), 'refresh');
					}
				}

				if(!empty($_FILES['image2']['name']))
				{
					$result2 = $this->functions->file_insert($path2, 'image2', 'pdf', '9097152');
					if($result2['status'] == 1){
						$data['image2'] = $path2.$result2['msg'];
					}
					else{
						$this->session->set_flashdata('error', $result2['msg']);
						redirect(base_url('admin/guides/'), 'refresh');
					}
				}

				if(!empty($_FILES['image3']['name']))
				{
					$result3 = $this->functions->file_insert($path3, 'image3', 'pdf', '9097152');
					if($result3['status'] == 1){
						$data['image3'] = $path3.$result3['msg'];
					}
					else{
						$this->session->set_flashdata('error', $result3['msg']);
						redirect(base_url('admin/guides/'), 'refresh');
					}
				}


				//Archivos para el formato de autorizacion


				if(!empty($_FILES['facgenerales']['name']))
				{
					$resultA = $this->functions->file_insert($pathA, 'facgenerales', 'pdf', '9097152');
					if($resultA['status'] == 1){
						$data['congrales'] = $pathA.$resultA['msg'];
					}
					else{
						$this->session->set_flashdata('error', $resultA['msg']);
						redirect(base_url('admin/guides/'), 'refresh');
					}
				}

				if(!empty($_FILES['fardiagnostico']['name']))
				{
					$resultB = $this->functions->file_insert($pathB, 'fardiagnostico', 'pdf', '9097152');
					if($resultB['status'] == 1){
						$data['rdiagnostico'] = $pathB.$resultB['msg'];
					}
					else{
						$this->session->set_flashdata('error', $resultB['msg']);
						redirect(base_url('admin/guides/'), 'refresh');
					}
				}

				if(!empty($_FILES['fartrans']['name']))
				{
					$resultC = $this->functions->file_insert($pathC, 'fartrans', 'pdf', '9097152');
					if($resultC['status'] == 1){
						$data['rtransunion'] = $pathC.$resultC['msg'];
					}
					else{
						$this->session->set_flashdata('error', $resultC['msg']);
						redirect(base_url('admin/guides/'), 'refresh');
					}
				}

				if(!empty($_FILES['fasfiscal']['name']))
				{
					$resultD = $this->functions->file_insert($pathD, 'fasfiscal', 'pdf', '9097152');
					if($resultD['status'] == 1){
						$data['sitfiscal'] = $pathD.$resultD['msg'];
					}
					else{
						$this->session->set_flashdata('error', $resultD['msg']);
						redirect(base_url('admin/guides/'), 'refresh');
					}
				}




				$data = $this->security->xss_clean($data);
				$result = $this->guide_model->add_guide($data);
				if($result){

					// Registro de actividades

					$this->activity_model->add_log(25);

					$this->session->set_flashdata('success', 'La Guía se ha agregado correctamente!');

					redirect(base_url('admin/guides'));

				}

			}

		}

		else{

			$this->load->view('admin/includes/_header');

			$this->load->view('admin/guides/guide_add', $data);

			$this->load->view('admin/includes/_footer');

		}

		

	}

	//-----------------------------------------------------------

	//  Editar Guia

	//-------------------------------------------------------

	public function edit($id = 0){

		$this->rbac->check_operation_access(); //comprobar el permiso de operación

		$data['brands']=$this->guide_model->get_all_simple_brands();
		$data['business']=$this->guide_model->get_all_simple_business();
		$data['admins']=$this->guide_model->get_all_simple_admins();
		$data['agencys']=$this->guide_model->get_all_simple_agencys_todas();
		$data['utilitys']=$this->guide_model->get_all_simple_utilitys();
		$data['years']=$this->guide_model->get_all_simple_years();
		$data['models']=$this->guide_model->get_all_simple_models();
		$data['tguias']=$this->guide_model->get_all_simple_tguias();

		if($this->input->post('submit')){

			$this->form_validation->set_rules('folio', 'Folio', 'trim|required');
			$this->form_validation->set_rules('pdudlv7', 'de Utilidad', 'trim|required');
			$this->form_validation->set_rules('reacondicionamiento', 'reacondicionamiento', 'trim|required');

			if ($this->form_validation->run() == FALSE) {
				$data = array(
					'errors' => validation_errors()
				);
				$this->session->set_flashdata('errors', $data['errors']);
				redirect(base_url('admin/guides/guide_edit/'.$id),'refresh');
			}

			else{

				if(!empty($this->input->post('modelo')))
				{
					$model = $this->input->post('modelo', true);
				}
				else{
					$model = $this->input->post('idmodelo', true);
				}

				if(!empty($this->input->post('siniestros'))){$siniestros = $this->input->post('siniestros', true);}else{$siniestros = 0;}

				if(!empty($this->input->post('prebasestado2'))){$s2 = $this->input->post('prebasestado2', true);}else{$s2 = 0;}
				if(!empty($this->input->post('prebasestado3'))){$s3 = $this->input->post('prebasestado3', true);}else{$s3 = 0;}
				if(!empty($this->input->post('prebasestado4'))){$s4 = $this->input->post('prebasestado4', true);}else{$s4 = 0;}

				$sr = $s2+$s3+$s4;

				if(($this->input->post('prebasestado2')+$this->input->post('prebasestado4')+$this->input->post('prebasestado4')) == 3)
					{$dostatus = 1;}
				else{$dostatus = 0;}

				$data = array(

					'siniestros' => $siniestros,
					'valor' => $this->input->post('valor', true),
					'reacondicionamiento' => $this->input->post('reacondicionamiento', true),
					'premio' => $this->input->post('premio', true),
					'equipamiento' => $this->input->post('equipamiento', true),
					'compraautometrica' => $this->input->post('compraautometrica', true),
					'ventaautometrica' => $this->input->post('ventaautometrica', true),
					'km' => $this->input->post('km', true),
					'vin' => $this->input->post('vin', true),
					'folio' => $this->input->post('folio', true),
					'pdvapsr1' => $this->input->post('pdvapsr1a', true),
					'pdlttfac2' => $this->input->post('pdlttfac2a', true),
					'crpdltsi3' => $this->input->post('crpdltsi3a', true),
					'pdvapsi4' => $this->input->post('pdvapsi4a', true),
					'pdvapciyr5' => $this->input->post('pdvapciyr5a', true),
					'udlv6' => $this->input->post('udlv6a', true),
					'pdudlv7' => $this->input->post('pdudlv7', true),
					'pmdvap8' => $this->input->post('pmdvap8a', true),
					'cccr9' => $this->input->post('cccr9a', true),
					'pdvap10' => $this->input->post('pdvap10a', true),
					'ucidlu11' => $this->input->post('ucidlu11a', true),
					'idlu12' => $this->input->post('idlu12a', true),
					'fdume13' => $this->input->post('fdume13a', true),
					'models_id' => 1,
					'users_id' => $this->session->userdata('admin_id'),
					'tipoguias_id' => $this->input->post('tipoguia', true),
					'agencys_id' => $this->input->post('agencia', true),
					//'status' => $this->input->post('status'),
					'status2' => $dostatus,
					'anio_id' => $this->input->post('anio', true),
					'modelotxt' => $this->input->post('modelo', true),
					'versiontxt' => $this->input->post('version', true),
					'brands_id' => $this->input->post('marca', true),
					'updated_at' => date('Y-m-d h:m:s'),
				);

				$old_logo = $this->input->post('old_logo');
				$path="assets/img/guides/";

				if(!empty($_FILES['image']['name']))
				{
					$this->functions->delete_file($old_logo);
					$result = $this->functions->file_insert($path, 'image', 'image', '9097152');
					if($result['status'] == 1){
						$data['image'] = $path.$result['msg'];
					}

					else{
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/guides/edit/'.$id), 'refresh');
					}
				}

				$data = $this->security->xss_clean($data);
				$result = $this->guide_model->edit_guide($data, $id);

				if($result){
					// Registro de actividades 
					$this->activity_model->add_log(26);
					$this->session->set_flashdata('success', 'La Guía ha sido actualizada con éxito!');
					redirect(base_url('admin/guides/edit/'.$id), 'refresh');
				}
			}
		}

		else{
			$data['guide'] = $this->guide_model->get_guide_by_id_business($id);
			$this->load->view('admin/includes/_header');
			$this->load->view('admin/guides/guide_edit', $data);
			$this->load->view('admin/includes/_footer');
		}
	}





	function crearMiniatura($filename){

		$config['image_library'] = 'gd2';

		$config['source_image'] = 'uploads/imagenes/'.$filename;

		$config['create_thumb'] = TRUE;

		$config['maintain_ratio'] = TRUE;

		$config['new_image']='uploads/imagenes/thumbs/';

        $config['thumb_marker']='';//captura_thumb.png

        $config['width'] = 150;

        $config['height'] = 150;

        $this->load->library('image_lib', $config); 

        $this->image_lib->resize();

    }







	//-----------------------------------------------------------

	//  Ver guia

	//-------------------------------------------------------

    public function view($id = 0){



		$this->rbac->check_operation_access(); //comprobar el permiso de operación



		$data['brands']=$this->guide_model->get_all_simple_brands();

		$data['business']=$this->guide_model->get_all_simple_business();

		$data['admins']=$this->guide_model->get_all_simple_admins();

		$data['agencys']=$this->guide_model->get_all_simple_agencys();

		$data['utilitys']=$this->guide_model->get_all_simple_utilitys();

		$data['brands']=$this->guide_model->get_all_simple_brands();

		$data['years']=$this->guide_model->get_all_simple_years();

		$data['models']=$this->guide_model->get_all_simple_models();

		$data['tguias']=$this->guide_model->get_all_simple_tguias();



		if($this->input->post('submit')){

			$this->form_validation->set_rules('folio', trans('folio'), 'trim|required');



			if ($this->form_validation->run() == FALSE) {

				$data = array(

					'errors' => validation_errors()

				);

				$this->session->set_flashdata('errors', $data['errors']);

				redirect(base_url('admin/guides/guide_edit/'.$id),'refresh');

			}

			else{

				$data = array(

					'valor' => $this->input->post('valor', true),

					'reacondicionamiento' => $this->input->post('reacondicionamiento', true),

					'premio' => $this->input->post('premio', true),

					'equipamiento' => $this->input->post('equipamiento', true),

					'compraautometrica' => $this->input->post('compraautometrica', true),

					'ventaautometrica' => $this->input->post('ventaautometrica', true),

					'km' => $this->input->post('km', true),

					'vin' => $this->input->post('vin', true),

					'folio' => $this->input->post('folio', true),

					'pdvapsr1' => $this->input->post('pdvapsr1a', true),

					'pdlttfac2' => $this->input->post('pdlttfac2a', true),

					'crpdltsi3' => $this->input->post('crpdltsi3a', true),

					'pdvapsi4' => $this->input->post('pdvapsi4a', true),

					'pdvapciyr5' => $this->input->post('pdvapciyr5a', true),

					'udlv6' => $this->input->post('udlv6a', true),

					'pdudlv7' => $this->input->post('pdudlv7', true),

					'pmdvap8' => $this->input->post('pmdvap8a', true),

					'cccr9' => $this->input->post('cccr9a', true),

					'pdvap10' => $this->input->post('pdvap10a', true),

					'ucidlu11' => $this->input->post('ucidlu11a', true),

					'idlu12' => $this->input->post('idlu12a', true),

					'fdume13' => $this->input->post('fdume13a', true),

					'models_id' => $this->input->post('modelo', true),

					'users_id' => $this->session->userdata('admin_id'),

					'tipoguias_id' => $this->input->post('tipoguia', true),

					'agencys_id' => $this->input->post('agencia', true),

					'status' => $this->input->post('status'),

					'updated_at' => date('Y-m-d h:m:s'),

				);

				$data = $this->security->xss_clean($data);

				$result = $this->guide_model->edit_model($data, $id);

				if($result){

					// Registro de actividades 

					$this->activity_model->add_log(26);

					$this->session->set_flashdata('success', 'La Guía ha sido actualizada con éxito!');

					redirect(base_url('admin/guides'));

				}

			}

		}

		else{

			$data['guide'] = $this->guide_model->get_guide_by_id_business_view($id);

			$data['guideb'] = $this->guide_model->get_autori_by_id_business_view($id);

			

			$this->load->view('admin/includes/_header');

			$this->load->view('admin/guides/guide_view', $data);

			$this->load->view('admin/includes/_footer');

		}

	}

	//-----------------------------------------------------------

	//  Eliminar Guia

	//-------------------------------------------------------

	public function delete($id = 0)

	{

		$this->rbac->check_operation_access(); // comprobar el permiso de operación

		$this->db->delete('anios', array('id' => $id));

		// Registro de actividades  

		$this->activity_model->add_log(27);

		$this->session->set_flashdata('success', 'La Marca se ha eliminado correctamente!');

		redirect(base_url('admin/guides'));

	}

	//-----------------------------------------------------------

	//  Exportar Guias a CSV

	//-------------------------------------------------------

	public function export_csv(){ 

	   // nombre del archivo

		$filename = 'guides_'.date('Y-m-d').'.csv'; 

		header("Content-Description: File Transfer"); 

		header("Content-Disposition: attachment; filename=$filename"); 

		header("Content-Type: application/csv; ");



	   // obtener datos

		$guide_data = $this->guide_model->get_guides_for_csv();



	   // crear archivo

		$file = fopen('php://output', 'w');

		$header = array("ID", "Name", "Created Date"); 

		fputcsv($file, $header);

		foreach ($guide_data as $key=>$line){ 

			fputcsv($file,$line); 

		}

		fclose($file); 

		exit; 

	}

	//---------------------------------------------------------------
	//  Exportar Autorizacion Individual a PDF
	//-------------------------------------------------------
	public function create_autori_pdf($id = 0)
	{
		$this->load->helper('pdf_tcpdf_helper'); // loaded pdf helper
		$data['guide'] = $this->guide_model->get_autori_by_id_business_pdf($id);
		$this->load->view('admin/guides/autori_pdf', $data);

	}




    //---------------------------------------------------------------
	//  Exportar Guia Individual a PDF
	//-------------------------------------------------------
	public function create_guide_pdf($id = 0)
	{
		$this->load->helper('pdf_tcpdf_helper'); // loaded pdf helper
		$data['guide'] = $this->guide_model->get_guide_by_id_business_pdf($id);
		$this->load->view('admin/guides/guide_pdf', $data);

	}

	//---------------------------------------------------------------

	//  Exportar Guias a PDF

	//-------------------------------------------------------

	public function create_guides_pdf()

	{

		$this->load->helper('pdf_tcpdf_helper'); // loaded pdf helper

		$data['all_guides'] = $this->guide_model->get_all_simple_guides_pdf();

		$this->load->view('admin/guides/guide_tcpdf', $data);

	}

	//---------------------------------------------------------------

	//  Exportar Guias Autorizadas a PDF

	//-------------------------------------------------------

	public function create_guides_pdf_autorizadas()

	{

		$this->load->helper('pdf_tcpdf_helper'); // loaded pdf helper

		$data['all_guides'] = $this->guide_model->get_all_simple_guides_pdf_autorizadas();

		$this->load->view('admin/guides/guide_tcpdf', $data);

	}

	//---------------------------------------------------------------

	//  Exportar Guias Autorizadas a PDF

	//-------------------------------------------------------

	public function create_guides_pdf_pendientes()

	{

		$this->load->helper('pdf_tcpdf_helper'); // loaded pdf helper

		$data['all_guides'] = $this->guide_model->get_all_simple_guides_pdf_pendientes();

		$this->load->view('admin/guides/guide_tcpdf', $data);

	}

	//---------------------------------------------------------------

	//  Exportar Guias Autorizadas a PDF

	//-------------------------------------------------------

	public function create_guides_pdf_tomadas()

	{

		$this->load->helper('pdf_tcpdf_helper'); // loaded pdf helper

		$data['all_guides'] = $this->guide_model->get_all_simple_guides_pdf_tomadas();

		$this->load->view('admin/guides/guide_tcpdf', $data);

	}

	//---------------------------------------------------------------

	//  Exportar Guias Activas a PDF

	//-------------------------------------------------------

	public function create_guides_pdf_activas()

	{

		$this->load->helper('pdf_tcpdf_helper'); // loaded pdf helper

		$data['all_guides'] = $this->guide_model->get_all_simple_guides_pdf();

		$this->load->view('admin/guides/guide_tcpdf', $data);

	}

	//---------------------------------------------------

	// Busqueda Avanzada

	//-------------------------------------------------------

	public function advance_search(){



		$this->session->unset_modeldata('model_search_type');

		$this->session->unset_modeldata('model_search_from');

		$this->session->unset_modeldata('model_search_to');

		$data['title'] = 'Advanced Search with Datatable';

		$this->load->view('admin/includes/_header');

		$this->load->view('admin/guides/advance_search', $data);

		$this->load->view('admin/includes/_footer');

	}

	//---------------------------------------------------------------

	//  Funcion de Busquedas

	//-------------------------------------------------------

	function search(){

		$this->session->set_modeldata('model_search_type',$this->input->post('model_search_type'));

		$this->session->set_modeldata('model_search_from',$this->input->post('model_search_from'));

		$this->session->set_modeldata('model_search_to',$this->input->post('model_search_to'));

	}



}





?>