<?php
$fecha = $guide['created_at']; 

$month = date('M', strtotime($fecha));
$year = date('Y', strtotime($fecha));
$day = date('d', strtotime($fecha));

$partes = explode('-', $month); 
$_fecha = "$year.$month.$day";

$fmt = new NumberFormatter('en_US', NumberFormatter::CURRENCY);
$formatterES = new NumberFormatter("es-ES", NumberFormatter::SPELLOUT);

$izquierda = intval(floor($guide['pdlttfac2']));
$derecha = intval(($guide['pdlttfac2'] - floor($guide['pdlttfac2'])) * 100);

 switch ($guide['tipoguias_id'])

    {
      case '1': 
      $tipoguia = "COMPRA";
      $tipopersona = "PERSONA FISICA O MORAL QUE FACTURA";
      break;

      case '2': 
      $tipoguia = "PROMEDIO";
      $tipopersona = "PERSONA FISICA O MORAL QUE FACTURA";
      break;

      case '3': 
      $tipoguia = "VENTA";
      $tipopersona = "PERSONA FISICA O MORAL QUE FACTURA";
      break;

      case '4': 
      $tipoguia = "COMPRA";
      $tipopersona = "PERSONA SIN OBLIGACION FISCAL";
      break;

      case '5': 
      $tipoguia = "PROMEDIO";
      $tipopersona = "PERSONA SIN OBLIGACION FISCAL";
      break;

      case '6': 
      $tipoguia = "VENTA";
      $tipopersona = "PERSONA SIN OBLIGACION FISCAL";
      break;
}

$html = '

<table  border="0">
	<tr bgcolor="white" align="center">
		<td><h2><font color="black">'.$guide['bursocial'].'</font></h2></td><br>
	</tr>

	<tr align="center">
		<td><h4><font color="black">CHECK LIST PARA LA TOMA DE UNIDADES USADAS</font></h4></td><br>
	</tr>

	<tr align="center">
		<td> </td><br>
	</tr>
	 
</table>

<table  border="0">

<tr align="right">
		<td>'.$guide['agciudad'].', ' .$guide['agestado'].' a: '.$day.' de ' .$month.' del ' .$year.'</td>
	</tr>

	<tr align="right">
		<td> </td>
	</tr>
	<tr align="justify">
		<td><b>No.FACTURA: '.$guide['facturavehi'] .', Proveedor:'.$guide['nombrescfdi'] .' '.$guide['apellido1'].' '. $guide['apellido2'].', Modelo:'.$guide['anname'] .', Tipo:'.$guide['modelotxt'].' - '.$guide['versiontxt'] .', VIN:'.$guide['nserie'] .', Importe de la Toma(COMPRA):'.$fmt->formatCurrency($guide['pdlttfac2'], "USD") .', Fecha:'.$guide['fechacontrato'] .', P/COSTO GUÍA AUTOMÉTRICA:'.$fmt->formatCurrency($guide['ventaautometrica'], "USD") .', DIREFENCIA:'.$fmt->formatCurrency($guide['ventaautometrica']-$guide['pdlttfac2'], "USD") .'     </b></td> 
	</tr>
	<tr align="center">
		<td> </td>
	</tr>
	
	<tr align="right">
		<td>Folio: '.$guide['folio'].'</td>
	</tr>
	<tr align="right">';
	//if($guide['status']==1){$html.= '<td> <i style="color:#15A615;">Autorizada</i> </td>';}else{$html.= '<td> <i style="color:#CC0000;">¡¡¡Pendiente de Autorización!!!</i> </td>';} 

	$html .= ' <td> </td> 
	</tr>
	<tr align="right">
		<td> </td>
	</tr>
</table>




<table width="100%" border="0" class="table table-bordered">
  <tbody>

    <tr>
      <td width="15%">   </td>
      <td width="3%"> </td>
      <td width="64%"> </td>
      <td width="3%"> </td>
      <td width="15%"> </td>
    </tr>';

    $html .= '<tr>
      <td> FL-GF-CMP-013 </td>
      <td> </td>
      <td> Cédula de Identificación Fiscal </td>
      <td> </td>';
      if($guide['cedulafiscal']==1){$html.= '<td> <i style="color:#15A615;">Correcto</i> </td></tr>';}else{$html.= '<td> <i style="color:#CC0000;">¡¡¡PENDIENTE!!!</i> </td></tr>';}

       $html .= '<tr>
      <td> FL-GF-CMP-013 </td>
      <td> </td>
      <td> XML de CFDI </td>
      <td> </td>';
      if($guide['xml']==1){$html.= '<td> <i style="color:#15A615;">Correcto</i> </td></tr>';}else{$html.= '<td> <i style="color:#CC0000;">¡¡¡PENDIENTE!!!</i> </td></tr>';}

       $html .= '<tr>
      <td> FL-GF-CMP-013 </td>
      <td> </td>
      <td> Validación Fiscal de CFDI </td>
      <td> </td>';
      if($guide['validacioncfdi']==1){$html.= '<td> <i style="color:#15A615;">Correcto</i> </td></tr>';}else{$html.= '<td> <i style="color:#CC0000;">¡¡¡PENDIENTE!!!</i> </td></tr>';}

       $html .= '<tr>
      <td> FL-GF-CMP-013 </td>
      <td> </td>
      <td> Verificación fiscal de CFDI </td>
      <td> </td>';
      if($guide['verificacioncfdi']==1){$html.= '<td> <i style="color:#15A615;">Correcto</i> </td></tr>';}else{$html.= '<td> <i style="color:#CC0000;">¡¡¡PENDIENTE!!!</i> </td></tr>';}

       $html .= '<tr>
      <td> FL-GF-CMP-013 </td>
      <td> </td>
      <td> Reporte de Transunion </td>
      <td> </td>';
      if($guide['reportetransunion']==1){$html.= '<td> <i style="color:#15A615;">Correcto</i> </td></tr>';}else{$html.= '<td> <i style="color:#CC0000;">¡¡¡PENDIENTE!!!</i> </td></tr>';}

       $html .= '<tr>
      <td> FL-GF-CMP-013 </td>
      <td> </td>
      <td> Carta de No retención de ISR Personas Fisicas con actividad empresarial (sólo si la unidad supera los $227,400) </td>
      <td> </td>';
      if($guide['noretencion']==1){$html.= '<td> <i style="color:#15A615;">Correcto</i> </td></tr>';}else{$html.= '<td> <i style="color:#CC0000;">¡¡¡PENDIENTE!!!</i> </td></tr>';}

       $html .= '<tr>
      <td> FL-GF-CMP-013 </td>
      <td> </td>
      <td> Secuencia de Facturas del Auto </td>
      <td> </td>';
      if($guide['secuenciafacturas']==1){$html.= '<td> <i style="color:#15A615;">Correcto</i> </td></tr>';}else{$html.= '<td> <i style="color:#CC0000;">¡¡¡PENDIENTE!!!</i> </td></tr>';}

       $html .= '<tr>
      <td> FL-GF-CMP-013 </td>
      <td> </td>
      <td> Identificación Oficial del Proveedor o Representanto Legal (En caso de persona moral agregar poder notarial) </td>
      <td> </td>';
      if($guide['identificacionoficial']==1){$html.= '<td> <i style="color:#15A615;">Correcto</i> </td></tr>';}else{$html.= '<td> <i style="color:#CC0000;">¡¡¡PENDIENTE!!!</i> </td></tr>';}

       $html .= '<tr>
      <td> FL-GF-CMP-013 </td>
      <td> </td>
      <td> CURP </td>
      <td> </td>';
      if($guide['curp']==1){$html.= '<td> <i style="color:#15A615;">Correcto</i> </td></tr>';}else{$html.= '<td> <i style="color:#CC0000;">¡¡¡PENDIENTE!!!</i> </td></tr>';}

       $html .= '<tr>
      <td> FL-GF-CMP-013 </td>
      <td> </td>
      <td> Comprobante de Domicilio </td>
      <td> </td>';
      if($guide['comprobante']==1){$html.= '<td> <i style="color:#15A615;">Correcto</i> </td></tr>';}else{$html.= '<td> <i style="color:#CC0000;">¡¡¡PENDIENTE!!!</i> </td></tr>';}

       $html .= '<tr>
      <td> FL-GF-CMP-013 </td>
      <td> </td>
      <td> Baja de placas del estado donde se encuentra la unidad </td>
      <td> </td>';
      if($guide['bajaplacas']==1){$html.= '<td> <i style="color:#15A615;">Correcto</i> </td></tr>';}else{$html.= '<td> <i style="color:#CC0000;">¡¡¡PENDIENTE!!!</i> </td></tr>';}

       $html .= '<tr>
      <td> FL-GF-CMP-013 </td>
      <td> </td>
      <td> Historial de tenencias (Últimos 5 Años) </td>
      <td> </td>';
      if($guide['tenencias']==1){$html.= '<td> <i style="color:#15A615;">Correcto</i> </td></tr>';}else{$html.= '<td> <i style="color:#CC0000;">¡¡¡PENDIENTE!!!</i> </td></tr>';}

       $html .= '<tr>
      <td> FL-GF-CMP-013 </td>
      <td> </td>
      <td> Tarjeta de Circulación </td>
      <td> </td>';
      if($guide['tarjetacirculacion']==1){$html.= '<td> <i style="color:#15A615;">Correcto</i> </td></tr>';}else{$html.= '<td> <i style="color:#CC0000;">¡¡¡PENDIENTE!!!</i> </td></tr>';}

       $html .= '<tr>
      <td> FL-GF-CMP-013 </td>
      <td> </td>
      <td> Carta Responsiva de Compra Venta </td>
      <td> </td>';
      if($guide['responsivacompraventa']==1){$html.= '<td> <i style="color:#15A615;">Correcto</i> </td></tr>';}else{$html.= '<td> <i style="color:#CC0000;">¡¡¡PENDIENTE!!!</i> </td></tr>';}

       $html .= '<tr>
      <td> FL-GF-CMP-013 </td>
      <td> </td>
      <td> Contrato de Compraventa </td>
      <td> </td>';
      if($guide['contratocompraventa']==1){$html.= '<td> <i style="color:#15A615;">Correcto</i> </td></tr>';}else{$html.= '<td> <i style="color:#CC0000;">¡¡¡PENDIENTE!!!</i> </td></tr>';}

       $html .= '<tr>
      <td> FL-GF-CMP-013 </td>
      <td> </td>
      <td> Aviso de Privacidad Integral </td>
      <td> </td>';
      if($guide['avisoprivacidad']==1){$html.= '<td> <i style="color:#15A615;">Correcto</i> </td></tr>';}else{$html.= '<td> <i style="color:#CC0000;">¡¡¡PENDIENTE!!!</i> </td></tr>';}

       $html .= '<tr>
      <td> FL-GF-CMP-013 </td>
      <td> </td>
      <td> Checklist de Revisión y Certificación 115 Puntos </td>
      <td> </td>';
      if($guide['checklistrevision']==1){$html.= '<td> <i style="color:#15A615;">Correcto</i> </td></tr>';}else{$html.= '<td> <i style="color:#CC0000;">¡¡¡PENDIENTE!!!</i> </td></tr>';}

       $html .= '<tr>
      <td> FL-GF-CMP-013 </td>
      <td> </td>
      <td> Evaluación Mecánica </td>
      <td> </td>';
      if($guide['evaluacionmecanica']==1){$html.= '<td> <i style="color:#15A615;">Correcto</i> </td></tr>';}else{$html.= '<td> <i style="color:#CC0000;">¡¡¡PENDIENTE!!!</i> </td></tr>';}

       $html .= '<tr>
      <td> FL-GF-CMP-013 </td>
      <td> </td>
      <td> Guía Autométrica (del mes de compra) </td>
      <td> </td>';
      if($guide['guiaautometrica']==1){$html.= '<td> <i style="color:#15A615;">Correcto</i> </td></tr>';}else{$html.= '<td> <i style="color:#CC0000;">¡¡¡PENDIENTE!!!</i> </td></tr>';}

       $html .= '<tr>
      <td> FL-GF-CMP-013 </td>
      <td> </td>
      <td> Póliza de Recepción de la Unidad Usada </td>
      <td> </td>';
      if($guide['polizaderecepcion']==1){$html.= '<td> <i style="color:#15A615;">Correcto</i> </td></tr>';}else{$html.= '<td> <i style="color:#CC0000;">¡¡¡PENDIENTE!!!</i> </td></tr>';}

       $html .= '<tr>
      <td> FL-GF-CMP-013 </td>
      <td> </td>
      <td> Cartera de Cuentas por Pagar </td>
      <td> </td>';
      if($guide['cuentasporpagar']==1){$html.= '<td> <i style="color:#15A615;">Correcto</i> </td></tr>';}else{$html.= '<td> <i style="color:#CC0000;">¡¡¡PENDIENTE!!!</i> </td></tr>';}

       $html .= '<tr>
      <td> FL-GF-CMP-013 </td>
      <td> </td>
      <td> Formato de Dación en Pago </td>
      <td> </td>';
      if($guide['formatodedacion']==1){$html.= '<td> <i style="color:#15A615;">Correcto</i> </td></tr>';}else{$html.= '<td> <i style="color:#CC0000;">¡¡¡PENDIENTE!!!</i> </td></tr>';}

       $html .= '<tr>
      <td> FL-GF-CMP-013 </td>
      <td> </td>
      <td> Consulta SIN REPORTE DE ROBO </td>
      <td> </td>';
      if($guide['reportederobo']==1){$html.= '<td> <i style="color:#15A615;">Correcto</i> </td></tr>';}else{$html.= '<td> <i style="color:#CC0000;">¡¡¡PENDIENTE!!!</i> </td></tr>';}

       $html .= '<tr>
      <td> FL-GF-CMP-013 </td>
      <td> </td>
      <td> Copia de Cheque a Proveedor </td>
      <td> </td>';
      if($guide['chequedeproveedor']==1){$html.= '<td> <i style="color:#15A615;">Correcto</i> </td></tr>';}else{$html.= '<td> <i style="color:#CC0000;">¡¡¡PENDIENTE!!!</i> </td></tr>';}

       $html .= '<tr>
      <td> FL-GF-CMP-013 </td>
      <td> </td>
      <td> Solicitud de Cheque </td>
      <td> </td>';
      if($guide['solicitudcheque']==1){$html.= '<td> <i style="color:#15A615;">Correcto</i> </td></tr>';}else{$html.= '<td> <i style="color:#CC0000;">¡¡¡PENDIENTE!!!</i> </td></tr>';}

       $html .= '<tr>
      <td> FL-GF-CMP-013 </td>
      <td> </td>
      <td> Estado de Cuenta </td>
      <td> </td>';
      if($guide['edocuenta']==1){$html.= '<td> <i style="color:#15A615;">Correcto</i> </td></tr>';}else{$html.= '<td> <i style="color:#CC0000;">¡¡¡PENDIENTE!!!</i> </td></tr>';}

       $html .= '<tr>
      <td> FL-GF-CMP-013 </td>
      <td> </td>
      <td> Autorizacion para la Toma de Unidades Seminuevas </td>
      <td> </td>';
      if($guide['autorizaciontoma']==1){$html.= '<td> <i style="color:#15A615;">Correcto</i> </td></tr>';}else{$html.= '<td> <i style="color:#CC0000;">¡¡¡PENDIENTE!!!</i> </td></tr>';}

       $html .= '<tr>
      <td> FL-GF-CMP-013 </td>
      <td> </td>
      <td> Guía Rápida para la Toma de un Usado </td>
      <td> </td>';
      if($guide['solicitudcfdi']==1){$html.= '<td> <i style="color:#15A615;">Correcto</i> </td></tr>';}else{$html.= '<td> <i style="color:#CC0000;">¡¡¡PENDIENTE!!!</i> </td></tr>';}

       




    $html .='
  </tbody>
</table>


<br>
<br>
<br>
<br>
<br>
<br>
 

<table align="center">
			<tr>
				<td>
					___________________________<br>
					<font color="#0A6ACF">'.$guide['ciadfirstnamegs'].' '.$guide['ciadlastnamegs'].'</font><br>
					Gerente de Seminuevos
				</td>
				<td>
					___________________________<br>
					<font color="#0A6ACF">'.$guide['ciadfirstnamega'].' '.$guide['ciadlastnamega'].'</font><br>
					Gerente Administrativo
				</td>
				
				<td>
					___________________________<br>
					<font color="#0A6ACF">'.$guide['ciadfirstnamegg'].' '.$guide['ciadlastnamegg'].'</font><br>
					Gerente General
				</td>
				
			</tr>
		</table>';






	

tcpdf();
$obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
header('Content-type: application/pdf');
$obj_pdf->SetCreator(PDF_CREATOR);
$title = "Checklist - ".$guide['folio'];
$obj_pdf->SetTitle($title);
$obj_pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $title, PDF_HEADER_STRING);
$obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$obj_pdf->SetDefaultMonospacedFont('helvetica');
$obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$obj_pdf->SetFont('helvetica', '', 9);
$obj_pdf->setFontSubsetting(false);
$obj_pdf->AddPage();
ob_start();
    // podemos tener cualquier parte de la vista aquí como HTML, PHP, etc.
$content = ob_get_contents();
ob_end_clean();
$obj_pdf->writeHTML($content, true, false, true, false, '');
$obj_pdf->writeHTML($html, true, false, true, false, '');


$filename = 'Checklist - '.$guide['folio'];

$obj_pdf->Output($filename . '.pdf', 'D');




?>