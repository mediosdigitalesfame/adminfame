<?php
class Model_model extends CI_Model{

	public function add_model($data){
		$this->db->insert('models', $data);
		return true;
	}

		//---------------------------------------------------
		// obtener todas los modelos para el procesamiento de tablas de datos del lado del servidor (basado en ajax)
		//-----------------------------------------------------
	public function get_all_models_brands(){
		$this->db->select('m.*, b.name as bname, a.name as aname');
		$this->db->join('brands b','b.id=m.brands_id', 'LEFT');
		$this->db->join('anios a','a.id=m.anios_id', 'LEFT');
			//$this->db->where('status',1);
		return $this->db->get('models m')->result_array();
	}

		//---------------------------------------------------
		// obtener todas los modelos con sus marcas para el procesamiento de tablas de datos del lado del servidor (basado en ajax)
		//-----------------------------------------------------
	public function get_all_models(){
		$this->db->select('*');
			//$this->db->where('status',1);
		return $this->db->get('models')->result_array();
	}

		//---------------------------------------------------
		// Obtener información de modelo por ID
		//-----------------------------------------------------
	public function get_model_by_id($id){
		$query = $this->db->get_where('models', array('id' => $id));
		return $result = $query->row_array();
	}

		//---------------------------------------------------
		// Editar registro de modelo
		//-----------------------------------------------------
	public function edit_model($data, $id){
		$this->db->where('id', $id);
		$this->db->update('models', $data);
		return true;
	}

		//---------------------------------------------------
		// Cambiar status de modelo
		//-----------------------------------------------------
	function change_status()
	{		
		$this->db->set('status', $this->input->post('status'));
		$this->db->where('id', $this->input->post('id'));
		$this->db->update('models');
	} 

		//---------------------------------------------------
		// obtener marcas para exportar a csv
		//-----------------------------------------------------
	public function get_models_for_csv(){
		$this->db->where('status', 1);
		$this->db->select('id, name, created_at');
		$this->db->from('models');
		$query = $this->db->get();
		return $result = $query->result_array();
	}

		//---------------------------------------------------
		// obtener todos los registros de marcas
		//-----------------------------------------------------
	public function get_all_simple_brands(){
		$this->db->where('status', 1);
		$this->db->order_by('id', 'desc');
		$query = $this->db->get('brands');
		return $result = $query->result_array();
	}

	//---------------------------------------------------
		// obtener todos los registros de años
		//-----------------------------------------------------
	public function get_all_simple_years(){
		$this->db->where('status', 1);
		$this->db->order_by('id', 'desc');
		$query = $this->db->get('anios');
		return $result = $query->result_array();
	}

	//---------------------------------------------------
		// obtener todos los registros de modelos
		//-----------------------------------------------------
	public function get_all_simple_models(){
		$this->db->where('status', 1);
		$this->db->order_by('id', 'desc');
		$query = $this->db->get('models');
		return $result = $query->result_array();
	}

		//---------------------------------------------------
		// obtener todos los registros de marcas 
		//-----------------------------------------------------
	function get_admin_roles()
	{
		$this->db->from('ci_admin_roles');
		$this->db->where('admin_role_status',1);
		$query=$this->db->get();
		return $query->result_array();
	}

}

?>