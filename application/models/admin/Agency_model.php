<?php
class Agency_model extends CI_Model{

	public function add_agency($data){
		$this->db->insert('agencys', $data);
		return true;
	}

		//---------------------------------------------------
		// obtener todas los agencias para el procesamiento de tablas de datos del lado del servidor (basado en ajax)
		//-----------------------------------------------------
	public function get_all_agencys_brands(){
		$this->db->select('m.*, b.name as bname, rsocial');
		$this->db->join('business b','b.id=m.business_id', 'LEFT');
			//$this->db->where('status',1);
		return $this->db->get('agencys m')->result_array();
	}

		//---------------------------------------------------
		// obtener todas los agencias con sus marcas para el procesamiento de tablas de datos del lado del servidor (basado en ajax)
		//-----------------------------------------------------
	public function get_all_agencys(){
		$this->db->select('*');
			//$this->db->where('status',1);
		return $this->db->get('agencys')->result_array();
	}

		//---------------------------------------------------
		// Obtener información de la agencia por ID
		//-----------------------------------------------------
	public function get_agency_by_id($id){
		$query = $this->db->get_where('agencys', array('id' => $id));
		return $result = $query->row_array();
	}

	//---------------------------------------------------
		// Obtener información de la agencia y sus tablas padre por ID
		//-----------------------------------------------------
	public function get_agency_by_id_business($id){
		$this->db->where('a.id', $id);
		$this->db->select('a.id, a.name, a.status, a.created_at, a.tel, a.calle, a.int, a.ext, a.colonia, a.cp, a.ciudad, a.estado, a.brands_id, a.business_id, a.users_id_gg, a.users_id_gs, a.users_id_ga, b.rsocial, b.rfc');
		$this->db->join('business b','b.id=a.business_id', 'LEFT');
		$this->db->from('agencys a');
		$query = $this->db->get();
		return $result = $query->row_array();
 
	}

		//---------------------------------------------------
		// Editar registro de agencia
		//-----------------------------------------------------
	public function edit_agency($data, $id){
		$this->db->where('id', $id);
		$this->db->update('agencys', $data);
		return true;
	}

		//---------------------------------------------------
		// Cambiar status de modelo
		//-----------------------------------------------------
	function change_status()
	{		
		$this->db->set('status', $this->input->post('status'));
		$this->db->where('id', $this->input->post('id'));
		$this->db->update('agencys');
	} 

		//---------------------------------------------------
		// obtener marcas para exportar a csv
		//-----------------------------------------------------
	public function get_agencys_for_csv(){
		$this->db->where('status', 1);
		$this->db->select('id, name, created_at');
		$this->db->from('agencys');
		$query = $this->db->get();
		return $result = $query->result_array();
	}

		//---------------------------------------------------
		// obtener todos los registros de marcas
		//-----------------------------------------------------
	public function get_all_simple_brands(){
		$this->db->where('status', 1);
		$this->db->order_by('id', 'desc');
		$query = $this->db->get('brands');
		return $result = $query->result_array();
	}


	//---------------------------------------------------
		// obtener todos los registros de años
		//-----------------------------------------------------
	public function get_all_simple_years(){
		$this->db->where('status', 1);
		$this->db->order_by('id', 'desc');
		$query = $this->db->get('anios');
		return $result = $query->result_array();
	}

	//---------------------------------------------------
		// obtener todos los registros de negocios
		//-----------------------------------------------------
	public function get_all_simple_business(){
		$this->db->where('status', 1);
		$this->db->order_by('id', 'desc');
		$query = $this->db->get('business');
		return $result = $query->result_array();
	}

	//---------------------------------------------------
		// obtener todos los registros de admins
		//-----------------------------------------------------
	public function get_all_simple_admins(){
		//$this->db->where('is_active', 1);
		$this->db->order_by('admin_id', 'desc');
		$query = $this->db->get('ci_admin');
		return $result = $query->result_array();
	}

	//---------------------------------------------------
		// obtener todos los registros de agencias
		//-----------------------------------------------------
	public function get_all_simple_agencys(){
		$this->db->where('status', 1);
		$this->db->order_by('id', 'desc');
		$query = $this->db->get('agencys');
		return $result = $query->result_array();
	}

		//---------------------------------------------------
		// obtener todos los registros de marcas 
		//-----------------------------------------------------
	function get_admin_roles()
	{
		$this->db->from('ci_admin_roles');
		$this->db->where('admin_role_status',1);
		$query=$this->db->get();
		return $query->result_array();
	}

}

?>