<?php
tcpdf();
$obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
header('Content-type: application/pdf');
$obj_pdf->SetCreator(PDF_CREATOR);
$title = "Toma de Seminuevos".'-'. $take['folio'];
$obj_pdf->SetTitle($title);
$obj_pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $title, PDF_HEADER_STRING);
$obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$obj_pdf->SetDefaultMonospacedFont('helvetica');
$obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$obj_pdf->SetFont('helvetica', '', 9);
$obj_pdf->setFontSubsetting(false);
$obj_pdf->AddPage();
ob_start();
    // podemos tener cualquier parte de la vista aquí como HTML, PHP, etc.
$content = ob_get_contents();
ob_end_clean();
$obj_pdf->writeHTML($content, true, false, true, false, '');

$fmt = new NumberFormatter('en_US', NumberFormatter::CURRENCY);
$formatterES = new NumberFormatter("es-ES", NumberFormatter::SPELLOUT);


$izquierda = intval(floor($take['pdlttfac2']));
$derecha = intval(($take['pdlttfac2'] - floor($take['pdlttfac2'])) * 100);

                

$html = '
<table border="0" style="width:100%">
 
<tbody></tbody></table>';

 
$subtable = '<table border="0" cellspacing="6" cellpadding="4"><tr><td>a</td><td>b</td></tr><tr><td>c</td><td>d</td></tr></table>';



$html .= ' 


<table border="0" cellpadding="4">

	<tr align="right">
		<td colspan="4">
			<h3>';
			if($take['status']==1){
				$html.= ' <i style="color:#15A615;">Autorizada!</i> ';}
				else{
					$html.= '<i style="color:#CC0000;">Pendiente!</i>';}
			$html .= ' </h3>
		</td>
	</tr>
	<tr align="right">
		<td colspan="4">
			<h2>';
			if($take['tipodetoma']==0){
				$html.= ' <i style="color:#3333ff;">Pago Directo</i> ';}
				else{
					$html.= '<i style="color:#ff7133;">Dación en Pago</i>';}
			$html .= ' </h2>
		</td>
	</tr>';

$html .= ' 
	<tr>
		<td colspan="2">
			<h4>Proveedor</h4>
			Nombre Completo: <font color="#0A6ACF">'.$take['nombre'].'</font><br>
			Fecha Contrato: <font color="#0A6ACF">'.$take['fechacontrato'].'</font><br>
			<h4>Domicilio</h4>
			Calle: <font color="#0A6ACF">'.$take['calle'].'</font><br>
			Num. Ext: <font color="#0A6ACF">'.$take['numext'].'</font><br>
			Num. Int:<font color="#0A6ACF">'.$take['numint'].'</font><br>
			Colonia: <font color="#0A6ACF">'.$take['colonia'].'</font><br>
			Ciudad: <font color="#0A6ACF">'.$take['ciudad'].'</font><br>
			Municipio: <font color="#0A6ACF">'.$take['municipio'].'</font><br>
			Estado: <font color="#0A6ACF">'.$take['estado'].'</font><br>
			CP: <font color="#0A6ACF">'.$take['cp'].'</font><br>
			RFC: <font color="#0A6ACF">'.$take['rfc'].'</font><br>
			Num. Id: <font color="#0A6ACF">'.$take['nidenti'].'</font><br>
			Tipo Id: <font color="#0A6ACF">'.$take['tipoidenti'].'</font><br>
			Mes Recibo: <font color="#0A6ACF">'.$take['mesrecibo'].'</font>
		</td>

		<td colspan="2">
			<h4>Solicitud CFDI:</h4><br>
			Nombre o Nombres (CFDI): <font color="#0A6ACF">'.$take['nombrescfdi'].'</font><br>
			Primer Apellido (CFDI): <font color="#0A6ACF">'.$take['apellido1'].'</font><br>
			Segundo Apellido (CFDI): <font color="#0A6ACF">'.$take['apellido2'].'</font><br>
			eMail (CFDI): <font color="#0A6ACF">'.$take['email'].'</font><br>
			Telefono Fijo (CFDI): <font color="#0A6ACF">'.$take['tel'].'</font><br>
			Telefono Movil (CFDI):  <font color="#0A6ACF">'.$take['cel'].'</font><br>
			Fecha Nacimiento:  <font color="#0A6ACF">'.$take['fechanaci'].'</font><br>
			Entidad de Nacimiento: <font color="#0A6ACF">'.$take['entidadnaci'].'</font><br>
			Curp: <font color="#0A6ACF">'.$take['curp'].'</font><br>
			Pais: <font color="#0A6ACF">'.$take['pais'].'</font>
		</td>
	</tr>

	<tr>
		<td colspan="2">
			<h4>Vehiculo</h4>
			Factura que ampara el vehiculo No.: <font color="#0A6ACF">'.$take['facturavehi'].'</font><br>
			Expedida por: <font color="#0A6ACF">'.$take['expedida'].'</font><br>
			Fecha de factura: <font color="#0A6ACF">'.$take['fechafac'].'</font><br>
			Marca Vehiculo: <font color="#0A6ACF">'.$take['brname'].'</font><br>
			Año Modelo: <font color="#0A6ACF">'.$take['anname'].'</font><br>
			Color Exterior: <font color="#0A6ACF">'.$take['colorext'].'</font><br>
			Color Interior: <font color="#0A6ACF">'.$take['colorint'].'</font><br>
			Tipo Version: <font color="#0A6ACF">'.$take['versiontxt'].'</font><br>
			Clave Vehicular: <font color="#0A6ACF">'.$take['clavevehi'].'</font><br>
			No. de Serie (17 digitos): <font color="#0A6ACF">'.$take['nserie'].'</font><br>
			No. Motor: <font color="#0A6ACF">'.$take['nmotor'].'</font><br>
			Kilometraje:  <font color="#0A6ACF">'.$take['km'].'</font><br>
			No. De baja vehicular:  <font color="#0A6ACF">'.$take['nbaja'].'</font><br>
			Estado que emite la baja:  <font color="#0A6ACF">'.$take['edoemisor'].'</font><br>
			Fecha de la baja:  <font color="#0A6ACF">'.$take['fechabaja'].'</font><br>
			Fecha de la baja:  <font color="#0A6ACF">'.$take['fechabaja'].'</font><br>
			Años de tenencias:  <font color="#0A6ACF">'.$take['tenencias'].'</font><br>
			Placas dadas de baja:  <font color="#0A6ACF">'.$take['placasbaja'].'</font><br>
			No. de verificacion:  <font color="#0A6ACF">'.$take['nverificacion'].'</font><br>
			Valor de compra (enajenacion):   <font color="#0A6ACF">'.$fmt->formatCurrency($take['pdlttfac2'], "USD") .'</font><br>
			Valor en letra:   <font color="#0A6ACF">'.$formatterES->format($izquierda) . " punto " . $formatterES->format($derecha).'</font><br>
			Valor Guia Autometrica: <font color="#0A6ACF">'.$take['valorautometrica'].'</font><br>
			Nombre a quien se vende: <font color="#0A6ACF">'.$take['aquiensevende'].'</font><br>
			Fecha de factura final: <font color="#0A6ACF">'.$take['fechafacfinal'].'</font><br>
			Fecha de toma por Grupo FAME: <font color="#0A6ACF">'.$take['fechatoma'].'</font></td>
		<td colspan="2">
			<h4>Datos Agencia:</h4><br>
			Razon Social:  <font color="#0A6ACF">'.$take['bursocial'].'</font><br>
			Nombre comercial:  <font color="#0A6ACF">'.$take['agname'].'</font><br>
			Calle: <font color="#0A6ACF">'.$take['agcalle'].'</font><br>
			Número: <font color="#0A6ACF">'.$take['agext'].'</font><br>
			Colonia: <font color="#0A6ACF">'.$take['agcolonia'].'</font><br>
			Codigo postal: <font color="#0A6ACF">'.$take['agcp'].'</font><br>
			Ciudad/Estado (Fiscal): <font color="#0A6ACF">'.$take['buestado'].'</font><br>
			Gerente General: <font color="#0A6ACF">'.$take['ciadfirstnamegg'].'</font> <font color="#0A6ACF">'.$take['ciadlastnamegg'].'</font><br>
			Gerente Seminuevos: <font color="#0A6ACF">'.$take['ciadfirstnamega'].'</font> <font color="#0A6ACF">'.$take['ciadlastnamega'].'</font><br>
			Gerente Administrativo: <font color="#0A6ACF">'.$take['ciadfirstnamegs'].'</font> <font color="#0A6ACF">'.$take['ciadlastnamegs'].'</font><br>
			Ciudad / Estado: <font color="#0A6ACF">'.$take['agestado'].'</font><br>
			Sitio WEB Agencia: <font color="#0A6ACF">'.$take['agurl'].'</font><br>
			Link Aviso de Privacidad: <font color="#0A6ACF">'.$take['agurlp'].'</font><br>
			<h4>Carta de No retencion:</h4><br>
			Costo de Adquisicion (Ultimo valor en Factura): <font color="#0A6ACF">'.$take['costoadquisicion'].'</font>
			<h4>Solicitud Cheque:</h4><br>
			Bnaco:<font color="#0A6ACF">'.$take['banco'].'</font><br>
			Cuenta:<font color="#0A6ACF">'.$take['cuenta'].'</font><br>
			Clabe:<font color="#0A6ACF">'.$take['clabe'].'</font><br>
			Sucursal:<font color="#0A6ACF">'.$take['sucursal'].'</font><br>
			Convenio:<font color="#0A6ACF">'.$take['convenio'].'</font><br>
			Referencia:<font color="#0A6ACF">'.$take['referencia'].'</font><br>
			Folio CFDI:<font color="#0A6ACF">'.$take['foliocfdi'].'</font>
		</td>         
	</tr>
</table>';

$obj_pdf->writeHTML($html, true, false, true, false, '');
$filename = 'Toma de Seminuevos'.'-'.$take['folio'];
$obj_pdf->Output($filename . '.pdf', 'D');
?>