    <!-- daterange picker -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/daterangepicker/daterangepicker-bs3.css">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/iCheck/all.css">
    <!-- Bootstrap Color Picker -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/colorpicker/bootstrap-colorpicker.min.css">
    <!-- Bootstrap time Picker -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/timepicker/bootstrap-timepicker.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/select2/select2.min.css">

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Main content -->
      <section class="content">
        <div class="card card-default">
          <div class="card-header">
            <div class="d-inline-block">
              <h3 class="card-title"> <i class="fa fa-pencil"></i>
                &nbsp; <?= trans('edit_agency') ?> </h3>
              </div>
              <div class="d-inline-block float-right">
                <a href="<?= base_url('admin/agencys'); ?>" class="btn btn-success"><i class="fa fa-list"></i> <?= trans('agencys_list') ?></a>
                <a href="<?= base_url('admin/agencys/add'); ?>" class="btn btn-success"><i class="fa fa-plus"></i> <?= trans('add_new_model') ?></a>
              </div>
            </div>
            <div class="card-body">

             <!-- For Messages -->
             <?php $this->load->view('admin/includes/_messages.php') ?>
             
             <?php echo form_open(base_url('admin/agencys/edit/'.$agency['id']), 'class="form-horizontal"' )?> 

             <div class="form-group">
               <div class="row">
                <div class="col-lg-4">
                  <div class="input-group">
                    <label for="name" class="col-md-6 control-label"><?= trans('name') ?></label>
                    <div class="col-md-12">
                      <input type="text" name="name" value="<?= $agency['name']; ?>" class="form-control" id="name" placeholder="" >
                    </div>
                  </div>
                </div>
                <div class="col-lg-5">
                  <div class="input-group">
                   <label for="rsocial" class="col-md-6 control-label"><?= trans('rsocial') ?></label>
                   <div class="col-md-12">
                    <input type="text" name="rsocial" value="<?= $agency['rsocial']; ?>" class="form-control" id="rsocial" placeholder="">
                  </div>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="rfc" class="col-md-6 control-label"><?= trans('rfc') ?></label>
                  <div class="col-md-12">
                    <input type="text" name="rfc" value="<?= $agency['rfc']; ?>" class="form-control" id="rfc" placeholder="">
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="form-group">
           <div class="row">
            <div class="col-lg-3">
              <div class="input-group">
                <label for="tel" class="col-md-12 control-label"><?= trans('tel') ?></label>
                <div class="col-md-12">
                  <input type="text" name="tel" value="<?= $agency['tel']; ?>" class="form-control" id="tel" placeholder="">
                </div>
              </div>
            </div>
            <div class="col-lg-5">
              <div class="input-group">
                <label for="calle" class="col-md-12 control-label"><?= trans('street') ?></label>
                <div class="col-md-12">
                  <input type="text" name="calle" value="<?= $agency['calle']; ?>" class="form-control" id="calle" placeholder="">
                </div>
              </div>
            </div>
            <div class="col-lg-2">
              <div class="input-group">
               <label for="int" class="col-md-12 control-label"><?= trans('int') ?></label>
               <div class="col-md-12">
                <input type="text" name="int" value="<?= $agency['int']; ?>" class="form-control" id="int" placeholder="">
              </div>
            </div>
          </div>
          <div class="col-lg-2">
            <div class="input-group">
              <label for="ext" class="col-md-12 control-label"><?= trans('ext') ?></label>
              <div class="col-md-12">
                <input type="text" name="ext" value="<?= $agency['ext']; ?>" class="form-control" id="ext" placeholder="">
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="form-group">
       <div class="row">
        <div class="col-lg-6">
          <div class="input-group">
           <label for="colonia" class="col-md-12 control-label"><?= trans('suburb') ?></label>
           <div class="col-md-12">
            <input type="text" name="colonia" value="<?= $agency['colonia']; ?>" class="form-control" id="colonia" placeholder="">
          </div>
        </div>
      </div>
      <div class="col-lg-2">
        <div class="input-group">
          <label for="cp" class="col-md-12 control-label"><?= trans('postcode') ?></label>
          <div class="col-md-12">
            <input type="text" name="cp" value="<?= $agency['cp']; ?>" class="form-control" id="cp" placeholder="">
          </div>
        </div>
      </div>
      <div class="col-lg-2">
        <div class="input-group">
          <label for="ciudad" class="col-md-12 control-label"><?= trans('city') ?></label>
          <div class="col-md-12">
            <input type="text" name="ciudad" value="<?= $agency['ciudad']; ?>" class="form-control" id="ciudad" placeholder="">
          </div>
        </div>
      </div>
      <div class="col-lg-2">
        <div class="input-group">
          <label for="estado" class="col-md-12 control-label"><?= trans('state') ?></label>
          <div class="col-md-12">
            <input type="text" name="estado" value="<?= $agency['estado']; ?>" class="form-control" id="estado" placeholder="">
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="form-group">
   <div class="row">
    <div class="col-lg-6">
      <div class="input-group">
       <label for="brand" class="col-md-12 control-label"><?= trans('select_brand') ?>*</label>
       <div class="col-md-12">
        <select name="brand" class="form-control">
          <option value=""><?= trans('select_brand') ?></option>
          <?php foreach($brands as $brand): ?>
            <?php if($brand['id'] == $agency['brands_id']): ?>
              <option value="<?= $brand['id']; ?>" selected><?= $brand['name']; ?></option>
              <?php else: ?>
                <option value="<?= $brand['id']; ?>"><?= $brand['name']; ?></option>
              <?php endif; ?>
            <?php endforeach; ?>
          </select>
        </div>
      </div>
    </div>
    <div class="col-lg-6">
      <div class="input-group">
        <label for="busin" class="col-md-12 control-label"><?= trans('select_business') ?>*</label>
        <div class="col-md-12">
          <select name="busin" class="form-control ">
            <option value=""><?= trans('select_business') ?></option>
            <?php foreach($business as $busin): ?>
             <?php if($busin['id'] == $agency['business_id']): ?>
              <option value="<?= $busin['id']; ?>" selected><?= $busin['name']; ?></option>
              <?php else: ?>
                <option value="<?= $busin['id']; ?>"><?= $busin['name']; ?></option>
              <?php endif; ?>
            <?php endforeach; ?>
          </select>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="form-group">
 <div class="row">
  <div class="col-lg-4">
    <div class="input-group">
     <label for="gg" class="col-md-12 control-label"><?= trans('select_gg') ?>*</label>
     <div class="col-md-12">
      <select name="gg" class="form-control">
        <option value=""><?= trans('select_gg') ?></option>
        <option value=""><?= trans('select_business') ?></option>
        <?php foreach($admins as $admin): ?>
         <?php if($admin['admin_id'] == $agency['users_id_gg']): ?>
          <option value="<?= $admin['admin_id']; ?>" selected><?= $admin['firstname'].' '.$admin['lastname']; ?></option>
          <?php else: ?>
            <option value="<?= $admin['admin_id']; ?>"><?= $admin['firstname'].' '.$admin['lastname']; ?></option>
          <?php endif; ?>
        <?php endforeach; ?>
      </select>
    </div>
  </div>
</div>
<div class="col-lg-4">
  <div class="input-group">
   <label for="gs" class="col-md-12 control-label"><?= trans('select_gs') ?>*</label>
   <div class="col-md-12">
    <select name="gs" class="form-control">
      <option value=""><?= trans('select_gs') ?></option>
      <option value=""><?= trans('select_business') ?></option>
      <?php foreach($admins as $admin): ?>
       <?php if($admin['admin_id'] == $agency['users_id_gs']): ?>
        <option value="<?= $admin['admin_id']; ?>" selected><?= $admin['firstname'].' '.$admin['lastname']; ?></option>
        <?php else: ?>
          <option value="<?= $admin['admin_id']; ?>"><?= $admin['firstname'].' '.$admin['lastname']; ?></option>
        <?php endif; ?>
      <?php endforeach; ?>
    </select>
  </div>
</div>
</div>
<div class="col-lg-4">
  <div class="input-group">
    <label for="ga" class="col-md-12 control-label"><?= trans('select_ga') ?>*</label>
    <div class="col-md-12">
      <select name="ga" class="form-control">
        <option value=""><?= trans('select_ga') ?></option>
        <option value=""><?= trans('select_business') ?></option>
        <?php foreach($admins as $admin): ?>
         <?php if($admin['admin_id'] == $agency['users_id_gg']): ?>
          <option value="<?= $admin['admin_id']; ?>" selected><?= $admin['firstname'].' '.$admin['lastname']; ?></option>
          <?php else: ?>
            <option value="<?= $admin['admin_id']; ?>"><?= $admin['firstname'].' '.$admin['lastname']; ?></option>
          <?php endif; ?>
        <?php endforeach; ?>
      </select>
    </div>
  </div>
</div>
</div>
</div>

<div class="form-group">
  <label for="role" class="col-md-2 control-label"><?= trans('status') ?></label>

  <div class="col-md-12">
    <select name="status" class="form-control">
      <option value=""><?= trans('select_status') ?></option>
      <option value="1" <?= ($agency['status'] == 1)?'selected': '' ?> ><?= trans('active') ?></option>
      <option value="0" <?= ($agency['status'] == 0)?'selected': '' ?>><?= trans('inactive') ?></option>
    </select>
  </div>
</div>
<div class="form-group">
  <div class="col-md-12">
    <input type="submit" name="submit" value="<?= trans('update_model') ?>" class="btn btn-primary pull-right">
  </div>
</div>
<?php echo form_close(); ?>
</div>
<!-- /.box-body -->
</div>  
</section> 
</div>



<!-- Select2 -->
<script src="<?= base_url() ?>assets/plugins/select2/select2.full.min.js"></script>
<!-- InputMask -->
<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src="<?= base_url() ?>assets/plugins/daterangepicker/daterangepicker.js"></script>
<!-- bootstrap color picker -->
<script src="<?= base_url() ?>assets/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="<?= base_url() ?>assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- Page script -->
<!-- iCheck 1.0.1 -->
<script src="<?= base_url() ?>assets/plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker         : true,
      timePickerIncrement: 30,
      format             : 'MM/DD/YYYY h:mm A'
    })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
    {
      ranges   : {
        'Today'       : [moment(), moment()],
        'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month'  : [moment().startOf('month'), moment().endOf('month')],
        'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      },
      startDate: moment().subtract(29, 'days'),
      endDate  : moment()
    },
    function (start, end) {
      $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
    }
    )

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>

<script>
  $("#forms").addClass('active');
  $("#advanced").addClass('active');
</script>  
