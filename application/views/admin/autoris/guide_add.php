  <!-- daterange picker -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/daterangepicker/daterangepicker-bs3.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/colorpicker/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/select2/select2.min.css">

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      <div class="card card-default">

        <div class="card-header">
          <div class="d-inline-block">
            <h3 class="card-title"> <i class="fa fa-plus"></i>
             <?= trans('add_new_authorization') ?> </h3>
           </div>
           <div class="d-inline-block float-right">
            <a href="<?= base_url('admin/autoris'); ?>" class="btn btn-success"><i class="fa fa-list"></i>Lista de Autorizaciones</a>
          </div>
        </div>

        <div class="card-body">

         <!-- For Messages -->
         <?php $this->load->view('admin/includes/_messages.php') ?>
         <?php echo form_open_multipart(base_url('admin/autoris/add'), 'class="form-horizontal"','id="formgu"','name="formgu"');  ?> 
         <?php $permitted_chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'; $iden = '-'.date('dmY').'-'.substr(str_shuffle($permitted_chars), 0, 8); ?>
         <input type="hidden" id="identi" name="identi" value="<?php echo html_escape($iden); ?>">
         <input type="hidden" id="id" name="id" value="">

         <div class="form-group">
           <div class="row">

            <div class="col-lg-1">
              <div class="input-group">
                <label for="firstname" alling="right" class="pull-right col-md-12 control-label" style="display: flex; justify-content: right; width: 100%;"> Guía Rápida:</label>  
              </div>
            </div>

            <div class="col-lg-3">
              <div class="input-group">
               <div class="col-md-12">
                <select name="check" id="check" class="form-control select2" >
                  <option value="">Seleccionar</option>
                  <?php foreach($checks as $check): ?>
                    <option value="<?= $check['identificador']; ?>"><?= $check['folio']; ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>
          </div>

          <div class="col-lg-4"></div>
            <div class="col-lg-4"></div>

          
        </div>
      </div>


<!-- <div class="form-group">
       <div class="row">

  <div class="col-lg-4">
    <div class="input-group">
     <label for="observaciones" class="col-md-12 control-label">Observaciones</label>
     <div class="col-md-12">
      <input onkeyup="javascript:this.value=this.value.toUpperCase();" type="text" value="<?= old("observaciones"); ?>" class="form-control " name="observaciones" id="observaciones">
      <div class="input-group">
       <h4 id="mensajevin" style="color: red;"></h4>
     </div>
   </div>
 </div>
</div> 

<div class="col-lg-5">
  <div class="input-group">
    <label class="col-md-12 control-label">Imágen Autometrica</label> 
    <div class="form-group">
      <td>
       <input type="file" name="image" accept=".png, .jpg, .jpeg, .gif, .svg, .pdf">
       <p><small class="text-success"><?= trans('allowed_types') ?>: gif, jpg, png, jpeg, pdf</small></p>
     </td>
   </div>
 </div>
</div>

</div>
</div>-->

<div class="form-group">
  <div class="row">
    <div class="col-lg-3">
      <div class="input-group">
        <label class="col-md-12 control-label">Condiciones Generales</label> 
        <div class="col-md-12">
         <input type="file" name="cgenerales" accept=".png, .jpg, .jpeg, .gif, .svg, .pdf">
         <p><small class="text-success"><?= trans('allowed_types') ?>: gif, jpg, png, jpeg, pdf</small></p>
       </div>
     </div>
   </div>
   <div class="col-lg-3">
    <div class="input-group">
     <label class="col-md-12 control-label">Reporte Diagnostico</label> 
     <div class="col-md-12">
      <input type="file" name="rdiagnostico" accept=".png, .jpg, .jpeg, .gif, .svg, .pdf">
      <p><small class="text-success"><?= trans('allowed_types') ?>: gif, jpg, png, jpeg, pdf</small></p>
    </div>
  </div>
</div>
<div class="col-lg-3">
  <div class="input-group">
   <label class="col-md-12 control-label">Reporte Transunion</label> 
   <div class="col-md-12">
    <input type="file" name="rtrans" accept=".png, .jpg, .jpeg, .gif, .svg, .pdf">
    <p><small class="text-success"><?= trans('allowed_types') ?>: gif, jpg, png, jpeg, pdf</small></p>
  </div>
</div>
</div>
<div class="col-lg-3">
  <div class="input-group">
    <label class="col-md-12 control-label">Situacion  Fiscal</label> 
    <div class="col-md-12">
      <input type="file" name="sfiscal" accept=".png, .jpg, .jpeg, .gif, .svg, .pdf">
      <p><small class="text-success"><?= trans('allowed_types') ?>: gif, jpg, png, jpeg, pdf</small></p>
    </div>
  </div>
</div>
</div>

<div class="row">
  <div class="col-lg-6">
    <div class="input-group">
      <label for="observaciones" class="col-md-12 control-label">Observaciones</label>
      <div class="col-md-12">
        <input onkeyup="javascript:this.value=this.value.toUpperCase();" type="text" value="<?= old("observaciones"); ?>" class="form-control " name="observaciones" id="observaciones">
        <div class="input-group">
        </div>
      </div>
    </div>
  </div>
</div>
</div>

<div class="row m-t-30">
  <!-- csrf token -->
  <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
</div> <!-- div class row 30 -->

</div>
<!-- /.box-body -->
</div>

<!--
<div class="card card-default">

 <div class="card-header">
  <div class="d-inline-block">
    <h3 class="card-title">Importes en Guia Autometrica</h3>
  </div>
</div>

<div class="card-body">

  <div class="d-inline-block">
    <h3 class="card-title">COMPRA</h3>
  </div>
  <br>

  <div class="form-group">
   <div class="row">
    <div class="col-lg-3">
      <div class="input-group">
       <label for="pcompra" class="col-md-12 control-label">Precio Compra Libro</label>
       <div class="col-md-12">
        <input type="text" class="form-control" placeholder="" id="pcompra" name="pcompra">
      </div>
    </div>
  </div>
  <div class="col-lg-3">
    <div class="input-group">
     <label for="bonoc" class="col-md-12 control-label">Bono x Equipamiento</label>
     <div class="col-md-12">
      <input type="text" data-role="tagsinput" class="form-control" placeholder="" id="bonoc" name="bonoc">
    </div>
  </div>
</div>
<div class="col-lg-3">
  <div class="input-group">
   <label for="premioc" class="col-md-12 control-label">Premio / Castigo</label>
   <div class="col-md-12">
    <input type="text" data-role="tagsinput" class="form-control" placeholder="" id="premioc" name="premioc">
  </div>
</div>
</div>
<div class="col-lg-3">
  <div class="input-group">
   <label for="precioc" class="col-md-12 control-label">Precio de Compra</label>
   <div class="col-md-12">
    <input type="text" data-role="tagsinput" class="form-control" placeholder="" id="precioc" name="precioc">
  </div>
</div>
</div>
</div>
</div>

<br>

<div class="d-inline-block">
  <h3 class="card-title">VENTA</h3>
</div>
<br>

<div class="form-group">
 <div class="row">
  <div class="col-lg-3">
    <div class="input-group">
     <label for="oventa" class="col-md-12 control-label">Precio de Venta</label>
     <div class="col-md-12">
      <input type="text" class="form-control" placeholder="" id="pventa" name="pventa">
    </div>
  </div>
</div>
<div class="col-lg-3">
  <div class="input-group">
   <label for="bonov" class="col-md-12 control-label">Bono x Equipamiento</label>
   <div class="col-md-12">
    <input type="text" class="form-control" placeholder="" id="bonov" name="bonov">
  </div>
</div>
</div>
<div class="col-lg-3">
  <div class="input-group">
   <label for="premiov" class="col-md-12 control-label">Premio / Castigo</label>
   <div class="col-md-12">
    <input type="text" class="form-control" placeholder="" id="premiov" name="premiov">
  </div>
</div>
</div>
<div class="col-lg-3">
  <div class="input-group">
   <label for="preciov" class="col-md-12 control-label">Precio de Venta</label>
   <div class="col-md-12">
    <input type="text" class="form-control" placeholder="" id="preciov" name="preciov">
  </div>
</div>
</div>

</div>
</div>

</div>
</div> 
-->   

<div class="card card-default">
  <div class="card-body">
    <div class="form-group">
      <div class="col-md-12" name="botones" id="botones">
        <input type="submit" name="submit" id="submit" value="<?= trans('add_authorization') ?>" class="btn btn-primary pull-right">
      </div>
    </div>
    <?php echo form_close( ); ?>
  </div>
</div>

</section> 
</div>

<!-- Select2 -->
<script src="<?= base_url() ?>assets/plugins/select2/select2.full.min.js"></script>
<!-- InputMask -->
<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<script src="<?= base_url() ?>assets/plugins/input-mask/inputmask.js"></script>
<script src="<?= base_url() ?>assets/plugins/input-mask/inputmask.min.js"></script>
<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.min.js"></script>
<!-- date-range-picker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src="<?= base_url() ?>assets/plugins/daterangepicker/daterangepicker.js"></script>
<!-- bootstrap color picker -->
<script src="<?= base_url() ?>assets/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="<?= base_url() ?>assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- Page script -->
<!-- iCheck 1.0.1 -->
<script src="<?= base_url() ?>assets/plugins/iCheck/icheck.min.js"></script>
<script>
  document.addEventListener('DOMContentLoaded', () => {
    document.querySelectorAll('input[type=text]').forEach( node => node.addEventListener('keypress', e => {
      if(e.keyCode == 13) {
        e.preventDefault();
      }
    }))
  });
</script>

<script>
 // ! function(window, document, $) {
  //  "use strict";
 //   $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
 // }(window, document, jQuery);
</script>

<script>
 $("#compraautometrica,#ventaautometrica,#reacondicionamiento,#equipamiento,#premio,#valor").inputmask("currency",{
   numericInput: true,
   rightAlign: true,
   digitsOptional: true,
   prefix:'',
   inputFormat: "999,999,999.99",
   outputFormat: "999999999.99",
   inputMode:"numeric",
   inputEventOnly: true,
   removeMaskOnSubmit: true,
   }); //default


 $(function () {

    //Initialize Select2 Elements
    $('.select2').select2()
    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker         : true,
      timePickerIncrement: 30,
      format             : 'MM/DD/YYYY h:mm A'
    })

    //Date range as a button
    $('#daterange-btn').daterangepicker(
    {
      ranges   : {
        'Today'       : [moment(), moment()],
        'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month'  : [moment().startOf('month'), moment().endOf('month')],
        'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      },
      startDate: moment().subtract(29, 'days'),
      endDate  : moment()
    },

    function (start, end) {$('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))})

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })

    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })

    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>

<script>
  $("#forms").addClass('active');
  $("#advanced").addClass('active');
</script>  

<script>
//-------------------------------------------------------------------
// Scrip para cambiar el año y los modelos 
//-------------------------------------------------------------------
var csfr_token_name = '<?php echo $this->security->get_csrf_token_name(); ?>';
var csfr_token_value = '<?php echo $this->security->get_csrf_hash(); ?>';

$(function(){

  $(document).on('change','.country',function()
  {
    if(this.value == '')
    {
      $('.state').html('<option value="">Select Option</option>');
      $('.city').html('<option value="">Select Option</option>');
      return false;
    }

    var data =  {country : this.value,}
    data[csfr_token_name] = csfr_token_value;

    $.ajax({
      type: "POST",
      url: "<?= base_url('admin/auth/get_brands_years') ?>",
      data: data,
      dataType: "json",

      success: function(obj) {
        $('.state').html(obj.msg);
      },
    });
  });

  $(document).on('change','.state',function()
  {
    var data =  {state : this.value,}
    data[csfr_token_name] = csfr_token_value;

    $.ajax({
      type: "POST",
      url: "<?= base_url('admin/auth/get_years_models') ?>",
      data: data,
      dataType: "json",
      success: function(obj) {
        $('.city').html(obj.msg);
      },
    });
  });
});
</script>

<script>
 $(document).ready(function(){

  $('#km').keyup(function(){
    var id=$(this).val();
    if (id.length > 1){ document.getElementById("compraautometrica").disabled = false;} 
    else { document.getElementById("compraautometrica").disabled = true; }
      //if(id > 80000){swal("Detalle!", "El Kilometraje No Puede ser superior a 80,000", "error")
     //   document.getElementById("km").value = "";
   // }
 });

  $('#compraautometrica').change(function(){
    var id=$(this).val();
    if (id.length > 2){ document.getElementById("ventaautometrica").disabled = false;} 
    else { document.getElementById("ventaautometrica").disabled = true;}
    if (document.getElementById("ventaautometrica").value>1){cambiaValores();}
  });

  $('#ventaautometrica').keyup(function(){
    var id=$(this).val();
    if (id.length > 3){ document.getElementById("premio").disabled = false;} 
    else { document.getElementById("premio").disabled = true;}
  });

  $('#equipamiento').keyup(function(){
    var id=$(this).val();
    if (id.length > 3){ document.getElementById("premio").disabled = false;} 
    else { document.getElementById("premio").disabled = true;}
  });

  $('#premio').keyup(function(){
    var id=$(this).val();
    if (id.length > 3){ document.getElementById("reacondicionamiento").disabled = false;} 
    else { document.getElementById("reacondicionamiento").disabled = true;}
  });

  $('#reacondicionamiento').keyup(function(){
    var id=$(this).val();
    if (id.length > 3){ document.getElementById("valor").disabled = false;} 
    else { document.getElementById("valor").disabled = true;}
  });


  $('#pdudlv7').change(function(){
   if(document.getElementById("valor").disabled === ''){
    alert("Introduce el Valor del Vehiculo!")}
    else { cambiaValores();}
  });

  $('#reacondicionamiento').blur(function(){
    if(document.getElementById("valor").value !== ''){cambiaValores();}
  });

  $('#vin').blur(function(){
    var id=$(this).val();
    msjerrorvin = "El VIN no puede contener mas de 17 Caracteres";
    if (id.length > 17)
    { 
     $("#vin").addClass('is-invalid');
     document.getElementById('mensajevin').innerHTML="<span style='font-size: 0.7em; color: red;'>"+msjerrorvin+"</span>";
   } 
 });
  return false;
});
</script>