<?php

$html = '
<table border="0" style="width:100%">
<tbody>

<table align="justify">
<tr>
<td>
<b>1.0 OBJETIVO:</b>
<br>
Asegurar que los expedientes emitidos por las agencias de Grupo Fame para la compra de unidades usadas sean alineados de acuerdo a lo estipulado en este reglamento.

<br><br>

<b>2.0 ALCANCE:</b>
<br>
Aplicable a todas las agencias y áreas de Grupo FAME que participen en el Proceso de la Compra de Unidades Usadas.			

<br><br>

<b>3.0 POLÍTICA APLICABLE, NORMATIVIDAD, LEYES, REGLAMENTOS Y CIRCULARES:</b>
<br>
En toda compra de auto usado se deberá tener un expediente, el cual contendrá sin excepción los siguientes documentos:

<br><br>

&nbsp;&nbsp;&nbsp;&nbsp;<font color="#0A6ACF">1)</font>    <b>FACTURA DE COMPRA DE UNIDADES USADAS:</b>		
<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

<font color="#0A6ACF">1.1.</font>       <b>Para Personas Físicas sin Actividad Empresarial</b> (no facturan directamente)	


<p>i)      Tener el Formato Solicitud de CFDI (ANEXO A) firmado por el proveedor conteniendo los datos del mismo, en el cual autoriza la expedición del Comprobante Fiscal Digital (CFDI), por la venta del vehículo usado.
<br>ii)     Enviar Formato Solicitud de CFDI (ANEXO A) acompañada de los documentos que comprueben los datos del proveedor (nombre, RFC, régimen fiscal, dirección, etc.).
<br>iii)    Tener la factura original y el endoso por el último propietario regularizado (cambios de propietario regularizados ante la Secretaría de Finanzas).</p>

<p>a)    Solo deben aceptar facturas endosadas que no tengan el IVA desglosado en la factura que ampara la compra del vehículo o que tengan el IVA desglosado con RFC genérico (XAXX010101000).
<br>b) Con RFC sin homoclave (ROMM9002929_ _ _), o con RFC completo pero cuyo último propietario tribute en el régimen de Arrendamiento, Ingresos por Dividendos, ingresos por intereses o bien sueldos y salarios comprobando con la Guía de obligaciones (FL-GF-CMP-013). (Art. 9 Frac. IV de LIVA y Art. 20 de RLIVA). Esto se demuestra con la guía de obligaciones emitida por el SAT actualizada.
<br>"Nota: Todos los clientes aplicables al punto 1.1. Para personas Físicas sin Actividad Empresarial o Sin actividad Profesional sin RFC, enviar la Ratificación del RFC. FL-GF-CMP-019
 La Ratificación se obtiene de la página www.SAT.gob.mx."			</p>

 <p>iv)   Demostrar la propiedad de la unidad usada mediante la factura electrónica (CFD O CFDI) (FL-GF-CMP-001) en original y tener el archivo “XML” (FL-GF-CMP-002) debidamente validado ( FL-GF-CMP-017) (sello digital valido) y verificado (FL-GF-CMP-016 )(folio fiscal vigente) (a validación y verificación del XML se realiza en la página del SAT www.consulta.sat.gob.mx) para poder asegurar la autenticidad de la factura deberán contar con el escrito por el encargado de usados (ANEXO E) de que se confirmo la expedición de dicho documento con la entidad emisora. 			</p>

<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="#0A6ACF">1.2.</font>        <b>Para Personas Morales (S.A., S.A. De C.V., S. De R.L., SOCIEDADES COOPERATIVAS) y Personas Físicas con Actividad Empresarial</b> (Régimen General de Ley, Régimen de incorporación Fiscal, Régimen de Agricultura, Ganadería, Pesca y Silvicultura) (facturan directamente):
<br><br>i)             Tener la factura original de la empresa o persona física que nos está vendiendo (FL-GF-CMP-018) a nombre de la agencia que está comprando, con el IVA desglosado (Total de la unidad entre 1.16 igual a subtotal y este por el 16% igual a IVA en factura).  En caso de personas Físicas con Actividad Profesional (honorarios), pueden facturar sin el IVA desglosado sustentado con la Guía de Obligaciones Vigente emitida por el SAT en donde acredite su Régimen Fiscal).
<br>ii)            Tener <b><u>Archivo “XML”</u> (FL-GF-CMP-002)</b> debidamente validado (sello digital valido) y verificado (folio fiscal vigente). <b>La validación</b> (FL-GF-CMP-017)  <b>y  verificación</b>  (FL-GF-CMP-016) <b>del XML se realiza en la página del SAT</b> www.consulta.sat.gob.mx. <b>No se debe tomar una unidad usada con factura electrónica si no cuenta con el archivo XML  (FL-GF-CMP-002)  (el PDF de la factura debe ser una copia fiel del archivo XML).</b>
<br>Nota: Están prohibidas las compras entre agencias de usados (si las unidades usadas fueron compradas a Personas Físicas por la agencia que vende, ya que solo desglosará el IVA sobre la utilidad) así como a  PERSONAS MORALES DE NATURALEZA NO EMPRESARIAL (Asociaciones o Sociedades Civiles, Dependencias de Gobierno, Instituciones de Asistencia Pública y Organismos Descentralizados), estas personas morales al vender la unidad no causan el IVA, pero si nos obliga al momento de venderlo, causar el IVA sobre el total de la venta, lo que representa una pérdida operativa.						
 </p>

<p><b>2.    CARTA DE NO RETENCIÓN</b>
<br>
2.1.        Para Personas Físicas con Actividad Empresarial, cuando la compra de unidad es mayor a $227,400.00, deberán firmar la CARTA DE NO RETENCIÓN DE ISR PERSONAS FISICAS CON ACTIVIDAD EMPRESARIAL. (ANEXO L)
<br>
2.2.        Para Personas Físicas sin Actividad Empresarial, cuando la compra de unidad es mayor a $227,400.00, se deberá firmar la CARTA DE NO RETENCIÓN DE ISR PERSONAS FISICAS SIN ACTIVIDAD EMPRESARIAL (ANEXO K)

<br><br><br>
3.   SECUENCIA DE FACTURAS DEL VEHÍCULO (copia) (FL-GF-CMP-004), desde la agencia que vendió la unidad como nueva.

<br><br>
4.    IDENTIFICACIÓN OFICIAL del proveedor vigente (copia) (FL-GF-CMP-005)
<br>
i)              En caso de persona moral: Identificación de representante legal y Acta notarial Carta Poder Notarial.
<br>
5.    CURP (copia) ( FL-GF-CMP-006)
<br>
6.  COMPROBANTE DE DOMICILIO no mayor a tres meses (copia) (CFE, Telmex, recibo de agua, Estado de Cuenta Bancario). (FL-GF-CMP-007)
<br>
7.    BAJA DE PLACAS a nombre del último dueño. (FL-GF-CMP-008)
<br>
8.   TODAS LAS TENENCIAS DESDE QUE SE VENDIÓ COMO NUEVO (Copia) (FL-GF-CMP-009). Verificar que las tenencias no sean apócrifas.
<br>
9.    TARJETA DE CIRCULACIÓN DEL ÚLTIMO DUEÑO (copia)  (FL-GF-CMP-012)
<br>
10.  Carta responsiva al comprar (ANEXO B) donde el vendedor se hace responsable de la autenticidad de los documentos, que sirven de base para esta transacción (factura original, endosos, tenencias, baja, etc.), asegurando que el vehículo no tiene problemas legales, fiscales o de procedencia, en esta responsiva se deberá mencionar que el vendedor recibe el pago de la cuenta del comprador y deberá recabar también copia de la identificación oficial con firma, la cual debe coincidir con la de la responsiva.
<br>
11.  Contrato de compra venta (ANEXO C) debidamente firmado por el Gerente General de la Empresa y el proveedor.
<br>
12.  AVISO DE PRIVACIDAD (ANEXO D) firmado por parte del proveedor de la unidad usada (solo aplica para personas físicas).
<br>
13.  AVALÚO DE AUTOS USADOS (ANEXO F) firmado por el gerente de usados, Gerente de Ventas o Gerente de Sucursal (en su caso) y Gerente General.
<br>
13.1.      En el avalúo deberá constar el precio de compra de la guía EBC o de la guía Auto métrica (el autorizado por la marca), mencionando de que fecha es dicha guía y nunca deberá exceder el precio de compra de la misma. En caso contrario deberá estar autorizado por el Director Regional.
<br>
14.  EVALUACIÓN MECÁNICA (ANEXO H) firmado por el gerente de usados, Gerente de Ventas o Gerente de Sucursal (en su caso) y Gerente General.
<br>
15.  GUÍA AUTO MÉTRICA O EBC (copia) (FL-GF-CMP-010)  (anotar el mes al que corresponde la copia de la guía). En caso de que la compra rebase el monto máximo marcado en la Guía EBC, se deberá tener la autorización del Director de Marca para que se pueda realizar la compra.
<br>
16.  PÓLIZA DE RECEPCIÓN DE LA UNIDAD USADA (ANEXO J)
<br>
17.  CARTERA DE CUENTAS POR PAGAR (ANEXO G)
<br>
18.  FORMATO DE DACIÓN EN PAGO, CUANDO APLIQUE. (ANEXO M)
<br>
19.  IMPRESIÓN DE CONSULTA SIN REPORTE DE ROBO  (FL-GF-CMP-011)
<br>
<b>-       www2.repuve.gob.mx:/8080/ciudadania/</b>
<br>
<b>-       rapi.pgjdf.gob.mx</b>
<br>
<b>-       www.amda.mx</b>
<br>
20.  COPIA DEL CHEQUE DE PROVEEDOR (S).  (FL-GF-CMP-014)
<br>
21.  SOLICITUD DE CHEQUE (ANEXO I)
<br>
22.  ESTADO DE CUENTA (FL-GF-CMP-015)

<br><br><br>
<b>5.0.  REGISTROS DE CALIDAD</b>
<br>
<b>5.1. Registros de calidad Internos</b><br>								 			
</p>
</td>
</tr>
</table>

<table border="0">
<tr>
<td WIDTH="80">
<font color="#0A6ACF"><u>Anexo A
<br>
Anexo B
<br>
Anexo C
<br>
Anexo D
<br>
Anexo E
<br>
Anexo F
<br>
Anexo G
<br>
Anexo H
<br>
Anexo I
<br>
Anexo J
<br>
Anexo K
<br>
Anexo L
<br>
Anexo M
</u></font>
</td>
<td WIDTH="300">
Solicitud CFDI
<br>
Carta responsiva de compra venta
<br>
Contrato de compraventa
<br>
Aviso de privacidad integral
<br>
Autentificación de documentos
<br>
Avalúo rápido
<br>
Cartera de cuentas por pagar
<br>
Evaluación mecánica
<br>
Solicitud de cheque
<br>
Póliza de recepción de la unidad usada
<br>
Carta de no retención de ISR personas físicas sin actividad empresarial	
<br>
Carta de no retención de ISR personas físicas con actividad empresarial		
<br>
Formato de Dación en pago
</td>
</tr>
</table>

<p><b>5.2. Registros de calidad externos</b></p>

<table>
<tr>
<td WIDTH="90"><font color="#0A6ACF"><u>FL-GF-CMP-001
<br>
FL-GF-CMP-002
<br>
FL-GF-CMP-003
<br>
FL-GF-CMP-004
<br>
FL-GF-CMP-005
<br>
FL-GF-CMP-006
<br>
FL-GF-CMP-007
<br>
FL-GF-CMP-008
<br>
FL-GF-CMP-009
<br>
FL-GF-CMP-010
<br>
FL-GF-CMP-011
<br>
FL-GF-CMP-012
<br>
FL-GF-CMP-013
<br>
FL-GF-CMP-014
<br>
FL-GF-CMP-015
<br>
FL-GF-CMP-016
<br>
FL-GF-CMP-017
<br>
FL-GF-CMP-018
<br>
FL-GF-CMP-019
</u></font>
</td>
<td WIDTH="300">
CFDI factura persona física sin actividad empresarial
<br>
XML de CFDI	
<br>
Factura de proveedor
<br>
Secuencia de factura del auto
<br>
Identificación oficial
<br>
CURP
<br>
Comprobante de domicilio
<br>
Baja de placas		
<br>
Historial de tenencias		
<br>
Guía autométrica		
<br>
Consulta sin reporte de robo		
<br>
Tarjeta de circulación		
<br>
Cedula de identificación fiscal		
<br>
Copia de cheque de proveedor		
<br>
Estado de cuenta 		
<br>
Verificación fiscal de CFDI		
<br>
Validación fiscal de CFDI		
<br>
CFDI factura persona moral		
<br>
Ratificación de RFC		
</td>
</tr>

</table>

<p><b>-       Formatos de Compra Integral</b></p>

';



$html .=	'</tbody>
</table>			
';

tcpdf();
$obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
header('Content-type: application/pdf');
$obj_pdf->SetCreator(PDF_CREATOR);
$title = "Reglamento compra de unidades usadas";
$obj_pdf->SetTitle($title);
$obj_pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $title, PDF_HEADER_STRING);
$obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$obj_pdf->SetDefaultMonospacedFont('helvetica');
$obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$obj_pdf->SetFont('helvetica', '', 9);
$obj_pdf->setFontSubsetting(false);
$obj_pdf->AddPage();
ob_start();
    // podemos tener cualquier parte de la vista aquí como HTML, PHP, etc.
$content = ob_get_contents();
ob_end_clean();
$obj_pdf->writeHTML($content, true, false, true, false, '');
$obj_pdf->writeHTML($html, true, false, true, false, '');


$filename = 'reglamento';

$obj_pdf->Output($filename . '.pdf', 'D');




?>