<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends My_Controller {

	public function __construct(){
		parent::__construct();
		auth_check(); // check login auth

		$this->rbac->check_module_access();

		if($this->uri->segment(3) != '')
		$this->rbac->check_operation_access();
		$this->load->model('admin/dashboard_model', 'dashboard_model');

	}

	//--------------------------------------------------------------------------

	public function index(){
		$data['all_users'] = $this->dashboard_model->get_all_users();
		$data['all_brands'] = $this->dashboard_model->get_all_brands();
		$data['all_models'] = $this->dashboard_model->get_all_models();
		$data['all_business'] = $this->dashboard_model->get_all_business();
		$data['all_agencys'] = $this->dashboard_model->get_all_agencys();
		$data['all_guides'] = $this->dashboard_model->get_all_guides();
		$data['all_takes'] = $this->dashboard_model->get_all_takes();

		$data['active_users'] = $this->dashboard_model->get_active_users();
		$data['active_guides'] = $this->dashboard_model->get_active_guides();
		$data['active_takes'] = $this->dashboard_model->get_active_takes();

		$data['take_guides'] = $this->dashboard_model->get_take_guides();
		$data['take_checks'] = $this->dashboard_model->get_take_checks();

		$data['deactive_users'] = $this->dashboard_model->get_deactive_users();
		$data['deactive_guides'] = $this->dashboard_model->get_deactive_guides();
		$data['deactive_takes'] = $this->dashboard_model->get_deactive_takes();

		$data['libre_checks'] = $this->dashboard_model->get_libre_checks();
		$data['pendiente_checks'] = $this->dashboard_model->get_pendiente_checks();
		$data['proceso_checks'] = $this->dashboard_model->get_proceso_checks();
		$data['terminado_checks'] = $this->dashboard_model->get_terminado_checks();

		$data['title'] = 'Dashboard';
		$this->load->view('admin/includes/_header', $data);
    	$this->load->view('admin/dashboard/index', $data);
    	$this->load->view('admin/includes/_footer');
	}

	//--------------------------------------------------------------------------

	public function index_1(){
		$data['all_users'] = $this->dashboard_model->get_all_users();
		$data['active_users'] = $this->dashboard_model->get_active_users();
		$data['deactive_users'] = $this->dashboard_model->get_deactive_users();
		$data['title'] = 'Dashboard';
		$this->load->view('admin/includes/_header', $data);
    	$this->load->view('admin/dashboard/index', $data);
    	$this->load->view('admin/includes/_footer');
	}


	//--------------------------------------------------------------------------

	public function index_2(){
		$data['all_users'] = $this->dashboard_model->get_all_users();
		$data['active_users'] = $this->dashboard_model->get_active_users();
		$data['deactive_users'] = $this->dashboard_model->get_deactive_users();
		$data['title'] = 'Dashboard';
		$this->load->view('admin/includes/_header', $data);
    	$this->load->view('admin/dashboard/index2', $data);
    	$this->load->view('admin/includes/_footer');
	}


	//--------------------------------------------------------------------------

	public function index_3(){
		$data['title'] = 'Dashboard';
		$this->load->view('admin/includes/_header');
    	$this->load->view('admin/dashboard/index3');
    	$this->load->view('admin/includes/_footer');

	}


}
?>	