<!-- DataTables -->

<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/datatables/dataTables.bootstrap4.css"> 



<!-- Content Wrapper. Contains page content -->

<div class="content-wrapper">

  <section class="content">

    <!-- For Messages -->

    <?php $this->load->view('admin/includes/_messages.php') ?>

    <div class="card">

      <div class="card-header">

        <div class="d-inline-block">

          <h3 class="card-title"><i class="fa fa-list"></i>&nbsp; Parrilla Redes Sociales</h3>

        </div>

        <div class="d-inline-block float-right">

          <?php if($this->rbac->check_operation_permission('kk')): ?>

            <a href="<?= base_url('admin/guides/add'); ?>" class="btn btn-success"><i class="fa fa-plus"></i> <?= trans('add_new_guide') ?></a>

          <?php endif; ?>  

          <?php if($this->rbac->check_operation_permission('kk')): ?>  

            <a href="<?= base_url('admin/guides/create_guides_pdf') ?>" class="btn btn-success"><?= trans('export_as_pdf') ?></a>

          <?php endif; ?>

          <?php if($this->rbac->check_operation_permission('kk')): ?>  

            <a href="<?= base_url('admin/guides/export_csv') ?>" class="btn btn-success"><?= trans('export_as_csv') ?></a>

          <?php endif; ?>

        </div>

      </div>

    </div>



    <!--<div class="card">

      <div class="card-body table-responsive"> 

        <?php echo form_open("/",'id="model_search"') ?>

        <div class="row">

          <div class="col-md-4">

            <label><?= trans('model_type') ?></label><hr style="margin:5px 0px;" />

            <input checked="checked" onchange="user_filter()" name="user_search_type" value="" type="radio"  /> <?= trans('all') ?>&nbsp;&nbsp;&nbsp;

            <input onchange="model_filter()" name="model_search_type" value="1" type="radio"  /> <?= trans('active') ?>&nbsp;&nbsp;&nbsp;

            <input onchange="model_filter()" name="model_search_type" value="0" type="radio"  /> <?= trans('inactive') ?>

          </div>

          <div class="col-md-3">

            <label>Date From:</label><input name="user_search_from" type="text" class="form-control form-control-inline input-medium datepicker" id="" />

          </div>

          <div class="col-md-3"> 

            <label>Date To:</label><input name="user_search_to" type="text" class="form-control form-control-inline input-medium datepicker" id="" /> 

          </div>

          <div class="col-md-2"> 

            <button type="button" style="margin-top:20px;" onclick="user_filter()" class="btn btn-info">Submit</button>

            <a href="<?= base_url('admin/guides/advance_search'); ?>" class="btn btn-danger" style="margin-top:20px;">

              <i class="fa fa-repeat"></i>

            </a>

          </div>

        </div>

        <?php echo form_close(); ?>

      </div>

    </div>-->  





    <div class="card">

      <div class="card-body table-responsive">

  

<?php
 
  switch ($idagen) {
    case 1:
        echo 'NA';
        break;
    case 2:
        echo "No Tiene";
        break;
    case 3:
        echo '<iframe width="100%" height="1050" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vRLZUSgFPWUzUnWH7zdKalGRS9goNfJ0zFzA1EtcV9O61EzjNs4qCBbn_2xIK0M3UldFu6NbIKxq6z3/pubhtml?widget=true&amp;headers=false"></iframe>';
        break;
        case 4:
        echo '<iframe width="100%" height="1050" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vS4t0q1iCzHbsBlyD1LnZWi09tfvoWniDFEydpyKQx_j2JvZ-fNljZUUSQbXdZVRVLQs57wAVIh-uny/pubhtml?widget=true&amp;headers=false"></iframe>';
        break;
        case 5:
        echo '<iframe width="100%" height="1050" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQ_c8pICDPOf5yphFFP2AMnS12jgDgGXo2-f3xHIkM3UD6Wa2pQ3442gMF44yaZ9UEYcsw5OyuWoI_H/pubhtml?widget=true&amp;headers=false"></iframe>';
        break;
        case 6:
        echo "No Tiene";
        break;
        case 7:
        echo '<iframe width="100%" height="1050" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vRtoaHvGnd7rlbPRiX8QEOXBJdKSUuv6djxqMvBF7fyVUoEuQCdTw4AnZT7SdbIB7RiLokuaQsc8jM_/pubhtml?widget=true&amp;headers=false"></iframe>';
        break;
        case 8:
        echo '<iframe width="100%" height="1050" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vR_-lleH4i6fQYX2LFcyPfPb3fjXtJtdTprayyK4XbB8d2UacPOJnwKGPkvxwTpyOmO5Ze0IT2d-ib-/pubhtml?widget=true&amp;headers=false"></iframe>';
        break;
        case 9:
        echo '<iframe width="100%" height="1050" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vT-QyuCJJexzajrLA2n4d6SpY6IzjM8E-7sGQw7oSd_fDhJieUVCs9hFrh81z4cY57Nfids77NUU4qL/pubhtml?widget=true&amp;headers=false"></iframe>';
        break;
        case 10:
        echo '<iframe width="100%" height="1050" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vTdJgPqDO2dLmcYHHBV-UHpw_aaHtv3EA65nmhkqR9TIcriu1SfRIj-o9U27aUiy3YoEhBbtb71r-Hu/pubhtml?widget=true&amp;headers=false"></iframe>';
        break;
        case 11:
        echo '<iframe width="100%" height="1050" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQvm4_oOuQSSbrIYVVFUVCYktT4T9u2z7D_NgigNSt2EqFnWIAAMqGquoaf7FkajvFUg2cdU7zGl5In/pubhtml?widget=true&amp;headers=false"></iframe>';
        break;
        case 12:
        echo '<iframe width="100%" height="1050" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vSZps7GzliSua5Cdl5TA8pDxsFbwtvWC31zQ2xHkPCarGQeoaynK5cIAory931tFXhcRep53KUbzHz1/pubhtml?widget=true&amp;headers=false"></iframe>';
        break;
        case 13:
        echo '<iframe width="100%" height="1050" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQMWYtgLfLAsJz4n86pVwzRhKQikp_S5j8_B7uqyI61uf85H_pDpq-Brmj5vJGVr5fIkPcStqWkf_1U/pubhtml?widget=true&amp;headers=false"></iframe>';
        break;
        case 14:
        echo '<iframe width="100%" height="1050" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQ8wICyInAOmuwibK8Awzjg6XwnLtU73buSRnKWS2HJnklG8964fNx9EuMlIqhftKEiG1qJuwAKaWCY/pubhtml?widget=true&amp;headers=false"></iframe>';
        break;
        case 15:
        echo '<iframe width="100%" height="1050" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vSzWZMOvtIbCC0puXnAYLANd5jetecomBx5CyOcoPbpsH3ys-9j_eD9SydxWc2QwlrZnHErJnIzIA2L/pubhtml?widget=true&amp;headers=false"></iframe>';
        break;
        case 16:
        echo '<iframe width="100%" height="1050" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vTwlkIXIFRQZSaxql8POfzHTlwoDqummyOQyUV0aRt6YowEwCp9pX9W6slht59cPsbRuHx3ZS1nBFfd/pubhtml?widget=true&amp;headers=false"></iframe>';
        break;
        case 17:
        echo '<iframe width="100%" height="1050" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQZWwb03cDi7RAA6VxHa5C54ocgtE7nTqRPzYqOzFCqLbAvtRvgTD9vjpCjY2b5qtEQCRCdnVjIgMOW/pubhtml?widget=true&amp;headers=false"></iframe>';
        break;
        case 18:
        echo 'No Tiene';
        break;
        case 19:
        echo '<iframe width="100%" height="1050" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vRO3F9Z7sQZowr8AhzV2WmPlFcxTDEtFk4w1D0Nbn2egT9ILeQEEQHwiNurctHP-K-ROzbccCqfr7CQ/pubhtml?widget=true&amp;headers=false"></iframe>';
        break;
        case 20:
        echo '<iframe width="100%" height="1050" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vTYgXI-7pEnalnzOUAXLug0vNjQhnKHuXTXg-WcStBDlQh-bEM0-oa_4L8UDGODRRqYGevm7B7kKrHk/pubhtml?widget=true&amp;headers=false"></iframe>';
        break;
        case 21:
        echo '<iframe width="100%" height="1050" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vRxJEl99v7ky-N9zNTMufU-2YumFeQjZ33JuFxfuNg4jdyQOWbomjOn_CQCP5g9xxgmoBo2BjcA4xlB/pubhtml?widget=true&amp;headers=false"></iframe>';
        break;
        case 22:
        echo '<iframe width="100%" height="1050" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vTFUjwDY_uLBe6c0YLQif5ZAnvmhdQ7N4Zsvu7tytB4j7j7BFquBtR6DHd04V8UCzvLCSefMwOsNGDf/pubhtml?widget=true&amp;headers=false"></iframe>';
        break;
        case 23:
        echo '<iframe width="100%" height="1050" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vRUrxTNltrMFUxJgbirmHgH3wlA28ocg_Na9IIQILxJyO0Jml_Jv0JZgZuNhkM0U_x_xFKOeR8KMTOP/pubhtml?widget=true&amp;headers=false"></iframe>';
        break;
        case 24:
        echo '<iframe width="100%" height="1050" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vRUrxTNltrMFUxJgbirmHgH3wlA28ocg_Na9IIQILxJyO0Jml_Jv0JZgZuNhkM0U_x_xFKOeR8KMTOP/pubhtml?widget=true&amp;headers=false"></iframe>';
        break;
        case 25:
        echo '<iframe width="100%" height="1050" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vR97HruUa6pgREpoivEf7RV0EWUERnI_Obt2hlDrGIYrsiKrH4vGfEGKIFAVArge1nbWm9gzz3ctqJ9/pubhtml?widget=true&amp;headers=false"></iframe>';
        break;
        case 26:
        echo '<iframe width="100%" height="1050" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQdlVRb6UFHZccR5uCTzJ8xJwybRbr0oTS_iE3HHFWtH2p79Ef8ajWW1RR5MzGKXTn_JU1TcOAz6oEI/pubhtml?widget=true&amp;headers=false"></iframe>';
        break;
        case 27:
        echo '<iframe width="100%" height="1050" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQWvyf0VoVNnulpJtJKu3O7O-TMTCW8oFLL-JohH048AflfP6ncOQgQGLGwdKIfR9EkkiUmvRxRqpVG/pubhtml?widget=true&amp;headers=false"></iframe>';
        break;
        case 28:
        echo '<iframe width="100%" height="1050" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vS8kk4w2QXHhpsfOzwx8tWu_tM1uBA4TGNJqnnnCisMLaw1E1LCxmFs9nmwIS8Ib0uOXkp1mN_84KJr/pubhtml?widget=true&amp;headers=false"></iframe>';
        break;
        case 29:
        echo '<iframe width="100%" height="1050" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vS8kk4w2QXHhpsfOzwx8tWu_tM1uBA4TGNJqnnnCisMLaw1E1LCxmFs9nmwIS8Ib0uOXkp1mN_84KJr/pubhtml?widget=true&amp;headers=false"></iframe>';
        break;
        case 30:
        echo '<iframe width="100%" height="1050" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vRCDhnsY0nTDRQ0Zad1bpkUS07hf0FROe3sYlmBXKvtzrc1KoGoEivXNLEt2PNwkL5SSnw2EskkxXct/pubhtml?widget=true&amp;headers=false"></iframe>';
        break;
         case 31:
        echo 'No Tiene';
        break;
         case 32:
        echo '<iframe width="100%" height="1050" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vR00TXMy9nxpudA99ywukGiPW3qepzrDhSjLQ3yh2_d45ZGnpRQhLuCEnRe04qEtbP-JMZi9hVMHj78/pubhtml?widget=true&amp;headers=false"></iframe>';
        break;
         case 33:
        echo '<iframe width="100%" height="1050" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vTslaGGqSq4nSs4j-tzCCcEVVE9wccuoY9ZUsRaNV1WU3Rivgva7fPEV3uWSQifbi0foDI25NrmP1m0/pubhtml?widget=true&amp;headers=false"></iframe>';
        break;
         case 34:
        echo '<iframe width="100%" height="1050" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vShZVkq_n3eR_K7vbKHHeGvMlTNeDNKnmTYZTy_s0q9jL46UxRbYqlOY_zpVLIsacom9yuDX-2QMWbg/pubhtml?widget=true&amp;headers=false"></iframe>';
        break;
         case 35:
        echo 'No Tiene';
        break;
         case 36:
        echo 'No Tiene';
        break;
         case 37:
        echo '<iframe width="100%" height="1050" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vTr4cjYTiIof5lvsd4UwlagLtMotGNh_M9qsbPfN4OsonDdt1rgdTzf9qvlatHEMv25LXmWsUsiMYxd/pubhtml?widget=true&amp;headers=false"></iframe>';
        break;
         case 38:
        echo "No Tiene";
        break;
         case 39:
        echo 'No Tiene';
        break;
         case 40:
        echo "No Tiene";
        break;
         case 41:
        echo "No Tiene";
        break;
         case 42:
        echo "No Tiene";
        break;

        case 91:
        echo "No Tiene";
        break;
        case 92:
        echo "No Tiene";
        break;
        case 93:
        echo "No Tiene";
        break;
        case 94:
        echo "No Tiene";
        break;
        case 95:
        echo "No Tiene";
        break;
        case 96:
        echo "No Tiene";
        break;

        case 100:
        echo "No Aplica";
        break;




}
?>



       

 

        <!--<table id="na_datatable" class="table table-bordered table-striped" width="100%">

          <thead>

            <tr>

              <th>#<?= trans('id') ?></th>

              <th>Nombre</th>

              

              <th><?= trans('status') ?></th>

              <th width="100" class="text-right"><?= trans('action') ?></th>

            </tr>

          </thead>

        </table>-->

      </div>

    </div>














  </section>  

</div>



<!-- bootstrap datepicker -->

<!-- datepicker -->

<script src="<?= base_url() ?>assets/plugins/datepicker/bootstrap-datepicker.js"></script>

<script>

  $('.datepicker').datepicker({

    autoclose: true

  });

</script>

<!-- DataTables -->

<script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.js"></script>

<script src="<?= base_url() ?>assets/plugins/datatables/dataTables.bootstrap4.js"></script>



<script>

  //---------------------------------------------------

  var table = $('#na_datatable').DataTable( {

    "processing": true,

    "serverSide": false,

    "ajax": "<?=base_url('admin/leads/datatable_json')?>",

    "order": [[0,'desc']],

    "columnDefs": [

    { "targets": 0, "name": "id", 'searchable':true, 'orderable':true},

    { "targets": 1, "name": "name", 'searchable':true, 'orderable':true},

     

    { "targets": 2, "name": "Action", 'searchable':false, 'orderable':false,'width':'100px'}

    ]

  });

</script>



<script>

  //---------------------------------------------------

  function model_filter()

  {

    var _form = $("#model_search").serialize();

    $.ajax({

      data: _form,

      type: 'post',

      url: '<?php echo base_url();?>admin/guides/search',

      async: true,

      success: function(output){

        table.ajax.reload( null, false );

      }

    });

  }

</script>



<script type="text/javascript">

  $("body").on("change",".tgl_checkbox",function(){

    console.log('checked');

    $.post('<?=base_url("admin/guides/change_status")?>',

    {

      '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>',

      id : $(this).data('id'),

      status : $(this).is(':checked') == true?1:0

    },

    <?php if($this->rbac->check_operation_permission('add')): ?>

      function(data){$.notify("El Status cambió con éxito", "success");}

      <?php else: ?>

        function(data){$.notify("No Tienes Permisos", "warn");}

      <?php endif; ?>

      );

  });

</script>





