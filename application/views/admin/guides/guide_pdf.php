<?php
$fecha = $guide['created_at']; 

$month = date('M', strtotime($fecha));
$year = date('Y', strtotime($fecha));
$day = date('d', strtotime($fecha));

$partes = explode('-', $month); 
$_fecha = "$year.$month.$day";

$fmt = new NumberFormatter('en_US', NumberFormatter::CURRENCY);
$formatterES = new NumberFormatter("es-ES", NumberFormatter::SPELLOUT);

$izquierda = intval(floor($guide['pdlttfac2']));
$derecha = intval(($guide['pdlttfac2'] - floor($guide['pdlttfac2'])) * 100);

$foliouno = $guide['id'];
$foliocero = str_pad($foliouno, 9, "0", STR_PAD_LEFT);

 switch ($guide['tipoguias_id'])

    {

      case '1': 
      $tipoguia = "COMPRA";
      $tipopersona = "PERSONA FISICA O MORAL QUE FACTURA";
      break;

      case '2': 
      $tipoguia = "PROMEDIO";
      $tipopersona = "PERSONA FISICA O MORAL QUE FACTURA";
      break;

      case '3': 
      $tipoguia = "VENTA";
      $tipopersona = "PERSONA FISICA O MORAL QUE FACTURA";
      break;

      case '4': 
      $tipoguia = "COMPRA";
      $tipopersona = "PERSONA SIN OBLIGACION FISCAL";
      break;

      case '5': 
      $tipoguia = "PROMEDIO";
      $tipopersona = "PERSONA SIN OBLIGACION FISCAL";
      break;

      case '6': 
      $tipoguia = "VENTA";
      $tipopersona = "PERSONA SIN OBLIGACION FISCAL";
      break;
}

$html = '

<table  border="0">
	<tr bgcolor="gray" align="center">
		<td><h2><font color="black">VERSION AÑO 2021 (TOMA FINAL = '.$tipoguia.' GUIA AUTOMETRICA)</font></h2></td><br>
	</tr>
	<tr align="center">
		<td><h4><font color="black">GUIA RAPIDA PARA LA TOMA DE UN USADO DE '.$tipopersona.'</font></h4></td><br>
	</tr>
	<tr align="center">
		<td><h3><font color="black">Porcentaje de Utilidad '.$guide['pdudlv7'].'%</font></h3></td><br>
	</tr>
	<tr align="center">
		<td> </td><br>
	</tr>
	<tr align="right">
		<td>'.$guide['agciudad'].', ' .$guide['agestado'].' a: '.$day.' de ' .$month.' del ' .$year.'</td>
	</tr>
	<tr align="right">
		<td>Folio: '.$guide['folio'].'-'.$foliocero.'</td>
	</tr>
	<tr align="right">';
	if($guide['status']==1){$html.= '<td> <i style="color:#15A615;">Autorizada!</i> </td>';}else{$html.= '<td> <i style="color:#CC0000;">Pendiente!</i> </td>';}

	$html .= '
	</tr>
</table>
<br>
<br>
<br>
<br>
<table border="0" class="table table-bordered">
  <tbody>

    <tr>
      <td width="60%" align="right" >   </td>
      <td  width="2%"> </td>
      <td> </td>
    </tr>
    <tr>
      <td width="60%" align="right" > Marca: </td>
      <td  width="2%"> </td>
      <td>'.$guide['brname'].'</td>
    </tr>
    <tr>
		<td width="60%" align="right"> Version: </td> 
		<td  width="2%"> </td>
		<td>'.$guide['modelotxt'].' - '.$guide['versiontxt'].'</td> 
	</tr>
	<tr>
		<td width="60%" align="right"> Año Modelo: </td> 
		<td  width="2%"> </td>
		<td>'.$guide['anname'].'</td> 
	</tr>
	<tr>
		<td width="60%" align="right"> Compra Guia Autometrica: </td> 
		<td  width="2%"> </td>
		<td>'.$fmt->formatCurrency($guide['compraautometrica'], "USD") .'</td>  
	</tr>
	<tr>
		<td width="60%" align="right"> Venta Guia Autometrica: </td> 
		<td  width="2%"> </td>
		<td>'.$fmt->formatCurrency($guide['ventaautometrica'], "USD") .'</td>  
	</tr>
	<tr>
		<td width="60%" align="right"> Añadir por Equipamiento Guia Autometrica: </td> 
		<td  width="2%"> </td>
		<td>'.$fmt->formatCurrency($guide['equipamiento'], "USD") .'</td>  
	</tr>
	<tr>
		<td width="60%" align="right"> Premio o Castigo por Kilometraje: </td> 
		<td  width="2%"> </td>
		<td>'. $fmt->formatCurrency($guide['premio'], "USD") .'</td>  
	</tr>
	<tr>
		<td width="60%" align="right"> Reacondicionamiento: </td> 
		<td  width="2%"> </td>
		<td>'.$fmt->formatCurrency($guide['reacondicionamiento'], "USD") .'</td>  
	</tr>
	<tr>
		<td width="60%" align="right"> Centrese en el Valor del Coche: </td> 
		<td  width="2%"> </td>
		<td>'.$fmt->formatCurrency($guide['valor'], "USD") .'</td>  
	</tr>
	<tr>
		<td width="60%" align="right"> Estimar Precio de Venta Publico sin Reacondicionamiento: </td> 
		<td  width="2%"> </td>
		<td>'.$fmt->formatCurrency($guide['pdvapsr1'], "USD") .'</td>  
	</tr>
	<tr>
		<td width="60%" align="right"> IVA: </td> 
		<td  width="2%"> </td>
		<td>16%</td>  
	</tr>
	<tr>
		<td width="60%" align="right"> Factor de Utilidad de la Venta: </td> 
		<td  width="2%"> </td>
		<td>'.$guide['pdudlv7'].'%</td>  
	</tr>
	<tr>
		<td width="60%" align="right"> Factor de Ganancia sobre el Costo: </td> 
		<td  width="2%"> </td>
		<td>'.$guide['fdume13'].'%</td>  
	</tr>
	<tr>
		<td width="60%" align="right"> Precio de la toma tope final al cliente con IVA: </td> 
		<td  width="2%"> </td>
		<td>'.$fmt->formatCurrency($guide['pdlttfac2'], "USD") .'</td>  
	</tr>
	<tr>
		<td width="60%" align="right"> Costo Real(Precio de la toma sin IVA): </td> 
		<td  width="2%"> </td>
		<td>'.$fmt->formatCurrency($guide['crpdltsi3'], "USD") .'</td>  
	</tr>
	<tr>
		<td width="60%" align="right"> Precio de Venta a publico sin IVA: </td> 
		<td  width="2%"> </td>
		<td>'.$fmt->formatCurrency($guide['pdvapsi4'], "USD") .'</td>  
	</tr>
	<tr>
		<td width="60%" align="right"> Precio de Venta a publico ocn IVA incluyendo Reacondicionamiento: </td> 
		<td  width="2%"> </td>
		<td>'.$fmt->formatCurrency($guide['pdvapciyr5'], "USD") .'</td>  
	</tr>
	<tr>
		<td width="60%" align="right"> Utilidad: </td> 
		<td  width="2%"> </td>
		<td>'.$fmt->formatCurrency($guide['udlv6'], "USD") .'</td>  
	</tr>
	<tr>
		<td width="60%" align="right"> Porcentaje de Utilidad de la Venta: </td> 
		<td  width="2%"> </td>
		<td>'.$fmt->formatCurrency($guide['pdudlv7'], "USD") .'%</td>  
	</tr>
		<tr>
		<td width="60%" align="right">  </td> 
		<td  width="2%"> </td>
		<td></td>  
	</tr>

  </tbody>
</table>
<br>
<br>
<br>
<br>
<br>
<br>
<table border="0" align="center" class="table table-bordered">
  <tbody>  
        <tr>
			<td>
				____________________________________<br>
				<font color="#0A6ACF">GERENTE GENERAL</font>
			</td>
			<td>
				____________________________________<br>
				<font color="#0A6ACF">ENCARGADO DE USADOS</font>
			</td>
		</tr>
  </tbody>
</table>';


	

tcpdf();
$obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
header('Content-type: application/pdf');
$obj_pdf->SetCreator(PDF_CREATOR);
$title = "Guía - ".$guide['folio'];
$obj_pdf->SetTitle($title);
$obj_pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $title, PDF_HEADER_STRING);
$obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$obj_pdf->SetDefaultMonospacedFont('helvetica');
$obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$obj_pdf->SetFont('helvetica', '', 9);
$obj_pdf->setFontSubsetting(false);
$obj_pdf->AddPage();
ob_start();
    // podemos tener cualquier parte de la vista aquí como HTML, PHP, etc.
$content = ob_get_contents();
ob_end_clean();
$obj_pdf->writeHTML($content, true, false, true, false, '');
$obj_pdf->writeHTML($html, true, false, true, false, '');


$filename = 'Guia - '.$guide['folio'];

$obj_pdf->Output($filename . '.pdf', 'D');




?>