<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Authorizations extends MY_Controller 
{
	public function __construct(){

		parent::__construct();
		$this->load->helper('form');
		$this->load->helper('functions');
		auth_check(); // comprobar la autenticación de inicio de sesión
		$this->rbac->check_module_access();

		$this->load->model('admin/authorization_model', 'authorization_model');
		$this->load->model('admin/Activity_model', 'activity_model');
	}

	//-----------------------------------------------------------
	//  Vista Principal
	//-------------------------------------------------------
	public function index(){
		$data['title'] = 'Lista de Autorizaciones';
		$this->load->view('admin/includes/_header');
		$this->load->view('admin/autoris/autori_list');
		$this->load->view('admin/includes/_footer');
	}

		//-----------------------------------------------------------
	//  Vista Principal de checklist pendientes
	//-------------------------------------------------------
	public function index_pendientes(){
		$data['title'] = 'Lista de Autorizaciones';
		$this->load->view('admin/includes/_header');
		$this->load->view('admin/autoris/autori_list_pen');
		$this->load->view('admin/includes/_footer');
	}

	//-----------------------------------------------------------
	//  Vista Principal de checklist en proceso
	//-------------------------------------------------------
	public function index_proceso(){
		$data['title'] = 'Lista de Autorizaciones';
		$this->load->view('admin/includes/_header');
		$this->load->view('admin/autoris/autori_list_pro');
		$this->load->view('admin/includes/_footer');
	}

	//-----------------------------------------------------------
	//  Vista Principal de checklist en proceso
	//-------------------------------------------------------
	public function index_terminados(){
		$data['title'] = 'Lista de Autorizaciones';
		$this->load->view('admin/includes/_header');
		$this->load->view('admin/autoris/autori_list_ter');
		$this->load->view('admin/includes/_footer');
	}

	//-----------------------------------------------------------
	//  Vista Principal de checklist en proceso
	//-------------------------------------------------------
	public function index_libres(){
		$data['title'] = 'Lista de Autorizaciones';
		$this->load->view('admin/includes/_header');
		$this->load->view('admin/autoris/autori_list_lib');
		$this->load->view('admin/includes/_footer');
	}
	
	//-----------------------------------------------------------
	//  Tabala para la vista principal
	//-------------------------------------------------------
	public function datatable_json(){				   					   
		$records['data'] = $this->authorization_model->get_all_autoris();
		$data = array();

		$i=0;
		foreach ($records['data']   as $row) 
		{  
			$status = $row['status'];

			switch($status)
			{
				case '0': 
				$status = 'libre';
				$tipo= 'info';
				break; 
				case '1': 
				$status = 'Pendiente';
				$tipo= 'danger';
				break; 
				case '2': 
				$status = 'En Proceso';
				$tipo= 'warning';
				break; 
				case '3': 
				$status = 'libre';
				$tipo= 'success';
				break; 
			}

			$data[]= array(
				++$i,
				$row['agname'],
				$row['folio'],
				date("d-m-Y", strtotime($row['created_at'])),
				'<span class=" badge bg-'.$tipo.'">'.$status.'!</span> ',
				'<a title="View" class="view btn btn-sm btn-info" href="'.base_url('admin/autoris/view/'.$row['id']).'"> <i class="fa fa-eye"></i></a>
				<a title="Edit" class="update btn btn-sm btn-warning" href="'.base_url('admin/autoris/edit/'.$row['id']).'"> <i class="fa fa-pencil-square-o"></i></a>
				<a title="Delete" class="delete btn btn-sm btn-danger" href='.base_url("admin/autoris/delete/".$row['id']).' title="Delete" onclick="return confirm(\'Do you want to delete ?\')"> <i class="fa fa-trash-o"></i></a>'	 		
			);
		}
		$records['data']=$data;
		echo json_encode($records);						   
	}

	//-----------------------------------------------------------
	//  Tabala para la vista principal
	//-------------------------------------------------------
	public function datatable_json_pendientes(){				   					   
		$records['data'] = $this->authorization_model->get_all_checks_pendientes();
		$data = array();

		$i=0;
		foreach ($records['data']   as $row) 
		{  
			$status = $row['status'];

			switch($status)
			{
				case '0': 
				$status = 'libre';
				$tipo= 'info';
				break; 
				case '1': 
				$status = 'Pendiente';
				$tipo= 'danger';
				break; 
				case '2': 
				$status = 'En Proceso';
				$tipo= 'warning';
				break; 
				case '3': 
				$status = 'libre';
				$tipo= 'success';
				break; 
			}

			$data[]= array(
				++$i,
				$row['agname'],
				$row['folio'],
				date("d-m-Y", strtotime($row['created_at'])),
				'<span class=" badge bg-'.$tipo.'">'.$status.'!</span> ',
				'<a title="View" class="view btn btn-sm btn-info" href="'.base_url('admin/autoris/view/'.$row['id']).'"> <i class="fa fa-eye"></i></a>
				<a title="Edit" class="update btn btn-sm btn-warning" href="'.base_url('admin/autoris/edit/'.$row['id']).'"> <i class="fa fa-pencil-square-o"></i></a>
				<a title="Delete" class="delete btn btn-sm btn-danger" href='.base_url("admin/autoris/delete/".$row['id']).' title="Delete" onclick="return confirm(\'Do you want to delete ?\')"> <i class="fa fa-trash-o"></i></a>'	 		
			);
		}
		$records['data']=$data;
		echo json_encode($records);						   
	}

	//-----------------------------------------------------------
	//  Tabala para la vista principal
	//-------------------------------------------------------
	public function datatable_json_proceso(){				   					   
		$records['data'] = $this->authorization_model->get_all_checks_proceso();
		$data = array();

		$i=0;
		foreach ($records['data']   as $row) 
		{  
			$status = $row['status'];

			switch($status)
			{
				case '0': 
				$status = 'libre';
				$tipo= 'info';
				break; 
				case '1': 
				$status = 'Pendiente';
				$tipo= 'danger';
				break; 
				case '2': 
				$status = 'En Proceso';
				$tipo= 'warning';
				break; 
				case '3': 
				$status = 'libre';
				$tipo= 'success';
				break; 
			}

			$data[]= array(
				++$i,
				$row['agname'],
				$row['folio'],
				date("d-m-Y", strtotime($row['created_at'])),
				'<span class=" badge bg-'.$tipo.'">'.$status.'!</span> ',
				'<a title="View" class="view btn btn-sm btn-info" href="'.base_url('admin/autoris/view/'.$row['id']).'"> <i class="fa fa-eye"></i></a>
				<a title="Edit" class="update btn btn-sm btn-warning" href="'.base_url('admin/autoris/edit/'.$row['id']).'"> <i class="fa fa-pencil-square-o"></i></a>
				<a title="Delete" class="delete btn btn-sm btn-danger" href='.base_url("admin/autoris/delete/".$row['id']).' title="Delete" onclick="return confirm(\'Do you want to delete ?\')"> <i class="fa fa-trash-o"></i></a>'	 		
			);
		}
		$records['data']=$data;
		echo json_encode($records);						   
	}

	//-----------------------------------------------------------
	//  Tabala para la vista principal
	//-------------------------------------------------------
	public function datatable_json_terminados(){				   					   
		$records['data'] = $this->authorization_model->get_all_checks_terminados();
		$data = array();

		$i=0;
		foreach ($records['data']   as $row) 
		{  
			$status = $row['status'];

			switch($status)
			{
				case '0': 
				$status = 'libre';
				$tipo= 'info';
				break; 
				case '1': 
				$status = 'Pendiente';
				$tipo= 'danger';
				break; 
				case '2': 
				$status = 'En Proceso';
				$tipo= 'warning';
				break; 
				case '3': 
				$status = 'libre';
				$tipo= 'success';
				break; 
			}

			$data[]= array(
				++$i,
				$row['agname'],
				$row['folio'],
				date("d-m-Y", strtotime($row['created_at'])),
				'<span class=" badge bg-'.$tipo.'">'.$status.'!</span> ',
				'<a title="View" class="view btn btn-sm btn-info" href="'.base_url('admin/autoris/view/'.$row['id']).'"> <i class="fa fa-eye"></i></a>
				<a title="Edit" class="update btn btn-sm btn-warning" href="'.base_url('admin/autoris/edit/'.$row['id']).'"> <i class="fa fa-pencil-square-o"></i></a>
				<a title="Delete" class="delete btn btn-sm btn-danger" href='.base_url("admin/autoris/delete/".$row['id']).' title="Delete" onclick="return confirm(\'Do you want to delete ?\')"> <i class="fa fa-trash-o"></i></a>'	 		
			);
		}
		$records['data']=$data;
		echo json_encode($records);						   
	}

	//-----------------------------------------------------------
	//  Tabala para la vista principal
	//-------------------------------------------------------
	public function datatable_json_libres(){				   					   
		$records['data'] = $this->authorization_model->get_all_checks_libres();
		$data = array();

		$i=0;
		foreach ($records['data']   as $row) 
		{  
			$status = $row['status'];

			switch($status)
			{
				case '0': 
				$status = 'libre';
				$tipo= 'info';
				break; 
				case '1': 
				$status = 'Pendiente';
				$tipo= 'danger';
				break; 
				case '2': 
				$status = 'En Proceso';
				$tipo= 'warning';
				break; 
				case '3': 
				$status = 'libre';
				$tipo= 'success';
				break; 
			}

			$data[]= array(
				++$i,
				$row['agname'],
				$row['folio'],
				date("d-m-Y", strtotime($row['created_at'])),
				'<span class=" badge bg-'.$tipo.'">'.$status.'!</span> ',
				'<a title="View" class="view btn btn-sm btn-info" href="'.base_url('admin/autoris/view/'.$row['id']).'"> <i class="fa fa-eye"></i></a>
				<a title="Edit" class="update btn btn-sm btn-warning" href="'.base_url('admin/autoris/edit/'.$row['id']).'"> <i class="fa fa-pencil-square-o"></i></a>
				<a title="Delete" class="delete btn btn-sm btn-danger" href='.base_url("admin/autoris/delete/".$row['id']).' title="Delete" onclick="return confirm(\'Do you want to delete ?\')"> <i class="fa fa-trash-o"></i></a>'	 		
			);
		}
		$records['data']=$data;
		echo json_encode($records);						   
	}


	//-----------------------------------------------------------
	//  Cambiar Status
	//-------------------------------------------------------
	function change_status()
	{   
		$this->rbac->check_operation_access('change_status'); //comprobar el permiso de operación
		$this->authorization_model->change_status();
	}

	//-----------------------------------------------------------
	//  Agregar Check
	//-------------------------------------------------------
	public function add(){
		
		$this->rbac->check_operation_access(); //comprobar el permiso de operación

		$data['takes']=$this->authorization_model->get_all_takes_guides_actives();


		if($this->input->post('submit')){

			 //$this->form_validation->set_rules('tomasel', trans('take'), 'trim|required');
			 $this->form_validation->set_rules('observaciones', 'Observaciones', 'trim|required');

			if ($this->form_validation->run() == FALSE) {
				$data = array(
					'errors' => validation_errors()
				);
				$this->session->set_flashdata('form_data', $this->input->post());
				$this->session->set_flashdata('errors', $data['errors']);
				redirect(base_url('admin/authorizations/add'),'refresh');
			}
			else{
				$data = array(
				'checklist_id' => 2,
				'observaciones' => $this->input->post('observaciones', true),
				'created_at' => date('Y-m-d h:m:s'),
				);

				$path="assets/img/guides/";

				if(!empty($_FILES['image']['name']))
				{
					$result = $this->functions->file_insert($path, 'image', 'image', '9097152');
					if($result['status'] == 1){
						$data['reporte'] = $path.$result['msg'];
					}
					else{
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/authorizations/'), 'refresh');
					}
				}				 

				if(!empty($_FILES['cgenerales']['name']))
				{
					$resultcg = $this->functions->file_insert($path, 'image', 'image', '9097152');
					if($resultcg['status'] == 1){
						$data['condiciones'] = $path.$resultcg['msg'];
					}
					else{
						$this->session->set_flashdata('error', $resultcg['msg']);
						redirect(base_url('admin/authorizations/'), 'refresh');
					}
				}				 

				$data = $this->security->xss_clean($data);
				//$datag=array('statuscheck' => 1);
				$result = $this->authorization_model->add_authorization($data);

				if($result){
					// Registro de actividades
					$this->activity_model->add_log(28);
					//$this->authorization_model->update($datag, $idg, 'seminuevostomados');
					$this->session->set_flashdata('success', 'La Autorizacion se ha agregado correctamente!');
					redirect(base_url('admin/authorizations'));
				}
			}
		}
		else{
			$this->load->view('admin/includes/_header');
			$this->load->view('admin/autoris/autori_add', $data);
			$this->load->view('admin/includes/_footer');
		}
		
	}

	//-----------------------------------------------------------
	//  Editar check
	//-------------------------------------------------------
	public function edit($id = 0){

		$this->rbac->check_operation_access(); //comprobar el permiso de operación

		$data['takes']=$this->authorization_model->get_all_takes_guides();

		if($this->input->post('submit')){

			$this->form_validation->set_rules('id', trans('take'), 'trim|required');
			 $this->form_validation->set_rules('observaciones', 'Observaciones', 'trim|required');
			 
			if ($this->form_validation->run() == FALSE) {
				$data = array(
					'errors' => validation_errors()
				);
				$this->session->set_flashdata('errors', $data['errors']);
				redirect(base_url('admin/autoris/edit/'.$id),'refresh');
			}
			else{

				

				if(!empty($this->input->post('autorizaciontoma'))){$autorizaciontoma = $this->input->post('autorizaciontoma', true);}
				else{$autorizaciontoma = 0;}

				$data = array(
					'solicitudcfdi' => $solicitudcfdi,
					'cambioderol' => $cambioderol,
					'cedulafiscal' => $cedulafiscal,
					'cfdisinactividad' => $cfdisinactividad,
					'xml' => $xml,
					'rfc' => $rfc,
					'validacioncfdi' => $validacioncfdi,
					'verificacioncfdi' => $verificacioncfdi,
					'autenticaciondoctos' => $autenticaciondoctos,
					'reportetransunion' => $reportetransunion,
					'noretencion' => $noretencion,
					'secuenciafacturas' => $secuenciafacturas,
					'identificacionoficial' => $identificacionoficial,
					'curp' => $curp,
					'comprobante' => $comprobante,
					'bajaplacas' => $bajaplacas,
					'tenencias' => $tenencias,
					'tarjetacirculacion' => $tarjetacirculacion,
					'responsivacompraventa' => $responsivacompraventa,
					'contratocompraventa' => $contratocompraventa,
					'avisoprivacidad' => $avisoprivacidad,
					'checklistrevision' => $checklistrevision,
					'evaluacionmecanica' => $evaluacionmecanica,
					'guiaautometrica' => $guiaautometrica,
					'polizaderecepcion' => $polizaderecepcion,
					'cuentasporpagar' => $cuentasporpagar,
					'formatodedacion' => $formatodedacion,
					'reportederobo' => $reportederobo,
					'chequedeproveedor' => $chequedeproveedor,
					'solicitudcheque' => $solicitudcheque,
					'edocuenta' => $edocuenta,
					'calculoretencion' => $calculoretencion,
					'autorizaciontoma' => $autorizaciontoma,
					'status' => $this->input->post('status'),
					'updated_at' => date('Y-m-d h:m:s'),
				);

				$datac=array(                
					'comentarios' => $this->input->post('comentarios', true),
					'checklist_id' => $this->input->post('id', true),
					'users_id' => $this->session->userdata('admin_id'),
					'created_at' => date('Y-m-d h:m:s')
				);

				$data = $this->security->xss_clean($data);
				$datac = $this->security->xss_clean($datac);

				$result = $this->authorization_model->edit_check($data, $id);

				if($result){
					// Registro de actividades 
					$this->activity_model->add_log(29);
					$this->authorization_model->insert($datac, 'comentarios_has_checklist');
					$this->session->set_flashdata('success', 'La Marca ha sido actualizado con éxito!');
					redirect(base_url('admin/checks'));
				}
			}
		}
		else{
			$data['checks'] = $this->authorization_model->get_autori_by_id($id);
			
			$this->load->view('admin/includes/_header');
			$this->load->view('admin/autoris/autori_edit', $data);
			$this->load->view('admin/includes/_footer');
		}
	}

	//-----------------------------------------------------------
	//  Ver check
	//-------------------------------------------------------
	public function view($id = 0){

		$this->rbac->check_operation_access(); //comprobar el permiso de operación
        
        $data['brands']=$this->authorization_model->get_all_simple_brands();
        $data['takes']=$this->authorization_model->get_all_takes_guides();
        $data['comments'] = $this->authorization_model->get_autori_comments_by_id($id);
		 

		if($this->input->post('submit')){

			$this->form_validation->set_rules('folio', trans('folio'), 'trim|required');

			if ($this->form_validation->run() == FALSE) {
				$data = array(
					'errors' => validation_errors()
				);
				$this->session->set_flashdata('errors', $data['errors']);
				redirect(base_url('admin/guides/guide_edit/'.$id),'refresh');
			}
			else{
				$data = array(
					'valor' => $this->input->post('valor', true),
					'reacondicionamiento' => $this->input->post('reacondicionamiento', true),
					'premio' => $this->input->post('premio', true),
					'equipamiento' => $this->input->post('equipamiento', true),
					'compraautometrica' => $this->input->post('compraautometrica', true),
					'ventaautometrica' => $this->input->post('ventaautometrica', true),
					'km' => $this->input->post('km', true),
					'vin' => $this->input->post('vin', true),
					'folio' => $this->input->post('folio', true),
					'pdvapsr1' => $this->input->post('pdvapsr1a', true),
					'pdlttfac2' => $this->input->post('pdlttfac2a', true),
					'crpdltsi3' => $this->input->post('crpdltsi3a', true),
					'pdvapsi4' => $this->input->post('pdvapsi4a', true),
					'pdvapciyr5' => $this->input->post('pdvapciyr5a', true),
					'udlv6' => $this->input->post('udlv6a', true),
					'pdudlv7' => $this->input->post('pdudlv7', true),
					'pmdvap8' => $this->input->post('pmdvap8a', true),
					'cccr9' => $this->input->post('cccr9a', true),
					'pdvap10' => $this->input->post('pdvap10a', true),
					'ucidlu11' => $this->input->post('ucidlu11a', true),
					'idlu12' => $this->input->post('idlu12a', true),
					'fdume13' => $this->input->post('fdume13a', true),
					'models_id' => $this->input->post('modelo', true),
					'users_id' => $this->session->userdata('admin_id'),
					'tipoguias_id' => $this->input->post('tipoguia', true),
					'agencys_id' => $this->input->post('agencia', true),
					'status' => $this->input->post('status'),
					'updated_at' => date('Y-m-d h:m:s'),
				);
				$data = $this->security->xss_clean($data);
				$result = $this->guide_model->edit_model($data, $id);
				if($result){
					// Registro de actividades 
					$this->activity_model->add_log(26);
					$this->session->set_flashdata('success', 'La Guía ha sido actualizada con éxito!');
					redirect(base_url('admin/checks'));
				}
			}
		}
		else{
			$data['guide'] = $this->authorization_model->get_guide_by_id_business_view($id);
			$data['checks'] = $this->authorization_model->get_autori_by_id($id);
			$data['comments'] = $this->authorization_model->get_autori_comments_by_id($id);

			$data['all_users'] =  $this->authorization_model->get_all_simple_users();
			
			$this->load->view('admin/includes/_header');
			$this->load->view('admin/autoris/autori_view', $data);
			$this->load->view('admin/includes/_footer');
		}
	}

	//-----------------------------------------------------------
	//  Eliminar Check
	//-------------------------------------------------------
	public function delete($id = 0)
	{
		$this->rbac->check_operation_access(); // comprobar el permiso de operación
		$this->db->delete('anios', array('id' => $id));
		// Registro de actividades  
		$this->activity_model->add_log(30);
		$this->session->set_flashdata('success', 'La Marca se ha eliminado correctamente!');
		redirect(base_url('admin/checks'));
	}
	
	//-----------------------------------------------------------
	//  Exportar checks a CSV
	//-------------------------------------------------------
	public function export_csv(){ 
	   // nombre del archivo
		$filename = 'checks_'.date('Y-m-d').'.csv'; 
		header("Content-Description: File Transfer"); 
		header("Content-Disposition: attachment; filename=$filename"); 
		header("Content-Type: application/csv; ");

	   // obtener datos
		$model_data = $this->authorization_model->get_checks_for_csv();

	   // crear archivo
		$file = fopen('php://output', 'w');
		$header = array("ID", "Name", "Created Date"); 
		fputcsv($file, $header);
		foreach ($model_data as $key=>$line){ 
			fputcsv($file,$line); 
		}
		fclose($file); 
		exit; 
	}


	//---------------------------------------------------------------
	//  Exportar Checklist a PDF
	//-------------------------------------------------------
	public function create_checks_pdf()
	{
		$this->load->helper('pdf_tcpdf_helper'); // loaded pdf helper
		$data['all_checks'] = $this->authorization_model->get_all_simple_checks();
		$this->load->view('admin/autoris/autori_tcpdf', $data);

	}

	//---------------------------------------------------------------
	//  Exportar Checklist Pendientes a PDF
	//-------------------------------------------------------
	public function create_checks_pdf_pendientes()
	{
		$this->load->helper('pdf_tcpdf_helper'); // loaded pdf helper
		$data['all_checks'] = $this->authorization_model->get_all_simple_checks_pendientes();
		$this->load->view('admin/autoris/autori_tcpdf', $data);

	}

	//---------------------------------------------------------------
	//  Exportar Checklist Proceso a PDF
	//-------------------------------------------------------
	public function create_checks_pdf_proceso()
	{
		$this->load->helper('pdf_tcpdf_helper'); // loaded pdf helper
		$data['all_checks'] = $this->authorization_model->get_all_simple_checks_proceso();
		$this->load->view('admin/autoris/autori_tcpdf', $data);

	}

	//---------------------------------------------------------------
	//  Exportar Checklist Terminados a PDF
	//-------------------------------------------------------
	public function create_checks_pdf_terminados()
	{
		$this->load->helper('pdf_tcpdf_helper'); // loaded pdf helper
		$data['all_checks'] = $this->authorization_model->get_all_simple_checks_terminados();
		$this->load->view('admin/autoris/autori_tcpdf', $data);

	}

	//---------------------------------------------------------------
	//  Exportar Checklist Pendientes a PDF
	//-------------------------------------------------------
	public function create_checks_pdf_libres()
	{
		$this->load->helper('pdf_tcpdf_helper'); // loaded pdf helper
		$data['all_checks'] = $this->authorization_model->get_all_simple_checks_libres();
		$this->load->view('admin/autoris/autori_tcpdf', $data);

	}



	//---------------------------------------------------
	// Busqueda Avanzada
	//-------------------------------------------------------
	public function advance_search(){

		$this->session->unset_modeldata('model_search_type');
		$this->session->unset_modeldata('model_search_from');
		$this->session->unset_modeldata('model_search_to');

		$data['title'] = 'Advanced Search with Datatable';

		$this->load->view('admin/includes/_header');
		$this->load->view('admin/autoris/advance_search', $data);
		$this->load->view('admin/includes/_footer');
	}

	//---------------------------------------------------------------
	//  Funcion de Busquedas
	//-------------------------------------------------------
	function search(){

		$this->session->set_modeldata('model_search_type',$this->input->post('model_search_type'));
		$this->session->set_modeldata('model_search_from',$this->input->post('model_search_from'));
		$this->session->set_modeldata('model_search_to',$this->input->post('model_search_to'));
	}




}


?>