<?php

$fmt = new NumberFormatter('en_US', NumberFormatter::CURRENCY);
$formatterES = new NumberFormatter("es-ES", NumberFormatter::SPELLOUT);

$izquierda = intval(floor($take['pdlttfac2']));
$derecha = intval(($take['pdlttfac2'] - floor($take['pdlttfac2'])) * 100);

$subtable = '<table border="0" cellspacing="6" cellpadding="4"><tr><td>a</td><td>b</td></tr><tr><td>c</td><td>d</td></tr></table>';

$html = '
<table border="0" style="width:100%">
	 
	<tbody>';
	$html = ' 
		<table bgcolor="black" border="0">
			<tr align="center">
				<td><h1><font color="white">CARTA DE NO RETENCIÓN DE ISR <br> PERSONAS FÍSICAS CON ACTIVIDAD EMPRESARIAL</font></h1></td><br>
			</tr>
		</table>

		<table border="0">
			<tr>
				<td> </td>
			</tr>
		</table>

		<table border="0">
			<tr>
				<td>Yo <font color="#0A6ACF">'.$take['nombre'].'</font> con domicilio en: Calle: '.$take['calle'].', No. exterior: '.$take['numext'].',  No. interior: '.$take['numint'].', Colonia: '.$take['colonia'].', Ciudad: '.$take['ciudad'].', Municipio: '.$take['municipio'].', Estado: '.$take['estado'].', CP: '.$take['cp'].', RFC: '.$take['rfc'].'<br>
				</td>
			</tr>
		</table>

		<table border="1">
		<tr>
			<td>
				De acuerdo al Articulo 126, cuarto parrafo, de la ley del ISR, solicito a: <font color="#0A6ACF">'.$take['bursocial'].'</font> que no me sea retenido el 20% por concepto de pago de ISR. Manifiesto a Ustedes que efectuaré un pago provisional menor al 20% de la retención, por la enajenación del vehículo, realizada a: <font color="#0A6ACF">'.$take['bursocial'].'</font>
			</td>
		</tr>
		</table>

		<table border="0">
			<tr>
				<td></td>
			</tr>
		</table>

		<table border="0">
			<tr>
				<td>
					<b>Datos del vehículo:</b><br>
				</td>
			</tr>
		</table>

		<table border="0">
			<tr>
				<td>
					Marca:<br>
					Tipo:<br>
					Serie:<br>
					Placa:<br>
					Factura:<br>
				</td>
				<td>
					<font color="#0A6ACF">'.$take['brname'].'</font><br>
					<font color="#0A6ACF">'.$take['versiontxt'].'</font><br>
					<font color="#0A6ACF">'.$take['nserie'].'</font><br>
					<font color="#0A6ACF">'.$take['placasbaja'].'</font><br>
					<font color="#0A6ACF">'.$take['facturavehi'].'</font><br>
				</td>
				<td>
					Modelo:<br>
					Color:<br>
					Motor:<br>
					Interior:<br>
					Fecha de adquisición:
				</td>
				<td>
					<font color="#0A6ACF">'.$take['modelotxt'].'</font><br>
					<font color="#0A6ACF">'.$take['colorext'].'</font><br>
					<font color="#0A6ACF">'.$take['nmotor'].'</font><br>
					<font color="#0A6ACF">'.$take['colorint'].'</font><br>
					<font color="#0A6ACF">'.$take['fechafac'].'</font>
				</td>				
			</tr>
			<tr>
			    <td colspan="2">
					Expedida por: <font color="#0A6ACF">'.$take['expedida'].'</font>
				</td> 
			</tr>
		</table>

		<table border="0" align="right">
		    <tr>
				<td></td>
			</tr>
			<tr>
				<td>
					Costo de adquisición(valor en factura): <font color="#0A6ACF">'.$fmt->formatCurrency($take['costoadquisicion'], "USD").'</font> <br>
					Monto de la Transacción(Compra): <font color="#0A6ACF">'.$fmt->formatCurrency($take['pdlttfac2'], "USD") .'</font> <br>
					<font color="#0A6ACF">('.strtoupper($formatterES->format($izquierda)).' PESOS ' .$derecha.'/100 M.N.)</font>
				</td>
				<td>
					
					
				</td>
			</tr>
		</table>

		<table border="0">
			<tr>
				<td></td>
				<td width="300">
					<u><font color="#0A6ACF">'.$take['valorletra'].'</font></u> <br>
				</td>
				<td width="50"></td>
			</tr>
		</table>

		<table border="0">
			<tr>
				<td>
					<b>*Anexo Guía de Obligaciones Vigente</b>
				</td>
			</tr>
		</table>

	<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>

	<table align="center">
		<tr>
			<td>
				____________________________________<br>
				<font color="#0A6ACF">'.$take['nombre'].'</font>
			</td>
			<td>
				____________________________________<br>
				<font color="#0A6ACF">'.$take['bursocial'].'</font>
			</td>
		</tr>

	';
$html .= '</tbody> </table>';

tcpdf();
$obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
header('Content-type: application/pdf');
$obj_pdf->SetCreator(PDF_CREATOR);
$title = "Carta de no retención de ISR";
$obj_pdf->SetTitle($title);
$obj_pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $title, PDF_HEADER_STRING);
$obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$obj_pdf->SetDefaultMonospacedFont('helvetica');
$obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$obj_pdf->SetFont('helvetica', '', 9);
$obj_pdf->setFontSubsetting(false);
$obj_pdf->AddPage();
ob_start();
    // podemos tener cualquier parte de la vista aquí como HTML, PHP, etc.
$content = ob_get_contents();
ob_end_clean();
$obj_pdf->writeHTML($content, true, false, true, false, '');
$obj_pdf->writeHTML($html, true, false, true, false, '');


$filename = 'carta_no_retencion';

$obj_pdf->Output($filename . '.pdf', 'D');




?>