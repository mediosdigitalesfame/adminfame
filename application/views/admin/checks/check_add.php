  <!-- daterange picker -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/daterangepicker/daterangepicker-bs3.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/colorpicker/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/select2/select2.min.css">

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">

      <div class="card card-default">
        <div class="card-header">
          <div class="d-inline-block">
            <h3 class="card-title"> <i class="fa fa-check"></i>
             <?= trans('add_new_check') ?> - Revisión</h3>
           </div>
           <div class="d-inline-block float-right">
            <a href="<?= base_url('admin/checks'); ?>" class="btn btn-success"><i class="fa fa-list"></i>  <?= trans('checks_list') ?></a>
          </div>
        </div>
        <div class="card-body">

         <!-- For Messages -->
         <?php $this->load->view('admin/includes/_messages.php') ?>
 
         <?php echo form_open_multipart(base_url('admin/checks/add'), 'class="form-horizontal"','id="formch"','name="formch"');  ?> 

         <?php  $options = 0  ?>


         <div class="form-group">
          <div class="row">



            <div class="col-lg-3">
              <div class="input-group">
                <label for="tomasel" class="col-md-12 control-label"><?= trans('select_take') ?>*</label>
                <div class="col-md-12">
                  <select id="tomasel" name="tomasel" class="form-control select2"  onChange="myNewFunction(this);" >
                    <option value=""><?= trans('select_take') ?></option>
                    <?php foreach($takes as $take): ?>
                      <option value="<?= $take['id']; ?>"><?= $take['folio']; ?> </option> <?php // $idregimen = $take['regimenes_id'];   ?>
                    <?php endforeach; ?>
                  </select>
                </div>
              </div>
            </div>

            <input type="hidden" id="folioenuso" name="folioenuso" value="0"><br>

            <script>
              function myNewFunction(sel) {
                var numero = sel.options[sel.selectedIndex].text;
                var s3 = document.getElementById("folioenuso");
                 s3.value = numero;}
            </script>

          </div>
        </div>

        <?php if($this->rbac->check_operation_permission('stats')): ?>

          <div class="form-group">
            <div class="row">
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">Solicitud CFDI</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="solicitudcfdi" id="solicitudcfdi" type="checkbox" value="1"  > <label for="solicitudcfdi"></label>
                  </div>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">Cambio de Rol</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="cambioderol" id="cambioderol" type="checkbox" value="1"  > <label for="cambioderol"></label>
                  </div>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">Cédula de Id. Fiscal</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="cedulafiscal" id="cedulafiscal" type="checkbox" value="1"  > <label for="cedulafiscal"></label>
                  </div>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">CFDI Sin Act. Emp.</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="cfdisinactividad" id="cfdisinactividad" type="checkbox" value="1"  > <label for="cfdisinactividad"></label>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="row">
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">XML de DFDI</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="xml" id="xml" type="checkbox" value="1"  > <label for="xml"></label>
                  </div>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">Ratificación de RFC</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="rfc" id="rfc" type="checkbox" value="1"  > <label for="rfc"></label>
                  </div>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">Validación Fiscal de CFDI</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="validacioncfdi" id="validacioncfdi" type="checkbox" value="1"  > <label for="validacioncfdi"></label>
                  </div>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">Verifiación Fiscal de CFDI</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="verificacioncfdi" id="verificacioncfdi" type="checkbox" value="1"  > <label for="verificacioncfdi"></label>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="row">
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">Autenticacion de Doctos.</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="autenticaciondoctos" id="autenticaciondoctos" type="checkbox" value="1"  > <label for="autenticaciondoctos"></label>
                  </div>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">Reporte de Transunion</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="reportetransunion" id="reportetransunion" type="checkbox" value="1"  > <label for="reportetransunion"></label>
                  </div>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">Cer. No Retencion de ISR</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="noretencion" id="noretencion" type="checkbox" value="1"  > <label for="noretencion"></label>
                  </div>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">Facturas del Auto</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="secuenciafacturas" id="secuenciafacturas" type="checkbox" value="1"  > <label for="secuenciafacturas"></label>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="row">
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">Identificación</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="identificacionoficial" id="identificacionoficial" type="checkbox" value="1"  > <label for="identificacionoficial"></label>
                  </div>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">CURP</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="curp" id="curp" type="checkbox" value="1"  > <label for="curp"></label>
                  </div>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">Comprobante Domicilio</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="comprobante" id="comprobante" type="checkbox" value="1"  > <label for="comprobante"></label>
                  </div>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">Baja de Placas</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="bajaplacas" id="bajaplacas" type="checkbox" value="1"  > <label for="bajaplacas"></label>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="row">
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">Historial de Tenencias</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="tenencias" id="tenencias" type="checkbox" value="1"  > <label for="tenencias"></label>
                  </div>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">Tarjeta de Circulación</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="tarjetacirculacion" id="tarjetacirculacion" type="checkbox" value="1"  > <label for="tarjetacirculacion"></label>
                  </div>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">Carta Res. Compraventa</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="responsivacompraventa" id="responsivacompraventa" type="checkbox" value="1"  > <label for="responsivacompraventa"></label>
                  </div>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">Contrato Compraventa</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="contratocompraventa" id="contratocompraventa" type="checkbox" value="1"  > <label for="contratocompraventa"></label>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="row">
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">Aviso de Privasidad</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="avisoprivacidad" id="avisoprivacidad" type="checkbox" value="1"  > <label for="avisoprivacidad"></label>
                  </div>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">Check de Rev. y Cert.</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="checklistrevision" id="checklistrevision" type="checkbox" value="1"  > <label for="checklistrevision"></label>
                  </div>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">Evaluación Mecánica</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="evaluacionmecanica" id="evaluacionmecanica" type="checkbox" value="1"  > <label for="evaluacionmecanica"></label>
                  </div>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">Guía Autométrica</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="guiaautometrica" id="guiaautometrica" type="checkbox" value="1"  > <label for="guiaautometrica"></label>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="row">
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">Póliza de Recepción</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="polizaderecepcion" id="polizaderecepcion" type="checkbox" value="1"  > <label for="polizaderecepcion"></label>
                  </div>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">Cuentas pos Pagar</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="cuentasporpagar" id="cuentasporpagar" type="checkbox" value="1"  > <label for="cuentasporpagar"></label>
                  </div>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">Formato Dación en Pago</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="formatodedacion" id="formatodedacion" type="checkbox" value="1"  > <label for="formatodedacion"></label>
                  </div>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">Consulta R. de Robo</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="reportederobo" id="reportederobo" type="checkbox" value="1"  > <label for="reportederobo"></label>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="row">
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">Cheque de Proveedor</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="chequedeproveedor" id="chequedeproveedor" type="checkbox" value="1"  > <label for="chequedeproveedor"></label>
                  </div>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">Solicitud de Cheque</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="solicitudcheque" id="solicitudcheque" type="checkbox" value="1"  > <label for="solicitudcheque"></label>
                  </div>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">Estado de Cuenta</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="edocuenta" id="edocuenta" type="checkbox" value="1"  > <label for="edocuenta"></label>
                  </div>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">Calculo de Retención</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="calculoretencion" id="calculoretencion" type="checkbox" value="1"  > <label for="calculoretencion"></label>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="row">

              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">Autorizacion P/Toma</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="autorizaciontoma" id="autorizaciontoma" type="checkbox" value="1"  > <label for="autorizaciontoma"></label>
                  </div>
                </div>
              </div>

              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">Avaluo Rápido</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="avaluorapido" id="avaluorapido" type="checkbox" value="1"  > <label for="avaluorapido"></label>
                  </div>
                </div>
              </div>

              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">Contrato de Adhesión</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="contratoadhesion" id="contratoadhesion" type="checkbox" value="1"  > <label for="contratoadhesion"></label>
                  </div>
                </div>
              </div>


            </div>
          </div>

        <?php endif; ?> 

      </div>
    </div>


    <?php if($this->rbac->check_operation_permission('docs')): ?>


      <div class="card card-default">
       <div class="card-header">
        <div class="d-inline-block">
          <h3 class="card-title"> <i class="fa fa-plus"></i>
          Documentación</h3>
        </div>

      </div>

      <div class="card-body">


        <div class="form-group">
          <div class="row">
            <div class="col-lg-3">
              <div class="input-group">
                <label for="gg" class="col-md-12 control-label">Solicitud CFDI</label>
                <div class="col-md-12">
                  <input type="file" name="solicitudcfdidocs" accept=".png, .jpg, .jpeg, .gif, .svg, .pdf">
                  <p><small class="text-success"><?= trans('allowed_types') ?>:  .pdf</small></p>
                </div>
              </div>
            </div>
            <div class="col-lg-3">
              <div class="input-group">
                <label for="gg" class="col-md-12 control-label">Cambio de Rol</label>
                <div class="col-md-12">
                  <input type="file" name="cambioderoldocs" accept=".png, .jpg, .jpeg, .gif, .svg, .pdf">
                  <p><small class="text-success"><?= trans('allowed_types') ?>: pdf</small></p>
                </div>
              </div>
            </div>
            <div class="col-lg-3">
              <div class="input-group">
                <label for="gg" class="col-md-12 control-label">Cédula de Id. Fiscal</label>
                <div class="col-md-12">
                  <input type="file" name="cedulafiscaldocs" accept=".png, .jpg, .jpeg, .gif, .svg, .pdf">
                  <p><small class="text-success"><?= trans('allowed_types') ?>: pdf</small></p>
                </div>
              </div>
            </div>
            <div class="col-lg-3">
              <div class="input-group">
                <label for="gg" class="col-md-12 control-label">CFDI Sin Act. Emp.</label>
                <div class="col-md-12">
                  <input type="file" name="cfdisinactividaddocs" accept=".png, .jpg, .jpeg, .gif, .svg, .pdf">
                  <p><small class="text-success"><?= trans('allowed_types') ?>: pdf</small></p>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="form-group">
          <div class="row">
            <div class="col-lg-3">
              <div class="input-group">
                <label for="gg" class="col-md-12 control-label">XML de DFDI</label>
                <div class="col-md-12">
                  <input type="file" name="xmldocs" accept=".png, .jpg, .jpeg, .gif, .svg, .pdf">
                  <p><small class="text-success"><?= trans('allowed_types') ?>: pdf</small></p>
                </div>
              </div>
            </div>
            <div class="col-lg-3">
              <div class="input-group">
                <label for="gg" class="col-md-12 control-label">Ratificación de RFC</label>
                <div class="col-md-12">
                  <input type="file" name="rfcdocs" accept=".png, .jpg, .jpeg, .gif, .svg, .pdf">
                  <p><small class="text-success"><?= trans('allowed_types') ?>: pdf</small></p>
                </div>
              </div>
            </div>
            <div class="col-lg-3">
              <div class="input-group">
                <label for="gg" class="col-md-12 control-label">Validación Fiscal de CFDI</label>
                <div class="col-md-12">
                  <input type="file" name="validacioncfdidocs" accept=".png, .jpg, .jpeg, .gif, .svg, .pdf">
                  <p><small class="text-success"><?= trans('allowed_types') ?>: pdf</small></p>
                </div>
              </div>
            </div>
            <div class="col-lg-3">
              <div class="input-group">
                <label for="gg" class="col-md-12 control-label">Verifiación Fiscal de CFDI</label>
                <div class="col-md-12">
                  <input type="file" name="verificacioncfdidocs" accept=".png, .jpg, .jpeg, .gif, .svg, .pdf">
                  <p><small class="text-success"><?= trans('allowed_types') ?>: pdf</small></p>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="form-group">
          <div class="row">
            <div class="col-lg-3">
              <div class="input-group">
                <label for="gg" class="col-md-12 control-label">Autenticacion de Doctos.</label>
                <div class="col-md-12">
                  <input type="file" name="autenticaciondoctosdocs" accept=".png, .jpg, .jpeg, .gif, .svg, .pdf">
                  <p><small class="text-success"><?= trans('allowed_types') ?>: pdf</small></p>
                </div>
              </div>
            </div>
            <div class="col-lg-3">
              <div class="input-group">
                <label for="gg" class="col-md-12 control-label">Reporte de Transunion</label>
                <div class="col-md-12">
                  <input type="file" name="reportetransuniondocs" accept=".png, .jpg, .jpeg, .gif, .svg, .pdf">
                  <p><small class="text-success"><?= trans('allowed_types') ?>: pdf</small></p>
                </div>
              </div>
            </div>
            <div class="col-lg-3">
              <div class="input-group">
                <label for="gg" class="col-md-12 control-label">Cer. No Retencion de ISR</label>
                <div class="col-md-12">
                  <input type="file" name="noretenciondocs" accept=".png, .jpg, .jpeg, .gif, .svg, .pdf">
                  <p><small class="text-success"><?= trans('allowed_types') ?>: pdf</small></p>
                </div>
              </div>
            </div>
            <div class="col-lg-3">
              <div class="input-group">
                <label for="gg" class="col-md-12 control-label">Facturas del Auto</label>
                <div class="col-md-12">
                  <input type="file" name="secuenciafacturasdocs" accept=".png, .jpg, .jpeg, .gif, .svg, .pdf">
                  <p><small class="text-success"><?= trans('allowed_types') ?>: pdf</small></p>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="form-group">
          <div class="row">
            <div class="col-lg-3">
              <div class="input-group">
                <label for="gg" class="col-md-12 control-label">Identificación</label>
                <div class="col-md-12">
                  <input type="file" name="identificacionoficialdocs" accept=".png, .jpg, .jpeg, .gif, .svg, .pdf">
                  <p><small class="text-success"><?= trans('allowed_types') ?>: pdf</small></p>
                </div>
              </div>
            </div>
            <div class="col-lg-3">
              <div class="input-group">
                <label for="gg" class="col-md-12 control-label">CURP</label>
                <div class="col-md-12">
                  <input type="file" name="curpdocs" accept=".png, .jpg, .jpeg, .gif, .svg, .pdf">
                  <p><small class="text-success"><?= trans('allowed_types') ?>: pdf</small></p>
                </div>
              </div>
            </div>
            <div class="col-lg-3">
              <div class="input-group">
                <label for="gg" class="col-md-12 control-label">Comprobante Domicilio</label>
                <div class="col-md-12">
                  <input type="file" name="comprobantedocs" accept=".png, .jpg, .jpeg, .gif, .svg, .pdf">
                  <p><small class="text-success"><?= trans('allowed_types') ?>: pdf</small></p>
                </div>
              </div>
            </div>
            <div class="col-lg-3">
              <div class="input-group">
                <label for="gg" class="col-md-12 control-label">Baja de Placas</label>
                <div class="col-md-12">
                  <input type="file" name="bajaplacasdocs" accept=".png, .jpg, .jpeg, .gif, .svg, .pdf">
                  <p><small class="text-success"><?= trans('allowed_types') ?>: pdf</small></p>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="form-group">
          <div class="row">
            <div class="col-lg-3">
              <div class="input-group">
                <label for="gg" class="col-md-12 control-label">Historial de Tenencias</label>
                <div class="col-md-12">
                  <input type="file" name="tenenciasdocs" accept=".png, .jpg, .jpeg, .gif, .svg, .pdf">
                  <p><small class="text-success"><?= trans('allowed_types') ?>: pdf</small></p>
                </div>
              </div>
            </div>
            <div class="col-lg-3">
              <div class="input-group">
                <label for="gg" class="col-md-12 control-label">Tarjeta de Circulación</label>
                <div class="col-md-12">
                  <input type="file" name="tarjetacirculaciondocs" accept=".png, .jpg, .jpeg, .gif, .svg, .pdf">
                  <p><small class="text-success"><?= trans('allowed_types') ?>: pdf</small></p>
                </div>
              </div>
            </div>
            <div class="col-lg-3">
              <div class="input-group">
                <label for="gg" class="col-md-12 control-label">Carta Res. Compraventa</label>
                <div class="col-md-12">
                  <input type="file" name="responsivacompraventadocs" accept=".png, .jpg, .jpeg, .gif, .svg, .pdf">
                  <p><small class="text-success"><?= trans('allowed_types') ?>: pdf</small></p>
                </div>
              </div>
            </div>
            <div class="col-lg-3">
              <div class="input-group">
                <label for="gg" class="col-md-12 control-label">Contrato Compraventa</label>
                <div class="col-md-12">
                  <input type="file" name="contratocompraventadocs" accept=".png, .jpg, .jpeg, .gif, .svg, .pdf">
                  <p><small class="text-success"><?= trans('allowed_types') ?>: pdf</small></p>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="form-group">
          <div class="row">
            <div class="col-lg-3">
              <div class="input-group">
                <label for="gg" class="col-md-12 control-label">Aviso de Privasidad</label>
                <div class="col-md-12">
                  <input type="file" name="avisoprivacidaddocs" accept=".png, .jpg, .jpeg, .gif, .svg, .pdf">
                  <p><small class="text-success"><?= trans('allowed_types') ?>: pdf</small></p>
                </div>
              </div>
            </div>
            <div class="col-lg-3">
              <div class="input-group">
                <label for="gg" class="col-md-12 control-label">Check de Rev. y Cert.</label>
                <div class="col-md-12">
                  <input type="file" name="checklistrevisiondocs" accept=".png, .jpg, .jpeg, .gif, .svg, .pdf">
                  <p><small class="text-success"><?= trans('allowed_types') ?>: pdf</small></p>
                </div>
              </div>
            </div>
            <div class="col-lg-3">
              <div class="input-group">
                <label for="gg" class="col-md-12 control-label">Evaluación Mecánica</label>
                <div class="col-md-12">
                  <input type="file" name="evaluacionmecanicadocs" accept=".png, .jpg, .jpeg, .gif, .svg, .pdf">
                  <p><small class="text-success"><?= trans('allowed_types') ?>: pdf</small></p>
                </div>
              </div>
            </div>
            <div class="col-lg-3">
              <div class="input-group">
                <label for="gg" class="col-md-12 control-label">Guía Autométrica</label>
                <div class="col-md-12">
                  <input type="file" name="guiaautometricadocs" accept=".png, .jpg, .jpeg, .gif, .svg, .pdf">
                  <p><small class="text-success"><?= trans('allowed_types') ?>: pdf</small></p>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="form-group">
          <div class="row">
            <div class="col-lg-3">
              <div class="input-group">
                <label for="gg" class="col-md-12 control-label">Póliza de Recepción</label>
                <div class="col-md-12">
                  <input type="file" name="polizaderecepciondocs" accept=".png, .jpg, .jpeg, .gif, .svg, .pdf">
                  <p><small class="text-success"><?= trans('allowed_types') ?>: pdf</small></p>
                </div>
              </div>
            </div>
            <div class="col-lg-3">
              <div class="input-group">
                <label for="gg" class="col-md-12 control-label">Cuentas pos Pagar</label>
                <div class="col-md-12">
                  <input type="file" name="cuentasporpagardocs" accept=".png, .jpg, .jpeg, .gif, .svg, .pdf">
                  <p><small class="text-success"><?= trans('allowed_types') ?>: pdf</small></p>
                </div>
              </div>
            </div>
            <div class="col-lg-3">
              <div class="input-group">
                <label for="gg" class="col-md-12 control-label">Formato Dación en Pago</label>
                <div class="col-md-12">
                  <input type="file" name="formatodedaciondocs" accept=".png, .jpg, .jpeg, .gif, .svg, .pdf">
                  <p><small class="text-success"><?= trans('allowed_types') ?>: pdf</small></p>
                </div>
              </div>
            </div>
            <div class="col-lg-3">
              <div class="input-group">
                <label for="gg" class="col-md-12 control-label">Consulta R. de Robo</label>
                <div class="col-md-12">
                  <input type="file" name="reportederobodocs" accept=".png, .jpg, .jpeg, .gif, .svg, .pdf">
                  <p><small class="text-success"><?= trans('allowed_types') ?>: pdf</small></p>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="form-group">
          <div class="row">
            <div class="col-lg-3">
              <div class="input-group">
                <label for="gg" class="col-md-12 control-label">Cheque de Proveedor</label>
                <div class="col-md-12">
                  <input type="file" name="chequedeproveedordocs" accept=".png, .jpg, .jpeg, .gif, .svg, .pdf">
                  <p><small class="text-success"><?= trans('allowed_types') ?>: pdf</small></p>
                </div>
              </div>
            </div>
            <div class="col-lg-3">
              <div class="input-group">
                <label for="gg" class="col-md-12 control-label">Solicitud de Cheque</label>
                <div class="col-md-12">
                  <input type="file" name="solicitudchequedocs" accept=".png, .jpg, .jpeg, .gif, .svg, .pdf">
                  <p><small class="text-success"><?= trans('allowed_types') ?>: pdf</small></p>
                </div>
              </div>
            </div>
            <div class="col-lg-3">
              <div class="input-group">
                <label for="gg" class="col-md-12 control-label">Estado de Cuenta</label>
                <div class="col-md-12">
                  <input type="file" name="edocuentadocs" accept=".png, .jpg, .jpeg, .gif, .svg, .pdf">
                  <p><small class="text-success"><?= trans('allowed_types') ?>: pdf</small></p>
                </div>
              </div>
            </div>
            <div class="col-lg-3">
              <div class="input-group">
                <label for="gg" class="col-md-12 control-label">Calculo de Retención</label>
                <div class="col-md-12">
                  <input type="file" name="calculoretenciondocs" accept=".png, .jpg, .jpeg, .gif, .svg, .pdf">
                  <p><small class="text-success"><?= trans('allowed_types') ?>: pdf</small></p>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="form-group">
          <div class="row">

            <div class="col-lg-3">
              <div class="input-group">
                <label for="gg" class="col-md-12 control-label">Autorizacion P/Toma</label>
                <div class="col-md-12">
                  <input type="file" name="autorizaciontomadocs" accept=".png, .jpg, .jpeg, .gif, .svg, .pdf">
                  <p><small class="text-success"><?= trans('allowed_types') ?>: pdf</small></p>
                </div>
              </div>
            </div>

            <div class="col-lg-3">
              <div class="input-group">
                <label for="gg" class="col-md-12 control-label">Avaluo Rápido</label>
                <div class="col-md-12">
                  <input type="file" name="avaluorapidodocs" accept=".png, .jpg, .jpeg, .gif, .svg, .pdf">
                  <p><small class="text-success"><?= trans('allowed_types') ?>: pdf</small></p>
                </div>
              </div>
            </div>

            <div class="col-lg-3">
              <div class="input-group">
                <label for="gg" class="col-md-12 control-label">Contrato de Adhesión</label>
                <div class="col-md-12">
                  <input type="file" name="contratoadhesiondocs" accept=".png, .jpg, .jpeg, .gif, .svg, .pdf">
                  <p><small class="text-success"><?= trans('allowed_types') ?>: pdf</small></p>
                </div>
              </div>
            </div>

          </div>
        </div>

        <div class="form-group">
          <div class="col-md-12">
            <input type="submit" name="submit" value="<?= trans('add_check') ?>" class="btn btn-primary pull-right">
          </div>
        </div>
        <?php echo form_close( ); ?>
      </div>
      <!-- /.box-body -->
    </div>

  <?php endif; ?> 



</section> 
</div>



<!-- Select2 -->
<script src="<?= base_url() ?>assets/plugins/select2/select2.full.min.js"></script>
<!-- InputMask -->
<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src="<?= base_url() ?>assets/plugins/daterangepicker/daterangepicker.js"></script>
<!-- bootstrap color picker -->
<script src="<?= base_url() ?>assets/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="<?= base_url() ?>assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- Page script -->
<!-- iCheck 1.0.1 -->
<script src="<?= base_url() ?>assets/plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker         : true,
      timePickerIncrement: 30,
      format             : 'MM/DD/YYYY h:mm A'
    })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
    {
      ranges   : {
        'Today'       : [moment(), moment()],
        'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month'  : [moment().startOf('month'), moment().endOf('month')],
        'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      },
      startDate: moment().subtract(29, 'days'),
      endDate  : moment()
    },
    function (start, end) {
      $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
    }
    )

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>

<script>
  $("#forms").addClass('active');
  $("#advanced").addClass('active');
</script>  
