<?php

date_default_timezone_set('America/Mexico_City');
   
$fecha = $take['fechacontrato']; 

$month = date('M', strtotime($fecha));
$year = date('Y', strtotime($fecha));
$day = date('d', strtotime($fecha));

$partes = explode('-', $month); 
$_fecha = "$year.$month.$day";


$fmt = new NumberFormatter('en_US', NumberFormatter::CURRENCY);
$formatterES = new NumberFormatter("es-ES", NumberFormatter::SPELLOUT);

$izquierda = intval(floor($take['pdlttfac2']));
$derecha = intval(($take['pdlttfac2'] - floor($take['pdlttfac2'])) * 100);

$html = '
<table border="0" style="width:100%">
	<tbody>

	<table border="0">
			<tr align="right">
				<td>'.$take['agciudad'].', ' .$take['agestado'].' a: '.$day.' de ' .$month.' del ' .$year.'</td>
			</tr>
			<tr align="right">
				<td></td>
			</tr>
		</table>

		<br>
		<br>
		<br>

	 <table border="0">
			<tr align="center">
				<td><h1><>CONTRATO DE COMPRAVENTA DE AUTOMOVIL:</h1></td><br>
			</tr>
			<tr align="center">
				<td></td><br>
			</tr>
		</table>
 <br>
 <br>
 <br>
		<label align="justify">QUE CELEBRAN POR UNA PARTE COMO COMPRADOR: <font color="#0A6ACF">'.$take['bursocial'].'</font>, CON DOMICILIO EN: <font color="#0A6ACF">'.$take['bucalle'].', '.$take['buint'].', COL.'.$take['bucolonia'].', C.P.'.$take['bucp'].', </font><font color="#0A6ACF">'.$take['buciudad'].'</font>. Y COMO VENDEDOR '.$take['nombrescfdi'].' '. $take['apellido1'].' '. $take['apellido2'].'. <br><br>
			<b>EN LOS TERMINOS SUJETANDOSE EXPRESAMENTE A LAS SIGUIENTES:</b>
			<p align="center"><b>DECLARACIONES:</b></p>
			PRIMERA.- El vendedor declara tener plena capacidad juridica y material para celebrar el presente contrato, asi como tener pleno conocimiento de sus alcances.<br><br>
			SEGUNDA.- El vendedor declara que su domicilio es: Calle: '.$take['calle'].', No. Exterior: '.$take['numext'].', No. Interior: '.$take['numint'].', Colonia: '.$take['colonia'].', Ciudad: '.$take['ciudad'].', Municipio: '.$take['municipio'].', CP: '.$take['cp'].', Estado: '.$take['estado'].'. DE ACUERDO AL COMPROBANTE DE CEA O CFE CON FECHA DE: <font color="#0A6ACF">'.$take['mesrecibo'].'</font><br><br>
			TERCERA.- El vendedor declara tener plena capacidad juridica y económica para Celebrar el presente contrato identificandose con: <font color="#0A6ACF">'.$take['tipoidenti'].'</font> con folio: <font color="#0A6ACF">'.$take['nidenti'].'</font><br><br>
			Una vez declarado lo anterior, las partes se obligan en términos de las siguientes:<br>
		</label>

		<p align="center"><b>CLAUSULAS:</b></p>
		<br><br>

		<label align="justify">PRIMERA.- Objeto del contrato, el vendedor hace formal entrega en éste acto al comprador del automóvil que se describre a continuación, en las condiciones y características de uso en que se encuentra a la fecha de celebración del presente contrato.
			<br><br>

			Marca:<font color="#0A6ACF">'.$take['brname'].' </font><br>
			Tipo:<font color="#0A6ACF">'.$take['versiontxt'].' </font><br>
			Año Modelo:<font color="#0A6ACF">'.$take['anname'].' </font><br>
			Kilometraje:<font color="#0A6ACF">'.$take['km'].' </font><br>
			Factura:<font color="#0A6ACF">'.$take['facturavehi'].' </font> <br>
					Modelo:<font color="#0A6ACF">'.$take['modelotxt'].' </font><br>
					Color:<font color="#0A6ACF">'.$take['colorext'].' </font><br>
					Motor:<font color="#0A6ACF">'.$take['nmotor'].' </font> <br>
					Interior:<font color="#0A6ACF">'.$take['colorint'].' </font><br>
					Número. Serie:<font color="#0A6ACF">'.$take['nserie'].' </font><br>
					Expedidia por: <font color="#0A6ACF">'.$take['expedida'].' </font><br>
					Número de verificación:<font color="#0A6ACF">'.$take['nverificacion'].' </font><br>
					Entidad:<font color="#0A6ACF">'.$take['edoemisor'].' </font><br>
					Tenencias por los años: <font color="#0A6ACF">'.$take['tenencias'].' </font><br>
					Folio de Baja:<font color="#0A6ACF">'.$take['nbaja'].' </font><br>
					Del año:<font color="#0A6ACF">'.$take['fechabaja'].' </font><br>
					Placas y tarjeta de circulación:<font color="#0A6ACF">'.$take['placasbaja'].' </font><br><br>
		</label>
<br><br>

		<label align="justify">SEGUNDA.- Como consecuencia de la compraventa del automóvil antes descrito, el vendedor hace formal y material entrega en éste acto al comprador de la documentación en original que ampara la propiedad.<br><br>

			TERCERA.- PRECIO.- Ambas partes convienen que el precio pactado por la presente comprevante es de: <font color="#0A6ACF">'.$fmt->formatCurrency($take['pdlttfac2'], "USD").'</font>   <font color="#0A6ACF">('.strtoupper($formatterES->format($izquierda)).' PESOS ' .$derecha.'/100 M.N.)</font><br><br>

			CUARTA.- Declaro expresamente bajo protesta de decir la verdad, que el vehículo que les he vendido lo adquirí legalmente y lo entrego completamente libre de cualquier gravamen o vicio, constituyéndome en único responsable de su saneamiento, solventaré todo problema que se presente respecto de su legitima propiedad, por su legal y definitiva estancia en el país o por cuaqlquier responsabilidad de carácter civil, penal o fiscal que por lo mismo resultare hasta la feha, por lo que, en su caso me obligo expresamente a efectuar de inmediato la devolución del dinero que he recibido independientemente de la liquidación y reparación del monto de los daños y perjuicios que le causaren con tal motivo, asi como las reparaciones hechas al vehículo.
					<br><br>
					QUINTA.- A partir de la firma del presente contrato, el comprador se hace responsable de cualquier daño en propiedad ajena o a terceros ocasionados con motivo materia de la compraventa; asi como de toda responsabilidad con motivo de que, a partir de ésta fecha, dicho comprador adquiere la posesión y porpiedad del vehículo en comento.
					<br><br>
					Las partes contratantes manifiestan que han leido en su integridad el contenido del presente contrato, estando conformes con sus alcances legales, reiterando que tiene plena capacidad jurídica para comprometerse y firmarlo con absoluta libertad y sin existir dolo alguno.<br><br><br>

					Firmado el contratante en dos originales que corresponden uno para cada parte, en
					<br><br><br>
		</label>
 <br><br>
		
		<table align="center">
			<tr>
				<td>
					VENDEDOR
					<br><br><br><br><br><br><br><br><br>
					_______________________________<br>
					<b>'.$take['nombre'].'</b> <br>
				</td>
				<td>
				</td>
				<td>
					COMPRADOR
					<br><br><br><br><br><br><br><br><br>
					_______________________________<br>
					<b>'.$take['bursocial'].'</b > <br>
				</td>
			</tr>
		</table> <label align="justify"> Firman las partes todas las hojas que integran el presente contrato</label>';



$html.='</tbody> </table>';

tcpdf();
$obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
header('Content-type: application/pdf');
$obj_pdf->SetCreator(PDF_CREATOR);
$title = "Contrato de compraventa de automovil";
$obj_pdf->SetTitle($title);
$obj_pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $title, PDF_HEADER_STRING);
$obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$obj_pdf->SetDefaultMonospacedFont('helvetica');
$obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$obj_pdf->SetFont('helvetica', '', 9);
$obj_pdf->setFontSubsetting(false);
$obj_pdf->AddPage();
ob_start();
    // podemos tener cualquier parte de la vista aquí como HTML, PHP, etc.
$content = ob_get_contents();
ob_end_clean();
$obj_pdf->writeHTML($content, true, false, true, false, '');
$obj_pdf->writeHTML($html, true, false, true, false, '');

$filename = 'contrato_compraventa';

$obj_pdf->Output($filename . '.pdf', 'D');
?>