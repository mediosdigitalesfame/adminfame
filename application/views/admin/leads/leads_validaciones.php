<!-- DataTables -->

<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/datatables/dataTables.bootstrap4.css"> 



<!-- Content Wrapper. Contains page content -->

<div class="content-wrapper">

  <section class="content">

    <!-- For Messages -->

    <?php $this->load->view('admin/includes/_messages.php') ?>

    <div class="card">

      <div class="card-header">

        <div class="d-inline-block">

          <h3 class="card-title"><i class="fa fa-list"></i>&nbsp; Grafico de Validaciones</h3>

        </div>

        <div class="d-inline-block float-right">

          <?php if($this->rbac->check_operation_permission('kk')): ?>

            <a href="<?= base_url('admin/guides/add'); ?>" class="btn btn-success"><i class="fa fa-plus"></i> <?= trans('add_new_guide') ?></a>

          <?php endif; ?>  

          <?php if($this->rbac->check_operation_permission('kk')): ?>  

            <a href="<?= base_url('admin/guides/create_guides_pdf') ?>" class="btn btn-success"><?= trans('export_as_pdf') ?></a>

          <?php endif; ?>

          <?php if($this->rbac->check_operation_permission('kk')): ?>  

            <a href="<?= base_url('admin/guides/export_csv') ?>" class="btn btn-success"><?= trans('export_as_csv') ?></a>

          <?php endif; ?>

        </div>

      </div>

    </div>



    <!--<div class="card">

      <div class="card-body table-responsive"> 

        <?php echo form_open("/",'id="model_search"') ?>

        <div class="row">

          <div class="col-md-4">

            <label><?= trans('model_type') ?></label><hr style="margin:5px 0px;" />

            <input checked="checked" onchange="user_filter()" name="user_search_type" value="" type="radio"  /> <?= trans('all') ?>&nbsp;&nbsp;&nbsp;

            <input onchange="model_filter()" name="model_search_type" value="1" type="radio"  /> <?= trans('active') ?>&nbsp;&nbsp;&nbsp;

            <input onchange="model_filter()" name="model_search_type" value="0" type="radio"  /> <?= trans('inactive') ?>

          </div>

          <div class="col-md-3">

            <label>Date From:</label><input name="user_search_from" type="text" class="form-control form-control-inline input-medium datepicker" id="" />

          </div>

          <div class="col-md-3"> 

            <label>Date To:</label><input name="user_search_to" type="text" class="form-control form-control-inline input-medium datepicker" id="" /> 

          </div>

          <div class="col-md-2"> 

            <button type="button" style="margin-top:20px;" onclick="user_filter()" class="btn btn-info">Submit</button>

            <a href="<?= base_url('admin/guides/advance_search'); ?>" class="btn btn-danger" style="margin-top:20px;">

              <i class="fa fa-repeat"></i>

            </a>

          </div>

        </div>

        <?php echo form_close(); ?>

      </div>

    </div>-->  





    <div class="card">

      <div class="card-body table-responsive">

  

<?php
 
  switch ($idagen) {
    case 1:
        echo 'NA';
        break;
    case 2:
        echo "No Tiene";
        break;
    case 3:
        echo '<iframe width="100%" height="1467" src="https://datastudio.google.com/embed/reporting/4d65f9e2-22f4-4b04-8399-acc419fa7e75/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 4:
        echo '<iframe width="100%" height="1467" src="https://datastudio.google.com/embed/reporting/7de48950-67fd-4d1d-bd65-cc54b6f737c2/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 5:
        echo '<iframe width="100%" height="1400" src="https://datastudio.google.com/embed/reporting/4324bdb9-bb73-410f-b392-54aef56f8159/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 6:
        echo "No Tiene";
        break;
        case 7:
        echo '<iframe width="100%" height="1467" src="https://datastudio.google.com/embed/reporting/b6dd6263-1e6a-49d9-a353-9f20c9a68d33/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 8:
        echo '<iframe width="100%" height="1467" src="https://datastudio.google.com/embed/reporting/a873bbcd-30cc-4c0a-8cb6-19b745e11f6c/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 9:
        echo '<iframe width="100%" height="1400" src="https://datastudio.google.com/embed/reporting/9bcb7e3d-6f84-441f-a6d2-79d327a04443/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 10:
        echo '<iframe width="100%" height="1467" src="https://datastudio.google.com/embed/reporting/6b622f3e-78c7-4338-bf29-e044c6ce02ba/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 11:
        echo '<iframe width="100%" height="1467" src="https://datastudio.google.com/embed/reporting/1d4fdcc9-9e5e-4085-9621-59f86b864061/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 12:
        echo '<iframe width="100%" height="1467" src="https://datastudio.google.com/embed/reporting/2e896e16-7911-4af9-bb07-d29d8853c68f/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 13:
        echo '<iframe width="100%" height="1467" src="https://datastudio.google.com/embed/reporting/c9a2ab46-83f9-4efa-a272-4b64a99e40f8/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 14:
        echo '<iframe width="100%" height="1467" src="https://datastudio.google.com/embed/reporting/60c099c9-4555-45a6-a090-083d268c672a/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 15:
        echo '<iframe width="100%" height="1467" src="https://datastudio.google.com/embed/reporting/cda3150b-59a6-4b69-bcec-d121db3d5f99/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 16:
        echo '<iframe width="100%" height="1467" src="https://datastudio.google.com/embed/reporting/621c12b1-e6f3-4d69-8d78-604eb5776187/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 17:
        echo 'No Tiene';
        break;
        case 18:
        echo '<iframe width="100%" height="1467" src="https://datastudio.google.com/embed/reporting/2b1b033e-9cba-4dd9-bf37-1b5dbb5d3f96/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 19:
        echo '<iframe width="100%" height="1467" src="https://datastudio.google.com/embed/reporting/c03ac2c1-33dd-4b83-a5d9-5760e1a2c51d/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 20:
        echo '<iframe width="100%" height="1467" src="https://datastudio.google.com/embed/reporting/f18f214a-275f-4931-9263-7e0d85d27038/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 21:
        echo '<iframe width="100%" height="1467" src="https://datastudio.google.com/embed/reporting/373aa1dd-c132-46dd-9e48-0ca006f4111b/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 22:
        echo '<iframe width="100%" height="1467" src="https://datastudio.google.com/embed/reporting/fea0b7b4-5467-4e94-9496-bde606c48462/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 23:
        echo 'No Tiene';
        break;
        case 24:
        echo '<iframe width="100%" height="1467" src="https://datastudio.google.com/embed/reporting/9abee179-3b25-4e51-846a-53f8bd394507/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 25:
        echo '<iframe width="100%" height="1467" src="https://datastudio.google.com/embed/reporting/e86d2fb5-1056-4a78-87fb-dce8502336f8/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 26:
        echo '<iframe width="100%" height="1467" src="https://datastudio.google.com/embed/reporting/554d4511-3c45-4d8f-9c38-29b7f63f4a03/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 27:
        echo '<iframe width="100%" height="1467" src="https://datastudio.google.com/embed/reporting/bd66de31-51a8-4636-b742-9166d8323d81/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 28:
        echo '<iframe width="100%" height="1467" src="https://datastudio.google.com/embed/reporting/ef1ed826-ce98-435d-8971-03edd43f78d5/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 29:
        echo '<iframe width="100%" height="1467" src="https://datastudio.google.com/embed/reporting/ef1ed826-ce98-435d-8971-03edd43f78d5/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 30:
        echo 'No Tiene';
        break;
         case 31:
        echo 'No Tiene';
        break;
         case 32:
        echo 'No Tiene';
        break;
         case 33:
        echo 'No Tiene';
        break;
         case 34:
        echo 'No Tiene';
        break;
         case 35:
        echo 'No Tiene';
        break;
         case 36:
        echo '<iframe width="100%" height="1467" src="https://datastudio.google.com/embed/reporting/c51acdef-7dd4-41f9-88ef-4527bd49b6f5/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
         case 37:
        echo '<iframe width="100%" height="1467" src="https://datastudio.google.com/embed/reporting/988ffaba-8e89-4542-8021-a2e7a5422d46/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
         case 38:
        echo "No Hay";
        break;
         case 39:
        echo 'No Tiene';
        break;
         case 40:
        echo "No Tiene";
        break;
         case 41:
        echo "No Tiene";
        break;
         case 42:
        echo "No Tiene";
        break;

        case 91:
        echo "No Tiene";
        break;
        case 92:
        echo "No Tiene";
        break;
        case 93:
        echo "No Tiene";
        break;
        case 94:
        echo "No Tiene";
        break;
        case 95:
        echo "No Tiene";
        break;
        case 96:
        echo "No Tiene";
        break;

        case 100:
        echo "No Aplica";
        break;




}
?>



       

 

        <!--<table id="na_datatable" class="table table-bordered table-striped" width="100%">

          <thead>

            <tr>

              <th>#<?= trans('id') ?></th>

              <th>Nombre</th>

              

              <th><?= trans('status') ?></th>

              <th width="100" class="text-right"><?= trans('action') ?></th>

            </tr>

          </thead>

        </table>-->

      </div>

    </div>














  </section>  

</div>



<!-- bootstrap datepicker -->

<!-- datepicker -->

<script src="<?= base_url() ?>assets/plugins/datepicker/bootstrap-datepicker.js"></script>

<script>

  $('.datepicker').datepicker({

    autoclose: true

  });

</script>

<!-- DataTables -->

<script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.js"></script>

<script src="<?= base_url() ?>assets/plugins/datatables/dataTables.bootstrap4.js"></script>



<script>

  //---------------------------------------------------

  var table = $('#na_datatable').DataTable( {

    "processing": true,

    "serverSide": false,

    "ajax": "<?=base_url('admin/leads/datatable_json')?>",

    "order": [[0,'desc']],

    "columnDefs": [

    { "targets": 0, "name": "id", 'searchable':true, 'orderable':true},

    { "targets": 1, "name": "name", 'searchable':true, 'orderable':true},

     

    { "targets": 2, "name": "Action", 'searchable':false, 'orderable':false,'width':'100px'}

    ]

  });

</script>



<script>

  //---------------------------------------------------

  function model_filter()

  {

    var _form = $("#model_search").serialize();

    $.ajax({

      data: _form,

      type: 'post',

      url: '<?php echo base_url();?>admin/guides/search',

      async: true,

      success: function(output){

        table.ajax.reload( null, false );

      }

    });

  }

</script>



<script type="text/javascript">

  $("body").on("change",".tgl_checkbox",function(){

    console.log('checked');

    $.post('<?=base_url("admin/guides/change_status")?>',

    {

      '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>',

      id : $(this).data('id'),

      status : $(this).is(':checked') == true?1:0

    },

    <?php if($this->rbac->check_operation_permission('add')): ?>

      function(data){$.notify("El Status cambió con éxito", "success");}

      <?php else: ?>

        function(data){$.notify("No Tienes Permisos", "warn");}

      <?php endif; ?>

      );

  });

</script>





