<?php
class Check_model extends CI_Model{

	public function add_check($data){
		$this->db->insert('checklist', $data);
		return true;
	}

	public function add_checkdocs($datadocs){
		$this->db->insert('checklist_docs', $datadocs);
		return true;
	}

	public function add_checkdates($datadates){
		$this->db->insert('checklist_dates', $datadates);
		return true;
	}

		//---------------------------------------------------
		// obtener todas los agencias para el procesamiento de tablas de datos del lado del servidor (basado en ajax)
		//-----------------------------------------------------
	public function get_all_checks(){
		$this->db->select('c.*, folio, ag.name as agname, gu.created_at as fechaguia, gu.id as guid');
		$this->db->join('seminuevostomados st','st.id=c.seminuevostomados_id', 'LEFT');
		$this->db->join('guides gu','gu.id=st.guias_id', 'LEFT');
		$this->db->join('agencys ag','ag.id=gu.agencys_id', 'LEFT');
			//$this->db->where('status',1);
		return $this->db->get('checklist c')->result_array();
	}

	//---------------------------------------------------
		// obtener todas los checklist pendientes de terminar para el procesamiento de tablas de datos del lado del servidor (basado en ajax)
		//-----------------------------------------------------
	public function get_all_checks_pendientes(){
		$this->db->select('c.*, folio, ag.name as agname');
		$this->db->where('c.status',1);
		$this->db->join('seminuevostomados st','st.id=c.seminuevostomados_id', 'LEFT');
		$this->db->join('guides gu','gu.id=st.guias_id', 'LEFT');
		$this->db->join('agencys ag','ag.id=gu.agencys_id', 'LEFT');
			//$this->db->where('status',1);
		return $this->db->get('checklist c')->result_array();
	}

	//---------------------------------------------------
		// obtener todas los checklist en proceso de terminar para el procesamiento de tablas de datos del lado del servidor (basado en ajax)
		//-----------------------------------------------------
	public function get_all_checks_proceso(){
		$this->db->select('c.*, folio, ag.name as agname');
		$this->db->where('c.status',2);
		$this->db->join('seminuevostomados st','st.id=c.seminuevostomados_id', 'LEFT');
		$this->db->join('guides gu','gu.id=st.guias_id', 'LEFT');
		$this->db->join('agencys ag','ag.id=gu.agencys_id', 'LEFT');
			//$this->db->where('status',1);
		return $this->db->get('checklist c')->result_array();
	}

	//---------------------------------------------------
		// obtener todas los checklist en proceso de terminar para el procesamiento de tablas de datos del lado del servidor (basado en ajax)
		//-----------------------------------------------------
	public function get_all_checks_terminados(){
		$this->db->select('c.*, folio, ag.name as agname');
		$this->db->where('c.status',3);
		$this->db->join('seminuevostomados st','st.id=c.seminuevostomados_id', 'LEFT');
		$this->db->join('guides gu','gu.id=st.guias_id', 'LEFT');
		$this->db->join('agencys ag','ag.id=gu.agencys_id', 'LEFT');
			//$this->db->where('status',1);
		return $this->db->get('checklist c')->result_array();
	}

	//---------------------------------------------------
		// obtener todas los checklist en proceso de terminar para el procesamiento de tablas de datos del lado del servidor (basado en ajax)
		//-----------------------------------------------------
	public function get_all_checks_libres(){
		$this->db->select('c.*, folio, ag.name as agname');
		$this->db->where('c.status',0);
		$this->db->join('seminuevostomados st','st.id=c.seminuevostomados_id', 'LEFT');
		$this->db->join('guides gu','gu.id=st.guias_id', 'LEFT');
		$this->db->join('agencys ag','ag.id=gu.agencys_id', 'LEFT');
			//$this->db->where('status',1);
		return $this->db->get('checklist c')->result_array();
	}

	    //---------------------------------------------------
		// obtener todas los agencias para el procesamiento de tablas de datos del lado del servidor (basado en ajax)
		//-----------------------------------------------------
	public function get_all_takes_guides_actives(){
		$this->db->select('st.*,gu.folio');
		$this->db->join('guides gu','gu.id=st.guias_id', 'LEFT');
		$this->db->where('st.status',1);
		$this->db->where('st.statuscheck',0);
		return $this->db->get('seminuevostomados st')->result_array();
	}

		//---------------------------------------------------
		// obtener todas los agencias para el procesamiento de tablas de datos del lado del servidor (basado en ajax)
		//-----------------------------------------------------
	public function get_all_takes_guides(){
		$this->db->select('st.*,gu.folio');
		$this->db->join('guides gu','gu.id=st.guias_id', 'LEFT');
		//$this->db->where('st.status',1);
		//$this->db->where('st.statuscheck',0);
		return $this->db->get('seminuevostomados st')->result_array();
	}

		//---------------------------------------------------
		// obtener todas los agencias con sus marcas para el procesamiento de tablas de datos del lado del servidor (basado en ajax)
		//-----------------------------------------------------
	public function get_all_agencys(){
		$this->db->select('*');
			//$this->db->where('status',1);
		return $this->db->get('agencys')->result_array();
	}

		//---------------------------------------------------
		// Obtener información de la agencia por ID
		//-----------------------------------------------------
	public function get_check_by_id($id){
		$this->db->where('cl.id', $id);
		$this->db->select('cl.*,
			chdo.rfc as rfcdocs,  
			chdo.solicitudcfdi as solicitudcfdidocs,
			chdo.cambioderol as cambioderoldocs,  
			chdo.cedulafiscal as cedulafiscaldocs,
			chdo.cfdisinactividad as cfdisinactividaddocs,
			chdo.xml as xmldocs,
			chdo.rfc as rfcdocs,
			chdo.validacioncfdi as validacioncfdidocs,
			chdo.verificacioncfdi as verificacioncfdidocs,
			chdo.autenticaciondoctos as autenticaciondoctosdocs,
			chdo.reportetransunion as reportetransuniondocs,
			chdo.noretencion as noretenciondocs,
			chdo.secuenciafacturas as secuenciafacturasdocs,
			chdo.identificacionoficial as identificacionoficialdocs,
			chdo.curp as curpdocs,
			chdo.comprobante as comprobantedocs,
			chdo.bajaplacas as bajaplacasdocs,
			chdo.tenencias as tenenciasdocs,
			chdo.tarjetacirculacion as tarjetacirculaciondocs,
			chdo.responsivacompraventa as responsivacompraventadocs,
			chdo.contratocompraventa as contratocompraventadocs,
			chdo.avisoprivacidad as avisoprivacidaddocs,
			chdo.checklistrevision as checklistrevisiondocs,
			chdo.evaluacionmecanica as evaluacionmecanicadocs,
			chdo.guiaautometrica as guiaautometricadocs,
			chdo.polizaderecepcion as polizaderecepciondocs,
			chdo.cuentasporpagar as cuentasporpagardocs,
			chdo.formatodedacion as formatodedaciondocs,
			chdo.reportederobo as reportederobodocs,
			chdo.chequedeproveedor as chequedeproveedordocs,
			chdo.solicitudcheque as solicitudchequedocs,
			chdo.edocuenta as edocuentadocs,
			chdo.calculoretencion as calculoretenciondocs,
			chdo.autorizaciontoma as autorizaciontomadocs,
			chdo.avaluorapido as avaluorapidodocs,
			chdo.contratoadhesion as contratoadhesiondocs,
			chda.rfc as rfcdates,  
			chda.solicitudcfdi as solicitudcfdidates,
			chda.cambioderol as cambioderoldates,  
			chda.cedulafiscal as cedulafiscaldates,
			chda.cfdisinactividad as cfdisinactividaddates,
			chda.xml as xmldates,
			chda.rfc as rfcdates,
			chda.validacioncfdi as validacioncfdidates,
			chda.verificacioncfdi as verificacioncfdidates,
			chda.autenticaciondoctos as autenticaciondoctosdates,
			chda.reportetransunion as reportetransuniondates,
			chda.noretencion as noretenciondates,
			chda.secuenciafacturas as secuenciafacturasdates,
			chda.identificacionoficial as identificacionoficialdates,
			chda.curp as curpdates,
			chda.comprobante as comprobantedates,
			chda.bajaplacas as bajaplacasdates,
			chda.tenencias as tenenciasdates,
			chda.tarjetacirculacion as tarjetacirculaciondates,
			chda.responsivacompraventa as responsivacompraventadates,
			chda.contratocompraventa as contratocompraventadates,
			chda.avisoprivacidad as avisoprivacidaddates,
			chda.checklistrevision as checklistrevisiondates,
			chda.evaluacionmecanica as evaluacionmecanicadates,
			chda.guiaautometrica as guiaautometricadates,
			chda.polizaderecepcion as polizaderecepciondates,
			chda.cuentasporpagar as cuentasporpagardates,
			chda.formatodedacion as formatodedaciondates,
			chda.reportederobo as reportederobodates,
			chda.chequedeproveedor as chequedeproveedordates,
			chda.solicitudcheque as solicitudchequedates,
			chda.edocuenta as edocuentadates,
			chda.calculoretencion as calculoretenciondates,
			chda.autorizaciontoma as autorizaciontomadates,
			chda.avaluorapido as avaluorapidodates,
			chda.contratoadhesion as contratoadhesiondates,
			gu.folio,
			 st.regimenes_id');
		$this->db->join('checklist_dates chda','cl.id=chda.checklist_id', 'LEFT');
		$this->db->join('checklist_docs chdo','cl.id=chdo.checklist_id', 'LEFT');
		$this->db->join('seminuevostomados st','st.id=cl.seminuevostomados_id', 'LEFT');
		$this->db->join('guides gu','gu.id=st.guias_id', 'LEFT');
		$this->db->from('checklist cl');
		$query = $this->db->get();
		return $result = $query->row_array();
	}

		//---------------------------------------------------
		// Obtener información de la agencia por ID
		//-----------------------------------------------------
	public function get_check_comments_by_id($id){
		$this->db->where('checklist_id', $id);
		$this->db->select('cc.*, cad.firstname, cad.lastname');
		$this->db->join('ci_admin cad','cad.admin_id=cc.users_id', 'LEFT');
		$this->db->from('comentarios_has_checklist cc');
		$query = $this->db->get();
		return $result = $query->result_array();
	}

		//---------------------------------------------------
		// get all user records for simple datatable example
	    //---------------------------------------------------
	public function get_all_simple_users(){
		$this->db->where('is_admin', 0);
		$this->db->order_by('created_at', 'desc');
		$query = $this->db->get('ci_users');
		return $result = $query->result_array();
	}

	    //---------------------------------------------------
		// Obtener información de la agencia y sus tablas padre por ID
		//-----------------------------------------------------
	public function get_agency_by_id_business($id){
		$this->db->where('a.id', $id);
		$this->db->select('a.id, a.name, a.status, a.created_at, a.tel, a.calle, a.int, a.ext, a.colonia, a.cp, a.ciudad, a.estado, a.brands_id, a.business_id, a.users_id_gg, a.users_id_gs, a.users_id_ga, b.rsocial, b.rfc');
		$this->db->join('business b','b.id=a.business_id', 'LEFT');
		$this->db->from('agencys a');
		$query = $this->db->get();
		return $result = $query->row_array();
	}

		//---------------------------------------------------
		// Editar registro de agencia
		//-----------------------------------------------------
	public function edit_check($data, $id){
		$this->db->where('id', $id);
		$this->db->update('checklist', $data);
		return true;
	}

		//---------------------------------------------------
		// Cambiar status de modelo
		//-----------------------------------------------------
	function change_status()
	{		
		$this->db->set('status', $this->input->post('status'));
		$this->db->where('id', $this->input->post('id'));
		$this->db->update('agencys');
	} 




	//---------------------------------------------------
//obtener el registro de la guia para hacer el PDF
//-----------------------------------------------------
	public function get_check_by_id_business_pdf($id){
		$this->db->where('ch.id', $id);
		$this->db->select('ch.*, gu.*, st.facturavehi, st.nserie, st.fechacontrato, st.nombrescfdi, st.apellido1, st.apellido2, an.name as anname, br.name as brname, ag.name as agname, ag.calle as agcalle, ag.ext as agext, ag.colonia as agcolonia, ag.cp as agcp, ag.ciudad as agciudad, ag.estado as agestado, ag.url as agurl, ag.urlp as agurlp, ag.tel as agtel, tg.version, tg.persona, br.name as brname, an.name as anname, bu.rsocial as bursocial, bu.calle as bucalle, bu.int as buint, bu.colonia as bucolonia, bu.cp as bucp, bu.ciudad as buciudad, bu.estado as buestado, 
			ciad.firstname as ciadfirstnamegg, 
			ciad.lastname as ciadlastnamegg,
			ciad1.firstname as ciadfirstnamega,
			ciad1.lastname as ciadlastnamega, 
			ciad2.firstname as ciadfirstnamegs, 
			ciad2.lastname as ciadlastnamegs
			');
		$this->db->join('seminuevostomados st','st.id=ch.seminuevostomados_id', 'LEFT');
		$this->db->join('guides gu','gu.id=st.guias_id', 'LEFT');
		$this->db->join('brands br','br.id=gu.brands_id', 'LEFT');
		$this->db->join('anios an','an.id=gu.anio_id', 'LEFT');
		
		$this->db->join('tipoguias tg','tg.id=gu.tipoguias_id', 'LEFT');
		$this->db->join('agencys ag','ag.id=gu.agencys_id', 'LEFT');
		$this->db->join('business bu','bu.id=ag.business_id', 'LEFT');
		
		$this->db->join('ci_admin ciad','ciad.admin_id=ag.users_id_gg', 'LEFT');
		$this->db->join('ci_admin ciad1','ciad1.admin_id=ag.users_id_ga', 'LEFT');
		$this->db->join('ci_admin ciad2','ciad2.admin_id=ag.users_id_gs', 'LEFT');
		//$this->db->join('models mo','mo.id=gu.models_id', 'LEFT');

		$this->db->from('checklist ch');
		$query = $this->db->get();
		return $result = $query->row_array();
	}





		//---------------------------------------------------
		// obtener marcas para exportar a csv
		//-----------------------------------------------------
	public function get_checks_for_csv(){
		//$this->db->where('status', 1);
		$this->db->select('cl.*, cia.firstname, cia.lastname, gu.folio');
		$this->db->join('seminuevostomados st','st.id=cl.seminuevostomados_id', 'LEFT');
		$this->db->join('guides gu','gu.id=st.guias_id', 'LEFT');
		$this->db->join('ci_admin cia','cia.admin_id=st.users_id', 'LEFT');
		$this->db->order_by('cl.id', 'desc');
		$this->db->from('checklist cl');
		$query=$this->db->get();
		return $result = $query->result_array();
	}

		//---------------------------------------------------
		// obtener todos los registros de marcas
		//-----------------------------------------------------
	public function get_all_simple_brands(){
		$this->db->where('status', 1);
		$this->db->order_by('id', 'desc');
		$query = $this->db->get('brands');
		return $result = $query->result_array();
	}

	    //---------------------------------------------------
		// obtener todos los registros de años
		//-----------------------------------------------------
	public function get_all_simple_years(){
		$this->db->where('status', 1);
		$this->db->order_by('id', 'desc');
		$query = $this->db->get('anios');
		return $result = $query->result_array();
	}

	    //---------------------------------------------------
		// obtener todos los registros de negocios
		//-----------------------------------------------------
	public function get_all_simple_business(){
		$this->db->where('status', 1);
		$this->db->order_by('id', 'desc');
		$query = $this->db->get('business');
		return $result = $query->result_array();
	}

	    //---------------------------------------------------
		// obtener todos los registros de admins
		//-----------------------------------------------------
	public function get_all_simple_admins(){
		//$this->db->where('is_active', 1);
		$this->db->order_by('admin_id', 'desc');
		$query = $this->db->get('ci_admin');
		return $result = $query->result_array();
	}

	    //---------------------------------------------------
		// obtener todos los registros de agencias
		//-----------------------------------------------------
	public function get_all_simple_agencys(){
		$this->db->where('status', 1);
		$this->db->order_by('id', 'desc');
		$query = $this->db->get('agencys');
		return $result = $query->result_array();
	}

	//---------------------------------------------------
		// obtener todos los registros de agencias
		//-----------------------------------------------------
	public function get_all_simple_checks(){
		//$this->db->where('status', 1);
		$this->db->select('cl.*, cia.firstname, cia.lastname, gu.folio');
		$this->db->join('seminuevostomados st','st.id=cl.seminuevostomados_id', 'LEFT');
		$this->db->join('guides gu','gu.id=st.guias_id', 'LEFT');
		$this->db->join('ci_admin cia','cia.admin_id=st.users_id', 'LEFT');
		$this->db->order_by('cl.id', 'desc');
		$this->db->from('checklist cl');
		$query=$this->db->get();
		return $result = $query->result_array();
	}

	//---------------------------------------------------
		// obtener todos los registros de los checkslist pendientes
		//-----------------------------------------------------
	public function get_all_simple_checks_pendientes(){
		$this->db->where('cl.status', 1);
		$this->db->select('cl.*, cia.firstname, cia.lastname, gu.folio');
		$this->db->join('seminuevostomados st','st.id=cl.seminuevostomados_id', 'LEFT');
		$this->db->join('guides gu','gu.id=st.guias_id', 'LEFT');
		$this->db->join('ci_admin cia','cia.admin_id=st.users_id', 'LEFT');
		$this->db->order_by('cl.id', 'desc');
		$this->db->from('checklist cl');
		$query=$this->db->get();
		return $result = $query->result_array();
	}

	//---------------------------------------------------
		// obtener todos los registros de los checkslist pendientes
		//-----------------------------------------------------
	public function get_all_simple_checks_proceso(){
		$this->db->where('cl.status', 2);
		$this->db->select('cl.*, cia.firstname, cia.lastname, gu.folio');
		$this->db->join('seminuevostomados st','st.id=cl.seminuevostomados_id', 'LEFT');
		$this->db->join('guides gu','gu.id=st.guias_id', 'LEFT');
		$this->db->join('ci_admin cia','cia.admin_id=st.users_id', 'LEFT');
		$this->db->order_by('cl.id', 'desc');
		$this->db->from('checklist cl');
		$query=$this->db->get();
		return $result = $query->result_array();
	}

	//---------------------------------------------------
		// obtener todos los registros de los checkslist pendientes
		//-----------------------------------------------------
	public function get_all_simple_checks_terminados(){
		$this->db->where('cl.status', 3);
		$this->db->select('cl.*, cia.firstname, cia.lastname, gu.folio');
		$this->db->join('seminuevostomados st','st.id=cl.seminuevostomados_id', 'LEFT');
		$this->db->join('guides gu','gu.id=st.guias_id', 'LEFT');
		$this->db->join('ci_admin cia','cia.admin_id=st.users_id', 'LEFT');
		$this->db->order_by('cl.id', 'desc');
		$this->db->from('checklist cl');
		$query=$this->db->get();
		return $result = $query->result_array();
	}

	//---------------------------------------------------
		// obtener todos los registros de los checkslist pendientes
		//-----------------------------------------------------
	public function get_all_simple_checks_libres(){
		$this->db->where('cl.status', 0);
		$this->db->select('cl.*, cia.firstname, cia.lastname, gu.folio');
		$this->db->join('seminuevostomados st','st.id=cl.seminuevostomados_id', 'LEFT');
		$this->db->join('guides gu','gu.id=st.guias_id', 'LEFT');
		$this->db->join('ci_admin cia','cia.admin_id=st.users_id', 'LEFT');
		$this->db->order_by('cl.id', 'desc');
		$this->db->from('checklist cl');
		$query=$this->db->get();
		return $result = $query->result_array();
	}

		//---------------------------------------------------
		// Obtener información de la agencia y sus tablas padre por ID
		//-----------------------------------------------------
	public function get_guide_by_id_business_view($id){
		$this->db->where('gu.id', $id);
		$this->db->select('gu.*, ag.name as agname, ag.tel as agtel, tg.version, tg.persona, br.name as brname, an.name as anname, mo.name as moname, mo.version as moversion ');
		$this->db->join('agencys ag','ag.id=gu.agencys_id', 'LEFT');
		$this->db->join('models mo','mo.id=gu.models_id', 'LEFT');
		$this->db->join('brands br','br.id=mo.brands_id', 'LEFT');
		$this->db->join('anios an','an.id=mo.anios_id', 'LEFT');
		$this->db->join('tipoguias tg','tg.id=gu.tipoguias_id', 'LEFT');
		$this->db->from('guides gu');
		$query = $this->db->get();
		return $result = $query->row_array();
	}

		//---------------------------------------------------
		// obtener todos los registros de marcas 
		//-----------------------------------------------------
	function get_admin_roles()
	{
		$this->db->from('ci_admin_roles');
		$this->db->where('admin_role_status',1);
		$query=$this->db->get();
		return $query->result_array();
	}

		//---------------------------------------------------
		// funcion general para actualizar registro
		//-----------------------------------------------------
	function update($action,$id,$table){
		$this->db->where('id',$id);
		$this->db->update($table,$action);
	}

        //---------------------------------------------------
		// funcion general para insertar registro
		//-----------------------------------------------------
	public function insert($data,$table){
		$this->db->insert($table,$data);        
		return $this->db->insert_id();
	}

}

?>