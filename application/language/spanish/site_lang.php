<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['contact'] = "Contacto";
$lang['leads'] = "Leads";
$lang['logout'] = "Salir";
$lang['search'] = "Buscar";
$lang['invoice'] = "Factura";
$lang['brand'] = "Marca";
$lang['name'] = "Nombre";
$lang['change_status'] = "Cambiar Status";
$lang['version'] = "Version";
$lang['all'] = "Todo";
$lang['name'] = "Nombre";
$lang['rfc'] = "RFC";
$lang['tel'] = "Teléfono";
$lang['street'] = "Calle";
$lang['int'] = "No.Int";
$lang['ext'] = "No.Ext";
$lang['suburb'] = "Colonia";
$lang['postcode'] = "CP";
$lang['bank'] = "Banco";
$lang['bill'] = "Cuenta";
$lang['pay'] = "Pago";
$lang['vin'] = "VIN";
$lang['km'] = "Kilometraje";
$lang['folio'] = "Folio";
$lang['comments'] = "Comentarios";
$lang['seminuevos'] = "Toma de Seminuevos";

$lang['select_gg'] = "Seleccionar Gerente General";
$lang['select_gs'] = "Seleccionar Gerente de Seminuevos";
$lang['select_ga'] = "Seleccionar Gerente Admnistrativo";
 
//Navbar
$lang['dashboard'] = "Dashboard";
$lang['dashboard_v1'] = "Dashboard v1";
$lang['dashboard_v2'] = "Dashboard v2";
$lang['dashboard_v3'] = "Dashboard v3";
$lang['admin'] = "Admin";
$lang['admin_list'] = "Lista de Admins";
$lang['add_new_admin'] = "Agregar Admin";
$lang['profile'] = "Perfil";
$lang['view_profile'] = "Ver Perfil";
$lang['change_password'] = "Cambiar Password";
$lang['role_and_permissions'] = "Roles & Permisos";
$lang['module_setting'] = "Configuración";
$lang['users'] = "Usuarios";
$lang['users_list'] = "Lista de Usuarios";
$lang['add_new_user'] = "Agregar Nuevo Usuario";
$lang['activity_log']= "Registro de Actividad";
$lang['settings'] = "Configuraciones";
$lang['general_settings'] = "Configuración General";
$lang['email_template_settings']                               = "Email Template Settings";
$lang['codeigniter_examples']                               = "Codeingiter Examples";
$lang['simple_datatable']                               = "Simple Datatable";
$lang['ajax_datatable']                               = "Ajax Datatable";
$lang['pagination']                               = "Pagination";
$lang['advance_search']                               = "Advance Search";
$lang['file_upload']                               = "File Upload";
$lang['multiple_files_upload']                               = "Multiple Files Upload";
$lang['backup_and_export']                               = "Backup & Export";
$lang['invoicing_system']                               = "Invoicing System";
$lang['invoice_list']                               = "Invoice List";
$lang['add_new_invoice']                               = "Add New Invoice";
$lang['database_joins_example']                               = "Database Joins Example";
$lang['serverside_join']                               = "Serverside Join";
$lang['simple_join']                               = "Simple Join";
$lang['language_setting']                               = "Language Setting";
$lang['locations']                               = "Locations";
$lang['country']                               = "País";
$lang['state']                               = "Estado";
$lang['city']                               = "Ciudad";
$lang['widgets']                               = "Widgets";
$lang['charts']                               = "Charts";
$lang['charts_js']                               = "ChartJS";
$lang['charts_flot']                               = "Flot";
$lang['charts_inline']                               = "Inline";
$lang['ui_elements']                               = "UI Elements";
$lang['general']                               = "General";
$lang['icons']                               = "Icons";
$lang['buttons']                               = "Buttons";
$lang['forms']                               = "Forms";
$lang['general_elements']                               = "General Elements";
$lang['advanced_elements']                               = "Adnvaced Elements";
$lang['editors']                               = "Editors";
$lang['tables']                               = "Tables";
$lang['simple_tables']                               = "Simple Tables";
$lang['data_tables']                               = "Data Tables";
$lang['mailbox']                               = "Mailbox";
$lang['inbox']                               = "Inbox";
$lang['compose']                               = "Compose";
$lang['read']                               = "Read";
$lang['pages']                               = "Pages";
$lang['login']                               = "Login";
$lang['register']                               = "Register";
$lang['lock_screen']                               = "Lock Screen";
$lang['extras']                               = "Extras";
$lang['error_404']                               = "Error 404";
$lang['error_500']                               = "Error 500";
$lang['blank_page']                               = "Blank Page";
$lang['starter_page']                               = "Starter Page";
$lang['miscellaneous']                               = "Miscellaneous";
$lang['documentation']                               = "Documentation";
$lang['labels']                               = "Labels";
$lang['important']                               = "Important";
$lang['warning']                               = "Warning";
$lang['informational']                               = "Informational";

//  Login
$lang['signin_to_start_your_session'] = "Regístrese para iniciar su sesión";
$lang['signin'] = "Iniciar";
$lang['username'] = "Username";
$lang['password'] = "Password";
$lang['remember_me'] = "Recordarme";
$lang['i_forgot_my_password'] = "Olvidé mi contraseña";
$lang['register_new_membership'] = "Registrar una nueva membresía";

//  Register
$lang['firstname'] = "Nombre";
$lang['lastname'] = "Apellido";
$lang['email'] = "Email";
$lang['confirm'] = "Confirmar";
$lang['i_agree_to_the'] = "Estoy de acuerdo con los";
$lang['terms'] = "Terminos";
$lang['register'] = "Registrar";
$lang['i_already_have_membership'] = "Ya tengo una membresía";

// Forgot Password
$lang['forgot_password'] = "Contraseña olvidada";
$lang['submit'] = "Enviar";
$lang['you_remember_password'] = "¿Recuerdas la contraseña? Inicia Sesion";

// Reset Password
$lang['reset_password'] = "Cambiar Password";
$lang['reset'] = "Cambiar";

//  Dashboard
$lang['home'] = "Inicio";
$lang['user_registrations']                   = "Usuarios Registrados";
$lang['active_users']                   = "Usuarios Activos";
$lang['inactive_users']                   = "Usuarios Inactivos";
$lang['unique_visitors']                   = "Visitantes Unicos";
$lang['others']                   = "Otros";
$lang['more_info']                   = "Más Información";
$lang['sales']                   = "Sales";
$lang['visitors']                   = "Visitors";
$lang['area']                   = "Area";
$lang['donut']                   = "Donut";
$lang['online']                   = "Online";
$lang['direct_chat']                   = "Direct Chat";
$lang['send']                   = "Send";
$lang['type_message']                   = "Type Message";
$lang['sales_graph']                   = "Sales Graph";
$lang['to_do_list']                   = "To Do List";
$lang['add_item']                   = "Add Item";
$lang['calendar']                   = "Calendar";

// Admin
$lang['mobile_no']                 = "Mobile No";
$lang['select_admin_role']                 = "Select Admin Role";
$lang['select_role']                 = "Select Role";
$lang['add_admin']                 = "Add Admin";
$lang['edit_admin']                 = "Edit Admin";
$lang['all_admin_types']                 = "Add Admin Types";
$lang['all_status']                 = "All Status";
$lang['active']                 = "Activo";
$lang['inactive']                 = "Inactivo";
$lang['search_from_here']                 = "Search from here";
$lang['id']                 = "ID";
$lang['user']                 = "User";
$lang['role']                 = "Role";
$lang['status']                 = "Status";
$lang['select_status']                 = "Selecciona Status";
$lang['action']                 = "Acción";

//  Profile
$lang['update_profile']             = "Actualizar Perfil";
$lang['new_password']             = "Nueva Contraseña";
$lang['confirm_password']             = "Confirmar Contraseña";

// Role & Permissions
$lang['add_new_module'] = "Agregar Nuevo Modulo";
$lang['operations'] = "Operaciones";
$lang['sub_module'] = "Sub Modulo";
$lang['controller_name'] = "Nombre del Controlador";
$lang['module_name'] = "Nombre del Modulo";
$lang['fa_icon'] = "Fa Icon";
$lang['module_list'] = "Lista de Modulos";
$lang['sort_order'] = "Orden";
$lang['add_module'] = "Agregar Modulo";
$lang['update_module'] = "Actualizar Modulo";
$lang['lang_index_message'] = "Índice de idioma según su archivo de idioma";
$lang['admin_role'] = "Admin Rol";
$lang['permission'] = "Permisos";
$lang['add_new_role'] = "Agregar Nuevo Rol";
$lang['back'] = "Atras";
$lang['edit_role'] = "Editar Rol";
$lang['admin_permissions'] = "Permisos de administrador";
$lang['permission_access'] = "Permiso de acceso";

// Usuarios
$lang['created_date'] = "Fecha de Creación";
$lang['email_verification'] = "Email Verification";
$lang['add_user'] = "Add User";
$lang['address'] = "Address";
$lang['edit_user'] = "Edit User";
$lang['update_user'] = "Update User";

// Guias
$lang['guide'] = "Guía";
$lang['guides'] = "Guías Rapidas";
$lang['guides_rapidas'] = "Guías Rapidas";
$lang['add_guide'] = "Agregar Guía";
$lang['select_guide'] = "Seleccionar Guía";
$lang['edit_guide'] = "Edit Guía";
$lang['update_guide'] = "Actualizar Guía";
$lang['add_new_guide'] = "Agregar Nueva Guía";
$lang['guides_list'] = "Lista de Guias";
$lang['utility'] = "Utilidad";

// Tomas
$lang['take'] = "Toma";
$lang['takes'] = "Tomas";
$lang['add_take'] = "Agregar Toma";
$lang['select_take'] = "Seleccionar Toma";
$lang['edit_take'] = "Edit Toma";
$lang['update_take'] = "Actualizar Toma";
$lang['add_new_take'] = "Agregar Nueva Toma";
$lang['takes_list'] = "Lista de Tomas";


// Inspecciones
$lang['inspect'] = "Inspeccion";
$lang['inspects'] = "Inspecciones";
$lang['add_inspect'] = "Agregar Inspeccion";
$lang['select_inspect'] = "Seleccionar Inspeccion";
$lang['edit_inspect'] = "Editar Inspeccion";
$lang['update_inspect'] = "Actualizar Inspeccion";
$lang['add_new_inspect'] = "Agregar Nueva Inspeccion";
$lang['inspects_list'] = "Lista de Inspecciones";
 

// Checklists
$lang['check'] = "Check";
$lang['checks'] = "Checks";
$lang['add_check'] = "Agregar Check";
$lang['edit_check'] = "Editar Check";
$lang['edit_checks'] = "Editar Checks";
$lang['update_check'] = "Actualizar Check";
$lang['add_new_check'] = "Agregar Nuevo Check";
$lang['checks_list'] = "Lista de Checks";
 

// Marcas
$lang['brands'] = "Marcas";
$lang['brands_list'] = "Lista de Marcas";
$lang['add_new_brand'] = "Agregar Nueva Marca";
$lang['add_brand'] = "Agregar Marca";
$lang['edit_brand'] = "Editar Marca";
$lang['update_brand'] = "Actualizar Marca";
$lang['select_brand'] = "Seleccionar Marca";

// Años
$lang['year'] = "Año";
$lang['years'] = "Años";
$lang['years_list'] = "Lista de Años";
$lang['add_new_year'] = "Agregar Nuevo Año";
$lang['add_year'] = "Agregar Año";
$lang['edit_year'] = "Editar Año";
$lang['update_year'] = "Actualizar Año";
$lang['select_year'] = "Seleccionar Año";

// Modelos
$lang['model'] = "Modelo";
$lang['models'] = "Modelos";
$lang['models_list'] = "Lista de Modelos";
$lang['add_new_model'] = "Agregar Nuevo Modelo";
$lang['add_model'] = "Agregar Modelo";
$lang['edit_model'] = "Editar Modelo";
$lang['update_model'] = "Actualizar Modelo";
$lang['model_type'] = "Tipo de Modelo";
$lang['select_model'] = "Seleccionar Modelo";
$lang['rsocial'] = "Razon Social";

// Negocios
$lang['business'] = "Negocios";
$lang['business_list'] = "Lista de Negocios";
$lang['add_new_business'] = "Agregar Nuevo Negocio";
$lang['add_business'] = "Agregar Negocio";
$lang['edit_business'] = "Editar Negocio";
$lang['update_business'] = "Actualizar Negocio";
$lang['busin_type'] = "Tipo de Negocio";
$lang['select_business'] = "Seleccionar Negocio";


// Autorizacionez
$lang['authorization'] = "Autorizacion";
$lang['authorizations'] = "Autorizaciones";
$lang['authorizations_list'] = "Lista de Autorizaciones";
$lang['add_new_authorization'] = "Agregar Nueva Autorizacion";
$lang['add_authorization'] = "Agregar Autorizacion";
$lang['edit_authorization'] = "Editar Autorizacion";
$lang['update_authorization'] = "Actualizar Autorizacion";
$lang['authorization_type'] = "Tipo de Autorizacion";
$lang['select_authorization'] = "Seleccionar Autorizacion";


// Leads
$lang['lead'] = "Lead";
$lang['leads'] = "Reportes";
$lang['leads_list'] = "Lista de Leads";
$lang['leads_global'] = "Grafico Global";
$lang['leads_sales'] = "Grafico de Ventas";
$lang['leads_aftersales'] = "Grafico de Postventa";
$lang['leads_analytics'] = "Reporte de Analiticos";
$lang['leads_parrilla'] = "Parrilla";
$lang['leads_validaciones'] = "Validaciones";

$lang['business_list'] = "Lista de Negocios";
$lang['add_new_business'] = "Agregar Nuevo Negocio";
$lang['add_business'] = "Agregar Negocio";
$lang['edit_business'] = "Editar Negocio";
$lang['update_business'] = "Actualizar Negocio";
$lang['busin_type'] = "Tipo de Negocio";
$lang['select_business'] = "Sleccionar Negocio";



// Agencias
$lang['agency'] = "Agencia";
$lang['agencys'] = "Agencias";
$lang['agencys_list'] = "Lista de Agencias";
$lang['add_new_agencys'] = "Agregar Nueva Agencia";
$lang['add_agencys'] = "Agregar Agencia";
$lang['edit_agencys'] = "Editar Agencia";
$lang['update_agencys'] = "Actualizar Agencia";
$lang['agencys_type'] = "Tipo de Agencia";
$lang['select_agencys'] = "Sleccionar Agencia";
$lang['url'] = "URL";
$lang['url_aviso'] = "URL Aviso de Privacidad";

// Activity Log
$lang['users_activity_log'] = "Users Activity Log";
$lang['activity'] = "Activity";
$lang['date'] = "Date";
$lang['time'] = "Time";

// Settings
$lang['general_setting']                               = "General Setting";
$lang['email_setting']                               = "Email Setting";
$lang['google_setting']                               = "Google reCAPTCHA";
$lang['favicon']                               = "Favicon";
$lang['logo']                               = "Logo";
$lang['application_name']                               = "Application Name";
$lang['allowed_types']                               = "Tipos permitidos";
$lang['timezone']                               = "Timezone";
$lang['default_language']                               = "Default Language";
$lang['currency']                               = "Currency";
$lang['copyright']                               = "Copyright";
$lang['save_changes']                               = "Save Changes";
$lang['email_from']                               = "Email From/ Reply to";
$lang['smtp_host']                               = "SMTP Host";
$lang['smtp_port']                               = "SMTP Port";
$lang['smtp_user']                               = "SMTP User";
$lang['smtp_password']                               = "SMTP Password";
$lang['site_key']                               = "Site Key";
$lang['secret_key']                               = "Secret Key";
$lang['language']                               = "Language";
$lang['email_templates']                               = "Email Templates";
$lang['select_a_template']                               = "Select a Template";
$lang['preview']                               = "Preview";
$lang['variables']                               = "Variables";
$lang['title']                               = "Title";

// Charts
$lang['area_chart'] = "Area Chart";
$lang['donut_chart'] = "Donut Chart";
$lang['line_chart'] = "Line Chart";
$lang['bar_chart'] = "Bar Chart";

// Codeigniter Examples
$lang['users_area_chart'] = "Registered Users Area Chart";
$lang['payment_line_chart'] = " Payment Line Chart";
$lang['admin_donut_chart'] = " Admin Donut Chart";
$lang['payment_bar_chart'] = "Payment Bar Chart";
$lang['simple_table_example'] = "User List with (Simple Datatables, Export PDF, CSV) Example";
$lang['datatable_example'] = "Datatable - ServerSide Processing (Ajax Base Search & Pagination)";
$lang['pagination_example'] = "User List with Pagination Example";
$lang['advance_search_example'] = "User List with Advanced Serarch Example";
$lang['file_upload_example'] = "File Upload Example";
$lang['multiple_file_upload_example'] = "Multiple Files Upload Example";
$lang['export_as_pdf'] = "Exportar a PDF";
$lang['export_as_csv'] = "Exportar a CSV";
$lang['max_allowed_size'] = "Maximum Allowed Size";
$lang['max_files'] = "Maximum Files";
$lang['multiple_files_uploader'] = "Multiple Files Uploader";
$lang['dynamic_charts'] = "Dynamic Charts";

// Backup
$lang['database_backup'] = "Database Backup";
$lang['download_and_create_backup'] = "Download & Create Backup";

// Invoicing
$lang['add_new_invoice'] = "Add New Invoice";
$lang['invoice'] = "Invoice";
$lang['client'] = "Client";
$lang['amount'] = "Amount";
$lang['due_date'] = "Due Date";
$lang['bill_from'] = "Bill From";
$lang['bill_to'] = "Bill To";
$lang['company_name'] = "Company Name";
$lang['address_line'] = "Address Line";
$lang['customer'] = "Customer";
$lang['billing_date'] = "Billing Date";
$lang['product'] = "Product";
$lang['quantity'] = "Quantity";
$lang['price'] = "Price";
$lang['tax'] = "Tax";
$lang['total'] = "Total";
$lang['subtotal'] = "Sub Total";
$lang['discount'] = "Discount";
$lang['client_note'] = "Client Note";
$lang['terms_and_conditions'] = "Terms & Conditions";
$lang['edit_invoice'] = "Edit Invoice";
$lang['download'] = "Download";
$lang['send_email'] = "Send Email";
