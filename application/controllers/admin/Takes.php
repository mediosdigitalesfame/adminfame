<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Takes extends MY_Controller 
{
	public function __construct(){

		parent::__construct();
		auth_check(); // comprobar la autenticación de inicio de sesión
		$this->rbac->check_module_access();

		$this->load->model('admin/take_model', 'take_model');
		$this->load->model('admin/Activity_model', 'activity_model');
	}

	//-----------------------------------------------------------
	//  Vista principal
	//-------------------------------------------------------
	public function index(){
		$data['title'] = 'Lista de Tomas';
		$this->load->view('admin/includes/_header');
		$this->load->view('admin/takes/take_list');
		$this->load->view('admin/includes/_footer');
	}

	//-----------------------------------------------------------
	//  Vista principal de tomas autorizadas
	//-------------------------------------------------------
	public function index_autorizadas(){
		$data['title'] = 'Lista de Tomas';
		$this->load->view('admin/includes/_header');
		$this->load->view('admin/takes/take_list_aut');
		$this->load->view('admin/includes/_footer');
	}

	//-----------------------------------------------------------
	//  Vista principal de tomas pendientes
	//-------------------------------------------------------
	public function index_pendientes(){
		$data['title'] = 'Lista de Tomas';
		$this->load->view('admin/includes/_header');
		$this->load->view('admin/takes/take_list_pen');
		$this->load->view('admin/includes/_footer');
	}

	//-----------------------------------------------------------
	//  Vista principal de tomas con checks
	//-------------------------------------------------------
	public function index_checks(){
		$data['title'] = 'Lista de Tomas';
		$this->load->view('admin/includes/_header');
		$this->load->view('admin/takes/take_list_che');
		$this->load->view('admin/includes/_footer');
	}
	
	//-----------------------------------------------------------
	//  Tabla para vista principal
	//-------------------------------------------------------
	public function datatable_json(){	

		$usr=$this->session->userdata('is_supper');
		$idusr=$this->session->userdata('admin_id');

		if($usr>0){
			$records['data'] = $this->take_model->get_all_takes();
		}else{
			$records['data'] = $this->take_model->get_all_takes_peruser($idusr);
		}

		$data = array();
		$i=0;
		foreach ($records['data'] as $row) 

			


		{  

			switch ($row['regimenes_id'])
    {
      case '1': 

      $status = ($row['status'] == 1)? 'checked': '';

      $foliouno = $row['id'];
        $foliocero = str_pad($foliouno, 9, "0", STR_PAD_LEFT);


			$data[]= array(
				++$i,
				$row['agname'],
				$row['folio']. "-" .$foliocero,
				//date("d-m-Y", strtotime($row['created_at'])),
				'<input class="tgl_checkbox tgl-ios"data-id="'.$row['id'].'"id="cb_'.$row['id'].'"type="checkbox"	'.$status.'><label for="cb_'.$row['id'].'"></label>',
				'<a title="View" class="view btn btn-sm btn-info" href="'.base_url('admin/takes/edit/'.$row['id']).'"> <i class="fa fa-eye"></i></a>
				<a title="Edit" class="update btn btn-sm btn-warning" href="'.base_url('admin/takes/edit/'.$row['id']).'"> <i class="fa fa-pencil-square-o"></i></a>
				<a title="Delete" class="delete btn btn-sm btn-danger" href='.base_url("admin/takes/delete/".$row['id']).' title="Delete" onclick="return confirm(\'Do you want to delete ?\')"> 
				<i class="fa fa-trash-o"></i></a>
				<a title="Exportar esta Toma a PDF" class="update btn btn-sm btn-success" href="'.base_url('admin/takes/create_take_pdf/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>
				<a title="Solicitud CFDI" class="update btn btn-sm btn-success" href="'.base_url('admin/takes/create_take_pdf_cfdi/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>
				<a title="Autentificacion de Documentos" class="update btn btn-sm btn-success" href="'.base_url('admin/takes/create_take_pdf_audoctos/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>
				<a title="Carta de No Retencion ISR" class="update btn btn-sm btn-success" href="'.base_url('admin/takes/create_take_pdf_reten/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>
				<a title="Carta de No Retencion ISR" class="update btn btn-sm btn-success" href="'.base_url('admin/takes/create_take_pdf_retensin/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>
				<a title="Carta Responsiva" class="update btn btn-sm btn-success" href="'.base_url('admin/takes/create_take_pdf_respon/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>
				<a title="contratocompraventa" class="update btn btn-sm btn-success" href="'.base_url('admin/takes/create_take_pdf_compra/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>
				<a title="Aviso de Privacidad" class="update btn btn-sm btn-success" href="'.base_url('admin/takes/create_take_pdf_aviso/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>
				<a title="Dacion en Pago" class="update btn btn-sm btn-success" href="'.base_url('admin/takes/create_take_pdf_dacionpago/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>
				<a title="Solicitud de Cheque" class="update btn btn-sm btn-success" href="'.base_url('admin/takes/create_take_pdf_solicitudcheque/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>
				<a title="Calculo de Retencion" class="update btn btn-sm btn-success" href="'.base_url('admin/takes/create_take_pdf_calculoret/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>
				<a title="Reglamento" class="update btn btn-sm btn-success" href="'.base_url('admin/takes/create_take_pdf_reglamento/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>'
			);
      break;

      case '2':
      $status = ($row['status'] == 1)? 'checked': '';

      $foliouno = $row['id'];
        $foliocero = str_pad($foliouno, 9, "0", STR_PAD_LEFT);

			$data[]= array(
				++$i,
				$row['agname'],
				$row['folio']. "-" .$foliocero,
				//date("d-m-Y", strtotime($row['created_at'])),
				'<input class="tgl_checkbox tgl-ios"data-id="'.$row['id'].'"id="cb_'.$row['id'].'"type="checkbox"	'.$status.'><label for="cb_'.$row['id'].'"></label>',
				'<a title="View" class="view btn btn-sm btn-info" href="'.base_url('admin/takes/edit/'.$row['id']).'"> <i class="fa fa-eye"></i></a>
				<a title="Edit" class="update btn btn-sm btn-warning" href="'.base_url('admin/takes/edit/'.$row['id']).'"> <i class="fa fa-pencil-square-o"></i></a>
				<a title="Delete" class="delete btn btn-sm btn-danger" href='.base_url("admin/takes/delete/".$row['id']).' title="Delete" onclick="return confirm(\'Do you want to delete ?\')"> 
				<i class="fa fa-trash-o"></i></a>
				<a title="Exportar esta Toma a PDF" class="update btn btn-sm btn-success" href="'.base_url('admin/takes/create_take_pdf/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>
				 
				<a title="Autentificacion de Documentos" class="update btn btn-sm btn-success" href="'.base_url('admin/takes/create_take_pdf_audoctos/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>

				<a title="Carta de No Retencion ISR" class="update btn btn-sm btn-success" href="'.base_url('admin/takes/create_take_pdf_retensin/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>

				<a title="Carta Responsiva" class="update btn btn-sm btn-success" href="'.base_url('admin/takes/create_take_pdf_respon/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>

				<a title="Contrato de Compraventa" class="update btn btn-sm btn-success" href="'.base_url('admin/takes/create_take_pdf_compra/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>

				<a title="Aviso de Privacidad" class="update btn btn-sm btn-success" href="'.base_url('admin/takes/create_take_pdf_aviso/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>

				<a title="Dacion en Pago" class="update btn btn-sm btn-success" href="'.base_url('admin/takes/create_take_pdf_dacionpago/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>

				<a title="Solicitud de Cheque" class="update btn btn-sm btn-success" href="'.base_url('admin/takes/create_take_pdf_solicitudcheque/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>

				<a title="Reglamento" class="update btn btn-sm btn-success" href="'.base_url('admin/takes/create_take_pdf_reglamento/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>'
			);
      break;

      case '3':
      $status = ($row['status'] == 1)? 'checked': '';

      $foliouno = $row['id'];
        $foliocero = str_pad($foliouno, 9, "0", STR_PAD_LEFT);


			$data[]= array(
				++$i,
				$row['agname'],
				$row['folio']. "-" .$foliocero,
				//date("d-m-Y", strtotime($row['created_at'])),
				'<input class="tgl_checkbox tgl-ios"data-id="'.$row['id'].'"id="cb_'.$row['id'].'"type="checkbox"	'.$status.'><label for="cb_'.$row['id'].'"></label>',
				'<a title="View" class="view btn btn-sm btn-info" href="'.base_url('admin/takes/edit/'.$row['id']).'"> <i class="fa fa-eye"></i></a>
				<a title="Edit" class="update btn btn-sm btn-warning" href="'.base_url('admin/takes/edit/'.$row['id']).'"> <i class="fa fa-pencil-square-o"></i></a>
				<a title="Delete" class="delete btn btn-sm btn-danger" href='.base_url("admin/takes/delete/".$row['id']).' title="Delete" onclick="return confirm(\'Do you want to delete ?\')"> 
				<i class="fa fa-trash-o"></i></a>
				<a title="Exportar esta Toma a PDF" class="update btn btn-sm btn-success" href="'.base_url('admin/takes/create_take_pdf/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>

				 

				<a title="Autentificacion de Documentos" class="update btn btn-sm btn-success" href="'.base_url('admin/takes/create_take_pdf_audoctos/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>

				 

				<a title="Carta Responsiva" class="update btn btn-sm btn-success" href="'.base_url('admin/takes/create_take_pdf_respon/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>

				<a title="Contrato de Compraventa" class="update btn btn-sm btn-success" href="'.base_url('admin/takes/create_take_pdf_compra/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>

				<a title="Aviso de Privacidad" class="update btn btn-sm btn-success" href="'.base_url('admin/takes/create_take_pdf_aviso/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>

				<a title="Dacion en Pago" class="update btn btn-sm btn-success" href="'.base_url('admin/takes/create_take_pdf_dacionpago/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>

				<a title="Solicitud de Cheque" class="update btn btn-sm btn-success" href="'.base_url('admin/takes/create_take_pdf_solicitudcheque/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>

				 

				<a title="Reglamento" class="update btn btn-sm btn-success" href="'.base_url('admin/takes/create_take_pdf_reglamento/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>'
			);
      break;
    }


			
		}


		$records['data']=$data;
		echo json_encode($records);						   
	}

	//-----------------------------------------------------------
	//  Tabla para vista principal de tomas autorizadas
	//-------------------------------------------------------
	public function datatable_json_autorizadas(){	

		$usr=$this->session->userdata('is_supper');
		$idusr=$this->session->userdata('admin_id');

		if($usr>0){
			$records['data'] = $this->take_model->get_all_takes_autorizadas();
		}else{
			$records['data'] = $this->take_model->get_all_takes_peruser_autorizadas($idusr);
		}

		$data = array();
		$i=0;
		foreach ($records['data']   as $row) 
		{  
			$status = ($row['status'] == 1)? 'checked': '';

			$foliouno = $row['id'];
        $foliocero = str_pad($foliouno, 9, "0", STR_PAD_LEFT);

			$data[]= array(
				++$i,
				$row['agname'],
				$row['folio']. "-" .$foliocero,
				//date("d-m-Y", strtotime($row['created_at'])),
				'<input class="tgl_checkbox tgl-ios"data-id="'.$row['id'].'"id="cb_'.$row['id'].'"type="checkbox"	'.$status.'><label for="cb_'.$row['id'].'"></label>',

				'<a title="View" class="view btn btn-sm btn-info" href="'.base_url('admin/takes/edit/'.$row['id']).'"> <i class="fa fa-eye"></i></a>
				<a title="Edit" class="update btn btn-sm btn-warning" href="'.base_url('admin/takes/edit/'.$row['id']).'"> <i class="fa fa-pencil-square-o"></i></a>
				<a title="Delete" class="delete btn btn-sm btn-danger" href='.base_url("admin/takes/delete/".$row['id']).' title="Delete" onclick="return confirm(\'Do you want to delete ?\')"> 
				<i class="fa fa-trash-o"></i></a>
				<a title="Exportar esta Toma a PDF" class="update btn btn-sm btn-success" href="'.base_url('admin/takes/create_take_pdf/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>
				<a title="Solicitud CFDI" class="update btn btn-sm btn-success" href="'.base_url('admin/takes/create_take_pdf_cfdi/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>
				<a title="Autentificacion de Documentos" class="update btn btn-sm btn-success" href="'.base_url('admin/takes/create_take_pdf_audoctos/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>
				<a title="Carta de No Retencion ISR" class="update btn btn-sm btn-success" href="'.base_url('admin/takes/create_take_pdf_reten/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>

				<a title="Carta de No Retencion ISR" class="update btn btn-sm btn-success" href="'.base_url('admin/takes/create_take_pdf_retensin/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>

				<a title="Carta Responsiva" class="update btn btn-sm btn-success" href="'.base_url('admin/takes/create_take_pdf_respon/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>
				<a title="contratocompraventa" class="update btn btn-sm btn-success" href="'.base_url('admin/takes/create_take_pdf_compra/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>
				<a title="Aviso de Privacidad" class="update btn btn-sm btn-success" href="'.base_url('admin/takes/create_take_pdf_aviso/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>
				<a title="Dacion en Pago" class="update btn btn-sm btn-success" href="'.base_url('admin/takes/create_take_pdf_dacionpago/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>
				<a title="Solicitud de Cheque" class="update btn btn-sm btn-success" href="'.base_url('admin/takes/create_take_pdf_solicitudcheque/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>
				<a title="Calculo de Retencion" class="update btn btn-sm btn-success" href="'.base_url('admin/takes/create_take_pdf_calculoret/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>
				<a title="Reglamento" class="update btn btn-sm btn-success" href="'.base_url('admin/takes/create_take_pdf_reglamento/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>
				'
			);
		}
		$records['data']=$data;
		echo json_encode($records);						   
	}

	//-----------------------------------------------------------
	//  Tabla para vista principal de tomas pendientes
	//-------------------------------------------------------
	public function datatable_json_pendientes(){	

		$usr=$this->session->userdata('is_supper');
		$idusr=$this->session->userdata('admin_id');

		if($usr>0){
			$records['data'] = $this->take_model->get_all_takes_pendientes();
		}else{
			$records['data'] = $this->take_model->get_all_takes_peruser_pendientes($idusr);
		}

		$data = array();
		$i=0;
		foreach ($records['data']   as $row) 
		{  
			$status = ($row['status'] == 1)? 'checked': '';

			$foliouno = $row['id'];
        $foliocero = str_pad($foliouno, 9, "0", STR_PAD_LEFT);

			$data[]= array(
				++$i,
				$row['agname'],
				$row['folio']. "-" .$foliocero,
				//date("d-m-Y", strtotime($row['created_at'])),
				'<input class="tgl_checkbox tgl-ios"data-id="'.$row['id'].'"id="cb_'.$row['id'].'"type="checkbox"	'.$status.'><label for="cb_'.$row['id'].'"></label>',

				'<a title="View" class="view btn btn-sm btn-info" href="'.base_url('admin/takes/edit/'.$row['id']).'"> <i class="fa fa-eye"></i></a>
				<a title="Edit" class="update btn btn-sm btn-warning" href="'.base_url('admin/takes/edit/'.$row['id']).'"> <i class="fa fa-pencil-square-o"></i></a>
				<a title="Delete" class="delete btn btn-sm btn-danger" href='.base_url("admin/takes/delete/".$row['id']).' title="Delete" onclick="return confirm(\'Do you want to delete ?\')"> 
				<i class="fa fa-trash-o"></i></a>
				<a title="Exportar esta Toma a PDF" class="update btn btn-sm btn-success" href="'.base_url('admin/takes/create_take_pdf/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>
				<a title="Solicitud CFDI" class="update btn btn-sm btn-success" href="'.base_url('admin/takes/create_take_pdf_cfdi/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>
				<a title="Autentificacion de Documentos" class="update btn btn-sm btn-success" href="'.base_url('admin/takes/create_take_pdf_audoctos/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>
				<a title="Carta de No Retencion ISR" class="update btn btn-sm btn-success" href="'.base_url('admin/takes/create_take_pdf_reten/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>

				<a title="Carta de No Retencion ISR" class="update btn btn-sm btn-success" href="'.base_url('admin/takes/create_take_pdf_retensin/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>

				<a title="Carta Responsiva" class="update btn btn-sm btn-success" href="'.base_url('admin/takes/create_take_pdf_respon/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>
				<a title="contratocompraventa" class="update btn btn-sm btn-success" href="'.base_url('admin/takes/create_take_pdf_compra/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>
				<a title="Aviso de Privacidad" class="update btn btn-sm btn-success" href="'.base_url('admin/takes/create_take_pdf_aviso/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>
				<a title="Dacion en Pago" class="update btn btn-sm btn-success" href="'.base_url('admin/takes/create_take_pdf_dacionpago/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>
				<a title="Solicitud de Cheque" class="update btn btn-sm btn-success" href="'.base_url('admin/takes/create_take_pdf_solicitudcheque/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>
				<a title="Calculo de Retencion" class="update btn btn-sm btn-success" href="'.base_url('admin/takes/create_take_pdf_calculoret/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>
				<a title="Reglamento" class="update btn btn-sm btn-success" href="'.base_url('admin/takes/create_take_pdf_reglamento/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>
				'
			);
		}
		$records['data']=$data;
		echo json_encode($records);						   
	}

	//-----------------------------------------------------------
	//  Tabla para vista principal de tomas con checks list
	//-------------------------------------------------------
	public function datatable_json_checks(){	

		$usr=$this->session->userdata('is_supper');
		$idusr=$this->session->userdata('admin_id');

		if($usr>0){
			$records['data'] = $this->take_model->get_all_takes_checks();
		}else{
			$records['data'] = $this->take_model->get_all_takes_peruser_checks($idusr);
		}

		$data = array();
		$i=0;
		foreach ($records['data']   as $row) 
		{  
			$status = ($row['status'] == 1)? 'checked': '';

			$foliouno = $row['id'];
        $foliocero = str_pad($foliouno, 9, "0", STR_PAD_LEFT);

			$data[]= array(
				++$i,
				$row['agname'],
				$row['folio']. "-" .$foliocero,
				//date("d-m-Y", strtotime($row['created_at'])),
				'<input class="tgl_checkbox tgl-ios"data-id="'.$row['id'].'"id="cb_'.$row['id'].'"type="checkbox"	'.$status.'><label for="cb_'.$row['id'].'"></label>',

				'<a title="View" class="view btn btn-sm btn-info" href="'.base_url('admin/takes/edit/'.$row['id']).'"> <i class="fa fa-eye"></i></a>
				<a title="Edit" class="update btn btn-sm btn-warning" href="'.base_url('admin/takes/edit/'.$row['id']).'"> <i class="fa fa-pencil-square-o"></i></a>
				<a title="Delete" class="delete btn btn-sm btn-danger" href='.base_url("admin/takes/delete/".$row['id']).' title="Delete" onclick="return confirm(\'Do you want to delete ?\')"> 
				<i class="fa fa-trash-o"></i></a>
				<a title="Exportar esta Toma a PDF" class="update btn btn-sm btn-success" href="'.base_url('admin/takes/create_take_pdf/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>
				<a title="Solicitud CFDI" class="update btn btn-sm btn-success" href="'.base_url('admin/takes/create_take_pdf_cfdi/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>
				<a title="Autentificacion de Documentos" class="update btn btn-sm btn-success" href="'.base_url('admin/takes/create_take_pdf_audoctos/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>
				<a title="Carta de No Retencion ISR" class="update btn btn-sm btn-success" href="'.base_url('admin/takes/create_take_pdf_reten/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>

				<a title="Carta de No Retencion ISR" class="update btn btn-sm btn-success" href="'.base_url('admin/takes/create_take_pdf_retensin/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>

				<a title="Carta Responsiva" class="update btn btn-sm btn-success" href="'.base_url('admin/takes/create_take_pdf_respon/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>
				<a title="contratocompraventa" class="update btn btn-sm btn-success" href="'.base_url('admin/takes/create_take_pdf_compra/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>
				<a title="Aviso de Privacidad" class="update btn btn-sm btn-success" href="'.base_url('admin/takes/create_take_pdf_aviso/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>
				<a title="Dacion en Pago" class="update btn btn-sm btn-success" href="'.base_url('admin/takes/create_take_pdf_dacionpago/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>
				<a title="Solicitud de Cheque" class="update btn btn-sm btn-success" href="'.base_url('admin/takes/create_take_pdf_solicitudcheque/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>
				<a title="Calculo de Retencion" class="update btn btn-sm btn-success" href="'.base_url('admin/takes/create_take_pdf_calculoret/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>
				<a title="Reglamento" class="update btn btn-sm btn-success" href="'.base_url('admin/takes/create_take_pdf_reglamento/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>
				'
			);
		}
		$records['data']=$data;
		echo json_encode($records);						   
	}

	//-----------------------------------------------------------
	//  Cambiar Status
	//-------------------------------------------------------
	function change_status()
	{   
		$this->rbac->check_operation_access('change_status'); //comprobar el permiso de operación
		$this->take_model->change_status();
	}

	//-----------------------------------------------------------
	//  Agregar Toma
	//-------------------------------------------------------
	public function add(){

		$usr=$this->session->userdata('is_supper');
		$idusr=$this->session->userdata('admin_id');
		
		$this->rbac->check_operation_access(); //comprobar el permiso de operación

		if($usr>0){
			$data['guides'] = $this->take_model->get_all_simple_guides_notomadas();
		}else{
			$data['guides'] = $this->take_model->get_all_simple_guides_notomadas_peruser($idusr);
		}

		$data['regis'] = $this->take_model->get_all_simple_regimenes();

		//$data['guides']=$this->take_model->get_all_simple_guides_notomadas();


		$idg = $this->input->post('guia', true);

		if($this->input->post('submit')){

			$this->form_validation->set_rules('nombre', trans('name'), 'trim|required');

			if ($this->form_validation->run() == FALSE) {
				$data = array(
					'errors' => validation_errors()
				);
				$this->session->set_flashdata('errors', $data['errors']);
				redirect(base_url('admin/takes/add'),'refresh');
			}
			else{
				$data = array(
					'regimenes_id' => $this->input->post('regimen', true),
					'tipodetoma' => $this->input->post('tipodetoma', true),
					'nombre' => $this->input->post('nombre', true),
					'fechacontrato' => $this->input->post('fechacontrato', true),
					'calle' => $this->input->post('calle', true),
					'numext' => $this->input->post('numext', true),
					'numint' => $this->input->post('numint', true),
					'colonia' => $this->input->post('colonia', true),
					'ciudad' => $this->input->post('ciudad', true),
					'municipio' => $this->input->post('municipio', true),
					'estado' => $this->input->post('estado', true),
					'cp' => $this->input->post('cp', true),
					'rfc' => $this->input->post('rfc', true),
					'nidenti' => $this->input->post('nidenti', true),
					'tipoidenti' => $this->input->post('tipoidenti', true),
					'mesrecibo' => $this->input->post('mesrecibo', true),
					'fechaidenti' => $this->input->post('fechaidenti', true),
					'facturavehi' => $this->input->post('facturavehi', true),
					'expedida' => $this->input->post('expedida', true),
					'fechafac' => $this->input->post('fechafac', true),
					'marcavehi' => $this->input->post('marcavehi', true),
					'modelovehi' => $this->input->post('modelovehi', true),
					'aniomodelo' => $this->input->post('aniomodelo', true),
					'versionvehi' => $this->input->post('versionvehi', true),
					'colorext' => $this->input->post('colorext', true),
					'colorint' => $this->input->post('colorint', true),
					'clavevehi' => $this->input->post('clavevehi', true),
					'nserie' => $this->input->post('nserie', true),
					'nmotor' => $this->input->post('nmotor', true),
					'nbaja' => $this->input->post('nbaja', true),
					'edoemisor' => $this->input->post('edoemisor', true),
					'fechabaja' => $this->input->post('fechabaja', true),
					'tenencias' => $this->input->post('tenencias', true),
					'placasbaja' => $this->input->post('placasbaja', true),
					'nverificacion' => $this->input->post('nverificacion', true),
					'aquiensevende' => $this->input->post('aquiensevende', true),
					'fechafacfinal' => $this->input->post('fechafacfinal', true),
					'fechatoma' => $this->input->post('fechacontrato', true),
					'costoadquisicion' => $this->input->post('costoadquisicion', true),
					'nombrescfdi' => $this->input->post('nombrescfdi', true),
					'apellido1' => $this->input->post('apellido1', true),
					'apellido2' => $this->input->post('apellido2', true),
					'email' => $this->input->post('email', true),
					'tel' => $this->input->post('tel', true),
					'cel' => $this->input->post('cel', true),
					'fechanaci' => $this->input->post('fechanaci', true),
					'entidadnaci' => $this->input->post('entidadnaci', true),
					'actividad' => $this->input->post('actividad', true),
					'curp' => $this->input->post('curp', true),
					'pais' => $this->input->post('pais', true),
					'banco' => $this->input->post('banco', true),
					'cuenta' => $this->input->post('cuenta', true),
					'clabe' => $this->input->post('clabe', true),
					'sucursal' => $this->input->post('sucursal', true),
					'convenio' => $this->input->post('convenio', true),
					'referencia' => $this->input->post('referencia', true),
					'foliocfdi' => $this->input->post('foliocfdi', true),
					'guias_id' => $this->input->post('guia', true),
					'users_id' => $this->session->userdata('admin_id'),
					'status' => 0,
					'created_at' => date('Y-m-d h:m:s'),
				);

				$path="uploads/tomas/tenencias/";

				if(!empty($_FILES['image']['name']))
				{
					$result = $this->functions->file_insert($path, 'image', 'pdf', '9097152');
					if($result['status'] == 1){
						$data['image'] = $path.$result['msg'];
					}
					else{
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/guides/'), 'refresh');
					}
				}

				$datag=array('statustoma' => 1);

				$data = $this->security->xss_clean($data);
				$result = $this->take_model->add_take($data);
				
				if($result){
					// Registro de actividades
					$this->activity_model->add_log(25);
					$this->take_model->update($datag, $idg, 'guides');
					$this->session->set_flashdata('success', 'La Guía se ha agregado correctamente!');
					redirect(base_url('admin/takes'));
				}
			}
		}
		else{
			$this->load->view('admin/includes/_header', $data);
			$this->load->view('admin/takes/take_add', $data);
			$this->load->view('admin/includes/_footer');
		}
		
	}

	//-----------------------------------------------------------
	//  Editar Toma
	//-------------------------------------------------------
	public function edit($id = 0){

		$this->rbac->check_operation_access(); //comprobar el permiso de operación

		$data['guides']=$this->take_model->get_all_simple_guides();
		$idg = $this->input->post('guia', true);
		$idgactual = $this->input->post('guiaactual', true);
		$data['regis'] = $this->take_model->get_all_simple_regimenes();

		if($this->input->post('submit')){
			$this->form_validation->set_rules('nombre', trans('name'), 'trim|required');

			if ($this->form_validation->run() == FALSE) {
				$data = array(
					'errors' => validation_errors()
				);
				$this->session->set_flashdata('errors', $data['errors']);
				redirect(base_url('admin/takes/take_edit/'.$id),'refresh');
			}
			else{
				$data = array(
					'nombre' => $this->input->post('nombre', true),
					'fechacontrato' => $this->input->post('fechacontrato', true),
					'calle' => $this->input->post('calle', true),
					'numext' => $this->input->post('numext', true),
					'numint' => $this->input->post('numint', true),
					'colonia' => $this->input->post('colonia', true),
					'ciudad' => $this->input->post('ciudad', true),
					'municipio' => $this->input->post('municipio', true),
					'estado' => $this->input->post('estado', true),
					'cp' => $this->input->post('cp', true),
					'rfc' => $this->input->post('rfc', true),
					'nidenti' => $this->input->post('nidenti', true),
					'tipoidenti' => $this->input->post('tipoidenti', true),
					'mesrecibo' => $this->input->post('mesrecibo', true),
					'fechaidenti' => $this->input->post('fechaidenti', true),
					'facturavehi' => $this->input->post('facturavehi', true),
					'expedida' => $this->input->post('expedida', true),
					'fechafac' => $this->input->post('fechafac', true),
					'marcavehi' => $this->input->post('marcavehi', true),
					'modelovehi' => $this->input->post('modelovehi', true),
					'aniomodelo' => $this->input->post('aniomodelo', true),
					'versionvehi' => $this->input->post('versionvehi', true),
					'colorext' => $this->input->post('colorext', true),
					'colorint' => $this->input->post('colorint', true),
					'clavevehi' => $this->input->post('clavevehi', true),
					'nserie' => $this->input->post('nserie', true),
					'nmotor' => $this->input->post('nmotor', true),
					'nbaja' => $this->input->post('nbaja', true),
					'edoemisor' => $this->input->post('edoemisor', true),
					'fechabaja' => $this->input->post('fechabaja', true),
					'tenencias' => $this->input->post('tenencias', true),
					'placasbaja' => $this->input->post('placasbaja', true),
					'nverificacion' => $this->input->post('nverificacion', true),
					'aquiensevende' => $this->input->post('aquiensevende', true),
					'fechafacfinal' => $this->input->post('fechafacfinal', true),
					'fechatoma' => $this->input->post('fechatoma', true),
					'costoadquisicion' => $this->input->post('costoadquisicion', true),
					'nombrescfdi' => $this->input->post('nombrescfdi', true),
					'apellido1' => $this->input->post('apellido1', true),
					'apellido2' => $this->input->post('apellido2', true),
					'email' => $this->input->post('email', true),
					'tel' => $this->input->post('tel', true),
					'cel' => $this->input->post('cel', true),
					'fechanaci' => $this->input->post('fechanaci', true),
					'entidadnaci' => $this->input->post('entidadnaci', true),
					'actividad' => $this->input->post('actividad', true),
					'curp' => $this->input->post('curp', true),
					'pais' => $this->input->post('pais', true),
					'banco' => $this->input->post('banco', true),
					'cuenta' => $this->input->post('cuenta', true),
					'clabe' => $this->input->post('clabe', true),
					'sucursal' => $this->input->post('sucursal', true),
					'convenio' => $this->input->post('convenio', true),
					'referencia' => $this->input->post('referencia', true),
					'foliocfdi' => $this->input->post('foliocfdi', true),
				 

					//'guias_id' => $this->input->post('guia', true),
					'users_id' => $this->session->userdata('admin_id'),
					//'status' => $this->input->post('status'),
					'updated_at' => date('Y-m-d h:m:s'),
				);

				$old_logo = $this->input->post('old_logo');
			 
				$path="uploads/tomas/tenencias/";

				if(!empty($_FILES['image']['name']))
				{
					$this->functions->delete_file($old_logo);
					$result = $this->functions->file_insert($path, 'image', 'image', '9097152');
					if($result['status'] == 1){
						$data['image'] = $path.$result['msg'];
					}

					else{
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/guides/edit/'.$id), 'refresh');
					}
				}

				$datag=array('statustoma' => 1);
				$data0=array('statustoma' => 0);

				$data = $this->security->xss_clean($data);
				$result = $this->take_model->edit_take($data, $id);
				if($result){
					// Registro de actividades 
					$this->activity_model->add_log(26);

					if($idgactual!=$idg)
						{
							$this->take_model->update($datag, $idg, 'guides');
							$this->take_model->update($data0, $idgactual, 'guides');
						}


					$this->session->set_flashdata('success', 'La Toma ha sido actualizada con éxito!');
					redirect(base_url('admin/takes'));
				}
			}
		}
		else{
			$data['take'] = $this->take_model->get_take_by_id_guides($id);
			
			$this->load->view('admin/includes/_header');
			$this->load->view('admin/takes/take_edit', $data);
			$this->load->view('admin/includes/_footer');
		}
	}

	//-----------------------------------------------------------
	//  Ver Toma
	//-------------------------------------------------------
	public function view($id = 0){

		$this->rbac->check_operation_access(); //comprobar el permiso de operación

		$data['brands']=$this->take_model->get_all_simple_brands();
		$data['business']=$this->take_model->get_all_simple_business();
		$data['admins']=$this->take_model->get_all_simple_admins();
		$data['agencys']=$this->take_model->get_all_simple_agencys();
		$data['utilitys']=$this->take_model->get_all_simple_utilitys();
		$data['brands']=$this->take_model->get_all_simple_brands();
		$data['years']=$this->take_model->get_all_simple_years();
		$data['models']=$this->take_model->get_all_simple_models();
		$data['tguias']=$this->take_model->get_all_simple_tguias();

		if($this->input->post('submit')){
			$this->form_validation->set_rules('folio', trans('folio'), 'trim|required');

			if ($this->form_validation->run() == FALSE) {
				$data = array(
					'errors' => validation_errors()
				);
				$this->session->set_flashdata('errors', $data['errors']);
				redirect(base_url('admin/takes/take_edit/'.$id),'refresh');
			}
			else{
				$data = array(
					'valor' => $this->input->post('valor', true),
					'reacondicionamiento' => $this->input->post('reacondicionamiento', true),
					'premio' => $this->input->post('premio', true),
					'equipamiento' => $this->input->post('equipamiento', true),
					'compraautometrica' => $this->input->post('compraautometrica', true),
					'ventaautometrica' => $this->input->post('ventaautometrica', true),
					'km' => $this->input->post('km', true),
					'vin' => $this->input->post('vin', true),
					'folio' => $this->input->post('folio', true),
					'pdvapsr1' => $this->input->post('pdvapsr1a', true),
					'pdlttfac2' => $this->input->post('pdlttfac2a', true),
					'crpdltsi3' => $this->input->post('crpdltsi3a', true),
					'pdvapsi4' => $this->input->post('pdvapsi4a', true),
					'pdvapciyr5' => $this->input->post('pdvapciyr5a', true),
					'udlv6' => $this->input->post('udlv6a', true),
					'pdudlv7' => $this->input->post('pdudlv7', true),
					'pmdvap8' => $this->input->post('pmdvap8a', true),
					'cccr9' => $this->input->post('cccr9a', true),
					'pdvap10' => $this->input->post('pdvap10a', true),
					'ucidlu11' => $this->input->post('ucidlu11a', true),
					'idlu12' => $this->input->post('idlu12a', true),
					'fdume13' => $this->input->post('fdume13a', true),
					'models_id' => $this->input->post('modelo', true),
					'users_id' => $this->session->userdata('admin_id'),
					'tipoguias_id' => $this->input->post('tipoguia', true),
					'agencys_id' => $this->input->post('agencia', true),
					'status' => $this->input->post('status'),
					'updated_at' => date('Y-m-d h:m:s'),
				);
				$data = $this->security->xss_clean($data);
				$result = $this->take_model->edit_model($data, $id);
				if($result){
					// Registro de actividades 
					$this->activity_model->add_log(26);
					$this->session->set_flashdata('success', 'La Guía ha sido actualizada con éxito!');
					redirect(base_url('admin/takes'));
				}
			}
		}
		else{
			$data['take'] = $this->take_model->get_take_by_id_business_view($id);
			
			$this->load->view('admin/includes/_header');
			$this->load->view('admin/takes/take_view', $data);
			$this->load->view('admin/includes/_footer');
		}
	}

	//-----------------------------------------------------------
	//  Eliminar toma
	//-------------------------------------------------------
	public function delete($id = 0)
	{
		$this->rbac->check_operation_access(); // comprobar el permiso de operación
		$this->db->delete('anios', array('id' => $id));
		// Registro de actividades  
		$this->activity_model->add_log(27);
		$this->session->set_flashdata('success', 'La Marca se ha eliminado correctamente!');
		redirect(base_url('admin/takes'));
	}

	//-----------------------------------------------------------
	//  Exportar Tomas a CSV
	//-------------------------------------------------------
	public function export_csv(){ 
	   // nombre del archivo
		$filename = 'takes_'.date('Y-m-d').'.csv'; 
		header("Content-Description: File Transfer"); 
		header("Content-Disposition: attachment; filename=$filename"); 
		header("Content-Type: application/csv; ");

	   // obtener datos
		$model_data = $this->take_model->get_takes_for_csv();

	   // crear archivo
		$file = fopen('php://output', 'w');
		$header = array("ID", "Name", "Created Date"); 
		fputcsv($file, $header);
		foreach ($model_data as $key=>$line){ 
			fputcsv($file,$line); 
		}
		fclose($file); 
		exit; 
	}


	//---------------------------------------------------------------
	//  Exportar Tomas a PDF en general 
	//-------------------------------------------------------
	public function create_takes_pdf()
	{
		$this->load->helper('pdf_tcpdf_helper'); // loaded pdf helper
		$data['all_takes'] = $this->take_model->get_all_simple_takes_pdf();
		$this->load->view('admin/takes/take_tcpdf', $data);

	}

	//---------------------------------------------------------------
	//  Exportar Tomas autorizadas a PDF 
	//-------------------------------------------------------
	public function create_takes_pdf_autorizadas()
	{
		$this->load->helper('pdf_tcpdf_helper'); // loaded pdf helper
		$data['all_takes'] = $this->take_model->get_all_simple_takes_pdf_autorizadas();
		$this->load->view('admin/takes/take_tcpdf', $data);

	}

	//---------------------------------------------------------------
	//  Exportar Tomas pendientes a PDF 
	//-------------------------------------------------------
	public function create_takes_pdf_pendientes()
	{
		$this->load->helper('pdf_tcpdf_helper'); // loaded pdf helper
		$data['all_takes'] = $this->take_model->get_all_simple_takes_pdf_pendientes();
		$this->load->view('admin/takes/take_tcpdf', $data);

	}

	//---------------------------------------------------------------
	//  Exportar Tomas con checklist a PDF 
	//-------------------------------------------------------
	public function create_takes_pdf_checks()
	{
		$this->load->helper('pdf_tcpdf_helper'); // loaded pdf helper
		$data['all_takes'] = $this->take_model->get_all_simple_takes_pdf_checks();
		$this->load->view('admin/takes/take_tcpdf', $data);

	}

	//---------------------------------------------------------------
	//  Exportar Toma Individual a PDF
	//-------------------------------------------------------
	public function create_take_pdf($id = 0)
	{
		$this->load->helper('pdf_tcpdf_helper'); // loaded pdf helper
		$data['take'] = $this->take_model->get_take_by_id_business_pdf($id);
		$this->load->view('admin/takes/take_pdf', $data);

	}


	//---------------------------------------------------------------
	//  Exportar a PDF CFDI
	//-------------------------------------------------------
	public function create_take_pdf_cfdi($id = 0)
	{
		$this->load->helper('pdf_tcpdf_helper'); // loaded pdf helper
		$data['take'] = $this->take_model->get_take_by_id_business_pdf($id);
		$this->load->view('admin/takes/take_cfdi', $data);

	}

	//---------------------------------------------------------------
	//  Exportar a PDF Autenticacion de Documentos
	//-------------------------------------------------------
	public function create_take_pdf_audoctos($id = 0)
	{
		$this->load->helper('pdf_tcpdf_helper'); // loaded pdf helper
		$data['take'] = $this->take_model->get_take_by_id_business_pdf($id);
		$this->load->view('admin/takes/take_auten', $data);

	}

	//---------------------------------------------------------------
	//  Exportar a PDF Autenticacion de Documentos
	//-------------------------------------------------------
	public function create_take_pdf_reten($id = 0)
	{
		$this->load->helper('pdf_tcpdf_helper'); // loaded pdf helper
		$data['take'] = $this->take_model->get_take_by_id_business_pdf($id);
		$this->load->view('admin/takes/take_reten', $data);

	}

		//---------------------------------------------------------------
	//  Exportar a PDF Autenticacion de Documentos
	//-------------------------------------------------------
	public function create_take_pdf_retensin($id = 0)
	{
		$this->load->helper('pdf_tcpdf_helper'); // loaded pdf helper
		$data['take'] = $this->take_model->get_take_by_id_business_pdf($id);
		$this->load->view('admin/takes/take_retensin', $data);

	}

	//---------------------------------------------------------------
	//  Exportar a PDF Autenticacion de Documentos
	//-------------------------------------------------------
	public function create_take_pdf_respon($id = 0)
	{
		$this->load->helper('pdf_tcpdf_helper'); // loaded pdf helper
		$data['take'] = $this->take_model->get_take_by_id_business_pdf($id);
		$this->load->view('admin/takes/take_respon', $data);

	}

	//---------------------------------------------------------------
	//  Exportar a PDF Autenticacion de Documentos
	//-------------------------------------------------------
	public function create_take_pdf_compra($id = 0)
	{
		$this->load->helper('pdf_tcpdf_helper'); // loaded pdf helper
		$data['take'] = $this->take_model->get_take_by_id_business_pdf($id);
		$this->load->view('admin/takes/take_compra', $data);

	}

	//---------------------------------------------------------------
	//  Exportar a PDF Autenticacion de Documentos
	//-------------------------------------------------------
	public function create_take_pdf_aviso($id = 0)
	{
		$this->load->helper('pdf_tcpdf_helper'); // loaded pdf helper
		$data['take'] = $this->take_model->get_take_by_id_business_pdf($id);
		$this->load->view('admin/takes/take_aviso', $data);

	}

	//---------------------------------------------------------------
	//  Exportar a PDF Autenticacion de Documentos
	//-------------------------------------------------------
	public function create_take_pdf_dacionpago($id = 0)
	{
		$this->load->helper('pdf_tcpdf_helper'); // loaded pdf helper
		$data['take'] = $this->take_model->get_take_by_id_business_pdf($id);
		$this->load->view('admin/takes/take_dacionpago', $data);

	}

	//---------------------------------------------------------------
	//  Exportar a PDF Autenticacion de Documentos
	//-------------------------------------------------------
	public function create_take_pdf_solicitudcheque($id = 0)
	{
		$this->load->helper('pdf_tcpdf_helper'); // loaded pdf helper
		$data['take'] = $this->take_model->get_take_by_id_business_pdf($id);
		$this->load->view('admin/takes/take_solicitudcheque', $data);

	}

	//---------------------------------------------------------------
	//  Exportar a PDF Autenticacion de Documentos
	//-------------------------------------------------------
	public function create_take_pdf_calculoret($id = 0)
	{
		$this->load->helper('pdf_tcpdf_helper'); // loaded pdf helper
		$data['take'] = $this->take_model->get_take_by_id_business_pdf($id);
		$this->load->view('admin/takes/take_calculoret', $data);

	}

	//---------------------------------------------------------------
	//  Exportar a PDF Autenticacion de Documentos
	//-------------------------------------------------------
	public function create_take_pdf_reglamento($id = 0)
	{
		$this->load->helper('pdf_tcpdf_helper'); // loaded pdf helper
		$data['take'] = $this->take_model->get_take_by_id_business_pdf($id);
		$this->load->view('admin/takes/take_reglamento', $data);

	}



	


	//---------------------------------------------------
	// Busqueda Avanzada
	//-------------------------------------------------------
	public function advance_search(){

		$this->session->unset_modeldata('model_search_type');
		$this->session->unset_modeldata('model_search_from');
		$this->session->unset_modeldata('model_search_to');

		$data['title'] = 'Advanced Search with Datatable';

		$this->load->view('admin/includes/_header');
		$this->load->view('admin/takes/advance_search', $data);
		$this->load->view('admin/includes/_footer');
	}

	//---------------------------------------------------------------
	//  Funcion de Busquedas
	//-------------------------------------------------------
	function search(){

		$this->session->set_modeldata('model_search_type',$this->input->post('model_search_type'));
		$this->session->set_modeldata('model_search_from',$this->input->post('model_search_from'));
		$this->session->set_modeldata('model_search_to',$this->input->post('model_search_to'));
	}




}


?>