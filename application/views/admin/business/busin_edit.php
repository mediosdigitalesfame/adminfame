  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      <div class="card card-default">
        <div class="card-header">
          <div class="d-inline-block">
            <h3 class="card-title"> <i class="fa fa-pencil"></i>
              &nbsp; <?= trans('edit_business') ?> </h3>
            </div>
            <div class="d-inline-block float-right">
              <a href="<?= base_url('admin/business'); ?>" class="btn btn-success"><i class="fa fa-list"></i> <?= trans('business_list') ?></a>
              <a href="<?= base_url('admin/business/add'); ?>" class="btn btn-success"><i class="fa fa-plus"></i> <?= trans('add_new_business') ?></a>
            </div>
          </div>
          <div class="card-body">

           <!-- For Messages -->
           <?php $this->load->view('admin/includes/_messages.php') ?>
           
           <?php echo form_open(base_url('admin/models/edit/'.$busin['id']), 'class="form-horizontal"' )?> 

           <div class="form-group">
             <div class="row">
              <div class="col-lg-4">
                <div class="input-group">
                  <label for="name" class="col-md-6 control-label"><?= trans('name') ?></label>
                  <div class="col-md-12">
                    <input type="text" name="name" value="<?= $busin['name']; ?>" class="form-control" id="name" placeholder="" >
                  </div>
                </div>
              </div>
              <div class="col-lg-5">
                <div class="input-group">
                 <label for="rsocial" class="col-md-6 control-label"><?= trans('rsocial') ?></label>
                 <div class="col-md-12">
                  <input type="text" name="rsocial" value="<?= $busin['rsocial']; ?>" class="form-control" id="rsocial" placeholder="">
                </div>
              </div>
            </div>
            <div class="col-lg-3">
              <div class="input-group">
                <label for="rfc" class="col-md-6 control-label"><?= trans('rfc') ?></label>
                <div class="col-md-12">
                  <input type="text" name="rfc" value="<?= $busin['rfc']; ?>" class="form-control" id="rfc" placeholder="">
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="form-group">
         <div class="row">
          <div class="col-lg-3">
            <div class="input-group">
              <label for="tel" class="col-md-12 control-label"><?= trans('tel') ?></label>
              <div class="col-md-12">
                <input type="text" name="tel" value="<?= $busin['tel']; ?>" class="form-control" id="tel" placeholder="">
              </div>
            </div>
          </div>
          <div class="col-lg-5">
            <div class="input-group">
              <label for="calle" class="col-md-12 control-label"><?= trans('street') ?></label>
              <div class="col-md-12">
                <input type="text" name="calle" value="<?= $busin['calle']; ?>" class="form-control" id="calle" placeholder="">
              </div>
            </div>
          </div>
          <div class="col-lg-2">
            <div class="input-group">
             <label for="int" class="col-md-12 control-label"><?= trans('int') ?></label>
             <div class="col-md-12">
              <input type="text" name="int" value="<?= $busin['int']; ?>" class="form-control" id="int" placeholder="">
            </div>
          </div>
        </div>
        <div class="col-lg-2">
          <div class="input-group">
            <label for="ext" class="col-md-12 control-label"><?= trans('ext') ?></label>
            <div class="col-md-12">
              <input type="text" name="ext" value="<?= $busin['ext']; ?>" class="form-control" id="ext" placeholder="">
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="form-group">
     <div class="row">
      <div class="col-lg-6">
        <div class="input-group">
         <label for="colonia" class="col-md-12 control-label"><?= trans('suburb') ?></label>
         <div class="col-md-12">
          <input type="text" name="colonia" value="<?= $busin['colonia']; ?>" class="form-control" id="colonia" placeholder="">
        </div>
      </div>
    </div>
    <div class="col-lg-2">
      <div class="input-group">
        <label for="cp" class="col-md-12 control-label"><?= trans('postcode') ?></label>
        <div class="col-md-12">
          <input type="text" name="cp" value="<?= $busin['cp']; ?>" class="form-control" id="cp" placeholder="">
        </div>
      </div>
    </div>
    <div class="col-lg-2">
      <div class="input-group">
        <label for="ciudad" class="col-md-12 control-label"><?= trans('city') ?></label>
        <div class="col-md-12">
          <input type="text" name="ciudad" value="<?= $busin['ciudad']; ?>" class="form-control" id="ciudad" placeholder="">
        </div>
      </div>
    </div>
    <div class="col-lg-2">
      <div class="input-group">
        <label for="estado" class="col-md-12 control-label"><?= trans('state') ?></label>
        <div class="col-md-12">
          <input type="text" name="estado" value="<?= $busin['estado']; ?>"class="form-control" id="estado" placeholder="">
        </div>
      </div>
    </div>
  </div>
</div>

<div class="form-group">
 <div class="row">
  <div class="col-lg-4">
    <div class="input-group">
      <label for="banco" class="col-md-12 control-label"><?= trans('bank') ?></label>
      <div class="col-md-12">
        <input type="text" name="banco" value="<?= $busin['bank']; ?>" class="form-control" id="banco" placeholder="">
      </div>
    </div>
  </div>
  <div class="col-lg-4">
    <div class="input-group">
      <label for="cuenta" class="col-md-12 control-label"><?= trans('bill') ?></label>
      <div class="col-md-12">
        <input type="text" name="cuenta" value="<?= $busin['account']; ?>" class="form-control" id="rsocial" placeholder="">
      </div>
    </div>
  </div>
  <div class="col-lg-4">
    <div class="input-group">
     <label for="pago" class="col-md-12 control-label"><?= trans('pay') ?></label>
     <div class="col-md-12">
      <input type="text" name="pago" value="<?= $busin['pay']; ?>" class="form-control" id="pago" placeholder="">
    </div>
  </div>
</div>
</div>
</div>

<div class="form-group">
  <label for="role" class="col-md-2 control-label"><?= trans('status') ?></label>
  <div class="col-md-12">
    <select name="status" class="form-control">
      <option value=""><?= trans('select_status') ?></option>
      <option value="1" <?= ($busin['status'] == 1)?'selected': '' ?> ><?= trans('active') ?></option>
      <option value="0" <?= ($busin['status'] == 0)?'selected': '' ?>><?= trans('inactive') ?></option>
    </select>
  </div>
</div>
<div class="form-group">
  <div class="col-md-12">
    <input type="submit" name="submit" value="<?= trans('update_model') ?>" class="btn btn-primary pull-right">
  </div>
</div>
<?php echo form_close(); ?>
</div>
<!-- /.box-body -->
</div>  
</section> 
</div>