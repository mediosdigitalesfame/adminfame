<!-- daterange picker -->
<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/daterangepicker/daterangepicker-bs3.css">
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/iCheck/all.css">
<!-- Bootstrap Color Picker -->
<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/colorpicker/bootstrap-colorpicker.min.css">
<!-- Bootstrap time Picker -->
<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/timepicker/bootstrap-timepicker.min.css">
<!-- Select2 -->
<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/select2/select2.min.css">

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <section class="content">
    <div class="card card-default">
      <div class="card-header">
        <div class="d-inline-block">
          <h3 class="card-title"> <i class="fa fa-eye"></i>&nbsp; Vista de Check </h3>
          </div>
          <div class="d-inline-block float-right">
            <a href="<?= base_url('admin/checks'); ?>" class="btn btn-success"><i class="fa fa-list"></i> <?= trans('checks_list') ?></a>
            <a href="<?= base_url('admin/checks/add'); ?>" class="btn btn-success"><i class="fa fa-plus"></i> <?= trans('add_new_check') ?></a>
          </div>
        </div>
        <div class="card-body">

          <div class="row">
            <div class="col-12">
              <h4>
                <i class="fa fa-list-ol"></i> Checklist - <?php echo $checks['folio']; ?>
                <small class="float-right">Fecha de Creación: <?php echo $checks['created_at']; ?></small>
              </h4>
            </div>
            <div class="col-12">
              <h4>
                <small class="float-right">Fecha de Terminación: <?php echo $checks['created_at']; ?></small>
              </h4>
            </div>
          </div>
          <br>

          <div class="row">
            <div class="col-3">
             <div class="card card-success">
              <div class="card-header">
                <h3 class="card-title">Listos</h3>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <label style="display: <?php if($checks['solicitudcfdi'] == 1){echo html_escape('flex');}else{echo html_escape('none');} ?> ;">
                    <input type="radio" name="l1" class="flat-red" checked>
                    Solicitud CFDI - <?php echo $checks['solicitudcfdidates']; ?>
                  </label>

                  <label style="display: <?php if($checks['cambioderol'] == 1){echo html_escape('flex');}else{echo html_escape('none');} ?> ;">
                    <input type="radio" name="l2" class="flat-red" checked>
                    Cambio de Rol - <?php echo $checks['cambioderoldates']; ?>
                  </label>

                  <label style="display: <?php if($checks['cedulafiscal'] == 1){echo html_escape('flex');}else{echo html_escape('none');} ?> ;">
                    <input type="radio" name="l3" class="flat-red" checked>
                    Cedula Fiscal - <?php echo $checks['cedulafiscaldates']; ?>
                  </label>

                  <label style="display: <?php if($checks['cfdisinactividad'] == 1){echo html_escape('flex');}else{echo html_escape('none');} ?> ;">
                    <input type="radio" name="l4" class="flat-red" checked>
                    CFDI Sin Actividad - <?php echo $checks['cfdisinactividaddates']; ?>
                  </label>

                  <label style="display: <?php if($checks['xml'] == 1){echo html_escape('flex');}else{echo html_escape('none');} ?> ;">
                    <input type="radio" name="l5" class="flat-red" checked>
                    XML de CFDI - <?php echo $checks['xmldates']; ?>
                  </label>

                  <label style="display: <?php if($checks['rfc'] == 1){echo html_escape('flex');}else{echo html_escape('none');} ?> ;">
                    <input type="radio" name="l6" class="flat-red" checked>
                    Ratificación RFC - <?php echo $checks['rfcdates']; ?>
                  </label>

                  <label style="display: <?php if($checks['validacioncfdi'] == 1){echo html_escape('flex');}else{echo html_escape('none');} ?> ;">
                    <input type="radio" name="l7" class="flat-red" checked>
                    Validación CFDI - <?php echo $checks['validacioncfdidates']; ?>
                  </label>

                  <label style="display: <?php if($checks['verificacioncfdi'] == 1){echo html_escape('flex');}else{echo html_escape('none');} ?> ;">
                    <input type="radio" name="l8" class="flat-red" checked>
                    Verificación CFDI - <?php echo $checks['verificacioncfdidates']; ?>
                  </label>

                  <label style="display: <?php if($checks['autenticaciondoctos'] == 1){echo html_escape('flex');}else{echo html_escape('none');} ?> ;">
                    <input type="radio" name="l9" class="flat-red" checked>
                    Autenticación Doctos - <?php echo $checks['autenticaciondoctosdates']; ?>
                  </label>

                  <label style="display: <?php if($checks['reportetransunion'] == 1){echo html_escape('flex');}else{echo html_escape('none');} ?> ;">
                    <input type="radio" name="l10" class="flat-red" checked>
                    Reporte TransUnion - <?php echo $checks['reportetransuniondates']; ?>
                  </label>

                  <label style="display: <?php if($checks['noretencion'] == 1){echo html_escape('flex');}else{echo html_escape('none');} ?> ;">
                    <input type="radio" name="l11" class="flat-red" checked>
                    Certificado No Retencion - <?php echo $checks['noretenciondates']; ?>
                  </label>

                  <label style="display: <?php if($checks['secuenciafacturas'] == 1){echo html_escape('flex');}else{echo html_escape('none');} ?> ;">
                    <input type="radio" name="l12" class="flat-red" checked>
                    Facturas del Auto - <?php echo $checks['secuenciafacturasdates']; ?>
                  </label>

                  <label style="display: <?php if($checks['identificacionoficial'] == 1){echo html_escape('flex');}else{echo html_escape('none');} ?> ;">
                    <input type="radio" name="l13" class="flat-red" checked>
                    Id. Oficial - <?php echo $checks['identificacionoficialdates']; ?>
                  </label>

                  <label style="display: <?php if($checks['curp'] == 1){echo html_escape('flex');}else{echo html_escape('none');} ?> ;">
                    <input type="radio" name="l14" class="flat-red" checked>
                    CURP - <?php echo $checks['curpdates']; ?>
                  </label>

                  <label style="display: <?php if($checks['comprobante'] == 1){echo html_escape('flex');}else{echo html_escape('none');} ?> ;">
                    <input type="radio" name="l15" class="flat-red" checked>
                    Comprobante Domicilio - <?php echo $checks['comprobantedates']; ?>
                  </label>

                  <label style="display: <?php if($checks['bajaplacas'] == 1){echo html_escape('flex');}else{echo html_escape('none');} ?> ;">
                    <input type="radio" name="l16" class="flat-red" checked>
                    Baja de Placas - <?php echo $checks['bajaplacasdates']; ?>
                  </label>

                  <label style="display: <?php if($checks['tenencias'] == 1){echo html_escape('flex');}else{echo html_escape('none');} ?> ;">
                    <input type="radio" name="l17" class="flat-red" checked>
                    Historial de Tenencias - <?php echo $checks['tenenciasdates']; ?>
                  </label>

                  <label style="display: <?php if($checks['tarjetacirculacion'] == 1){echo html_escape('flex');}else{echo html_escape('none');} ?> ;">
                    <input type="radio" name="l18" class="flat-red" checked>
                    Tarjeta de Circulacion - <?php echo $checks['tarjetacirculaciondates']; ?>
                  </label>

                  <label style="display: <?php if($checks['responsivacompraventa'] == 1){echo html_escape('flex');}else{echo html_escape('none');} ?> ;">
                    <input type="radio" name="l19" class="flat-red" checked>
                    Responsiva Compraventa - <?php echo $checks['responsivacompraventadates']; ?>
                  </label>

                  <label style="display: <?php if($checks['contratocompraventa'] == 1){echo html_escape('flex');}else{echo html_escape('none');} ?> ;">
                    <input type="radio" name="l20" class="flat-red" checked>
                    Minimal red skin radio - <?php echo $checks['contratocompraventadates']; ?>
                  </label>

                  <label style="display: <?php if($checks['avisoprivacidad'] == 1){echo html_escape('flex');}else{echo html_escape('none');} ?> ;">
                    <input type="radio" name="l21" class="flat-red" checked>
                    Aviso de Privacidad - <?php echo $checks['avisoprivacidaddates']; ?>
                  </label>

                  <label style="display: <?php if($checks['checklistrevision'] == 1){echo html_escape('flex');}else{echo html_escape('none');} ?> ;">
                    <input type="radio" name="l22" class="flat-red" checked>
                    Checklist Revisión - <?php echo $checks['checklistrevisiondates']; ?>
                  </label>

                  <label style="display: <?php if($checks['evaluacionmecanica'] == 1){echo html_escape('flex');}else{echo html_escape('none');} ?> ;">
                    <input type="radio" name="l23" class="flat-red" checked>
                    Evaluación Mecánica - <?php echo $checks['evaluacionmecanicadates']; ?>
                  </label>

                  <label style="display: <?php if($checks['guiaautometrica'] == 1){echo html_escape('flex');}else{echo html_escape('none');} ?> ;">
                    <input type="radio" name="l24" class="flat-red" checked>
                    Guía Autométrica - <?php echo $checks['guiaautometricadates']; ?>
                  </label>

                  <label style="display: <?php if($checks['polizaderecepcion'] == 1){echo html_escape('flex');}else{echo html_escape('none');} ?> ;">
                    <input type="radio" name="l25" class="flat-red" checked>
                    Póliza de Recepción - <?php echo $checks['polizaderecepciondates']; ?>
                  </label>

                  <label style="display: <?php if($checks['cuentasporpagar'] == 1){echo html_escape('flex');}else{echo html_escape('none');} ?> ;">
                    <input type="radio" name="l26" class="flat-red" checked>
                    Cuentas por Pagar - <?php echo $checks['cuentasporpagardates']; ?>
                  </label>

                  <label style="display: <?php if($checks['formatodedacion'] == 1){echo html_escape('flex');}else{echo html_escape('none');} ?> ;">
                    <input type="radio" name="l27" class="flat-red" checked>
                    Dación en Pago - <?php echo $checks['formatodedaciondates']; ?>
                  </label>

                  <label style="display: <?php if($checks['reportederobo'] == 1){echo html_escape('flex');}else{echo html_escape('none');} ?> ;">
                    <input type="radio" name="l28" class="flat-red" checked>
                    Reporte de Robo - <?php echo $checks['reportederobodates']; ?>
                  </label>

                  <label style="display: <?php if($checks['chequedeproveedor'] == 1){echo html_escape('flex');}else{echo html_escape('none');} ?> ;">
                    <input type="radio" name="l29" class="flat-red" checked>
                    Cheque de Proveedor - <?php echo $checks['chequedeproveedordates']; ?>
                  </label>

                  <label style="display: <?php if($checks['solicitudcheque'] == 1){echo html_escape('flex');}else{echo html_escape('none');} ?> ;">
                    <input type="radio" name="l30" class="flat-red" checked>
                    Solicitud de Cheque - <?php echo $checks['solicitudchequedates']; ?>
                  </label>

                  <label style="display: <?php if($checks['edocuenta'] == 1){echo html_escape('flex');}else{echo html_escape('none');} ?> ;">
                    <input type="radio" name="l31" class="flat-red" checked>
                    Edo de Cuenta - <?php echo $checks['edocuentadates']; ?>
                  </label>

                  <label style="display: <?php if($checks['calculoretencion'] == 1){echo html_escape('flex');}else{echo html_escape('none');} ?> ;">
                    <input type="radio" name="l32" class="flat-red" checked>
                    Calculo de Retención - <?php echo $checks['calculoretenciondates']; ?>
                  </label>

                  <label style="display: <?php if($checks['autorizaciontoma'] == 1){echo html_escape('flex');}else{echo html_escape('none');} ?> ;">
                    <input type="radio" name="l33" class="flat-red" checked>
                    autorización toma - <?php echo $checks['autorizaciontomadates']; ?>
                  </label>

                  <label style="display: <?php if($checks['contratoadhesion'] == 1){echo html_escape('flex');}else{echo html_escape('none');} ?> ;">
                    <input type="radio" name="l34" class="flat-red" checked>
                    Contrato de Adhesión - <?php echo $checks['contratoadhesiondates']; ?>
                  </label>

                </div>
              </div>

              <div class="card-footer">
              </div>
            </div>

          </div>
          <div class="col-3">
           <div class="card card-danger">
            <div class="card-header">
              <h3 class="card-title">Pendientes</h3>
            </div>
            <div class="card-body">
              <div class="form-group">
                <label style="display: <?php if($checks['solicitudcfdi'] == 1){echo html_escape('none');}else{echo html_escape('flex');} ?> ;">
                  <input type="radio" name="p1" class="minimal-red" checked>
                  Solicitud CFDI
                </label>

                <label style="display: <?php if($checks['cambioderol'] == 1){echo html_escape('none');}else{echo html_escape('flex');} ?> ;">
                  <input type="radio" name="p2" class="minimal-red" checked>
                  Cambio de Rol
                </label>

                <label style="display: <?php if($checks['cedulafiscal'] == 1){echo html_escape('none');}else{echo html_escape('flex');} ?> ;">
                  <input type="radio" name="p3" class="minimal-red" checked>
                  Cedula Fiscal
                </label>

                <label style="display: <?php if($checks['cfdisinactividad'] == 1){echo html_escape('none');}else{echo html_escape('flex');} ?> ;">
                  <input type="radio" name="p4" class="minimal-red" checked>
                  CFDI Sin Actividad
                </label>

                <label style="display: <?php if($checks['xml'] == 1){echo html_escape('none');}else{echo html_escape('flex');} ?> ;">
                  <input type="radio" name="p5" class="minimal-red" checked>
                  XML de CFDI
                </label>

                <label style="display: <?php if($checks['rfc'] == 1){echo html_escape('none');}else{echo html_escape('flex');} ?> ;">
                  <input type="radio" name="p6" class="minimal-red" checked>
                  Ratificación RFC
                </label>

                <label style="display: <?php if($checks['validacioncfdi'] == 1){echo html_escape('none');}else{echo html_escape('flex');} ?> ;">
                  <input type="radio" name="p7" class="minimal-red" checked>
                  Validación CFDI
                </label>

                <label style="display: <?php if($checks['verificacioncfdi'] == 1){echo html_escape('none');}else{echo html_escape('flex');} ?> ;">
                  <input type="radio" name="p8" class="minimal-red" checked>
                  Verificación CFDI
                </label>

                <label style="display: <?php if($checks['autenticaciondoctos'] == 1){echo html_escape('none');}else{echo html_escape('flex');} ?> ;">
                  <input type="radio" name="p9" class="minimal-red" checked>
                  Autenticación Doctos
                </label>

                <label style="display: <?php if($checks['reportetransunion'] == 1){echo html_escape('none');}else{echo html_escape('flex');} ?> ;">
                  <input type="radio" name="p10" class="minimal-red" checked>
                  Reporte TransUnion
                </label>

                <label style="display: <?php if($checks['noretencion'] == 1){echo html_escape('none');}else{echo html_escape('flex');} ?> ;">
                  <input type="radio" name="p11" class="minimal-red" checked>
                  Certificado No Retencion
                </label>

                <label style="display: <?php if($checks['secuenciafacturas'] == 1){echo html_escape('none');}else{echo html_escape('flex');} ?> ;">
                  <input type="radio" name="p12" class="minimal-red" checked>
                  Facturas del Auto
                </label>

                <label style="display: <?php if($checks['identificacionoficial'] == 1){echo html_escape('none');}else{echo html_escape('flex');} ?> ;">
                  <input type="radio" name="p13" class="minimal-red" checked>
                  Id. Oficial
                </label>

                <label style="display: <?php if($checks['curp'] == 1){echo html_escape('none');}else{echo html_escape('flex');} ?> ;">
                  <input type="radio" name="p14" class="minimal-red" checked>
                  CURP
                </label>

                <label style="display: <?php if($checks['comprobante'] == 1){echo html_escape('none');}else{echo html_escape('flex');} ?> ;">
                  <input type="radio" name="p15" class="minimal-red" checked>
                  Comprobante Domicilio
                </label>

                <label style="display: <?php if($checks['bajaplacas'] == 1){echo html_escape('none');}else{echo html_escape('flex');} ?> ;">
                  <input type="radio" name="p16" class="minimal-red" checked>
                  Baja de Placas
                </label>

                <label style="display: <?php if($checks['tenencias'] == 1){echo html_escape('none');}else{echo html_escape('flex');} ?> ;">
                  <input type="radio" name="p17" class="minimal-red" checked>
                  Historial de Tenencias
                </label>

                <label style="display: <?php if($checks['tarjetacirculacion'] == 1){echo html_escape('none');}else{echo html_escape('flex');} ?> ;">
                  <input type="radio" name="p18" class="minimal-red" checked>
                  Tarjeta de Circulacion
                </label>

                <label style="display: <?php if($checks['responsivacompraventa'] == 1){echo html_escape('none');}else{echo html_escape('flex');} ?> ;">
                  <input type="radio" name="p19" class="minimal-red" checked>
                  Responsiva Compraventa
                </label>

                <label style="display: <?php if($checks['contratocompraventa'] == 1){echo html_escape('none');}else{echo html_escape('flex');} ?> ;">
                  <input type="radio" name="p20" class="minimal-red" checked>
                  Minimal red skin radio
                </label>

                <label style="display: <?php if($checks['avisoprivacidad'] == 1){echo html_escape('none');}else{echo html_escape('flex');} ?> ;">
                  <input type="radio" name="p21" class="minimal-red" checked>
                  Aviso de Privacidad
                </label>

                <label style="display: <?php if($checks['checklistrevision'] == 1){echo html_escape('none');}else{echo html_escape('flex');} ?> ;">
                  <input type="radio" name="p22" class="minimal-red" checked>
                  Checklist Revisión
                </label>

                <label style="display: <?php if($checks['evaluacionmecanica'] == 1){echo html_escape('none');}else{echo html_escape('flex');} ?> ;">
                  <input type="radio" name="p23" class="minimal-red" checked>
                  Evaluación Mecánica
                </label>

                <label style="display: <?php if($checks['guiaautometrica'] == 1){echo html_escape('none');}else{echo html_escape('flex');} ?> ;">
                  <input type="radio" name="p24" class="minimal-red" checked>
                  Guía Autométrica
                </label>

                <label style="display: <?php if($checks['polizaderecepcion'] == 1){echo html_escape('none');}else{echo html_escape('flex');} ?> ;">
                  <input type="radio" name="p25" class="minimal-red" checked>
                  Póliza de Recepción
                </label>

                <label style="display: <?php if($checks['cuentasporpagar'] == 1){echo html_escape('none');}else{echo html_escape('flex');} ?> ;">
                  <input type="radio" name="p26" class="minimal-red" checked>
                  Cuentas por Pagar
                </label>

                <label style="display: <?php if($checks['formatodedacion'] == 1){echo html_escape('none');}else{echo html_escape('flex');} ?> ;">
                  <input type="radio" name="p27" class="minimal-red" checked>
                  Dación en Pago
                </label>

                <label style="display: <?php if($checks['reportederobo'] == 1){echo html_escape('none');}else{echo html_escape('flex');} ?> ;">
                  <input type="radio" name="p28" class="minimal-red" checked>
                  Reporte de Robo
                </label>

                <label style="display: <?php if($checks['chequedeproveedor'] == 1){echo html_escape('none');}else{echo html_escape('flex');} ?> ;">
                  <input type="radio" name="p29" class="minimal-red" checked>
                  Cheque de Proveedor
                </label>

                <label style="display: <?php if($checks['solicitudcheque'] == 1){echo html_escape('none');}else{echo html_escape('flex');} ?> ;">
                  <input type="radio" name="p30" class="minimal-red" checked>
                  Solicitud de Cheque
                </label>

                <label style="display: <?php if($checks['edocuenta'] == 1){echo html_escape('none');}else{echo html_escape('flex');} ?> ;">
                  <input type="radio" name="p31" class="minimal-red" checked>
                  Edo de Cuenta
                </label>

                <label style="display: <?php if($checks['calculoretencion'] == 1){echo html_escape('none');}else{echo html_escape('flex');} ?> ;">
                  <input type="radio" name="p32" class="minimal-red" checked>
                  Calculo de Retención
                </label>

                <label style="display: <?php if($checks['autorizaciontoma'] == 1){echo html_escape('none');}else{echo html_escape('flex');} ?> ;">
                  <input type="radio" name="p33" class="minimal-red" checked>
                  autorización toma
                </label>

                <label style="display: <?php if($checks['contratoadhesion'] == 1){echo html_escape('none');}else{echo html_escape('flex');} ?> ;">
                  <input type="radio" name="p34" class="minimal-red" checked>
                  Contrato de Adhesión
                </label>

              </div>
            </div>
            <div class="card-footer">
            </div>
          </div>
        </div>

        <div class="col-6">
         <div class="card card-info">
          <div class="card-header">
            <h3 class="card-title">Mensajes</h3>
          </div>
          <div class="card-body">
            <div class="form-group">
              <div class="row">
                <div class="col-12 table-responsive">
                 <table id="msj_table" class="table table-bordered table-striped" width="100%">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Usuario</th>
                      <th>Comentarios</th>
                      <th>Fecha</th>
                    </tr>
                  </thead>
                  <tbody>

                    <?php $i=0; foreach($comments as $row): ?>
                    <tr>
                      <td><?= ++$i; ?></td>
                      <td><?= $row['firstname'];?> <?= $row['lastname']; ?> </td>
                      <td><?= $row['comentarios']; ?></td>
                      <td><?= date_time($row['created_at']) ?></td>

                    </tr>
                  <?php endforeach; ?>

                </tbody>
              </table>
            </div>
            <!-- /.col -->
          </div>
        </div>
      </div>
      <div class="card-footer">
      </div>
    </div>
  </div>
</div>
</div>
</div>


</section>
</div>

<!-- Select2 -->
<script src="<?= base_url() ?>assets/plugins/select2/select2.full.min.js"></script>
<!-- InputMask -->
<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src="<?= base_url() ?>assets/plugins/daterangepicker/daterangepicker.js"></script>
<!-- bootstrap color picker -->
<script src="<?= base_url() ?>assets/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="<?= base_url() ?>assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- Page script -->
<!-- iCheck 1.0.1 -->
<script src="<?= base_url() ?>assets/plugins/iCheck/icheck.min.js"></script>

<!-- DataTables -->
<script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?= base_url() ?>assets/plugins/datatables/dataTables.bootstrap4.js"></script>

<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker         : true,
      timePickerIncrement: 30,
      format             : 'MM/DD/YYYY h:mm A'
    })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
    {
      ranges   : {
        'Today'       : [moment(), moment()],
        'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month'  : [moment().startOf('month'), moment().endOf('month')],
        'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      },
      startDate: moment().subtract(29, 'days'),
      endDate  : moment()
    },
    function (start, end) {
      $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
    }
    )

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>

<script>
  $("#forms").addClass('active');
  $("#advanced").addClass('active');
</script>  
