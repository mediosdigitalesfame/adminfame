  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      <div class="card card-default">
        <div class="card-header">
          <div class="d-inline-block">
              <h3 class="card-title"> <i class="fa fa-pencil"></i>
              &nbsp; <?= trans('edit_brand') ?> </h3>
          </div>
          <div class="d-inline-block float-right">
            <a href="<?= base_url('admin/brands'); ?>" class="btn btn-success"><i class="fa fa-list"></i> <?= trans('brands_list') ?></a>
            <a href="<?= base_url('admin/brands/add'); ?>" class="btn btn-success"><i class="fa fa-plus"></i> <?= trans('add_new_brand') ?></a>
          </div>
        </div>
        <div class="card-body">
   
           <!-- For Messages -->
            <?php $this->load->view('admin/includes/_messages.php') ?>
           
            <?php echo form_open(base_url('admin/brands/edit/'.$brand['id']), 'class="form-horizontal"' )?> 
              <div class="form-group">
                <label for="name" class="col-md-2 control-label"><?= trans('name') ?></label>

                <div class="col-md-12">
                  <input type="text" name="name" value="<?= $brand['name']; ?>" class="form-control" id="name" placeholder="">
                </div>
              </div>
              
              <div class="form-group">
                <label for="role" class="col-md-2 control-label"><?= trans('status') ?></label>

                <div class="col-md-12">
                  <select name="status" class="form-control">
                    <option value=""><?= trans('select_status') ?></option>
                    <option value="1" <?= ($brand['status'] == 1)?'selected': '' ?> ><?= trans('active') ?></option>
                    <option value="0" <?= ($brand['status'] == 0)?'selected': '' ?>><?= trans('inactive') ?></option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <div class="col-md-12">
                  <input type="submit" name="submit" value="<?= trans('update_brand') ?>" class="btn btn-primary pull-right">
                </div>
              </div>
            <?php echo form_close(); ?>
        </div>
          <!-- /.box-body -->
      </div>  
    </section> 
  </div>