<!-- DataTables -->

<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/datatables/dataTables.bootstrap4.css"> 



<!-- Content Wrapper. Contains page content -->

<div class="content-wrapper">

  <section class="content">

    <!-- For Messages -->

    <?php $this->load->view('admin/includes/_messages.php') ?>

    <div class="card">

      <div class="card-header">

        <div class="d-inline-block">

          <h3 class="card-title"><i class="fa fa-list"></i>&nbsp; Lista de Leads</h3>

        </div>

        <div class="d-inline-block float-right">

          <?php if($this->rbac->check_operation_permission('kk')): ?>

            <a href="<?= base_url('admin/guides/add'); ?>" class="btn btn-success"><i class="fa fa-plus"></i> <?= trans('add_new_guide') ?></a>

          <?php endif; ?>  

          <?php if($this->rbac->check_operation_permission('kk')): ?>  

            <a href="<?= base_url('admin/guides/create_guides_pdf') ?>" class="btn btn-success"><?= trans('export_as_pdf') ?></a>

          <?php endif; ?>

          <?php if($this->rbac->check_operation_permission('kk')): ?>  

            <a href="<?= base_url('admin/guides/export_csv') ?>" class="btn btn-success"><?= trans('export_as_csv') ?></a>

          <?php endif; ?>

        </div>

      </div>

    </div>



    <!--<div class="card">

      <div class="card-body table-responsive"> 

        <?php echo form_open("/",'id="model_search"') ?>

        <div class="row">

          <div class="col-md-4">

            <label><?= trans('model_type') ?></label><hr style="margin:5px 0px;" />

            <input checked="checked" onchange="user_filter()" name="user_search_type" value="" type="radio"  /> <?= trans('all') ?>&nbsp;&nbsp;&nbsp;

            <input onchange="model_filter()" name="model_search_type" value="1" type="radio"  /> <?= trans('active') ?>&nbsp;&nbsp;&nbsp;

            <input onchange="model_filter()" name="model_search_type" value="0" type="radio"  /> <?= trans('inactive') ?>

          </div>

          <div class="col-md-3">

            <label>Date From:</label><input name="user_search_from" type="text" class="form-control form-control-inline input-medium datepicker" id="" />

          </div>

          <div class="col-md-3"> 

            <label>Date To:</label><input name="user_search_to" type="text" class="form-control form-control-inline input-medium datepicker" id="" /> 

          </div>

          <div class="col-md-2"> 

            <button type="button" style="margin-top:20px;" onclick="user_filter()" class="btn btn-info">Submit</button>

            <a href="<?= base_url('admin/guides/advance_search'); ?>" class="btn btn-danger" style="margin-top:20px;">

              <i class="fa fa-repeat"></i>

            </a>

          </div>

        </div>

        <?php echo form_close(); ?>

      </div>

    </div>-->  





    <div class="card">

      <div class="card-body table-responsive">

  

<?php
 
  switch ($idagen) {
    case 1:
        echo '<iframe width="100%" height="1100" src="https://datastudio.google.com/embed/reporting/58c358bb-8155-4d21-8405-8b3805e27096/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
    case 2:
        echo "No Hay";
        break;
    case 3:
        echo '<iframe width="100%" height="1100" src="https://datastudio.google.com/embed/reporting/9b4c1fc6-8419-4e07-be14-4ab9c56e458d/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 4:
        echo '<iframe width="100%" height="1100" src="https://datastudio.google.com/embed/reporting/5734b2bb-867a-4477-ba9d-0105965968d6/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 5:
        echo '<iframe width="100%" height="1050" src="https://datastudio.google.com/embed/reporting/33c5a707-1bcd-47a2-89a7-7d6dbd5a4bde/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 6:
        echo "No Hay";
        break;
        case 7:
        echo '<iframe width="100%" height="1100" src="https://datastudio.google.com/embed/reporting/d44570bc-6c7c-48ef-97dc-9c79c132e038/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 8:
        echo '<iframe width="100%" height="1100" src="https://datastudio.google.com/embed/reporting/16478f4e-b6da-4096-9917-ba69e80b163c/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 9:
        echo '<iframe width="100%" height="1100" src="https://datastudio.google.com/embed/reporting/a83a8a52-b251-4ae2-8f77-fd09540d5754/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 10:
        echo '<iframe width="100%" height="1100" src="https://datastudio.google.com/embed/reporting/67ee0c25-97a6-434e-a8bb-fea2a6778c15/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 11:
        echo '<iframe width="100%" height="1100" src="https://datastudio.google.com/embed/reporting/85c66183-2ef5-4ba2-84b3-f86d630e34c1/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 12:
        echo '<iframe width="100%" height="1100" src="https://datastudio.google.com/embed/reporting/17954632-7d31-4637-a20b-57f427f334e2/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 13:
        echo '<iframe width="100%" height="1100" src="https://datastudio.google.com/embed/reporting/f7f905c3-f5c3-4313-8b38-f0cbb71aba75/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 14:
        echo '<iframe width="100%" height="1100" src="https://datastudio.google.com/embed/reporting/2312ff99-3900-4655-a5fa-18db742b9889/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 15:
        echo '<iframe width="100%" height="1100" src="https://datastudio.google.com/embed/reporting/344eb310-ee2b-4d9f-a1ce-a03f7bf06422/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 16:
        echo '<iframe width="100%" height="1100" src="https://datastudio.google.com/embed/reporting/06188a4f-e1d4-4f86-9f65-f531cb2637ea/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 17:
        echo '<iframe width="100%" height="1100" src="https://datastudio.google.com/embed/reporting/4543d7ae-a768-4418-aeb6-272684489d85/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 18:
        echo '<iframe width="100%" height="1100" src="https://datastudio.google.com/embed/reporting/448f60fe-da77-4884-bfb4-b8d2785a9f18/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 19:
        echo '<iframe width="100%" height="1100" src="https://datastudio.google.com/embed/reporting/aa5029f1-7b3f-4fec-9d25-4eb90c74be35/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 20:
        echo '<iframe width="100%" height="1100" src="https://datastudio.google.com/embed/reporting/1a503e4c-a8c7-4856-aeec-97449d82c9a0/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 21:
        echo '<iframe width="100%" height="1100" src="https://datastudio.google.com/embed/reporting/e0ae5ecc-5573-4b34-88a8-91067e319d2c/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 22:
        echo '<iframe width="100%" height="1100" src="https://datastudio.google.com/embed/reporting/f4fc04d8-92a9-4e85-9b40-386dea2a2890/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 23:
        echo '<iframe width="100%" height="1100" src="https://datastudio.google.com/embed/reporting/228cdede-02e5-4ffa-9255-4a8acbaad8fa/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 24:
        echo '<iframe width="100%" height="1100" src="https://datastudio.google.com/embed/reporting/12fd0085-f918-4f6f-8981-a1dc051fa8a8/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 25:
        echo '<iframe width="100%" height="1100" src="https://datastudio.google.com/embed/reporting/b3b75a60-ed64-4320-a89d-9b6209f87db8/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 26:
        echo '<iframe width="100%" height="1100" src="https://datastudio.google.com/embed/reporting/56d4ff9a-e7f1-468f-88b1-a73bf4f5fb90/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 27:
        echo '<iframe width="100%" height="1100" src="https://datastudio.google.com/embed/reporting/65806042-27bf-4b62-b54b-281ad96cc308/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 28:
        echo '<iframe width="100%" height="1100" src="https://datastudio.google.com/embed/reporting/875c493d-51a4-4a02-ab4d-774d8b3f0ce4/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 29:
        echo '<iframe width="100%" height="1100" src="https://datastudio.google.com/embed/reporting/5a79f5b3-0b37-4351-bc5c-0cb7884eff57/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 30:
        echo '<iframe width="100%" height="1100" src="https://datastudio.google.com/embed/reporting/a1b94aaa-046b-4fcd-840b-25061c3cf8dc/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
         case 31:
        echo 'No Tiene';
        break;
         case 32:
        echo '<iframe width="100%" height="1100" src="https://datastudio.google.com/embed/reporting/6e466dd2-3766-48ae-a9f4-3b656cafd187/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
         case 33:
        echo '<iframe width="100%" height="1100" src="https://datastudio.google.com/embed/reporting/769bd547-51e8-496d-8219-958d19b3bca6/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
         case 34:
        echo '<iframe width="100%" height="1100" src="https://datastudio.google.com/embed/reporting/5ed159b8-e32f-4e72-a6c6-d992ddb887b9/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
         case 35:
        echo '<iframe width="100%" height="1100" src="https://datastudio.google.com/embed/reporting/e3b37dc1-2ab0-47d6-8d07-bc50ca8c034e/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
         case 36:
        echo '<iframe width="100%" height="1100" src="https://datastudio.google.com/embed/reporting/ead26d2e-9e22-4352-b8e7-099fc347f005/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
         case 37:
        echo '<iframe width="100%" height="1100" src="https://datastudio.google.com/embed/reporting/fac3a6cb-c916-484f-b939-b01088c2322e/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
         case 38:
        echo "No Tiene";
        break;
         case 39:
        echo 'No Tiene';
        break;
         case 40:
        echo "No Tiene";
        break;
         case 41:
        echo "No Tiene";
        break;
         case 42:
        echo "No Tiene";
        break;

        case 91:
        echo '<iframe width="100%" height="1100" src="https://datastudio.google.com/embed/reporting/946732d4-475b-4242-89c7-3f3335f70b88/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
         case 92:
        echo '<iframe width="100%" height="1100" src="https://datastudio.google.com/embed/reporting/5ff3ebc5-7240-4518-a2a8-130e17a6629f/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 93:
        echo '<iframe width="100%" height="1100" src="https://datastudio.google.com/embed/reporting/233dea93-a455-4fc2-b5f9-4faa5017b6db/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 94:
        echo '<iframe width="100%" height="1100" src="https://datastudio.google.com/embed/reporting/7693a1e9-2055-4512-b453-6e3fe047b8a2/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 95:
        echo '<iframe width="100%" height="1100" src="https://datastudio.google.com/embed/reporting/29da769c-9ba2-4d78-b3a8-37db2fa8cb5e/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 96:
        echo '<iframe width="100%" height="1100" src="https://datastudio.google.com/embed/reporting/05f082e7-94d6-47a0-988b-a4bc83e206aa/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;

        case 100:
        echo '<iframe width="100%" height="1467" src="https://datastudio.google.com/embed/reporting/c1ad6714-2872-4ead-b1f4-b35de796f59d/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>
        <iframe width="100%" height="1100" src="https://datastudio.google.com/embed/reporting/58c358bb-8155-4d21-8405-8b3805e27096/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;




}
?>



       

 

        <!--<table id="na_datatable" class="table table-bordered table-striped" width="100%">

          <thead>

            <tr>

              <th>#<?= trans('id') ?></th>

              <th>Nombre</th>

              

              <th><?= trans('status') ?></th>

              <th width="100" class="text-right"><?= trans('action') ?></th>

            </tr>

          </thead>

        </table>-->

      </div>

    </div>














  </section>  

</div>



<!-- bootstrap datepicker -->

<!-- datepicker -->

<script src="<?= base_url() ?>assets/plugins/datepicker/bootstrap-datepicker.js"></script>

<script>

  $('.datepicker').datepicker({

    autoclose: true

  });

</script>

<!-- DataTables -->

<script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.js"></script>

<script src="<?= base_url() ?>assets/plugins/datatables/dataTables.bootstrap4.js"></script>



<script>

  //---------------------------------------------------

  var table = $('#na_datatable').DataTable( {

    "processing": true,

    "serverSide": false,

    "ajax": "<?=base_url('admin/leads/datatable_json')?>",

    "order": [[0,'desc']],

    "columnDefs": [

    { "targets": 0, "name": "id", 'searchable':true, 'orderable':true},

    { "targets": 1, "name": "name", 'searchable':true, 'orderable':true},

     

    { "targets": 2, "name": "Action", 'searchable':false, 'orderable':false,'width':'100px'}

    ]

  });

</script>



<script>

  //---------------------------------------------------

  function model_filter()

  {

    var _form = $("#model_search").serialize();

    $.ajax({

      data: _form,

      type: 'post',

      url: '<?php echo base_url();?>admin/guides/search',

      async: true,

      success: function(output){

        table.ajax.reload( null, false );

      }

    });

  }

</script>



<script type="text/javascript">

  $("body").on("change",".tgl_checkbox",function(){

    console.log('checked');

    $.post('<?=base_url("admin/guides/change_status")?>',

    {

      '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>',

      id : $(this).data('id'),

      status : $(this).is(':checked') == true?1:0

    },

    <?php if($this->rbac->check_operation_permission('add')): ?>

      function(data){$.notify("El Status cambió con éxito", "success");}

      <?php else: ?>

        function(data){$.notify("No Tienes Permisos", "warn");}

      <?php endif; ?>

      );

  });

</script>





