<?php
$fecha = $guide['created_at']; 

$month = date('M', strtotime($fecha));
$year = date('Y', strtotime($fecha));
$day = date('d', strtotime($fecha));

$partes = explode('-', $month); 
$_fecha = "$year.$month.$day";

$fmt = new NumberFormatter('en_US', NumberFormatter::CURRENCY);
$formatterES = new NumberFormatter("es-ES", NumberFormatter::SPELLOUT);

$izquierda = intval(floor($guide['pdlttfac2']));
$derecha = intval(($guide['pdlttfac2'] - floor($guide['pdlttfac2'])) * 100);

$foliouno = $guide['id'];

$foliocero = str_pad($foliouno, 9, "0", STR_PAD_LEFT);

 

 switch ($guide['tipoguias_id'])
    {
      case '1': 
      $tipoguia = "COMPRA";
      $tipopersona = "PERSONA FISICA O MORAL QUE FACTURA";
      break;

      case '2': 
      $tipoguia = "PROMEDIO";
      $tipopersona = "PERSONA FISICA O MORAL QUE FACTURA";
      break;

      case '3': 
      $tipoguia = "VENTA";
      $tipopersona = "PERSONA FISICA O MORAL QUE FACTURA";
      break;

      case '4': 
      $tipoguia = "COMPRA";
      $tipopersona = "PERSONA SIN OBLIGACION FISCAL";
      break;

      case '5': 
      $tipoguia = "PROMEDIO";
      $tipopersona = "PERSONA SIN OBLIGACION FISCAL";
      break;

      case '6': 
      $tipoguia = "VENTA";
      $tipopersona = "PERSONA SIN OBLIGACION FISCAL";
      break;
}

$html = '

<table  border="0">
  <tr bgcolor="gray" align="center">
    <td><h2><font >AUTORIZACIÓN PARA LA TOMA DE UNIDADES SEMINUEVAS</font></h2></td><br>
  </tr>
  <tr align="center">
    <td> </td><br>
  </tr>
  <tr align="left">
    <td><h3><font color="black">Agencia: '.$guide['agname'].' - '.$guide['bursocial'].'</font></h3></td><br>
  </tr>
  <tr align="center">
    <td> </td><br>
  </tr>
  <tr align="right">
    <td>'.$guide['agciudad'].', ' .$guide['agestado'].' a: '.$day.' de ' .$month.' del ' .$year.'</td>
  </tr>
  <tr align="right">
    <td>Folio: '.$guide['folio'].'-'.$foliocero.'</td>
  </tr>
  <tr align="right">';
  if($guide['status']==1){$html.= '<td> <i style="color:#15A615;">Autorizada!</i> </td>';}else{$html.= '<td> <i style="color:#CC0000;">Pendiente!</i> </td>';}

  $html .= '
  </tr>
</table>
 
<br>
<br>
<br>
<br>

 <table  border="1">
  <tr>
      <td width="11%" align="center" > Vehiculo </td>
      <td width="11%" align="center"> Version</td>
      <td width="11%" align="center"> Modelo</td>
      <td width="11%" align="center"> Color</td>
      <td width="11%" align="center"> KMS</td>
      <td width="11%" align="center"> Placas</td>
      <td width="11%" align="center"> Edo.</td>
      <td width="23%" align="center"> VIN</td>
    </tr>
    <tr>
      <td align="center" >'.$guide['brname'].'</td>
      <td align="center">'.$guide['versiontxt'].'</td>
      <td align="center">'.$guide['modelotxt'].'</td>
      <td align="center">'.$guide['color'].'</td>
       <td align="center">'.$guide['km'].'</td>
       <td align="center">'.$guide['placas'].'</td>
       <td align="center">'.$guide['edo'].'</td>
       <td align="center">'.$guide['vin'].'</td>
    </tr>
</table>

<table border="0" class="table table-bordered">
  <tbody>

    <tr>
      <td width="30%" align="right" >   </td>
      <td  width="2%"> </td>
      <td> </td>
    </tr>
    <tr>
      <td width="30%" align="right"> Proveedor / Cliente: </td>
      <td  width="2%"> </td>
      <td>'.$guide['proveecliente'].' </td>
    </tr>
     <tr>
      <td width="30%" align="right"> Teléfono: </td>
      <td  width="2%"> </td>
      <td>'.$guide['telefono'].'</td>
    </tr>
    <tr>
    <td width="30%" align="right"> Version: </td> 
    <td  width="2%"> </td>
    <td>'.$guide['modelotxt'].' - '.$guide['versiontxt'].'</td> 
  </tr>
  <tr>
    <td width="30%" align="right"> Situación Fiscal: </td> 
    <td  width="2%"> </td>
    <td>'.$guide['regname'].'</td> 
  </tr>
  <tr>
    <td width="30%" align="right"> Observaciones: </td> 
    <td  width="2%"> </td>
    <td>'.$guide['observaciones'].'</td>  
  </tr>

  </tbody>
</table>

<br>
<br>
<br>
<br>

<table  border="0">
	<tr bgcolor="gray" align="center">
		<td><h2><font >IMPORTES EN GUIA AUTOMETRICA</font></h2></td> 
	</tr>
	<tr align="center">
		<td>  </td> 
	</tr>
</table>

<br>
<br>

 <table  border="1">
  <tr>
      <td rowspan="2" width="10%" valign="middle" align="center" > <div style="vertical-align: middle;"> COMPRA </div> </td>
      <td width="22%" align="center"> Auto</td>
      <td width="15%" align="center"> Precio Compra Libro</td>
      <td width="15%" align="center"> Equipamiento</td>
      <td width="15%" align="center"> Premio/Castigo</td>
      <td rowspan="2" width="3%" align="center"> <div style="vertical-align: middle;"> = </div> </td>
      <td width="20%" align="center"> Precio de Compra Guía</td>
    </tr>
    <tr>
      <td align="center">'.$guide['modelotxt'].' - '.$guide['versiontxt'].'</td>
      <td align="center">'.$fmt->formatCurrency($guide['compraautometrica'], "USD") .'</td>
      <td align="center">'.$fmt->formatCurrency($guide['equipamiento'], "USD") .'</td>
       <td align="center">'.$fmt->formatCurrency($guide['premio'], "USD") .'</td>
       <td align="center">'.$fmt->formatCurrency($guide['compraautometrica'], "USD") .'</td>
    </tr>
</table>

<br>
<br>

 <table  border="1">
  <tr>
      <td rowspan="2" width="10%" valign="middle" align="center" > <div style="vertical-align: middle;"> VENTA </div> </td>
      <td width="22%" align="center"> Auto</td>
      <td width="15%" align="center"> Precio Venta Libro</td>
      <td width="15%" align="center"> Equipamiento</td>
      <td width="15%" align="center"> Premio/Castigo</td>
      <td rowspan="2" width="3%" align="center"> <div style="vertical-align: middle;"> = </div> </td>
      <td width="20%" align="center"> Precio de Venta Guía</td>
    </tr>
    <tr>
      <td align="center">'.$guide['modelotxt'].' - '.$guide['versiontxt'].'</td>
      <td align="center">'.$fmt->formatCurrency($guide['ventaautometrica'], "USD") .'</td>
      <td align="center">'.$fmt->formatCurrency($guide['equipamiento'], "USD") .'</td>
       <td align="center">'.$fmt->formatCurrency($guide['premio'], "USD") .'</td>
       <td align="center">'.$fmt->formatCurrency($guide['ventaautometrica'], "USD") .'</td>
    </tr>
</table>

<br>
<br>
<br>
<br>

<table  border="0">
  <tr>
      <td border="1" width="30%" align="center"> Importe de Compra</td>
      <td width="40%" align="center"> </td>
      <td border="1" width="30%" align="center"> Importe de Venta (Guía Rapida)</td>
    </tr>
    <tr>
      <td border="1" align="center">'.$fmt->formatCurrency($guide['equipamiento'], "USD") .'</td>
      <td align="center"> </td>
      <td border="1" align="center">'.$fmt->formatCurrency($guide['ventaautometrica'], "USD") .'</td>
    </tr>
</table>

<br>
<br>
<br>
<br>
<br>
<br>

<table border="0" align="center" class="table table-bordered">
  <tbody>  
        <tr>
			<td>
				_________________________________<br>
				<font color="#0A6ACF">GERENCIA SEMINUEVOS</font>
			</td>
			<td>
				_________________________________<br>
				<font color="#0A6ACF">GERENCIA GENERAL</font>
			</td>
			<td>
				_________________________________<br>
				<font color="#0A6ACF">DIRECCIÓN REGIONAL</font>
			</td>
		</tr>
  </tbody>
</table>';

tcpdf();
$obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
header('Content-type: application/pdf');
$obj_pdf->SetCreator(PDF_CREATOR);
$title = "Autorización - ".$guide['folio'];
$obj_pdf->SetTitle($title);
$obj_pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $title, PDF_HEADER_STRING);
$obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$obj_pdf->SetDefaultMonospacedFont('helvetica');
$obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$obj_pdf->SetFont('helvetica', '', 9);
$obj_pdf->setFontSubsetting(false);
$obj_pdf->AddPage();
ob_start();
    // podemos tener cualquier parte de la vista aquí como HTML, PHP, etc.
$content = ob_get_contents();
ob_end_clean();
$obj_pdf->writeHTML($content, true, false, true, false, '');
$obj_pdf->writeHTML($html, true, false, true, false, '');

$filename = 'Autorización - '.$guide['folio'];

$obj_pdf->Output($filename . '.pdf', 'D');




?>