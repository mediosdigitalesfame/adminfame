<?php
class Autori_model extends CI_Model{

	public function __construct()
	{
		parent::__construct();
	}

	public function add_autori($data){
		$this->db->insert('autoris', $data);
		return true;
	}

		//---------------------------------------------------
		// obtener todas los agencias para el procesamiento de tablas de datos del lado del servidor (basado en ajax)
		//-----------------------------------------------------
	public function get_all_autoris_brands(){
		$this->db->select('m.*, b.name as bname, rsocial');
		$this->db->join('business b','b.id=m.business_id', 'LEFT');
			//$this->db->where('status',1);
		return $this->db->get('autoris m')->result_array();
	}

		//---------------------------------------------------
		// obtener todas los guias con sus marcas para el procesamiento de tablas de datos del lado del servidor (basado en ajax)
		//-----------------------------------------------------
	public function get_all_autoris(){
		$this->db->select('au.*');
		//$this->db->where('status',1);
		//$this->db->join('models m','m.id=g.models_id', 'LEFT');
		//$this->db->join('agencys a','a.id=g.agencys_id', 'LEFT');
		$this->db->join('checklist ch','ch.id=au.checklist_id', 'LEFT');
		return $this->db->get('autoris au')->result_array();
	}

	public function get_all_autoris_peruser($idusr){
		$this->db->select('au.*');
		$this->db->join('checklist ch','ch.id=au.checklist_id', 'LEFT');
		//$this->db->select('g.*,a.name as agname');
		//$this->db->where('users_id',$idusr);
		//$this->db->join('models m','m.id=g.models_id', 'LEFT');
		//$this->db->join('agencys a','a.id=g.agencys_id', 'LEFT');
		return $this->db->get('autoris au')->result_array();
	}

	//---------------------------------------------------
		// obtener todas los guias autorizadas con sus marcas para el procesamiento de tablas de datos del lado del servidor (basado en ajax)
		//-----------------------------------------------------
	public function get_all_autoris_autorizadas(){
		$this->db->select('g.*, a.name as agname');
		$this->db->where('g.status',1);
		$this->db->join('models m','m.id=g.models_id', 'LEFT');
		$this->db->join('agencys a','a.id=g.agencys_id', 'LEFT');
		return $this->db->get('autoris g')->result_array();
	}

	public function get_all_autoris_peruser_autorizadas($idusr){
		$this->db->select('g.*,a.name as agname');
		$this->db->where('users_id',$idusr);
		$this->db->where('g.status',1);
		$this->db->join('models m','m.id=g.models_id', 'LEFT');
		$this->db->join('agencys a','a.id=g.agencys_id', 'LEFT');
		return $this->db->get('autoris g')->result_array();
	}

	//---------------------------------------------------
		// obtener todas los guias pendientes con sus marcas para el procesamiento de tablas de datos del lado del servidor (basado en ajax)
		//-----------------------------------------------------
	public function get_all_autoris_pendientes(){
		$this->db->select('g.*, a.name as agname');
		$this->db->where('g.status',0);
		$this->db->join('models m','m.id=g.models_id', 'LEFT');
		$this->db->join('agencys a','a.id=g.agencys_id', 'LEFT');
		return $this->db->get('autoris g')->result_array();
	}

	public function get_all_autoris_peruser_pendientes($idusr){
		$this->db->select('g.*,a.name as agname');
		$this->db->where('users_id',$idusr);
		$this->db->where('g.status',0);
		$this->db->join('models m','m.id=g.models_id', 'LEFT');
		$this->db->join('agencys a','a.id=g.agencys_id', 'LEFT');
		return $this->db->get('autoris g')->result_array();
	}

	//---------------------------------------------------
		// obtener todas los guias tomadas con sus marcas para el procesamiento de tablas de datos del lado del servidor (basado en ajax)
		//-----------------------------------------------------
	public function get_all_autoris_tomadas(){
		$this->db->select('g.*, a.name as agname');
		$this->db->where('g.status',1);
		$this->db->where('g.statustoma',1);
		$this->db->join('models m','m.id=g.models_id', 'LEFT');
		$this->db->join('agencys a','a.id=g.agencys_id', 'LEFT');
		return $this->db->get('autoris g')->result_array();
	}

	public function get_all_autoris_peruser_tomadas($idusr){
		$this->db->select('g.*,a.name as agname');
		$this->db->where('users_id',$idusr);
		$this->db->where('g.status',1);
		$this->db->where('g.statustoma',1);
		$this->db->join('models m','m.id=g.models_id', 'LEFT');
		$this->db->join('agencys a','a.id=g.agencys_id', 'LEFT');
		return $this->db->get('autoris g')->result_array();
	}


//---------------------------------------------------
//obtener el registro de la autorizacion para hacer el PDF
//-----------------------------------------------------
	public function get_autori_by_id_business_pdf($id){
		$this->db->where('gu.id', $id);
		$this->db->select('au.*, gu.*, st.tel as sttel, an.name as anname, br.name as brname, ag.name as agname, ag.calle as agcalle, ag.ext as agext, ag.colonia as agcolonia, ag.cp as agcp, ag.ciudad as agciudad, ag.estado as agestado, ag.url as agurl, ag.urlp as agurlp, ag.tel as agtel, tg.version, tg.persona, br.name as brname, an.name as anname, bu.rsocial as bursocial, bu.calle as bucalle, bu.int as buint, bu.colonia as bucolonia, bu.cp as bucp, bu.ciudad as buciudad, bu.estado as buestado, st.colorext, st.placasbaja, st.edoemisor, st.nombrescfdi, st.apellido1, st.apellido2, re.name as regname
			');
		$this->db->join('checklist ch','ch.id=au.checklist_id', 'LEFT');
		$this->db->join('seminuevostomados st','st.id=ch.seminuevostomados_id', 'LEFT');
		$this->db->join('guides gu','gu.id=st.guias_id', 'LEFT');
		$this->db->join('regimenes re','re.id=st.regimenes_id', 'LEFT');

		$this->db->join('agencys ag','ag.id=gu.agencys_id', 'LEFT');
		$this->db->join('business bu','bu.id=ag.business_id', 'LEFT');
		$this->db->join('tipoguias tg','tg.id=gu.tipoguias_id', 'LEFT');
		//$this->db->join('models mo','mo.id=gu.models_id', 'LEFT');
		$this->db->join('brands br','br.id=gu.brands_id', 'LEFT');
		$this->db->join('anios an','an.id=gu.anio_id', 'LEFT');
		$this->db->from('autoris au');
		$query = $this->db->get();
		return $result = $query->row_array();
	}


		//---------------------------------------------------
		// Obtener información de la agencia por ID
		//-----------------------------------------------------
	public function get_autori_by_id($id){
		$query = $this->db->get_where('autoris', array('id' => $id));
		return $result = $query->row_array();
	}

	//---------------------------------------------------
		// Obtener información de la agencia y sus tablas padre por ID
		//-----------------------------------------------------
	public function get_autori_by_id_business($id){
		$this->db->where('au.id', $id);
		$this->db->select('au.*, gu.*, mo.name as moname, br.name as brname, an.name as anname');
		$this->db->join('checklist ch','ch.id=au.checklist_id', 'LEFT');
		$this->db->join('seminuevostomados st','st.id=ch.seminuevostomados_id', 'LEFT');
		$this->db->join('guides gu','gu.id=st.guias_id', 'LEFT');

		$this->db->join('models mo','mo.id=gu.models_id', 'LEFT');
		$this->db->join('brands br','br.id=mo.brands_id', 'LEFT');
		$this->db->join('anios an','an.id=gu.anio_id', 'LEFT');
		$this->db->from('autoris au');
		$query = $this->db->get();
		return $result = $query->row_array();

	}

		//---------------------------------------------------
		// Obtener información de la agencia y sus tablas padre por ID
		//-----------------------------------------------------
	public function get_autori_by_id_business_view($id){
		$this->db->where('au.id', $id);
		$this->db->select('au.*, ag.name as agname, ag.tel as agtel, gu.folio, mo.name as moname, mo.version as moversion, tg.version, tg.persona, br.name as brname, an.name as anname, gu.pdudlv7, gu.km, gu.modelotxt, st.versionvehi, gu.compraautometrica, gu.ventaautometrica, gu.status as gustatus, ch.status as chstatus, st.status as statustoma');
		$this->db->join('checklist ch','ch.id=au.checklist_id', 'LEFT');
		$this->db->join('seminuevostomados st','st.id=ch.seminuevostomados_id', 'LEFT');
		$this->db->join('guides gu','gu.id=st.guias_id', 'LEFT');
		$this->db->join('agencys ag','ag.id=gu.agencys_id', 'LEFT');
		$this->db->join('tipoguias tg','tg.id=gu.tipoguias_id', 'LEFT');
		$this->db->join('models mo','mo.id=gu.models_id', 'LEFT');
		$this->db->join('brands br','br.id=mo.brands_id', 'LEFT');
		$this->db->join('anios an','an.id=mo.anios_id', 'LEFT');
		//$this->db->join('agencys ag','ag.id=gu.agencys_id', 'LEFT');
		//$this->db->join('models mo','mo.id=gu.models_id', 'LEFT');
		//$this->db->join('brands br','br.id=mo.brands_id', 'LEFT');
		//$this->db->join('anios an','an.id=mo.anios_id', 'LEFT');
		//$this->db->join('tipoguias tg','tg.id=gu.tipoguias_id', 'LEFT');
		$this->db->from('autoris au');
		$query = $this->db->get();
		return $result = $query->row_array();
	}

		//---------------------------------------------------
		// Editar registro de agencia
		//-----------------------------------------------------
	public function edit_autori($data, $id){
		$this->db->where('id', $id);
		$this->db->update('autoris', $data);
		return true;
	}


	function subir($id,$imagen)
	{
		$data = array(

			'favicon' => $imagen
		);

		$this->db->where('id', $id);
		$this->db->update('autoris', $data);
		return true;
	}

		//---------------------------------------------------
		// Cambiar status de modelo
		//-----------------------------------------------------
	function change_status()
	{		
		$this->db->set('status', $this->input->post('status'));
		$this->db->where('id', $this->input->post('id'));
		$this->db->update('autoris');
	} 

		//---------------------------------------------------
		// obtener todos los registros de guias
		//-----------------------------------------------------
	public function get_all_simple_autoris_pdf(){
		//$this->db->where('status', 1);
		$this->db->select('au.*, ag.name as agname, ag.tel as agtel, gu.folio, mo.name as moname, mo.version as moversion, tg.version, tg.persona, br.name as brname, an.name as anname, gu.pdudlv7, gu.km, gu.modelotxt, st.versionvehi, gu.compraautometrica, gu.ventaautometrica, gu.status as gustatus, ch.status as chstatus, st.status as statustoma');
		$this->db->join('checklist ch','ch.id=au.checklist_id', 'LEFT');
		$this->db->join('seminuevostomados st','st.id=ch.seminuevostomados_id', 'LEFT');
		$this->db->join('guides gu','gu.id=st.guias_id', 'LEFT');
		$this->db->join('agencys ag','ag.id=gu.agencys_id', 'LEFT');
		$this->db->join('tipoguias tg','tg.id=gu.tipoguias_id', 'LEFT');
		$this->db->join('models mo','mo.id=gu.models_id', 'LEFT');
		$this->db->join('brands br','br.id=mo.brands_id', 'LEFT');
		$this->db->join('anios an','an.id=mo.anios_id', 'LEFT');
		$this->db->from('autoris au');
		$query = $this->db->get();
		return $result = $query->result_array();
	}




	 

	//---------------------------------------------------
		// obtener todos los registros de guias autorizadas
		//-----------------------------------------------------
	public function get_all_simple_autoris_pdf_autorizadas(){
		//$this->db->where('status', 1);
		$this->db->select('gu.*, ag.name as agname, ag.tel as agtel, tg.version, tg.persona, br.name as brname, an.name as anname, mo.name as moname, mo.version as moversion ');
		$this->db->where('gu.status',1);
		$this->db->join('agencys ag','ag.id=gu.agencys_id', 'LEFT');
		$this->db->join('models mo','mo.id=gu.models_id', 'LEFT');
		$this->db->join('brands br','br.id=mo.brands_id', 'LEFT');
		$this->db->join('anios an','an.id=mo.anios_id', 'LEFT');
		$this->db->join('tipoguias tg','tg.id=gu.tipoguias_id', 'LEFT');
		$this->db->from('autoris gu');
		$query = $this->db->get();
		return $result = $query->result_array();
	}

	//---------------------------------------------------
		// obtener todos los registros de guias autorizadas
		//-----------------------------------------------------
	public function get_all_simple_autoris_pdf_pendientes(){
		//$this->db->where('status', 1);
		$this->db->select('gu.*, ag.name as agname, ag.tel as agtel, tg.version, tg.persona, br.name as brname, an.name as anname, mo.name as moname, mo.version as moversion ');
		$this->db->where('gu.status',0);
		$this->db->join('agencys ag','ag.id=gu.agencys_id', 'LEFT');
		$this->db->join('models mo','mo.id=gu.models_id', 'LEFT');
		$this->db->join('brands br','br.id=mo.brands_id', 'LEFT');
		$this->db->join('anios an','an.id=mo.anios_id', 'LEFT');
		$this->db->join('tipoguias tg','tg.id=gu.tipoguias_id', 'LEFT');
		$this->db->from('autoris gu');
		$query = $this->db->get();
		return $result = $query->result_array();
	}

	//---------------------------------------------------
		// obtener todos los registros de guias autorizadas
		//-----------------------------------------------------
	public function get_all_simple_autoris_pdf_tomadas(){
		//$this->db->where('status', 1);
		$this->db->select('gu.*, ag.name as agname, ag.tel as agtel, tg.version, tg.persona, br.name as brname, an.name as anname, mo.name as moname, mo.version as moversion ');
		$this->db->where('gu.status',1);
		$this->db->where('gu.statustoma',1);
		$this->db->join('agencys ag','ag.id=gu.agencys_id', 'LEFT');
		$this->db->join('models mo','mo.id=gu.models_id', 'LEFT');
		$this->db->join('brands br','br.id=mo.brands_id', 'LEFT');
		$this->db->join('anios an','an.id=mo.anios_id', 'LEFT');
		$this->db->join('tipoguias tg','tg.id=gu.tipoguias_id', 'LEFT');
		$this->db->from('autoris gu');
		$query = $this->db->get();
		return $result = $query->result_array();
	}

		//---------------------------------------------------
		// obtener marcas para exportar a csv
		//-----------------------------------------------------
	public function get_autoris_for_csv(){
		$this->db->select('au.*, ag.name as agname, ag.tel as agtel, gu.folio, mo.name as moname, mo.version as moversion, tg.version, tg.persona, br.name as brname, an.name as anname, gu.pdudlv7, gu.km, gu.modelotxt, st.versionvehi, gu.compraautometrica, gu.ventaautometrica, gu.status as gustatus, ch.status as chstatus, st.status as statustoma');
		$this->db->join('checklist ch','ch.id=au.checklist_id', 'LEFT');
		$this->db->join('seminuevostomados st','st.id=ch.seminuevostomados_id', 'LEFT');
		$this->db->join('guides gu','gu.id=st.guias_id', 'LEFT');
		$this->db->join('agencys ag','ag.id=gu.agencys_id', 'LEFT');
		$this->db->join('tipoguias tg','tg.id=gu.tipoguias_id', 'LEFT');
		$this->db->join('models mo','mo.id=gu.models_id', 'LEFT');
		$this->db->join('brands br','br.id=mo.brands_id', 'LEFT');
		$this->db->join('anios an','an.id=mo.anios_id', 'LEFT');
		$this->db->from('autoris au');
		$query = $this->db->get();
		return $result = $query->result_array();
	}

		//---------------------------------------------------
		// obtener todos los registros de marcas
		//-----------------------------------------------------
	public function get_all_simple_brands(){
		$this->db->where('status', 1);
		$this->db->order_by('id', 'desc');
		$query = $this->db->get('brands');
		return $result = $query->result_array();
	}


	//---------------------------------------------------
		// obtener todos los registros de años
		//-----------------------------------------------------
	public function get_all_simple_years(){

		$actual=date("Y");
		$ultimo=$actual-10;

		$this->db->where('status', 1);
		$this->db->where('name >=', $ultimo);
		$this->db->order_by('id', 'desc');
		$query = $this->db->get('anios');
		return $result = $query->result_array();
	}

	//---------------------------------------------------
		// obtener todos los registros de negocios
		//-----------------------------------------------------
	public function get_all_simple_business(){
		$this->db->where('status', 1);
		$this->db->order_by('id', 'desc');
		$query = $this->db->get('business');
		return $result = $query->result_array();
	}

	//---------------------------------------------------
		// obtener todos los registros de admins
		//-----------------------------------------------------
	public function get_all_simple_admins(){
		//$this->db->where('is_active', 1);
		$this->db->order_by('admin_id', 'desc');
		$query = $this->db->get('ci_admin');
		return $result = $query->result_array();
	}

	//---------------------------------------------------
		// obtener todos los registros de agencias
		//-----------------------------------------------------
	public function get_all_simple_autoris(){
		$this->db->where('status', 1);
		$this->db->order_by('id', 'desc');
		$query = $this->db->get('autoris');
		return $result = $query->result_array();
	}

	//---------------------------------------------------
		// obtener todos los registros de agencias
		//-----------------------------------------------------
	public function get_all_simple_agencys(){
		$this->db->where('status', 1);
		$this->db->order_by('id', 'desc');
		$query = $this->db->get('agencys');
		return $result = $query->result_array();
	}

		//---------------------------------------------------
		// obtener todos los registros de agencias
		//-----------------------------------------------------
	public function get_all_simple_agencys_todas(){
		//$this->db->where('status', 1);
		$this->db->order_by('id', 'desc');
		$query = $this->db->get('agencys');
		return $result = $query->result_array();
	}

	//---------------------------------------------------
		// obtener todos los registros de chcks
		//-----------------------------------------------------
	public function get_all_simple_checks_todas(){
		$this->db->select('ch.id as identificador, st.id, gu.folio');
		$this->db->join('seminuevostomados st','st.id=ch.seminuevostomados_id', 'LEFT');
		$this->db->join('guides gu','gu.id=st.guias_id', 'LEFT');
		$this->db->where('ch.statusautori', 0);
		$this->db->order_by('ch.id', 'desc');
		$query = $this->db->get('checklist ch');
		return $result = $query->result_array();
	}

	//---------------------------------------------------
		// obtener todos los registros de agencias
		//-----------------------------------------------------
	public function get_all_simple_utilitys(){
		$this->db->where('status', 1);
		$this->db->order_by('id', 'asc');
		$query = $this->db->get('utilidades');
		return $result = $query->result_array();
	}

	//---------------------------------------------------
		// obtener todos los registros de agencias
		//-----------------------------------------------------
	public function get_all_simple_models(){
		$this->db->where('status', 1);
		$this->db->order_by('id', 'asc');
		$query = $this->db->get('models');
		return $result = $query->result_array();
	}

		//---------------------------------------------------
		// obtener todos los registros de agencias
		//-----------------------------------------------------
	public function get_all_simple_tguias(){
		//$this->db->where('status', 1);
		$this->db->order_by('id', 'asc');
		$query = $this->db->get('tipoguias');
		return $result = $query->result_array();
	}


		//---------------------------------------------------
		// obtener todos los registros de marcas 
		//-----------------------------------------------------
	function get_admin_roles()
	{
		$this->db->from('ci_admin_roles');
		$this->db->where('admin_role_status',1);
		$query=$this->db->get();
		return $query->result_array();
	}

	//-----------------------------------------------------------
	//  Funcion Global para actualizar registro
	//-------------------------------------------------------
	function update($action,$id,$table){
		$this->db->where('id',$id);
		$this->db->update($table,$action);
	}















	 // image upload function with resize option
	function upload_image($max_size){

            // set upload path
		$config['upload_path']  = "./uploads/";
		$config['allowed_types']= 'gif|jpg|png|jpeg';
		$config['max_size']     = '92000';
		$config['max_width']    = '92000';
		$config['max_height']   = '92000';

		$this->load->library('upload', $config);

		if ($this->upload->do_upload("photo")) {


			$data = $this->upload->data();

                // set upload path
			$source             = "./uploads/".$data['file_name'] ;
			$destination_thumb  = "./uploads/thumbnail/" ;
			$destination_medium = "./uploads/medium/" ;
			$main_img = $data['file_name'];
                // Permission Configuration
			chmod($source, 0777) ;

			/* Resizing Processing */
                // Configuration Of Image Manipulation :: Static
			$this->load->library('image_lib') ;
			$img['image_library'] = 'GD2';
			$img['create_thumb']  = TRUE;
			$img['maintain_ratio']= TRUE;

                /// Limit Width Resize
			$limit_medium   = $max_size ;
			$limit_thumb    = 150;

                // Size Image Limit was using (LIMIT TOP)
			$limit_use  = $data['image_width'] > $data['image_height'] ? $data['image_width'] : $data['image_height'] ;

                // Percentase Resize
			if ($limit_use > $limit_medium || $limit_use > $limit_thumb) {
				$percent_medium = $limit_medium/$limit_use ;
				$percent_thumb  = $limit_thumb/$limit_use ;
			}

                //// Making THUMBNAIL ///////
			$img['width']  = $limit_use > $limit_thumb ?  $data['image_width'] * $percent_thumb : $data['image_width'] ;
			$img['height'] = $limit_use > $limit_thumb ?  $data['image_height'] * $percent_thumb : $data['image_height'] ;

                // Configuration Of Image Manipulation :: Dynamic
			$img['thumb_marker'] = '_thumb-'.floor($img['width']).'x'.floor($img['height']) ;
			$img['quality']      = ' 100%' ;
			$img['source_image'] = $source ;
			$img['new_image']    = $destination_thumb ;

			$thumb_nail = $data['raw_name']. $img['thumb_marker'].$data['file_ext'];
                // Do Resizing
			$this->image_lib->initialize($img);
			$this->image_lib->resize();
			$this->image_lib->clear() ;

                ////// Making MEDIUM /////////////
			$img['width']   = $limit_use > $limit_medium ?  $data['image_width'] * $percent_medium : $data['image_width'] ;
			$img['height']  = $limit_use > $limit_medium ?  $data['image_height'] * $percent_medium : $data['image_height'] ;

                // Configuration Of Image Manipulation :: Dynamic
			$img['thumb_marker'] = '_medium-'.floor($img['width']).'x'.floor($img['height']) ;
			$img['quality']      = '100%' ;
			$img['source_image'] = $source ;
			$img['new_image']    = $destination_medium ;

			$mid = $data['raw_name']. $img['thumb_marker'].$data['file_ext'];
                // Do Resizing
			$this->image_lib->initialize($img);
			$this->image_lib->resize();
			$this->image_lib->clear() ;

                // set upload path
			$images = 'uploads/medium/'.$mid;
			$thumb  = 'uploads/thumbnail/'.$thumb_nail;
			unlink($source) ;

			return array(
				'images' => $images,
				'thumb' => $thumb
			);
		}
		else {
			echo "Fallo al subir la imagen" ;
		}

	}



    // edit function
	function edit_option($action, $id, $table){
		$this->db->where('id',$id);
		$this->db->update($table,$action);
		return;
	} 



}

?>