  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1> </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#"><?= trans('home') ?></a></li>
              <li class="breadcrumb-item active"><?= trans('guide') ?></li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

            <!-- Main content -->
            <div class="invoice p-3 mb-3">
              <!-- title row -->
              <div class="row">
                <div class="col-12">
                  <h4>
                    <i class="fa fa-clipboard"></i> Guía para la Toma de Seminuevos
                    <small class="float-right">Fecha de Creación: <?php echo $take['created_at']; ?></small>
                  </h4>
                </div>
                <!-- /.col -->
              </div>
              <br>
              <!-- info row -->
              <div class="row invoice-info">
                <div class="col-sm-4 invoice-col">
                  <address>
                    <strong><?php echo $take['agname']; ?></strong><br>
                    <?php echo $take['agtel']; ?><br>
                    <?php echo $take['folio']; ?><br>
                  </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                  <address>
                    <strong><?php echo $take['version'].' '.$take['persona'];; ?></strong><br>
                    % de Utilidad: <?php echo $take['pdudlv7']; ?>%<br>
                  </address>
                </div>
                <div class="col-sm-4 invoice-col">
                  <address>
                    <strong>Status</strong><br>

                    <?php if($take['status'] == 1): ?>
                      <span class="badge bg-info">Activa!</span>
                      <?php else: ?>
                       <span class=" badge bg-danger">Pendiente!</span>
                     <?php endif; ?>

                     <br>
                   </address>
                 </div>
                <!-- /.col 
                <div class="col-sm-4 invoice-col">
                  <b><?php echo $take['brname']; ?></b><br>
                  <b>Año:</b><?php echo $take['anname']; ?><br>
                  <b>Modelo:</b><?php echo $take['moname']; ?><br>
                  <b>Versión:</b><?php echo $take['moversion']; ?><br>
                </div>
                /.col -->
              </div>
              <!-- /.row -->

              <!-- Table row -->
              <div class="row">
                <div class="col-12 table-responsive">
                  <table class="table table-striped">
                    <thead>
                      <tr>
                        <th>Marca</th>
                        <th>Año</th>
                        <th>Modelo</th>
                        <th>Description</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td><?php echo $take['brname']; ?></td>
                        <td><?php echo $take['anname']; ?></td>
                        <td><?php echo $take['moname']; ?></td>
                        <td><?php echo $take['moversion']; ?></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <div class="row">
                <!-- accepted payments column -->
                <div class="col-3">

                </div>
                <!-- /.col -->
                <div class="col-9">

                  <div class="table-responsive">
                    <table class="table">

                      <?php if($take['pmdvap8'] >= 1): ?>
                        <tr style="display: flex;">
                          <?php else: ?>
                           <tr style="display: none;">
                           <?php endif; ?>

                           <th style="width:70%"  >Precio Minimo de Venta al Publico Reacondicionado:</th>
                           <td>$<?php echo $take['pmdvap8']; ?></td>
                         </tr>

                         <tr>
                           <th>Precio de la toma tope al cliente con iva:</th>
                           <td>$<?php echo $take['pdlttfac2']; ?></td>
                         </tr>

                         <tr>
                          <th>Precio de venta al publico con iva y reacondicionamiento:</th>
                          <td>$<?php echo $take['pdvapciyr5']; ?></td>
                        </tr>

                      </table>
                    </div>
                  </div>
                  <!-- /.col -->
                </div>
                <!-- /.row -->

                <!-- this row will not appear when printing -->
                <div class="row no-print">
                  <div class="col-12">
                    <!--<a href="invoice-print.html" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>-->

                    <!--<button type="button" class="btn btn-primary float-right" style="margin-right: 5px;">
                      <i class="fa fa-pencil"></i> Editar
                    </button>-->
                  </div>
                </div>
              </div>
              <!-- /.invoice -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </section>
      <!-- /.content -->
    </div>
  <!-- /.content-wrapper -->