<?php

$fecha = $take['fechacontrato']; 

$month = date('M', strtotime($fecha));
$year = date('Y', strtotime($fecha));
$day = date('d', strtotime($fecha));

$partes = explode('-', $month); 
$_fecha = "$year.$month.$day";

$fmt = new NumberFormatter('en_US', NumberFormatter::CURRENCY);
$formatterES = new NumberFormatter("es-ES", NumberFormatter::SPELLOUT);

$izquierda = intval(floor($take['pdlttfac2']));
$derecha = intval(($take['pdlttfac2'] - floor($take['pdlttfac2'])) * 100);

$html = '
<table border="0" style="width:100%">
	<tbody>

	    <table border="0">
			<tr align="center">
				<td><h1>DACION EN PAGO</h1></td><br>
			</tr>
			<tr align="right">
				<td>'.$take['fechacontrato'].'</td><br>
			</tr>
		</table>

		<table border="0">
			<tr align="center">
				<td> </td> 
			</tr>
			<tr align="right">
				<td> </td> 
			</tr>
		</table>
			 
 

		<table align="justify">
			<tr>
				<td>

				El que suscribe, <font color="#0A6ACF">'.$take['nombre'].'</font>, autorizo a <font color="#0A6ACF">'.$take['bursocial'].'</font>, para que de acuerdo a los artículos 2185 al 2223 del C.C. Aplique en compensación el importe correspondiente a la venta que le estoy haciendo del vehículo que ampara la factura número: <font color="#0A6ACF">'.$take['facturavehi'].'</font> expedida por <font color="#0A6ACF">'.$take['expedida'].'</font> de fecha <font color="#0A6ACF">'.$take['fechafac'].'</font>, marca <font color="#0A6ACF">'.$take['marcavehi'].'</font>, modelo <font color="#0A6ACF">'.$take['modelovehi'].'</font>, tipo <font color="#0A6ACF">'.$take['versionvehi'].'</font> y No. De serie <font color="#0A6ACF">'.$take['nserie'].'</font> mismo que en la actualidad es usado en el estado y condiciones de uso en que se encuentra. <br><br>

				Dicha compensación, es por el concepto del vehículo que le estoy vendiendo a: <font color="#0A6ACF">'.$take['bursocial'].'</font> en la cantidad de: <font color="#0A6ACF">'.$fmt->formatCurrency($take['pdlttfac2'], "USD").'</font> <font color="#0A6ACF">('.strtoupper($formatterES->format($izquierda)).' PESOS ' .$derecha.'/100 M.N.)</font> importe que debe ser compensado contra el adeudo que con ellos tengo a nombre de: '.$take['nombrescfdi'].' '. $take['apellido1'].' '. $take['apellido2'].'.<br>
				</td>
				</tr>
				</table>

				 

				<table>
				<tr>
				<td>
				
				</td>
				</tr>
				</table>

				<table align="justify">
				<tr>
				<td>
				
				</td>
				</tr>
				</table>

				<table align="center">
				<tr>
				<td>
				<font color="#0A6ACF"></font><br><br><br><br><br>
				</td>
				</tr>
				</table>

				<table align="center">
				<tr>
				<td>
				Autorización
				<br><br><br><br><br><br><br><br><br><br>

				________________________________
				<br>
				<font color="#0A6ACF">'.$take['nombre'].'</font>
				<br>
				Testigo
				</td>
				<td>
				El apoderado
				<br><br><br><br><br><br><br><br><br><br>

				________________________________
				<br>
				(Nombre y firma)
				<br>
				Testigo
				<br><br><br><br><br><br><br><br><br><br>
				</td>
				</tr>

				<tr>
				<td>
				________________________________
				<br>
				Nombre y firma
				<br>
				Testigo
				</td>
				<td>
				________________________________
				<br>
				Nombre y Firma
				<br>
				Testigo
			</td>
		</tr>
	</table>
	';
$html.='</tbody> </table>';

tcpdf();
$obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
header('Content-type: application/pdf');
$obj_pdf->SetCreator(PDF_CREATOR);
$title = "Dación en pago";
$obj_pdf->SetTitle($title);
$obj_pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $title, PDF_HEADER_STRING);
$obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$obj_pdf->SetDefaultMonospacedFont('helvetica');
$obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$obj_pdf->SetFont('helvetica', '', 9);
$obj_pdf->setFontSubsetting(false);
$obj_pdf->AddPage();
ob_start();
    // podemos tener cualquier parte de la vista aquí como HTML, PHP, etc.
$content = ob_get_contents();
ob_end_clean();
$obj_pdf->writeHTML($content, true, false, true, false, '');
$obj_pdf->writeHTML($html, true, false, true, false, '');


$filename = 'dacion_pago';

$obj_pdf->Output($filename . '.pdf', 'D');




?>