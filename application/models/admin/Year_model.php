<?php
	class Year_model extends CI_Model{

		public function add_year($data){
			$this->db->insert('years', $data);
			return true;
		}

		//---------------------------------------------------
		// obtener todas los años para el procesamiento de tablas de datos del lado del servidor (basado en ajax)
		//-----------------------------------------------------
		public function get_all_years(){
			$this->db->select('*');
			//$this->db->where('status',1);
			return $this->db->get('anios')->result_array();
		}

		//---------------------------------------------------
		// Obtener información de marca por ID
		//-----------------------------------------------------
		public function get_year_by_id($id){
			$query = $this->db->get_where('anios', array('id' => $id));
			return $result = $query->row_array();
		}

		//---------------------------------------------------
		// Editar registro de marca
		//-----------------------------------------------------
		public function edit_year($data, $id){
			$this->db->where('id', $id);
			$this->db->update('anios', $data);
			return true;
		}

		//---------------------------------------------------
		// Cambiar status de marca
		//-----------------------------------------------------
		function change_status()
		{		
			$this->db->set('status', $this->input->post('status'));
			$this->db->where('id', $this->input->post('id'));
			$this->db->update('years');
		} 

		//---------------------------------------------------
		// obtener marcas para exportar a csv
		//-----------------------------------------------------
		public function get_years_for_csv(){
			$this->db->where('status', 1);
			$this->db->select('id, name, created_at');
			$this->db->from('anios');
			$query = $this->db->get();
			return $result = $query->result_array();
		}

		//---------------------------------------------------
		// obtener todos los registros de marcas 
		//-----------------------------------------------------
		public function get_all_simple_years(){
			$this->db->where('status', 1);
			$this->db->order_by('id', 'desc');
			$query = $this->db->get('anios');
			return $result = $query->result_array();
		}

	}

?>