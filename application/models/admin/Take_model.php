<?php
class Take_model extends CI_Model{

	public function add_take($data){
		$this->db->insert('seminuevostomados', $data);
		return true;
	}

	//---------------------------------------------------
	// obtener todas los agencias para el procesamiento de tablas de datos del lado del servidor (basado en ajax)
	//-----------------------------------------------------
	public function get_all_takes_brands(){
		$this->db->select('m.*, b.name as bname, rsocial');
		$this->db->join('business b','b.id=m.business_id', 'LEFT');
			//$this->db->where('status',1);
		return $this->db->get('seminuevostomados m')->result_array();
	}

	//---------------------------------------------------
	// obtener todas los agencias con sus marcas para el procesamiento de tablas de datos del lado del servidor (basado en ajax)
	//-----------------------------------------------------
	public function get_all_takes(){
		$this->db->select('st.*, gu.folio, ag.name as agname');
		//$this->db->where('status',1);
		//$this->db->join('models m','m.id=g.models_id', 'LEFT');
		$this->db->join('guides gu','gu.id=st.guias_id', 'LEFT');
		$this->db->join('agencys ag','ag.id=gu.agencys_id', 'LEFT');
		return $this->db->get('seminuevostomados st')->result_array();
	}

	//---------------------------------------------------
	// obtener todas los agencias con sus marcas para el procesamiento de tablas de datos del lado del servidor (basado en ajax) de tomas autorizadas
	//-----------------------------------------------------
	public function get_all_takes_autorizadas(){
		$this->db->select('st.*, gu.folio, ag.name as agname');
		$this->db->where('st.status',1);
		//$this->db->join('models m','m.id=g.models_id', 'LEFT');
		$this->db->join('guides gu','gu.id=st.guias_id', 'LEFT');
		$this->db->join('agencys ag','ag.id=gu.agencys_id', 'LEFT');
		return $this->db->get('seminuevostomados st')->result_array();
	}

	//---------------------------------------------------
	// obtener todas los agencias con sus marcas para el procesamiento de tablas de datos del lado del servidor (basado en ajax) de tomas autorizadas
	//-----------------------------------------------------
	public function get_all_takes_pendientes(){
		$this->db->select('st.*, gu.folio, ag.name as agname');
		$this->db->where('st.status',0);
		//$this->db->join('models m','m.id=g.models_id', 'LEFT');
		$this->db->join('guides gu','gu.id=st.guias_id', 'LEFT');
		$this->db->join('agencys ag','ag.id=gu.agencys_id', 'LEFT');
		return $this->db->get('seminuevostomados st')->result_array();
	}

	//---------------------------------------------------
	// obtener todas los agencias con sus marcas para el procesamiento de tablas de datos del lado del servidor (basado en ajax) de tomas autorizadas
	//-----------------------------------------------------
	public function get_all_takes_checks(){
		$this->db->select('st.*, gu.folio, ag.name as agname');
		$this->db->where('st.status',1);
		$this->db->where('st.statuscheck',1);
		//$this->db->join('models m','m.id=g.models_id', 'LEFT');
		$this->db->join('guides gu','gu.id=st.guias_id', 'LEFT');
		$this->db->join('agencys ag','ag.id=gu.agencys_id', 'LEFT');
		return $this->db->get('seminuevostomados st')->result_array();
	}

	//---------------------------------------------------
	// obtener todas los agencias con sus marcas para el procesamiento de tablas de datos del lado del servidor (basado en ajax)
	//-----------------------------------------------------
	public function get_all_takes_peruser($idusr){
		$this->db->select('st.*, gu.folio, ag.name as agname');
		$this->db->where('st.users_id',$idusr);
		//$this->db->join('models m','m.id=g.models_id', 'LEFT');
		$this->db->join('guides gu','gu.id=st.guias_id', 'LEFT');
		$this->db->join('agencys ag','ag.id=gu.agencys_id', 'LEFT');
		return $this->db->get('seminuevostomados st')->result_array();
	}

	//---------------------------------------------------
	// obtener todas los agencias con sus marcas para el procesamiento de tablas de datos del lado del servidor (basado en ajax) de tomas autorizadas
	//-----------------------------------------------------
	public function get_all_takes_peruser_autorizadas($idusr){
		$this->db->select('st.*, gu.folio, ag.name as agname');
		$this->db->where('st.users_id',$idusr);
		$this->db->where('st.status',1);
		//$this->db->join('models m','m.id=g.models_id', 'LEFT');
		$this->db->join('guides gu','gu.id=st.guias_id', 'LEFT');
		$this->db->join('agencys ag','ag.id=gu.agencys_id', 'LEFT');
		return $this->db->get('seminuevostomados st')->result_array();
	}

	//---------------------------------------------------
	// obtener todas los agencias con sus marcas para el procesamiento de tablas de datos del lado del servidor (basado en ajax) de tomas autorizadas
	//-----------------------------------------------------
	public function get_all_takes_peruser_pendientes($idusr){
		$this->db->select('st.*, gu.folio, ag.name as agname');
		$this->db->where('st.users_id',$idusr);
		$this->db->where('st.status',0);
		//$this->db->join('models m','m.id=g.models_id', 'LEFT');
		$this->db->join('guides gu','gu.id=st.guias_id', 'LEFT');
		$this->db->join('agencys ag','ag.id=gu.agencys_id', 'LEFT');
		return $this->db->get('seminuevostomados st')->result_array();
	}

	//---------------------------------------------------
	// obtener todas los agencias con sus marcas para el procesamiento de tablas de datos del lado del servidor (basado en ajax) de tomas autorizadas
	//-----------------------------------------------------
	public function get_all_takes_peruser_checks($idusr){
		$this->db->select('st.*, gu.folio, ag.name as agname');
		$this->db->where('st.users_id',$idusr);
		$this->db->where('st.status',1);
		$this->db->where('st.statuscheck',1);
		//$this->db->join('models m','m.id=g.models_id', 'LEFT');
		$this->db->join('guides gu','gu.id=st.guias_id', 'LEFT');
		$this->db->join('agencys ag','ag.id=gu.agencys_id', 'LEFT');
		return $this->db->get('seminuevostomados st')->result_array();
	}

	//---------------------------------------------------
	// Obtener información de la agencia por ID
	//-----------------------------------------------------
	public function get_take_by_id($id){
		$query = $this->db->get_where('seminuevostomados', array('id' => $id));
		return $result = $query->row_array();
	}

	//---------------------------------------------------
	// Obtener información de la agencia y sus tablas padre por ID
	//-----------------------------------------------------
	public function get_take_by_id_guides($id){
		$this->db->where('st.id', $id);
		$this->db->select('st.*');
		$this->db->join('guides g','g.id=st.guias_id', 'LEFT');
		$this->db->from('seminuevostomados st');
		$query = $this->db->get();
		return $result = $query->row_array();

	}

	//---------------------------------------------------
	// Obtener información de la agencia y sus tablas padre por ID
	//-----------------------------------------------------
	public function get_take_by_id_business_view($id){
		$this->db->where('st.id', $id);
		$this->db->select('st.*, ag.name as agname, ag.tel as agtel, tg.version, tg.persona, br.name as brname, st.aniomodelo as anname, st.modelovehi as moname, st.modelovehi as moversion ');
		$this->db->join('guides gu','gu.id=st.guias_id', 'LEFT');
		$this->db->join('tipoguias tg','tg.id=gu.tipoguias_id', 'LEFT');
		$this->db->join('agencys ag','ag.id=gu.agencys_id', 'LEFT');
		//$this->db->join('models mo','mo.id=gu.models_id', 'LEFT');
		$this->db->join('brands br','br.id=gu.brands_id', 'LEFT');
		//$this->db->join('anios an','an.id=mo.anios_id', 'LEFT');
		$this->db->from('seminuevostomados st');
		$query = $this->db->get();
		return $result = $query->row_array();
	}

	//---------------------------------------------------
	// Obtener información de la agencia y sus tablas padre por ID
	//-----------------------------------------------------
	public function get_take_by_id_business_pdf1($id){
		$this->db->where('st.id', $id);
		$this->db->select('st.*, gu.folio, gu.version, ag.name as agname, ag.tel as agtel, tg.version, tg.persona, br.name as brname, st.aniomodelo as anname, st.modelovehi as moname, st.modelovehi as moversion ');
		$this->db->join('guides gu','gu.id=st.guias_id', 'LEFT');
		$this->db->join('tipoguias tg','tg.id=gu.tipoguias_id', 'LEFT');
		$this->db->join('agencys ag','ag.id=gu.agencys_id', 'LEFT');
		//$this->db->join('models mo','mo.id=gu.models_id', 'LEFT');
		$this->db->join('brands br','br.id=gu.brands_id', 'LEFT');
		//$this->db->join('anios an','an.id=mo.anios_id', 'LEFT');
		$this->db->from('seminuevostomados st');
		$query = $this->db->get();
		return $result = $query->row_array();
	}

	public function get_take_by_id_business_pdf($id){
		$this->db->where('st.id', $id);
		$this->db->select('st.*, re.name as reginame, gu.modelotxt, gu.versiontxt, gu.folio, gu.pdlttfac2, gu.km, gu.versiontxt, ag.id as agid, ag.name as agname, ag.calle as agcalle, ag.ext as agext, ag.colonia as agcolonia, ag.cp as agcp, ag.ciudad as agciudad, ag.estado as agestado, ag.url as agurl, ag.urlp as agurlp, ag.tel as agtel, tg.version, tg.persona, br.name as brname, an.name as anname, st.modelovehi as moname, st.modelovehi as moversion, bu.id as idbus, bu.rsocial as bursocial, bu.calle as bucalle, bu.int as buint, bu.colonia as bucolonia, bu.cp as bucp, bu.ciudad as buciudad, bu.estado as buestado,
			ciad.firstname as ciadfirstnamegg, 
			ciad.lastname as ciadlastnamegg,
			ciad1.firstname as ciadfirstnamega,
			ciad1.lastname as ciadlastnamega, 
			ciad2.firstname as ciadfirstnamegs, 
			ciad2.lastname as ciadlastnamegs
			');

		$this->db->join('guides gu','gu.id=st.guias_id', 'LEFT');
		$this->db->join('agencys ag','ag.id=gu.agencys_id', 'LEFT');
		$this->db->join('business bu','bu.id=ag.business_id', 'LEFT');
		$this->db->join('regimenes re','re.id=st.regimenes_id', 'LEFT');

		$this->db->join('tipoguias tg','tg.id=gu.tipoguias_id', 'LEFT');
		
		$this->db->join('ci_admin ciad','ciad.admin_id=ag.users_id_gg', 'LEFT');
		$this->db->join('ci_admin ciad1','ciad1.admin_id=ag.users_id_ga', 'LEFT');
		$this->db->join('ci_admin ciad2','ciad2.admin_id=ag.users_id_gs', 'LEFT');
		
		//$this->db->join('models mo','mo.id=gu.models_id', 'LEFT');
		$this->db->join('brands br','br.id=gu.brands_id', 'LEFT');
		$this->db->join('anios an','an.id=gu.anio_id', 'LEFT');
		$this->db->from('seminuevostomados st');
		$query = $this->db->get();
		return $result = $query->row_array();
	}

	//---------------------------------------------------
	// Editar registro de agencia
	//-----------------------------------------------------
	public function edit_take($data, $id){
		$this->db->where('id', $id);
		$this->db->update('seminuevostomados', $data);
		return true;
	}

	//---------------------------------------------------
	// Cambiar status de modelo
	//-----------------------------------------------------
	function change_status()
	{		
		$this->db->set('status', $this->input->post('status'));
		$this->db->where('id', $this->input->post('id'));
		$this->db->update('seminuevostomados');
	} 

	//---------------------------------------------------
	// obtener tomas en general  para exportar a PDF
	//-----------------------------------------------------
	public function get_all_simple_takes_pdf(){
		//$this->db->where('status', 1);
		$this->db->select('gu.*, gu.folio, ag.name as agname, ag.tel as agtel, tg.version, tg.persona, br.name as brname, st.aniomodelo as anname, st.modelovehi as moname, st.modelovehi as moversion ');
		$this->db->join('guides gu','gu.id=st.guias_id', 'LEFT');
		$this->db->join('agencys ag','ag.id=gu.agencys_id', 'LEFT');
		$this->db->join('tipoguias tg','tg.id=gu.tipoguias_id', 'LEFT');
		//$this->db->join('models mo','mo.id=gu.models_id', 'LEFT');
		$this->db->join('brands br','br.id=gu.brands_id', 'LEFT');
		//$this->db->join('anios an','an.id=mo.anios_id', 'LEFT');
		$this->db->from('seminuevostomados st');
		$query = $this->db->get();
		return $result = $query->result_array();
	}

	//---------------------------------------------------
	// obtener tomas autorizadas  para exportar a PDF
	//-----------------------------------------------------
	public function get_all_simple_takes_pdf_autorizadas(){
		//$this->db->where('status', 1);
		$this->db->select('gu.*, gu.folio, ag.name as agname, ag.tel as agtel, tg.version, tg.persona, br.name as brname, st.aniomodelo as anname, st.modelovehi as moname, st.modelovehi as moversion ');
		$this->db->where('st.status',1);
		$this->db->join('guides gu','gu.id=st.guias_id', 'LEFT');
		$this->db->join('agencys ag','ag.id=gu.agencys_id', 'LEFT');
		$this->db->join('tipoguias tg','tg.id=gu.tipoguias_id', 'LEFT');
		//$this->db->join('models mo','mo.id=gu.models_id', 'LEFT');
		$this->db->join('brands br','br.id=gu.brands_id', 'LEFT');
		//$this->db->join('anios an','an.id=mo.anios_id', 'LEFT');
		$this->db->from('seminuevostomados st');
		$query = $this->db->get();
		return $result = $query->result_array();
	}

	//---------------------------------------------------
	// obtener tomas pendientes  para exportar a PDF
	//-----------------------------------------------------
	public function get_all_simple_takes_pdf_pendientes(){
		//$this->db->where('status', 1);
		$this->db->select('gu.*, gu.folio, ag.name as agname, ag.tel as agtel, tg.version, tg.persona, br.name as brname, st.aniomodelo as anname, st.modelovehi as moname, st.modelovehi as moversion ');
		$this->db->where('st.status',0);
		$this->db->join('guides gu','gu.id=st.guias_id', 'LEFT');
		$this->db->join('agencys ag','ag.id=gu.agencys_id', 'LEFT');
		$this->db->join('tipoguias tg','tg.id=gu.tipoguias_id', 'LEFT');
		//$this->db->join('models mo','mo.id=gu.models_id', 'LEFT');
		$this->db->join('brands br','br.id=gu.brands_id', 'LEFT');
		//$this->db->join('anios an','an.id=mo.anios_id', 'LEFT');
		$this->db->from('seminuevostomados st');
		$query = $this->db->get();
		return $result = $query->result_array();
	}

	//---------------------------------------------------
	// obtener tomas con checks  para exportar a PDF
	//-----------------------------------------------------
	public function get_all_simple_takes_pdf_checks(){
		//$this->db->where('status', 1);
		$this->db->select('gu.*, gu.folio, ag.name as agname, ag.tel as agtel, tg.version, tg.persona, br.name as brname, st.aniomodelo as anname, st.modelovehi as moname, st.modelovehi as moversion ');
		$this->db->where('st.status',1);
		$this->db->where('st.statuscheck',1);
		$this->db->join('guides gu','gu.id=st.guias_id', 'LEFT');
		$this->db->join('agencys ag','ag.id=gu.agencys_id', 'LEFT');
		$this->db->join('tipoguias tg','tg.id=gu.tipoguias_id', 'LEFT');
		//$this->db->join('models mo','mo.id=gu.models_id', 'LEFT');
		$this->db->join('brands br','br.id=gu.brands_id', 'LEFT');
		//$this->db->join('anios an','an.id=mo.anios_id', 'LEFT');
		$this->db->from('seminuevostomados st');
		$query = $this->db->get();
		return $result = $query->result_array();
	}

	//---------------------------------------------------
	// obtener marcas para exportar a csv
	//-----------------------------------------------------
	public function get_takes_for_csv(){
		//$this->db->where('status', 1);
		$this->db->select('gu.vin, gu.folio, ag.name as agname, ag.tel as agtel, tg.version, tg.persona, br.name as brname, st.aniomodelo as anname, st.modelovehi as moname, st.modelovehi as moversion ');
		$this->db->join('guides gu','gu.id=st.guias_id', 'LEFT');
		$this->db->join('agencys ag','ag.id=gu.agencys_id', 'LEFT');
		$this->db->join('tipoguias tg','tg.id=gu.tipoguias_id', 'LEFT');
		//$this->db->join('models mo','mo.id=gu.models_id', 'LEFT');
		$this->db->join('brands br','br.id=gu.brands_id', 'LEFT');
		//$this->db->join('anios an','an.id=mo.anios_id', 'LEFT');
		$this->db->from('seminuevostomados st');
		$query = $this->db->get();
		return $result = $query->result_array();
	}

	//---------------------------------------------------
	// obtener todos los registros de marcas
	//-----------------------------------------------------
	public function get_all_simple_brands(){
		$this->db->where('status', 1);
		$this->db->order_by('id', 'desc');
		$query = $this->db->get('brands');
		return $result = $query->result_array();
	}

	//---------------------------------------------------
	// obtener todos los registros de regimenes
	//-----------------------------------------------------
	public function get_all_simple_regimenes(){
		$this->db->where('status', 1);
		$this->db->order_by('id', 'asc');
		$query = $this->db->get('regimenes');
		return $result = $query->result_array();
	}


	//---------------------------------------------------
	// obtener todos los registros de años
	//-----------------------------------------------------
	public function get_all_simple_years(){
		$this->db->where('status', 1);
		$this->db->order_by('id', 'desc');
		$query = $this->db->get('anios');
		return $result = $query->result_array();
	}

	//---------------------------------------------------
	// obtener todos los registros de negocios
	//-----------------------------------------------------
	public function get_all_simple_business(){
		$this->db->where('status', 1);
		$this->db->order_by('id', 'desc');
		$query = $this->db->get('business');
		return $result = $query->result_array();
	}

	//---------------------------------------------------
	// obtener todos los registros de admins
	//-----------------------------------------------------
	public function get_all_simple_admins(){
		//$this->db->where('is_active', 1);
		$this->db->order_by('admin_id', 'desc');
		$query = $this->db->get('ci_admin');
		return $result = $query->result_array();
	}

	//---------------------------------------------------
	// obtener todos los registros de agencias
	//-----------------------------------------------------
	public function get_all_simple_takes(){
		$this->db->where('status', 1);
		$this->db->order_by('id', 'desc');
		$query = $this->db->get('seminuevostomados');
		return $result = $query->result_array();
	}

	//---------------------------------------------------
	// obtener todos los registros de agencias
	//-----------------------------------------------------
	public function get_all_simple_agencys(){
		$this->db->where('status', 1);
		$this->db->order_by('id', 'desc');
		$query = $this->db->get('agencys');
		return $result = $query->result_array();
	}

	//---------------------------------------------------
	// obtener todos los registros de agencias
	//-----------------------------------------------------
	public function get_all_simple_utilitys(){
		$this->db->where('status', 1);
		$this->db->order_by('id', 'asc');
		$query = $this->db->get('utilidades');
		return $result = $query->result_array();
	}

	//---------------------------------------------------
	// obtener todos los registros de agencias
	//-----------------------------------------------------
	public function get_all_simple_models(){
		$this->db->where('status', 1);
		$this->db->order_by('id', 'asc');
		$query = $this->db->get('models');
		return $result = $query->result_array();
	}

	//---------------------------------------------------
	// obtener todos los registros de agencias
	//-----------------------------------------------------
	public function get_all_simple_tguias(){
		//$this->db->where('status', 1);
		$this->db->order_by('id', 'asc');
		$query = $this->db->get('tipoguias');
		return $result = $query->result_array();
	}

	//---------------------------------------------------
	// obtener todos los registros de tomas 
	//-----------------------------------------------------
	public function get_all_simple_guides_notomadas(){
		$this->db->where('status', 1);
		$this->db->where('statustoma', 0);
		$this->db->order_by('id', 'asc');
		$query = $this->db->get('guides');
		return $result = $query->result_array();
	}

	//---------------------------------------------------
	// obtener todos los registros de tomas por usuario
	//-----------------------------------------------------
	public function get_all_simple_guides_notomadas_peruser($idusr){
		$this->db->where('status', 1);
		$this->db->where('statustoma', 0);
		$this->db->where('users_id',$idusr);
		$this->db->order_by('id', 'asc');
		$query = $this->db->get('guides');
		return $result = $query->result_array();
	}


	//---------------------------------------------------
	// obtener todos los registros de agencias
	//-----------------------------------------------------
	public function get_all_simple_guides(){
		//$this->db->where('status', 1);
		//$this->db->where('statustoma', 0);
		$this->db->order_by('id', 'asc');
		$query = $this->db->get('guides');
		return $result = $query->result_array();
	}

	//---------------------------------------------------
	// obtener todos los registros de marcas 
	//-----------------------------------------------------
	function get_admin_roles()
	{
		$this->db->from('ci_admin_roles');
		$this->db->where('admin_role_status',1);
		$query=$this->db->get();
		return $query->result_array();
	}

	//-----------------------------------------------------------
	//  Funcion Global para actualizar registro
	//-------------------------------------------------------
	function update($action,$id,$table){
		$this->db->where('id',$id);
		$this->db->update($table,$action);
	}








	//-----------------------------------------------------------
	//  funcion para cargar una imagen
	//-------------------------------------------------------
	function upload_image($max_size){

            // set upload path
		$config['upload_path']  = "./uploads/";
		$config['allowed_types']= 'gif|jpg|png|jpeg';
		$config['max_size']     = '92000';
		$config['max_width']    = '92000';
		$config['max_height']   = '92000';

		$this->load->library('upload', $config);

		if ($this->upload->do_upload("photo")) {


			$data = $this->upload->data();

                // set upload path
			$source             = "./uploads/".$data['file_name'] ;
			$destination_thumb  = "./uploads/thumbnail/" ;
			$destination_medium = "./uploads/medium/" ;
			$main_img = $data['file_name'];
                // Permission Configuration
			chmod($source, 0777) ;

			/* Resizing Processing */
                // Configuration Of Image Manipulation :: Static
			$this->load->library('image_lib') ;
			$img['image_library'] = 'GD2';
			$img['create_thumb']  = TRUE;
			$img['maintain_ratio']= TRUE;

                /// Limit Width Resize
			$limit_medium   = $max_size ;
			$limit_thumb    = 150;

                // Size Image Limit was using (LIMIT TOP)
			$limit_use  = $data['image_width'] > $data['image_height'] ? $data['image_width'] : $data['image_height'] ;

                // Percentase Resize
			if ($limit_use > $limit_medium || $limit_use > $limit_thumb) {
				$percent_medium = $limit_medium/$limit_use ;
				$percent_thumb  = $limit_thumb/$limit_use ;
			}

                //// Making THUMBNAIL ///////
			$img['width']  = $limit_use > $limit_thumb ?  $data['image_width'] * $percent_thumb : $data['image_width'] ;
			$img['height'] = $limit_use > $limit_thumb ?  $data['image_height'] * $percent_thumb : $data['image_height'] ;

                // Configuration Of Image Manipulation :: Dynamic
			$img['thumb_marker'] = '_thumb-'.floor($img['width']).'x'.floor($img['height']) ;
			$img['quality']      = ' 100%' ;
			$img['source_image'] = $source ;
			$img['new_image']    = $destination_thumb ;

			$thumb_nail = $data['raw_name']. $img['thumb_marker'].$data['file_ext'];
                // Do Resizing
			$this->image_lib->initialize($img);
			$this->image_lib->resize();
			$this->image_lib->clear() ;

                ////// Making MEDIUM /////////////
			$img['width']   = $limit_use > $limit_medium ?  $data['image_width'] * $percent_medium : $data['image_width'] ;
			$img['height']  = $limit_use > $limit_medium ?  $data['image_height'] * $percent_medium : $data['image_height'] ;

                // Configuration Of Image Manipulation :: Dynamic
			$img['thumb_marker'] = '_medium-'.floor($img['width']).'x'.floor($img['height']) ;
			$img['quality']      = '100%' ;
			$img['source_image'] = $source ;
			$img['new_image']    = $destination_medium ;

			$mid = $data['raw_name']. $img['thumb_marker'].$data['file_ext'];
                // Do Resizing
			$this->image_lib->initialize($img);
			$this->image_lib->resize();
			$this->image_lib->clear() ;

                // set upload path
			$images = 'uploads/medium/'.$mid;
			$thumb  = 'uploads/thumbnail/'.$thumb_nail;
			unlink($source) ;

			return array(
				'images' => $images,
				'thumb' => $thumb
			);
		}
		else {
			echo "Fallo al subir la imagen" ;
		}

	}



    //-----------------------------------------------------------
	//  Funcion global para Editar
	//-------------------------------------------------------
	function edit_option($action, $id, $table){
		$this->db->where('id',$id);
		$this->db->update($table,$action);
		return;
	} 



}

?>