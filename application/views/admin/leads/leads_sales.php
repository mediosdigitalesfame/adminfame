<!-- DataTables -->

<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/datatables/dataTables.bootstrap4.css"> 



<!-- Content Wrapper. Contains page content -->

<div class="content-wrapper">

  <section class="content">

    <!-- For Messages -->

    <?php $this->load->view('admin/includes/_messages.php') ?>

    <div class="card">

      <div class="card-header">

        <div class="d-inline-block">

          <h3 class="card-title"><i class="fa fa-list"></i>&nbsp; Grafico de Leads de Ventas</h3>

        </div>

        <div class="d-inline-block float-right">

          <?php if($this->rbac->check_operation_permission('kk')): ?>

            <a href="<?= base_url('admin/guides/add'); ?>" class="btn btn-success"><i class="fa fa-plus"></i> <?= trans('add_new_guide') ?></a>

          <?php endif; ?>  

          <?php if($this->rbac->check_operation_permission('kk')): ?>  

            <a href="<?= base_url('admin/guides/create_guides_pdf') ?>" class="btn btn-success"><?= trans('export_as_pdf') ?></a>

          <?php endif; ?>

          <?php if($this->rbac->check_operation_permission('kk')): ?>  

            <a href="<?= base_url('admin/guides/export_csv') ?>" class="btn btn-success"><?= trans('export_as_csv') ?></a>

          <?php endif; ?>

        </div>

      </div>

    </div>



    <!--<div class="card">

      <div class="card-body table-responsive"> 

        <?php echo form_open("/",'id="model_search"') ?>

        <div class="row">

          <div class="col-md-4">

            <label><?= trans('model_type') ?></label><hr style="margin:5px 0px;" />

            <input checked="checked" onchange="user_filter()" name="user_search_type" value="" type="radio"  /> <?= trans('all') ?>&nbsp;&nbsp;&nbsp;

            <input onchange="model_filter()" name="model_search_type" value="1" type="radio"  /> <?= trans('active') ?>&nbsp;&nbsp;&nbsp;

            <input onchange="model_filter()" name="model_search_type" value="0" type="radio"  /> <?= trans('inactive') ?>

          </div>

          <div class="col-md-3">

            <label>Date From:</label><input name="user_search_from" type="text" class="form-control form-control-inline input-medium datepicker" id="" />

          </div>

          <div class="col-md-3"> 

            <label>Date To:</label><input name="user_search_to" type="text" class="form-control form-control-inline input-medium datepicker" id="" /> 

          </div>

          <div class="col-md-2"> 

            <button type="button" style="margin-top:20px;" onclick="user_filter()" class="btn btn-info">Submit</button>

            <a href="<?= base_url('admin/guides/advance_search'); ?>" class="btn btn-danger" style="margin-top:20px;">

              <i class="fa fa-repeat"></i>

            </a>

          </div>

        </div>

        <?php echo form_close(); ?>

      </div>

    </div>-->  





    <div class="card">

      <div class="card-body table-responsive">

  

<?php
 
  switch ($idagen) {
    case 1:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/898e82ac-abc9-47b5-bb4e-99b5af089cd9/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
    case 2:
        echo "No Tiene";
        break;
    case 3:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/005b6046-7110-48fa-970d-eb3eeaace713/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 4:
        echo '<iframe width="100%" height="1284" src="https://datastudio.google.com/embed/reporting/279a786f-70a0-45c9-9bd3-7a1ce511d148/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 5:
        echo '<iframe width="100%" height="788" src="https://datastudio.google.com/embed/reporting/0a693cdf-6ba1-4e70-b992-af22d81b8382/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 6:
        echo "No Tiene";
        break;
        case 7:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/3f255b94-5be5-43c1-8262-a4eaf52fd944/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 8:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/9399a24c-6475-4514-9d58-a2aa9f41445b/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 9:
        echo '<iframe width="100%" height="1300" src="https://datastudio.google.com/embed/reporting/fcb4cee0-efa3-43ce-95cb-c8af1fc59b4a/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 10:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/a2563079-7db5-45fa-bb9d-3ae12b78ee98/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 11:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/d62ac3cf-d51c-4258-ab66-69bdc245a560/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 12:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/73376fac-b5ce-4a7c-bd4a-5d7f4a8730d0/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 13:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/9cfb4b34-a6bb-4107-9f2a-1581c8f22665/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 14:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/f8529d12-df4e-410d-bd1b-e6d0effee653/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 15:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/634bf23f-23d5-43fe-9cb4-1e11d171624b/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 16:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/960603ab-4070-40fd-ab97-48390386b1ef/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 17:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/86bc3314-6abc-443a-8cd1-6421fd280ec3/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 18:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/c378a615-d826-493b-bd87-bf3f603eb97a/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 19:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/703cbc9d-15b2-48d3-861e-f48644adb1b1/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 20:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/03156a04-0ab1-4cb1-951d-d0dac0b0498e/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 21:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/48849c9f-3512-4514-af5e-0b927dafd647/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 22:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/f46ef641-8d3d-4918-96c1-12ec5bb6a01f/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 23:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/cc348d11-cd59-420e-82ce-83cd9c053aff/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 24:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/55d909a5-064c-483f-9151-cd6ad1954afe/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 25:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/d525e5f1-1169-4ad0-90f7-338d4f126386/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 26:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/7d2cfeef-2dc7-41fc-b312-4bc5dc534b6d/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 27:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/9ea29026-2167-4011-8847-dab08d2dd060/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 28:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/c03caa43-fd6b-4e58-ad9d-86ad1b27fa87/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 29:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/64b4eb5b-f0f0-4e06-98c7-d958bf128bb9/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 30:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/14b7f9ec-404c-4b89-8b53-605cf0de0e0f/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
         case 31:
        echo 'No Tiene';
        break;
         case 32:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/5092ddc3-3da9-4f43-8cdc-525010510b93/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
         case 33:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/4c4a1ff5-b6db-42c1-ac72-50ea43c41d51/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
         case 34:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/bb7d1273-d7b0-491e-9445-9093e56c8226/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
         case 35:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/8c098c30-cef3-4f4a-8091-31cfbb856fa4/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
         case 36:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/efe7b02c-3e5a-4639-8b2d-04dc3d22ee08/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
         case 37:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/ed7676f5-632e-47ce-aa2f-51d66a8982e5/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
         case 38:
        echo "No Tiene";
        break;
         case 39:
        echo 'No Tiene';
        break;
         case 40:
        echo "No Tiene";
        break;
         case 41:
        echo "No Tiene";
        break;
         case 42:
        echo "No Tiene";
        break;

        case 91:
        echo '<iframe width="100%" height="1742" src="https://datastudio.google.com/embed/reporting/34a3f582-613f-4fd7-a73c-e3f762b3169e/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 92:
        echo '<iframe width="100%" height="1742" src="https://datastudio.google.com/embed/reporting/6460986a-f85f-41d4-a58b-d220eb424083/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 93:
        echo '<iframe width="100%" height="1742" src="https://datastudio.google.com/embed/reporting/9d4243c6-5da7-42c7-8dea-d6ace34bf79f/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 94:
        echo '<iframe width="100%" height="1742" src="https://datastudio.google.com/embed/reporting/03b04f23-72e0-41c9-bb57-1f8ed5eaef41/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 95:
        echo '<iframe width="100%" height="1742" src="https://datastudio.google.com/embed/reporting/c44cc9b7-6cf6-46de-b042-38b57be2f3d3/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;
        case 96:
        echo '<iframe width="100%" height="1742" src="https://datastudio.google.com/embed/reporting/e6805094-9868-43f4-b2d8-eb2d866f1ded/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;

        case 100:
        echo '<iframe width="100%" height="1192" src="https://datastudio.google.com/embed/reporting/898e82ac-abc9-47b5-bb4e-99b5af089cd9/page/1M" frameborder="0" style="border:0" allowfullscreen></iframe>';
        break;




}
?>



       

 

        <!--<table id="na_datatable" class="table table-bordered table-striped" width="100%">

          <thead>

            <tr>

              <th>#<?= trans('id') ?></th>

              <th>Nombre</th>

              

              <th><?= trans('status') ?></th>

              <th width="100" class="text-right"><?= trans('action') ?></th>

            </tr>

          </thead>

        </table>-->

      </div>

    </div>














  </section>  

</div>



<!-- bootstrap datepicker -->

<!-- datepicker -->

<script src="<?= base_url() ?>assets/plugins/datepicker/bootstrap-datepicker.js"></script>

<script>

  $('.datepicker').datepicker({

    autoclose: true

  });

</script>

<!-- DataTables -->

<script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.js"></script>

<script src="<?= base_url() ?>assets/plugins/datatables/dataTables.bootstrap4.js"></script>



<script>

  //---------------------------------------------------

  var table = $('#na_datatable').DataTable( {

    "processing": true,

    "serverSide": false,

    "ajax": "<?=base_url('admin/leads/datatable_json')?>",

    "order": [[0,'desc']],

    "columnDefs": [

    { "targets": 0, "name": "id", 'searchable':true, 'orderable':true},

    { "targets": 1, "name": "name", 'searchable':true, 'orderable':true},

     

    { "targets": 2, "name": "Action", 'searchable':false, 'orderable':false,'width':'100px'}

    ]

  });

</script>



<script>

  //---------------------------------------------------

  function model_filter()

  {

    var _form = $("#model_search").serialize();

    $.ajax({

      data: _form,

      type: 'post',

      url: '<?php echo base_url();?>admin/guides/search',

      async: true,

      success: function(output){

        table.ajax.reload( null, false );

      }

    });

  }

</script>



<script type="text/javascript">

  $("body").on("change",".tgl_checkbox",function(){

    console.log('checked');

    $.post('<?=base_url("admin/guides/change_status")?>',

    {

      '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>',

      id : $(this).data('id'),

      status : $(this).is(':checked') == true?1:0

    },

    <?php if($this->rbac->check_operation_permission('add')): ?>

      function(data){$.notify("El Status cambió con éxito", "success");}

      <?php else: ?>

        function(data){$.notify("No Tienes Permisos", "warn");}

      <?php endif; ?>

      );

  });

</script>





