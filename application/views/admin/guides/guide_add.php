  <!-- daterange picker -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/daterangepicker/daterangepicker-bs3.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/colorpicker/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/select2/select2.min.css">

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      <div class="card card-default">

        <div class="card-header">
          <div class="d-inline-block">
            <h3 class="card-title"> <i class="fa fa-plus"></i>
             <?= trans('add_new_guide') ?> </h3>
           </div>
           <div class="d-inline-block float-right">
            <a href="<?= base_url('admin/guides'); ?>" class="btn btn-success"><i class="fa fa-list"></i>  <?= trans('guides_list') ?></a>
          </div>
        </div>
        
        

        <div class="card-body" id="formulario">
         <!-- For Messages -->
         <?php $this->load->view('admin/includes/_messages.php') ?>
         <?php echo form_open_multipart(base_url('admin/guides/add'), 'class="form-horizontal"','id="formgu"','name="formgu"');  ?> 
         <?php $permitted_chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'; $iden = '-'.date('dmY').'-'.substr(str_shuffle($permitted_chars), 0, 8); ?>

         <?php $this->security->get_csrf_token_name(); ?>  

         <input type="hidden" id="identi" name="identi" value="<?php echo html_escape($iden); ?>">
         <input type="hidden" id="id" name="id" value="">

         <div class="form-group">
           <div class="row">

            <div class="col-lg-3">
              <div class="input-group">
               <label for="agency" class="col-md-12 control-label" style="display: flex; justify-content: right; width: 100%;">Versión de Guía*</label>
               <div class="col-md-12">
                <select id="tipoguia" name="tipoguia" onChange="mostrar(this.value),cambiaValores();" class="form-control select2" >
                  <option value="">Tipo de Guía Rápida</option>
                  <?php foreach($tguias as $tguia): ?>
                    <option value="<?= $tguia['id']; ?>"> <?= $tguia['version'].' '.$tguia['persona']; ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>
          </div>

          <!--<div class="col-lg-2">
            <div class="input-group">
             <label for="pdudlv7" class="col-md-12 control-label" style="display: flex; justify-content: right; width: 100%;" >% de <?= trans('utility') ?>*</label>
           </div>
         </div>-->

         <div class="col-lg-2">
          <div class="input-group">
            <label for="pdudlv7" class="col-md-12 control-label" style="display: flex; justify-content: right; width: 100%;" >% de <?= trans('utility') ?>*</label>
            <div class="col-md-12">
              <select id="pdudlv7" name="pdudlv7" class="form-control select2" >
                <!--<option value=""><?= trans('utility') ?></option>-->
                <?php foreach($utilitys as $utility): ?>
                  <option value="<?= $utility['valor']; ?>"><?= html_escape($utility['valor']); ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>
        </div>

      <!--<div class="col-lg-2">
        <div class="input-group">
          <label for="firstname" alling="right" class="pull-right col-md-12 control-label" style="display: flex; justify-content: right; width: 100%;"> Agencia:</label>  
        </div>
      </div>-->

      <div class="col-lg-3">
        <div class="input-group">
          <label for="firstname" alling="right" class="pull-right col-md-12 control-label" style="display: flex; justify-content: right; width: 100%;"> Agencia:</label>  
          <div class="col-md-12">
            <select name="agencia" id="agencia" class="form-control select2" onChange="cambiafolio();">
              <option value=""><?= trans('select_agencys') ?></option>
              <?php foreach($agencys as $agency): ?>
                <option value="<?= $agency['id']; ?>"><?= $agency['name']; ?></option>
              <?php endforeach; ?>
            </select>
          </div>
        </div>
      </div>

    <!--<div class="col-lg-1">
      <div class="input-group">
        <label for="firstname" alling="right" class="col-md-12 control-label" style="display: flex; justify-content: right; width: 100%;" >Folio:</label>
      </div>
    </div>-->

    <div class="col-lg-4">
      <div class="input-group">
        <label for="firstname" alling="right" class="col-md-12 control-label" style="display: flex; justify-content: right; width: 100%;" >Folio:</label>
        <div class="col-md-12">
          <input type="text" id="folio" name="folio" class="form-control" placeholder="" readonly required>
        </div>
      </div>
    </div>

  </div>
</div>



<div class="form-group">

 <div class="row">


<!--
    <div class="col-lg-2">
      <div class="input-group">
       <label for="pdudlv7" class="col-md-12 control-label">% <?= trans('utility') ?>*</label>
       <div class="col-md-12">
        <select id="pdudlv7" name="pdudlv7" class="form-control select2">-->
          <!--<option value=""><?= trans('utility') ?></option>-->
        <!--  <?php foreach($utilitys as $utility): ?>
            <option value="<?= $utility['valor']; ?>"><?= html_escape($utility['valor']); ?></option>
          <?php endforeach; ?>
        </select>
      </div>
    </div>
  </div>-->



  <div class="col-lg-2">

    <div class="input-group">

     <label for="anio" class="col-md-12 control-label">Año*</label>

     <div class="col-md-12">

      <select id="anio" name="anio" class="form-control select2" >

        <!--<option value=""><?= trans('utility') ?></option>-->

        <?php foreach($years as $year): ?>

          <option value="<?= $year['id']; ?>"><?= html_escape($year['name']); ?></option>

        <?php endforeach; ?>

      </select>

    </div>

  </div>

</div>



<div class="col-lg-2">

  <div class="input-group">

   <label for="marca" class="col-md-12 control-label">Marca*</label>

   <div class="col-md-12">

    <select id="marca" name="marca" class="form-control select2">

      <!--<option value=""><?= trans('utility') ?></option>-->

      <?php foreach($brands as $brand): ?>

        <option value="<?= $brand['id']; ?>"><?= html_escape($brand['name']); ?></option>

      <?php endforeach; ?>

    </select>

  </div>

</div>

</div>



<div class="col-lg-3">
  <div class="input-group">
    <label for="modelo" class="col-md-12 control-label" >Modelo</label>
    <div class="col-md-12">
      <input onkeyup="javascript:this.value=this.value.toUpperCase();" type="text" value="<?= old("modelo"); ?>" class="form-control " name="modelo" id="modelo">
      <div class="input-group">
      </div>
    </div>
  </div>
</div>

<div class="col-lg-5">
  <div class="input-group">
    <label for="version" class="col-md-12 control-label">Version</label>
    <div class="col-md-12">
      <input onkeyup="javascript:this.value=this.value.toUpperCase();" type="text" value="<?= old("version"); ?>" class="form-control " name="version" id="version">
      <div class="input-group">
      </div>
    </div>
  </div>
</div>

      <!--

      <div class="col-lg-2">

        <div class="input-group">

          <label for="marca" class="col-md-12 control-label">Marca <?php echo $ultimo; ?> </label>

          <div class="col-md-12">

            <?php  

            $options = array('' => 'Sel. Marca')+array_column($brands, 'name','id');

            echo form_dropdown('marca',$options,'','class="form-control country select2" ');

            ?>

          </div>

        </div>

      </div>

      <div class="col-lg-2">

        <div class="input-group">

          <label for="anio" class="col-md-12 control-label">Año</label>

          <div class="col-md-12">

            <select class="form-control state select2" id="anio" name="anio">

              <option value="">Sel. Año</option>

            </select>

          </div>

        </div>

      </div>

      <div class="col-lg-3">

        <div class="input-group">

          <label for="modelo" class="col-md-12 control-label">Modelo</label>

          <div class="col-md-12">

            <select class="form-control city select2" id="modelo" name="modelo">

              <option value="">Selecciona Modelo</option>

            </select>

          </div>

        </div>

      </div>

    -->



  </div>

</div>



<div class="form-group">
 <div class="row">
     
  <div class="col-lg-4">
    <div class="input-group">
     <label for="vin" class="col-md-12 control-label"><?= trans('vin') ?></label>
     <div class="col-md-12">
      <input onkeyup="javascript:this.value=this.value.toUpperCase();" type="text" value="<?= old("vin"); ?>" class="form-control " name="vin" id="vin">
      <div class="input-group">
       <h4 id="mensajevin" style="color: red;" class="forminput-error" >  <span style='font-size: 0.7em; color: red;'>El VIN debe contener de 10 a 16 Caracteres sin simbolos</span> </h4>
     </div>
   </div>
 </div>
</div>

<div class="col-lg-2">
  <div class="input-group">
   <label for="url" class="col-md-12 control-label"><?= trans('km') ?></label>
   <div class="col-md-12">
    <input type="text" value="<?= old("km"); ?>" class="form-control" placeholder="" id="km" name="km">
  </div>
  <div class="input-group">
       <h4 id="mensajekm" style="color: red;" class="forminput-error" >  <span style='font-size: 0.7em; color: red;'>El Km debe tener de 1 a 9 digitos</span> </h4>
     </div>
</div>
</div>

<div class="col-lg-3">

  <div class="input-group">

   <label for="urlp" class="col-md-12 control-label">Compra Guía</label>

   <div class="col-md-12">

    <div class="input-group">

      <div class="input-group-prepend">

        <span class="input-group-text">$</span>

      </div>

      <input type="text" value="<?= old("compraautometrica"); ?>" class="form-control" placeholder="" name="compraautometrica" id="compraautometrica">

    </div>

  </div>

</div>

</div>

<div class="col-lg-3">

  <div class="input-group">

   <label for="urlp" class="col-md-12 control-label">Venta Guía</label>

   <div class="col-md-12">

    <div class="input-group">

      <div class="input-group-prepend">

        <span class="input-group-text">$</span>

      </div>

      <input type="text" value="<?= old("ventaautometrica"); ?>"class="form-control" placeholder="" id="ventaautometrica" name="ventaautometrica">

    </div>

  </div>

</div>

</div>

</div>

</div>



<div class="form-group">

 <div class="row">

  <div class="col-lg-3">

    <div class="input-group">

     <label for="urlp" class="col-md-12 control-label">Equipamiento</label>

     <div class="col-md-12">

      <div class="input-group">

        <div class="input-group-prepend">

          <span class="input-group-text">$</span>

        </div>

        <input type="text" value="<?= old("equipamiento"); ?>" id="equipamiento" name="equipamiento" class="form-control" placeholder="">

      </div>

    </div>

  </div>

</div>

<div class="col-lg-3">

  <div class="input-group">

    <label for="tel" class="col-md-12 control-label">Premio/castigo</label>

    <div class="col-md-12">

      <div class="input-group">

        <div class="input-group-prepend">

          <span class="input-group-text">$</span>

        </div>

        <input type="text" value="<?= old("premio"); ?>" id="premio" name="premio" class="form-control" placeholder="">

      </div>

    </div>

  </div>

</div>

<div class="col-lg-3">

  <div class="input-group">

    <label for="calle" class="col-md-12 control-label">Reacondicionamiento</label>

    <div class="col-md-12">

      <div class="input-group">

        <div class="input-group-prepend">

          <span class="input-group-text">$</span>

        </div>

        <input type="text" value="<?= old("reacondicionamiento"); ?>"name="reacondicionamiento" id="reacondicionamiento" class="form-control" placeholder="">

      </div>

    </div>

  </div>

</div>

<div class="col-lg-3">

  <div class="input-group">

   <label for="int" class="col-md-12 control-label">Valor Coche</label>

   <div class="col-md-12">

    <div class="input-group">

      <div class="input-group-prepend">

        <span class="input-group-text">$</span>

      </div>

      <input type="text" value="<?= old("valor"); ?>" name="valor" id="valor" class="form-control" placeholder="" onblur="cambiaValores()">

    </div>

  </div>

</div>

</div>

</div>

</div>



<div class="form-group">
 <div class="row">

  <div class="col-lg-3">
    <div class="input-group">
      <label for="siniestros" class="col-md-12 control-label">Tiene Siniestros</label>
      <div class="col-md-12">
        <input class="tgl_checkbox tgl-ios" name="siniestros" id="siniestros" type="checkbox" value="1"  > <label for="siniestros"></label>
      </div>
    </div>
  </div>

  <div class="col-lg-3">
    <div class="input-group">
      <label class="col-md-12 control-label">Imágen Autometrica (Costo)</label> 
      <div class="form-group">
        <td>
         <input type="file" name="image" accept=".png, .jpg, .jpeg, .gif, .svg, .pdf">
         <p><small class="text-success"><?= trans('allowed_types') ?>:  .pdf</small></p>
       </td>
     </div>
   </div>
 </div>

 <div class="col-lg-3">
  <div class="input-group">
    <label class="col-md-12 control-label">Imágen Autometrica (Km)</label> 
    <div class="form-group">
      <td>
       <input type="file" name="image2" accept=".png, .jpg, .jpeg, .gif, .svg, .pdf">
       <p><small class="text-success"><?= trans('allowed_types') ?>:  .pdf</small></p>
     </td>
   </div>
 </div>
</div>

<div class="col-lg-3">
  <div class="input-group">
    <label class="col-md-12 control-label">Imágen Autometrica(Portada)</label> 
    <div class="form-group">
      <td>
       <input type="file" name="image3" accept=".png, .jpg, .jpeg, .gif, .svg, .pdf">
       <p><small class="text-success"><?= trans('allowed_types') ?>:  .pdf</small></p>
     </td>
   </div>
 </div>
</div>


 

<div class="col-lg-3">

  <div class="input-group">

    <!--<label class="col-md-12 control-label">Imágen Autometrica</label> -->

    <?php if(!empty($guide['image'])): ?>

     <p><img src="<?= base_url($guide['image']); ?>" class="logo" width="150"></p>

   <?php endif; ?>

 </div>

</div>


</div>
</div>


<div class="row m-t-30">

  <!-- <h4>Costos</h4> -->

  <div class="row m-t-30" id="x1" style="display: flex;">

    <div class="col-sm-6" >

      <div class="form-group">

       <!-- <label>Precio de Venta sin Reacondicionamiento</label>-->

       <input type="hidden" class="form-control" name="pdvapsr1" id="pdvapsr1" value="" readonly ><p class="help-block text-danger"></p>

       <input type="hidden" id="pdvapsr1a" name="pdvapsr1a" value="">

       <input type="hidden" id="status2" name="status2" value="">

     </div>

   </div>

   <div class="col-sm-4">

    <div class="form-group">

      <!--<label>Precio de la Toma tope al cliente con IVA</label>-->

      <input type="hidden" class="form-control" name="pdlttfac2" id="pdlttfac2" readonly value="" ><p class="help-block text-danger"></p>

      <input type="hidden" id="pdlttfac2a" name="pdlttfac2a" value="">

    </div>

  </div>

  <div class="col-sm-4">

    <div class="form-group">

      <!--<label>Utilidad de la Venta</label>-->

      <input type="hidden" class="form-control" name="udlv6" id="udlv6" readonly value=""><p class="help-block text-danger"></p>

      <input type="hidden" id="udlv6a" name="udlv6a" value="">

    </div>

  </div>

</div>

<div class="row m-t-30" id="x2" style="display: 'flex';">

  <div class="col-sm-3">

    <div class="form-group">

     <!-- <label>Precio de Venta al Publico sin IVA</label>-->

     <input type="hidden" class="form-control" name="pdvapsi4" id="pdvapsi4" readonly value="" ><p class="help-block text-danger"></p>

     <input type="hidden" id="pdvapsi4a" name="pdvapsi4a" value="">

   </div>

 </div>

 <div class="col-sm-6">

  <div class="form-group">

   <!-- <label>Precio de Venta al Publico con IVA y Reacondicionamiento</label>-->

   <input type="hidden" class="form-control" name="pdvapciyr5" id="pdvapciyr5" readonly value="" ><p class="help-block text-danger"></p>

   <input type="hidden" id="pdvapciyr5a" name="pdvapciyr5a" value="">

 </div>

</div>

<div class="col-sm-3">

  <div class="form-group">

    <!--<label>Costo Real</label>-->

    <input type="hidden" class="form-control" name="crpdltsi3" id="crpdltsi3" readonly value="" ><p class="help-block text-danger"></p>

    <input type="hidden" id="crpdltsi3a" name="crpdltsi3a" value="">

  </div>

</div>

</div>

<div class="row m-t-30" id="x3" style="display: 'flex';">

  <div class="col-sm-4">

    <div class="form-group">

     <!-- <label>Precio Minimo de Venta al Publico Reacondicionado</label>-->

     <input type="hidden" class="form-control" name="pmdvap8" id="pmdvap8" readonly value="" ><p class="help-block text-danger"></p>

     <input type="hidden" id="pmdvap8a" name="pmdvap8a" value="">

   </div>

 </div>

 <div class="col-sm-4">

  <div class="form-group">

    <!--<label>Costo Compra con Reacondicionamiento</label>-->

    <input type="hidden" class="form-control" name="cccr9" id="cccr9" readonly value="" ><p class="help-block text-danger"></p>

    <input type="hidden" id="cccr9a" name="cccr9a" value="">

  </div>

</div>

<div class="col-sm-4">

  <div class="form-group">

    <!--<label>Precio de Venta al Publico</label>-->

    <input type="hidden" class="form-control" name="pdvap10" id="pdvap10" readonly value="" ><p class="help-block text-danger"></p>

    <input type="hidden" id="pdvap10a" name="pdvap10a" value="">

  </div>

</div>

</div>

<div class="row m-t-30" id="x4" style="display:'flex';">

  <div class="col-sm-4">

    <div class="form-group">

      <!--<label>Utilidad con IVA de la Utilidad</label>-->

      <input type="hidden" class="form-control" name="ucidlu11" id="ucidlu11" readonly value=""><p class="help-block text-danger"></p>

      <input type="hidden" id="ucidlu11a" name="ucidlu11a" value="">

    </div>

  </div>

  <div class="col-sm-3">

    <div class="form-group">

     <!-- <label>IVA de la Utilidad</label>-->

     <input type="hidden" class="form-control" name="idlu12" id="idlu12" readonly value="" ><p class="help-block text-danger"></p>

     <input type="hidden" id="idlu12a" name="idlu12a" value="">

   </div>

 </div>

 <div class="col-sm-3">

  <div class="form-group">

    <!--<label>Factor de Utilidad Minima Esperada</label>-->

    <input type="hidden" class="form-control" name="fdume13" id="fdume13" readonly value="" ><p class="help-block text-danger"></p>

    <input type="hidden" id="fdume13a" name="fdume13a" value="">

  </div>

</div>

</div>

<input type="hidden" id="prebasestado2" name="prebasestado2" value="1"><br>
<input type="hidden" id="prebasestado3" name="prebasestado3" value="1"><br>
<input type="hidden" id="prebasestado4" name="prebasestado4" value="1"><br>

<input type="hidden" id="id" name="id" value="">

<!--<input type="hidden" id="idmarca" name="idmarca" value="">-->

<input type="hidden" id="idanio" name="idanio" value="">

<input type="hidden" id="idmodelo" name="idmodelo" value="">

<!-- csrf token -->

<input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">

</div> <!-- div class row 30 -->





</div>

<!-- /.box-body -->

</div>



<div class="card card-default">
  <div class="card-header">
    <div class="d-inline-block">
      <h3 class="card-title">Formato de Autorización</h3>
    </div>
    <div class="d-inline-block float-right">        
    </div>
  </div>

  <div class="card-body">
    <div class="form-group">
      <div class="col-md-12">  

        <div class="form-group">
           <div class="row">

            <div class="col-lg-4">
        <div class="input-group">
         <label for="vin" class="col-md-12 control-label">Proveedor Nombre Completo</label>
         <div class="col-md-12">
          <input type="text" class="form-control" placeholder="" id="fanombre" name="fanombre" maxlength="70" onkeyup="javascript:this.value=this.value.toUpperCase();">
        </div>
      </div>
    </div>

            <div class="col-lg-3">
              <div class="input-group">
               <label for="faregimen" class="col-md-12 control-label">Régimen*</label>
               <div class="col-md-12">
                <select name="faregimen" id="faregimen" class="form-control select2" >
                  <option value=""><?= trans('select_guide') ?></option>
                  <?php foreach($regis as $regi): ?>
                    <option value="<?= $regi['id']; ?>"><?= $regi['name']; ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>
          </div>

          

 </div>
        </div>


 <div class="form-group">
           <div class="row">

            <div class="col-lg-3">
  <div class="input-group">
   <label for="urlp" class="col-md-12 control-label">Placas de Baja</label>
   <div class="col-md-12">
    <input type="text" class="form-control" placeholder="" id="faplacasbaja" name="faplacasbaja" style="text-transform:uppercase;">
  </div>
</div>
</div>

<div class="col-lg-3">
    <div class="input-group">
     <label for="urlp" class="col-md-12 control-label">Edo Emisor Baja</label>
     <div class="col-md-12">
      <input type="text" class="form-control" placeholder="" id="faedoemisor" name="faedoemisor" style="text-transform:uppercase;">
    </div>
  </div>
</div>



          <div class="col-lg-2">
  <div class="input-group">
   <label for="vin" class="col-md-12 control-label">Color Ext</label>
   <div class="col-md-12">
    <input type="text" class="form-control" placeholder="" id="facolorext" name="facolorext" maxlength="17" onkeyup="javascript:this.value=this.value.toUpperCase();">
  </div>
</div>
</div>

 <div class="col-lg-2">
  <div class="input-group">
   <label for="vin" class="col-md-12 control-label">Telefono</label>
   <div class="col-md-12">
    <input type="text" class="form-control" placeholder="" id="fatel" name="fatel" maxlength="17" onkeyup="javascript:this.value=this.value.toUpperCase();">
  </div>
</div>
</div>



 </div>
        </div>

 
        <div class="form-group">
          <div class="row">
            <div class="col-lg-3">
              <div class="input-group">
                <label class="col-md-12 control-label">Condiciones Generales</label> 
                <div class="col-md-12">
                 <input type="file" name="facgenerales" accept=".png, .jpg, .jpeg, .gif, .svg, .pdf">
                 <p><small class="text-success"><?= trans('allowed_types') ?>: gif, jpg, png, jpeg, pdf</small></p>
               </div>
             </div>
           </div>
           <div class="col-lg-3">
            <div class="input-group">
             <label class="col-md-12 control-label">Reporte Diagnostico</label> 
             <div class="col-md-12">
              <input type="file" name="fardiagnostico" accept=".png, .jpg, .jpeg, .gif, .svg, .pdf">
              <p><small class="text-success"><?= trans('allowed_types') ?>: gif, jpg, png, jpeg, pdf</small></p>
            </div>
          </div>
        </div>
        <div class="col-lg-3">
          <div class="input-group">
           <label class="col-md-12 control-label">Reporte Transunion</label> 
           <div class="col-md-12">
            <input type="file" name="fartrans" accept=".png, .jpg, .jpeg, .gif, .svg, .pdf">
            <p><small class="text-success"><?= trans('allowed_types') ?>: gif, jpg, png, jpeg, pdf</small></p>
          </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="input-group">
          <label class="col-md-12 control-label">Situacion  Fiscal</label> 
          <div class="col-md-12">
            <input type="file" name="fasfiscal" accept=".png, .jpg, .jpeg, .gif, .svg, .pdf">
            <p><small class="text-success"><?= trans('allowed_types') ?>: gif, jpg, png, jpeg, pdf</small></p>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-lg-6">
        <div class="input-group">
          <label for="modelo" class="col-md-12 control-label">Observaciones</label>
          <div class="col-md-12">
            <input onkeyup="javascript:this.value=this.value.toUpperCase();" type="text" value="<?= old("faobservaciones"); ?>" class="form-control " name="faobservaciones" id="faobservaciones">
            <div class="input-group">
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
 
      </div>
    </div>
  </div>
</div>



<div class="card card-default">

  <div class="card-body">

    <div class="form-group">

     <div class="row">



      <div class="col-md-1"></div>



      <div class="col-md-10">

        <div class="card-body" >



          <div class="row" style="display: 'flex';">

            <div class="col-lg-3"></div>

            <div class="col-lg-6">

              <div class="input-group">

               <h4 id="msjr" style="color: green;"></h4>

             </div>

           </div>

         </div>



         <div class="callout callout-danger" >

          <div class="row" id="xc1" style="display: 'flex';">

            <div class="col-lg-9">

              <div class="input-group">

               <label class="col-md-12 control-label">Precio Minimo de Venta al Publico Reacondicionado</label>

             </div>

           </div>

           <div class="col-lg-3">

            <div class="input-group">

             <h3> <p id="premindeven" style="color: black;">$</p></h3>

           </div>

         </div>

       </div>

     </div>

     <div class="callout callout-success">

      <div class="row" id="xa2" style="display: flex;">

        <div class="col-lg-9">



          <div class="input-group">

           <label class="col-md-12 control-label">Precio de la toma tope al cliente con iva</label>

         </div>

       </div>

       <div class="col-lg-3">

        <div class="input-group">

         <h3> <p id="predetoma" style="color: black;">$</p></h3>

       </div>

     </div>

   </div>

 </div>

 <div class="callout callout-success">

  <div class="row" id="xb2" style="display:'flex';">

    <div class="col-lg-9">

      <div class="input-group">

       <label class="col-md-12 control-label">Precio de venta al publico con iva y reacondicionamiento</label>

     </div>

   </div>

   <div class="col-lg-3">

    <div class="input-group">

     <h3> <p id="predevencon" style="color: black;">$</p></h3>

   </div>

 </div>

</div>

</div>

</div>

</div>



<div class="col-md-1"></div>

<div class="col-md-2"></div>



<div class="col-md-8">

  <div class="card-body">

    <div class="callout callout-info">



      <div class="form-group">

        <div class="row" id="xa1" style="display: flex;">

          <div class="col-lg-9">

            <div class="input-group">

              <p>Precio de venta sin reacondicionamiento:</p>

            </div>

          </div>

          <div class="col-lg-3">

            <div class="input-group">

              <p id="predeventa" style="color: black;">$</p>

            </div>

          </div>

        </div>

        <div class="row" id="xc6" style="display: 'flex';">

          <div class="col-lg-9">

            <div class="input-group">

              <p>Factor de utilidad minima esperada:</p>

            </div>

          </div>

          <div class="col-lg-3">

            <div class="input-group">

              <p id="facdeu" style="color: black;">$</p>

            </div>

          </div>

        </div>

        <div class="row" id="xb3" style="display: 'flex';">

          <div class="col-lg-9">

            <div class="input-group">

              <p>Costo real:</p>

            </div>

          </div>

          <div class="col-lg-3">

            <div class="input-group">

              <p id="ctoreal" style="color: black;">$</p>

            </div>

          </div>

        </div>

        <div class="row" id="xb1" style="display: 'flex';">

          <div class="col-lg-9">

            <div class="input-group">

              <p>Precio de venta al publico sin iva:</p>

            </div>

          </div>

          <div class="col-lg-3">

            <div class="input-group">

              <p id="predevensin" style="color: black;">$</p>

            </div>

          </div>

        </div>

        <div class="row" id="xc2" style="display:'flex';">

          <div class="col-lg-9">

            <div class="input-group">

              <p>Costo compra con reacondicionamiento:</p>

            </div>

          </div>

          <div class="col-lg-3">

            <div class="input-group">

              <p id="coconrea" style="color: black;">$</p>

            </div>

          </div>

        </div>

        <div class="row" id="xc3" style="display: 'flex';">

          <div class="col-lg-9">

            <div class="input-group">

              <p>Precio de venta al publico:</p>

            </div>

          </div>

          <div class="col-lg-3">

            <div class="input-group">

              <p id="predevenpu" style="color: black;">$</p>

            </div>

          </div>

        </div>

        <div class="row" id="xc4" style="display: 'flex';">

          <div class="col-lg-9">

            <div class="input-group">

              <p>Utilidad con IVA de la utilidad:</p>

            </div>

          </div>

          <div class="col-lg-3">

            <div class="input-group">

              <p id="uconi" style="color: black;">$</p>

            </div>

          </div>

        </div>

        <div class="row" id="xc5" style="display: 'flex';">

          <div class="col-lg-9">

            <div class="input-group">

              <p>IVA de la utilidad:</p>

            </div>

          </div>

          <div class="col-lg-3">

            <div class="input-group">

              <p id="idelau" style="color: black;">$</p>

            </div>

          </div>

        </div>

        <div class="row" id="xa3" style="display: flex;">

          <div class="col-lg-9">

            <div class="input-group">

              <p>Utilidad de la venta:</p>

            </div>

          </div>

          <div class="col-lg-3">

            <div class="input-group">

              <p id="udelaven" style="color: black;">$</p>

            </div>

          </div>

        </div>



      </div>

    </div>

  </div>

</div>

</div>

</div>

</div>

</div>



<div class="card card-default">

  <div class="card-body">
      
      <div class="alert alert-warning alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>Warning!</strong> Better check yourself, you're not looking too good.
</div>

    <div class="form-group">

      <div class="col-md-12" name="botones" id="botones" style="display: flex;">

        <input type="submit" name="submit" id="submit" value="<?= trans('add_guide') ?>" class="btn btn-primary pull-right">

      </div>

    </div>

    <?php echo form_close( ); ?>

  </div>

</div>



</section> 

</div>



<!-- Select2 -->

<script src="<?= base_url() ?>assets/plugins/select2/select2.full.min.js"></script>

<!-- InputMask -->

<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.js"></script>



<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>

<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>



<script src="<?= base_url() ?>assets/plugins/input-mask/inputmask.js"></script>

<script src="<?= base_url() ?>assets/plugins/input-mask/inputmask.min.js"></script>

<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.min.js"></script>



<!-- date-range-picker -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>

<script src="<?= base_url() ?>assets/plugins/daterangepicker/daterangepicker.js"></script>

<!-- bootstrap color picker -->

<script src="<?= base_url() ?>assets/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>

<!-- bootstrap time picker -->

<script src="<?= base_url() ?>assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>

<!-- Page script -->

<!-- iCheck 1.0.1 -->

<script src="<?= base_url() ?>assets/plugins/iCheck/icheck.min.js"></script>

<script src="<?= base_url() ?>assets/js/validaguides.js"></script>








