<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Models extends MY_Controller 
{
	public function __construct(){

		parent::__construct();
		auth_check(); // comprobar la autenticación de inicio de sesión
		$this->rbac->check_module_access();

		$this->load->model('admin/model_model', 'model_model');
		$this->load->model('admin/Activity_model', 'activity_model');
	}

	//-----------------------------------------------------------
	public function index(){

		$data['title'] = 'Lista de Modelos';

		$this->load->view('admin/includes/_header');
		$this->load->view('admin/models/model_list');
		$this->load->view('admin/includes/_footer');

	}
	
	public function datatable_json(){				   					   
		$records['data'] = $this->model_model->get_all_models_brands();
		$data = array();

		$i=0;
		foreach ($records['data']   as $row) 
		{  
			$status = ($row['status'] == 1)? 'checked': '';

			$data[]= array(
				++$i,
				$row['bname'],
				$row['aname'],
				$row['name'],
				$row['version'],
				date("d-m-Y", strtotime($row['created_at'])),
				'<input class="tgl_checkbox tgl-ios"data-id="'.$row['id'].'"id="cb_'.$row['id'].'"type="checkbox"	'.$status.'><label for="cb_'.$row['id'].'"></label>',	
				'<a title="View" class="view btn btn-sm btn-info" href="'.base_url('admin/models/edit/'.$row['id']).'"> <i class="fa fa-eye"></i></a>
				<a title="Edit" class="update btn btn-sm btn-warning" href="'.base_url('admin/models/edit/'.$row['id']).'"> <i class="fa fa-pencil-square-o"></i></a>
				<a title="Delete" class="delete btn btn-sm btn-danger" href='.base_url("admin/models/delete/".$row['id']).' title="Delete" onclick="return confirm(\'Do you want to delete ?\')"> <i class="fa fa-trash-o"></i></a>'	 		
			);
		}
		$records['data']=$data;
		echo json_encode($records);						   
	}

	//-----------------------------------------------------------
	function change_status()
	{   
		$this->rbac->check_operation_access('change_status'); //comprobar el permiso de operación
		$this->model_model->change_status();
	}

	public function add(){
		
		$this->rbac->check_operation_access(); //comprobar el permiso de operación

		$data['brands']=$this->model_model->get_all_simple_brands();
		$data['years']=$this->model_model->get_all_simple_years();

		if($this->input->post('submit')){
			$this->form_validation->set_rules('name', trans('name'), 'trim|required');
			$this->form_validation->set_rules('brand', trans('select_brand'), 'trim|required');
			$this->form_validation->set_rules('year', trans('select_year'), 'trim|required');
			 

			if ($this->form_validation->run() == FALSE) {
				$data = array(
					'errors' => validation_errors()
				);
				$this->session->set_flashdata('errors', $data['errors']);
				redirect(base_url('admin/models/add'),'refresh');
			}
			else{
				$data = array(
					'name' => $this->input->post('name'),
					'version' => $this->input->post('version'),
					'status' => 1,
					'brands_id' => $this->input->post('brand'),
					'anios_id' => $this->input->post('year'),

					'created_at' => date('Y-m-d : h:m:s'),
					'updated_at' => date('Y-m-d : h:m:s'),
				);
				$data = $this->security->xss_clean($data);
				$result = $this->model_model->add_model($data);
				if($result){

					// Registro de actividades
					$this->activity_model->add_log(13);
					$this->session->set_flashdata('success', 'El Modelo se ha agregado correctamente!');
					redirect(base_url('admin/models'));
				}
			}
		}
		else{
			$this->load->view('admin/includes/_header', $data);
			$this->load->view('admin/models/model_add');
			$this->load->view('admin/includes/_footer');
		}
		
	}

	public function edit($id = 0){

		$this->rbac->check_operation_access(); //comprobar el permiso de operación

		if($this->input->post('submit')){
			$this->form_validation->set_rules('name', 'Name', 'trim|required');
			$this->form_validation->set_rules('status', 'Status', 'trim|required');
			if ($this->form_validation->run() == FALSE) {
				$data = array(
					'errors' => validation_errors()
				);
				$this->session->set_flashdata('errors', $data['errors']);
				redirect(base_url('admin/models/model_edit/'.$id),'refresh');
			}
			else{
				$data = array(
					'name' => $this->input->post('name'),
					'status' => $this->input->post('status'),
					'updated_at' => date('Y-m-d : h:m:s'),
				);
				$data = $this->security->xss_clean($data);
				$result = $this->model_model->edit_model($data, $id);
				if($result){
					// Registro de actividades 
					$this->activity_model->add_log(14);
					$this->session->set_flashdata('success', 'La Marca ha sido actualizado con éxito!');
					redirect(base_url('admin/models'));
				}
			}
		}
		else{
			$data['model'] = $this->model_model->get_model_by_id($id);
			
			$this->load->view('admin/includes/_header');
			$this->load->view('admin/models/model_edit', $data);
			$this->load->view('admin/includes/_footer');
		}
	}

	public function delete($id = 0)
	{
		$this->rbac->check_operation_access(); // comprobar el permiso de operación
		$this->db->delete('anios', array('id' => $id));
		// Registro de actividades  
		$this->activity_model->add_log(15);
		$this->session->set_flashdata('success', 'La Marca se ha eliminado correctamente!');
		redirect(base_url('admin/models'));
	}


	public function export_csv(){ 
	   // nombre del archivo
		$filename = 'models_'.date('Y-m-d').'.csv'; 
		header("Content-Description: File Transfer"); 
		header("Content-Disposition: attachment; filename=$filename"); 
		header("Content-Type: application/csv; ");

	   // obtener datos
		$model_data = $this->model_model->get_models_for_csv();

	   // crear archivo
		$file = fopen('php://output', 'w');
		$header = array("ID", "Name", "Created Date"); 
		fputcsv($file, $header);
		foreach ($model_data as $key=>$line){ 
			fputcsv($file,$line); 
		}
		fclose($file); 
		exit; 
	}


	//---------------------------------------------------------------
	//  Exportar Marcas a PDF
	//-------------------------------------------------------
	public function create_models_pdf()
	{
		$this->load->helper('pdf_tcpdf_helper'); // loaded pdf helper
		$data['all_models'] = $this->model_model->get_all_simple_models();
		$this->load->view('admin/models/models_tcpdf', $data);

	}

	//---------------------------------------------------
	// Busqueda Avanzada
	//-------------------------------------------------------
	public function advance_search(){

		$this->session->unset_modeldata('model_search_type');
		$this->session->unset_modeldata('model_search_from');
		$this->session->unset_modeldata('model_search_to');

		$data['title'] = 'Advanced Search with Datatable';

		$this->load->view('admin/includes/_header');
		$this->load->view('admin/models/advance_search', $data);
		$this->load->view('admin/includes/_footer');
	}

	//---------------------------------------------------------------
	//  Funcion de Busquedas
	//-------------------------------------------------------
	function search(){

		$this->session->set_modeldata('model_search_type',$this->input->post('model_search_type'));
		$this->session->set_modeldata('model_search_from',$this->input->post('model_search_from'));
		$this->session->set_modeldata('model_search_to',$this->input->post('model_search_to'));
	}




}


?>