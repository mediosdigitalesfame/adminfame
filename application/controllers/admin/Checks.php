<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Checks extends MY_Controller 
{
	public function __construct(){

		parent::__construct();
		$this->load->helper('form');
		$this->load->helper('functions');
		auth_check(); // comprobar la autenticación de inicio de sesión
		$this->rbac->check_module_access();

		$this->load->model('admin/check_model', 'check_model');
		$this->load->model('admin/Activity_model', 'activity_model');
	}

	//-----------------------------------------------------------
	//  Vista Principal
	//-------------------------------------------------------
	public function index(){
		$data['title'] = 'Lista de Checklist';
		$this->load->view('admin/includes/_header');
		$this->load->view('admin/checks/check_list');
		$this->load->view('admin/includes/_footer');
	}

		//-----------------------------------------------------------
	//  Vista Principal de checklist pendientes
	//-------------------------------------------------------
	public function index_pendientes(){
		$data['title'] = 'Lista de Checklist';
		$this->load->view('admin/includes/_header');
		$this->load->view('admin/checks/check_list_pen');
		$this->load->view('admin/includes/_footer');
	}

	//-----------------------------------------------------------
	//  Vista Principal de checklist en proceso
	//-------------------------------------------------------
	public function index_proceso(){
		$data['title'] = 'Lista de Checklist';
		$this->load->view('admin/includes/_header');
		$this->load->view('admin/checks/check_list_pro');
		$this->load->view('admin/includes/_footer');
	}

	//-----------------------------------------------------------
	//  Vista Principal de checklist en proceso
	//-------------------------------------------------------
	public function index_terminados(){
		$data['title'] = 'Lista de Checklist';
		$this->load->view('admin/includes/_header');
		$this->load->view('admin/checks/check_list_ter');
		$this->load->view('admin/includes/_footer');
	}

	//-----------------------------------------------------------
	//  Vista Principal de checklist en proceso
	//-------------------------------------------------------
	public function index_libres(){
		$data['title'] = 'Lista de Checklist';
		$this->load->view('admin/includes/_header');
		$this->load->view('admin/checks/check_list_lib');
		$this->load->view('admin/includes/_footer');
	}
	
	//-----------------------------------------------------------
	//  Tabala para la vista principal
	//-------------------------------------------------------
	public function datatable_json(){				   					   
		$records['data'] = $this->check_model->get_all_checks();
		$data = array();

		$i=0;
		foreach ($records['data']   as $row) 
		{  
			$status = $row['status'];

			$foliouno = $row['guid'];
        $foliocero = str_pad($foliouno, 9, "0", STR_PAD_LEFT);

			switch($status)
			{
				case '0': 
				$status = 'libre';
				$tipo= 'info';
				break; 
				case '1': 
				$status = 'Pendiente';
				$tipo= 'danger';
				break; 
				case '2': 
				$status = 'En Proceso';
				$tipo= 'warning';
				break; 
				case '3': 
				$status = 'Terminado';
				$tipo= 'success';
				break; 
			}

			$data[]= array(
				++$i,
				$row['agname'],
				$row['folio']. "-" .$foliocero,
				date("d-m-Y", strtotime($row['fechaguia'])),
				date("d-m-Y", strtotime($row['created_at'])),
				'<span class=" badge bg-'.$tipo.'">'.$status.'!</span> ',
				'<a title="Exportar a PDF" class="update btn btn-sm btn-success" href="'.base_url('admin/checks/create_check_pdf/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>
				<a title="View" class="view btn btn-sm btn-info" href="'.base_url('admin/checks/view/'.$row['id']).'"> <i class="fa fa-eye"></i></a>
				<a title="Edit" class="update btn btn-sm btn-warning" href="'.base_url('admin/checks/edit/'.$row['id']).'"> <i class="fa fa-pencil-square-o"></i></a>
				<a title="Delete" class="delete btn btn-sm btn-danger" href='.base_url("admin/checks/delete/".$row['id']).' title="Delete" onclick="return confirm(\'Do you want to delete ?\')"> <i class="fa fa-trash-o"></i></a>'	 		
			);
		}
		$records['data']=$data;
		echo json_encode($records);						   
	}

	//-----------------------------------------------------------
	//  Tabala para la vista principal
	//-------------------------------------------------------
	public function datatable_json_pendientes(){				   					   
		$records['data'] = $this->check_model->get_all_checks_pendientes();
		$data = array();

		$i=0;
		foreach ($records['data']   as $row) 
		{  
			$status = $row['status'];

			switch($status)
			{
				case '0': 
				$status = 'libre';
				$tipo= 'info';
				break; 
				case '1': 
				$status = 'Pendiente';
				$tipo= 'danger';
				break; 
				case '2': 
				$status = 'En Proceso';
				$tipo= 'warning';
				break; 
				case '3': 
				$status = 'libre';
				$tipo= 'success';
				break; 
			}

			$data[]= array(
				++$i,
				$row['agname'],
				$row['folio'],
				date("d-m-Y", strtotime($row['created_at'])),
				'<span class=" badge bg-'.$tipo.'">'.$status.'!</span> ',
				'<a title="Exportar a PDF" class="update btn btn-sm btn-success" href="'.base_url('admin/checks/create_check_pdf/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>
				<a title="View" class="view btn btn-sm btn-info" href="'.base_url('admin/checks/view/'.$row['id']).'"> <i class="fa fa-eye"></i></a>
				<a title="Edit" class="update btn btn-sm btn-warning" href="'.base_url('admin/checks/edit/'.$row['id']).'"> <i class="fa fa-pencil-square-o"></i></a>
				<a title="Delete" class="delete btn btn-sm btn-danger" href='.base_url("admin/checks/delete/".$row['id']).' title="Delete" onclick="return confirm(\'Do you want to delete ?\')"> <i class="fa fa-trash-o"></i></a>'	 		
			);
		}
		$records['data']=$data;
		echo json_encode($records);						   
	}

	//-----------------------------------------------------------
	//  Tabala para la vista principal
	//-------------------------------------------------------
	public function datatable_json_proceso(){				   					   
		$records['data'] = $this->check_model->get_all_checks_proceso();
		$data = array();

		$i=0;
		foreach ($records['data']   as $row) 
		{  
			$status = $row['status'];

			switch($status)
			{
				case '0': 
				$status = 'libre';
				$tipo= 'info';
				break; 
				case '1': 
				$status = 'Pendiente';
				$tipo= 'danger';
				break; 
				case '2': 
				$status = 'En Proceso';
				$tipo= 'warning';
				break; 
				case '3': 
				$status = 'libre';
				$tipo= 'success';
				break; 
			}

			$data[]= array(
				++$i,
				$row['agname'],
				$row['folio'],
				date("d-m-Y", strtotime($row['created_at'])),
				'<span class=" badge bg-'.$tipo.'">'.$status.'!</span> ',
				'<a title="Exportar a PDF" class="update btn btn-sm btn-success" href="'.base_url('admin/checks/create_check_pdf/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>
				<a title="View" class="view btn btn-sm btn-info" href="'.base_url('admin/checks/view/'.$row['id']).'"> <i class="fa fa-eye"></i></a>
				<a title="Edit" class="update btn btn-sm btn-warning" href="'.base_url('admin/checks/edit/'.$row['id']).'"> <i class="fa fa-pencil-square-o"></i></a>
				<a title="Delete" class="delete btn btn-sm btn-danger" href='.base_url("admin/checks/delete/".$row['id']).' title="Delete" onclick="return confirm(\'Do you want to delete ?\')"> <i class="fa fa-trash-o"></i></a>'	 		
			);
		}
		$records['data']=$data;
		echo json_encode($records);						   
	}

	//-----------------------------------------------------------
	//  Tabala para la vista principal
	//-------------------------------------------------------
	public function datatable_json_terminados(){				   					   
		$records['data'] = $this->check_model->get_all_checks_terminados();
		$data = array();

		$i=0;
		foreach ($records['data']   as $row) 
		{  
			$status = $row['status'];

			switch($status)
			{
				case '0': 
				$status = 'libre';
				$tipo= 'info';
				break; 
				case '1': 
				$status = 'Pendiente';
				$tipo= 'danger';
				break; 
				case '2': 
				$status = 'En Proceso';
				$tipo= 'warning';
				break; 
				case '3': 
				$status = 'libre';
				$tipo= 'success';
				break; 
			}

			$data[]= array(
				++$i,
				$row['agname'],
				$row['folio'],
				date("d-m-Y", strtotime($row['created_at'])),
				'<span class=" badge bg-'.$tipo.'">'.$status.'!</span> ',
				'<a title="Exportar a PDF" class="update btn btn-sm btn-success" href="'.base_url('admin/checks/create_check_pdf/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>
				<a title="View" class="view btn btn-sm btn-info" href="'.base_url('admin/checks/view/'.$row['id']).'"> <i class="fa fa-eye"></i></a>
				<a title="Edit" class="update btn btn-sm btn-warning" href="'.base_url('admin/checks/edit/'.$row['id']).'"> <i class="fa fa-pencil-square-o"></i></a>
				<a title="Delete" class="delete btn btn-sm btn-danger" href='.base_url("admin/checks/delete/".$row['id']).' title="Delete" onclick="return confirm(\'Do you want to delete ?\')"> <i class="fa fa-trash-o"></i></a>'	 		
			);
		}
		$records['data']=$data;
		echo json_encode($records);						   
	}

	//-----------------------------------------------------------
	//  Tabala para la vista principal
	//-------------------------------------------------------
	public function datatable_json_libres(){				   					   
		$records['data'] = $this->check_model->get_all_checks_libres();
		$data = array();

		$i=0;
		foreach ($records['data']   as $row) 
		{  
			$status = $row['status'];

			switch($status)
			{
				case '0': 
				$status = 'libre';
				$tipo= 'info';
				break; 
				case '1': 
				$status = 'Pendiente';
				$tipo= 'danger';
				break; 
				case '2': 
				$status = 'En Proceso';
				$tipo= 'warning';
				break; 
				case '3': 
				$status = 'libre';
				$tipo= 'success';
				break; 
			}

			$data[]= array(
				++$i,
				$row['agname'],
				$row['folio'],
				date("d-m-Y", strtotime($row['created_at'])),
				'<span class=" badge bg-'.$tipo.'">'.$status.'!</span> ',
				'<a title="Exportar a PDF" class="update btn btn-sm btn-success" href="'.base_url('admin/checks/create_check_pdf/'.$row['id']).'"> <i class="fa fa-file-pdf-o"></i></a>
				<a title="View" class="view btn btn-sm btn-info" href="'.base_url('admin/checks/view/'.$row['id']).'"> <i class="fa fa-eye"></i></a>
				<a title="Edit" class="update btn btn-sm btn-warning" href="'.base_url('admin/checks/edit/'.$row['id']).'"> <i class="fa fa-pencil-square-o"></i></a>
				<a title="Delete" class="delete btn btn-sm btn-danger" href='.base_url("admin/checks/delete/".$row['id']).' title="Delete" onclick="return confirm(\'Do you want to delete ?\')"> <i class="fa fa-trash-o"></i></a>'	 		
			);
		}
		$records['data']=$data;
		echo json_encode($records);						   
	}


	//-----------------------------------------------------------
	//  Cambiar Status
	//-------------------------------------------------------
	function change_status()
	{   
		$this->rbac->check_operation_access('change_status'); //comprobar el permiso de operación
		$this->check_model->change_status();
	}

	//-----------------------------------------------------------
	//  Agregar Check
	//-------------------------------------------------------
	public function add(){
		
		$this->rbac->check_operation_access(); //comprobar el permiso de operación

		$data['takes']=$this->check_model->get_all_takes_guides_actives();

		if($this->input->post('submit')){

			$this->form_validation->set_rules('tomasel', trans('take'), 'trim|required');

			if ($this->form_validation->run() == FALSE) {
				$data = array('errors' => validation_errors());
				$this->session->set_flashdata('form_data', $this->input->post());
				$this->session->set_flashdata('errors', $data['errors']);
				redirect(base_url('admin/checks/add'),'refresh');
			}
			else{

				if(!empty($this->input->post('solicitudcfdi'))){$solicitudcfdi = $this->input->post('solicitudcfdi', true);}
					else{$solicitudcfdi = 0;}

				if(!empty($this->input->post('cambioderol'))){$cambioderol = $this->input->post('cambioderol', true);}
					else{$cambioderol = 0;}

				if(!empty($this->input->post('cedulafiscal'))){$cedulafiscal = $this->input->post('cedulafiscal', true);}
					else{$cedulafiscal = 0;}

				if(!empty($this->input->post('cfdisinactividad'))){$cfdisinactividad = $this->input->post('cfdisinactividad', true);}
					else{$cfdisinactividad = 0;}

				if(!empty($this->input->post('xml'))){$xml = $this->input->post('xml', true);}
					else{$xml = 0;}

				if(!empty($this->input->post('rfc'))){$rfc = $this->input->post('rfc', true);}
					else{$rfc = 0;}

				if(!empty($this->input->post('validacioncfdi'))){$validacioncfdi = $this->input->post('validacioncfdi', true);}
					else{$validacioncfdi = 0;}

				if(!empty($this->input->post('verificacioncfdi'))){$verificacioncfdi = $this->input->post('verificacioncfdi', true);}
					else{$verificacioncfdi = 0;}

				if(!empty($this->input->post('autenticaciondoctos'))){$autenticaciondoctos = $this->input->post('autenticaciondoctos', true);}
					else{$autenticaciondoctos = 0;}

				if(!empty($this->input->post('reportetransunion'))){$reportetransunion = $this->input->post('reportetransunion', true);}
					else{$reportetransunion = 0;}

				if(!empty($this->input->post('noretencion'))){$noretencion = $this->input->post('noretencion', true);}
					else{$noretencion = 0;}

				if(!empty($this->input->post('secuenciafacturas'))){$secuenciafacturas = $this->input->post('secuenciafacturas', true);}
					else{$secuenciafacturas = 0;}

				if(!empty($this->input->post('identificacionoficial'))){$identificacionoficial = $this->input->post('identificacionoficial', true);}
					else{$identificacionoficial = 0;}

				if(!empty($this->input->post('curp'))){$curp = $this->input->post('curp', true);}
					else{$curp = 0;}

				if(!empty($this->input->post('comprobante'))){$comprobante = $this->input->post('comprobante', true);}
					else{$comprobante = 0;}

				if(!empty($this->input->post('bajaplacas'))){$bajaplacas = $this->input->post('bajaplacas', true);}
					else{$bajaplacas = 0;}

				if(!empty($this->input->post('tenencias'))){$tenencias = $this->input->post('tenencias', true);}
					else{$tenencias = 0;}

				if(!empty($this->input->post('tarjetacirculacion'))){$tarjetacirculacion = $this->input->post('tarjetacirculacion', true);}
					else{$tarjetacirculacion = 0;}

				if(!empty($this->input->post('responsivacompraventa'))){$responsivacompraventa = $this->input->post('responsivacompraventa', true);}
					else{$responsivacompraventa = 0;}

				if(!empty($this->input->post('contratocompraventa'))){$contratocompraventa = $this->input->post('contratocompraventa', true);}
					else{$contratocompraventa = 0;}

				if(!empty($this->input->post('avisoprivacidad'))){$avisoprivacidad = $this->input->post('avisoprivacidad', true);}
					else{$avisoprivacidad = 0;}

				if(!empty($this->input->post('checklistrevision'))){$checklistrevision = $this->input->post('checklistrevision', true);}
					else{$checklistrevision = 0;}

				if(!empty($this->input->post('evaluacionmecanica'))){$evaluacionmecanica = $this->input->post('evaluacionmecanica', true);}
					else{$evaluacionmecanica = 0;}

				if(!empty($this->input->post('guiaautometrica'))){$guiaautometrica = $this->input->post('guiaautometrica', true);}
					else{$guiaautometrica = 0;}

				if(!empty($this->input->post('polizaderecepcion'))){$polizaderecepcion = $this->input->post('polizaderecepcion', true);}
					else{$polizaderecepcion = 0;}

				if(!empty($this->input->post('cuentasporpagar'))){$cuentasporpagar = $this->input->post('cuentasporpagar', true);}
					else{$cuentasporpagar = 0;}

				if(!empty($this->input->post('formatodedacion'))){$formatodedacion = $this->input->post('formatodedacion', true);}
					else{$formatodedacion = 0;}

				if(!empty($this->input->post('reportederobo'))){$reportederobo = $this->input->post('reportederobo', true);}
					else{$reportederobo = 0;}

				if(!empty($this->input->post('chequedeproveedor'))){$chequedeproveedor = $this->input->post('chequedeproveedor', true);}
					else{$chequedeproveedor = 0;}

				if(!empty($this->input->post('solicitudcheque'))){$solicitudcheque = $this->input->post('solicitudcheque', true);}
					else{$solicitudcheque = 0;}

				if(!empty($this->input->post('edocuenta'))){$edocuenta = $this->input->post('edocuenta', true);}
					else{$edocuenta = 0;}

				if(!empty($this->input->post('calculoretencion'))){$calculoretencion = $this->input->post('calculoretencion', true);}
					else{$calculoretencion = 0;}

				if(!empty($this->input->post('autorizaciontoma'))){$autorizaciontoma = $this->input->post('autorizaciontoma', true);}
					else{$autorizaciontoma = 0;}

				if(!empty($this->input->post('avaluorapido'))){$avaluorapido = $this->input->post('avaluorapido', true);}
					else{$avaluorapido = 0;}

				if(!empty($this->input->post('contratoadhesion'))){$contratoadhesion = $this->input->post('contratoadhesion', true);}
					else{$contratoadhesion = 0;}					    

				if(!empty($this->input->post('folioenuso'))){$folioenuso = $this->input->post('folioenuso', true);}
					else{$folioensuso = "Sin Folio";}

				$idg = $this->input->post('tomasel', true);

				$data = array(
					'solicitudcfdi' => $solicitudcfdi,
					'cambioderol' => $cambioderol,
					'cedulafiscal' => $cedulafiscal,
					'cfdisinactividad' => $cfdisinactividad,
					'xml' => $xml,
					'rfc' => $rfc,
					'validacioncfdi' => $validacioncfdi,
					'verificacioncfdi' => $verificacioncfdi,
					'autenticaciondoctos' => $autenticaciondoctos,
					'reportetransunion' => $reportetransunion,
					'noretencion' => $noretencion,
					'secuenciafacturas' => $secuenciafacturas,
					'identificacionoficial' => $identificacionoficial,
					'curp' => $curp,
					'comprobante' => $comprobante,
					'bajaplacas' => $bajaplacas,
					'tenencias' => $tenencias,
					'tarjetacirculacion' => $tarjetacirculacion,
					'responsivacompraventa' => $responsivacompraventa,
					'contratocompraventa' => $contratocompraventa,
					'avisoprivacidad' => $avisoprivacidad,
					'checklistrevision' => $checklistrevision,
					'evaluacionmecanica' => $evaluacionmecanica,
					'guiaautometrica' => $guiaautometrica,
					'polizaderecepcion' => $polizaderecepcion,
					'cuentasporpagar' => $cuentasporpagar,
					'formatodedacion' => $formatodedacion,
					'reportederobo' => $reportederobo,
					'chequedeproveedor' => $chequedeproveedor,
					'solicitudcheque' => $solicitudcheque,
					'edocuenta' => $edocuenta,
					'calculoretencion' => $calculoretencion,
					'autorizaciontoma' => $autorizaciontoma,
					'avaluorapido' => $avaluorapido,
					'contratoadhesion' => $contratoadhesion,
					'status' => 0,
					'created_at' => date('Y-m-d h:m:s'),
					//'updated_at' => date('Y-m-d h:m:s'),
					'seminuevostomados_id' => $this->input->post('tomasel', true),);

				$path="uploads/checklistdocs/".$folioenuso."/";

				//---- Archivo de 
			
				if(!empty($_FILES['solicitudcfdidocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'solicitudcfdidocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$solicitudcfdidocs = $path.$result['msg'];
						$solicitudcfdidates = date('Y-m-d h:m:s');
					}
					else{
						$solicitudcfdidates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}

				//---- Archivo de 

				if(!empty($_FILES['cambioderoldocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'cambioderoldocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$cambioderoldocs = $path.$result['msg'];
						$cambioderoldates = date('Y-m-d h:m:s'); 
					}
					else{
						$cambioderoldates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}

			   
			    //---- Archivo de 

				if(!empty($_FILES['cedulafiscaldocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'cedulafiscaldocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$cedulafiscaldocs = $path.$result['msg'];
						$cedulafiscaldates = date('Y-m-d h:m:s');
					}
					else{
						$cedulafiscaldates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}

				//---- Archivo de 
					 

					if(!empty($_FILES['cfdisinactividaddocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'cfdisinactividaddocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$cfdisinactividaddocs = $path.$result['msg'];
						$cfdisinactividaddates = date('Y-m-d h:m:s');
					}
					else{
						$cfdisinactividaddates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}
					 
					 //---- Archivo de 

					if(!empty($_FILES['xmldocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'xmldocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$xmldocs = $path.$result['msg'];
						$xmldates = date('Y-m-d h:m:s');
					}
					else{
						$xmldates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}
					 

					 //---- Archivo de 


					if(!empty($_FILES['rfcdocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'rfcdocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$rfcdocs = $path.$result['msg'];
						$rfcdates = date('Y-m-d h:m:s');
					}
					else{
						$rfcdates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}
					 

					 //---- Archivo de 


					if(!empty($_FILES['validacioncfdidocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'validacioncfdidocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$validacioncfdidocs = $path.$result['msg'];
						$validacioncfdidates = date('Y-m-d h:m:s');
					}
					else{
						$validacioncfdidates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}
					

					//---- Archivo de 


					if(!empty($_FILES['verificacioncfdidocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'verificacioncfdidocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$verificacioncfdidocs = $path.$result['msg'];
						$verificacioncfdidates = date('Y-m-d h:m:s');
					}
					else{
						$verificacioncfdidates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}
					

					//---- Archivo de 


					if(!empty($_FILES['autenticaciondoctosdocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'autenticaciondoctosdocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$autenticaciondoctosdocs = $path.$result['msg'];
						$autenticaciondoctosdates = date('Y-m-d h:m:s');
					}
					else{
						$autenticaciondoctosdates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}
					

					//---- Archivo de 


					if(!empty($_FILES['reportetransuniondocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'reportetransuniondocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$reportetransuniondocs = $path.$result['msg'];
						$reportetransuniondates = date('Y-m-d h:m:s');
					}
					else{
						$reportetransuniondates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}
					

					//---- Archivo de 


					if(!empty($_FILES['noretenciondocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'noretenciondocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$noretenciondocs = $path.$result['msg'];
						$noretenciondates = date('Y-m-d h:m:s');
					}
					else{
						$noretenciondates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}
					

					//---- Archivo de 


					if(!empty($_FILES['secuenciafacturasdocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'secuenciafacturasdocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$secuenciafacturasdocs = $path.$result['msg'];
						$secuenciafacturasdates = date('Y-m-d h:m:s');
					}
					else{
						$secuenciafacturasdates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}

					

					//---- Archivo de 


					if(!empty($_FILES['identificacionoficialdocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'identificacionoficialdocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$identificacionoficialdocs = $path.$result['msg'];
						$identificacionoficialdates = date('Y-m-d h:m:s');
					}
					else{
						$identificacionoficialdates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}

					

					//---- Archivo de 


					if(!empty($_FILES['curpdocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'curpdocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$curpdocs = $path.$result['msg'];
						$curpdates = date('Y-m-d h:m:s');
					}
					else{
						$curpdates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}

					

					//---- Archivo de 


					if(!empty($_FILES['comprobantedocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'comprobantedocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$comprobantedocs = $path.$result['msg'];
						$comprobantedates = date('Y-m-d h:m:s');
					}
					else{
						$comprobantedates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}

					

					//---- Archivo de 


					if(!empty($_FILES['bajaplacasdocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'bajaplacasdocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$bajaplacasdocs = $path.$result['msg'];
						$bajaplacasdates = date('Y-m-d h:m:s');
					}
					else{
						$bajaplacasdates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}

					

					//---- Archivo de 


					if(!empty($_FILES['tenenciasdocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'tenenciasdocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$tenenciasdocs = $path.$result['msg'];
						$tenenciasdates = date('Y-m-d h:m:s');
					}
					else{
						$tenenciasdates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}

					

					//---- Archivo de 


					if(!empty($_FILES['tarjetacirculaciondocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'tarjetacirculaciondocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$tarjetacirculaciondocs = $path.$result['msg'];
						$tarjetacirculaciondates = date('Y-m-d h:m:s');
					}
					else{
						$tarjetacirculaciondates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}

					

					//---- Archivo de 


					if(!empty($_FILES['responsivacompraventadocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'responsivacompraventadocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$responsivacompraventadocs = $path.$result['msg'];
						$responsivacompraventadates = date('Y-m-d h:m:s');
					}
					else{
						$responsivacompraventadates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}

					

					//---- Archivo de 


					if(!empty($_FILES['contratocompraventadocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'contratocompraventadocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$contratocompraventadocs = $path.$result['msg'];
						$contratocompraventadates = date('Y-m-d h:m:s');
					}
					else{
						$contratocompraventadates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}

					

					//---- Archivo de 


					if(!empty($_FILES['avisoprivacidaddocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'avisoprivacidaddocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$avisoprivacidaddocs = $path.$result['msg'];
						$avisoprivacidaddates = date('Y-m-d h:m:s');
					}
					else{
						$avisoprivacidaddates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}

					

					//---- Archivo de 


					if(!empty($_FILES['checklistrevisiondocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'checklistrevisiondocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$checklistrevisiondocs = $path.$result['msg'];
						$checklistrevisiondates = date('Y-m-d h:m:s');
					}
					else{
						$checklistrevisiondates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}

					

					//---- Archivo de 


					if(!empty($_FILES['evaluacionmecanicadocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'evaluacionmecanicadocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$evaluacionmecanicadocs = $path.$result['msg'];
						$evaluacionmecanicadates = date('Y-m-d h:m:s');
					}
					else{
						$evaluacionmecanicadates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}

					

					//---- Archivo de 


					if(!empty($_FILES['guiaautometricadocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'guiaautometricadocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$guiaautometricadocs = $path.$result['msg'];
						$guiaautometricadates = date('Y-m-d h:m:s');
					}
					else{
						$guiaautometricadates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}

					

					//---- Archivo de 


					if(!empty($_FILES['polizaderecepciondocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'polizaderecepciondocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$polizaderecepciondocs = $path.$result['msg'];
						$polizaderecepciondates = date('Y-m-d h:m:s');
					}
					else{
						$polizaderecepciondates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}

					

					//---- Archivo de 


					if(!empty($_FILES['cuentasporpagardocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'cuentasporpagardocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$cuentasporpagardocs = $path.$result['msg'];
						$cuentasporpagardates = date('Y-m-d h:m:s');
					}
					else{
						$cuentasporpagardates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}

					

					//---- Archivo de 


					if(!empty($_FILES['formatodedaciondocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'formatodedaciondocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$formatodedaciondocs = $path.$result['msg'];
						$formatodedaciondates = date('Y-m-d h:m:s');
					}
					else{
						$formatodedaciondates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}

					

					//---- Archivo de 


					if(!empty($_FILES['reportederobodocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'reportederobodocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$reportederobodocs = $path.$result['msg'];
						$reportederobodates = date('Y-m-d h:m:s');
					}
					else{
						$reportederobodates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}

					

					//---- Archivo de 


					if(!empty($_FILES['chequedeproveedordocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'chequedeproveedordocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$chequedeproveedordocs = $path.$result['msg'];
						$chequedeproveedordates = date('Y-m-d h:m:s');
					}
					else{
						$chequedeproveedordates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}

					

					//---- Archivo de 


					if(!empty($_FILES['solicitudchequedocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'solicitudchequedocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$solicitudchequedocs = $path.$result['msg'];
						$solicituddechequedates = date('Y-m-d h:m:s');
					}
					else{
						$solicituddechequedates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}

					

					//---- Archivo de 


					if(!empty($_FILES['edocuentadocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'edocuentadocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$edocuentadocs = $path.$result['msg'];
						$edocuentadates = date('Y-m-d h:m:s');
					}
					else{
						$edocuentadates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}

					

					//---- Archivo de 


					if(!empty($_FILES['calculoretenciondocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'calculoretenciondocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$calculoretenciondocs = $path.$result['msg'];
						$calculoretenciondates = date('Y-m-d h:m:s');
					}
					else{
						$calculoretenciondates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}
					

					//---- Archivo de 


					if(!empty($_FILES['autorizaciontomadocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'autorizaciontomadocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$autorizaciontomadocs = $path.$result['msg'];
						$autorizaciontomadates = date('Y-m-d h:m:s');
					}
					else{
						$autorizaciontomadates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}
					

					//---- Archivo de 


					if(!empty($_FILES['avaluorapidodocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'avaluorapidodocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$avaluorapidodocs = $path.$result['msg'];
						$avaluorapidodates = date('Y-m-d h:m:s');
					}
					else{
						$avaluorapidodates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}
					

					//---- Archivo de 


					if(!empty($_FILES['contratoadhesiondocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'contratoadhesiondocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$contratoadhesiondocs = $path.$result['msg'];
						$contratoadhesiondates = date('Y-m-d h:m:s');
					}
					else{
						$contratoadhesiondates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}

				//Crear el primer insert del checklist
				$data = $this->security->xss_clean($data);
				$datag=array('statuscheck' => 1);
				$result = $this->check_model->add_check($data);

				$datadocs = array( 
					'checklist_id' => $this->db->insert_id(),
					'rfc' => $this->db->insert_id(),  
					'solicitudcfdi' => $solicitudcfdidocs,
					'cambioderol' => $cambioderoldocs,  
					'cedulafiscal' => $cedulafiscaldocs,
					'cfdisinactividad' => $cfdisinactividaddocs,
					'xml' => $xmldocs,
					'rfc' => $rfcdocs,
					'validacioncfdi' => $validacioncfdidocs,
					'verificacioncfdi' => $verificacioncfdidocs,
					'autenticaciondoctos' => $autenticaciondoctosdocs,
					'reportetransunion' => $reportetransuniondocs,
					'noretencion' => $noretenciondocs,
					'secuenciafacturas' => $secuenciafacturasdocs,
					'identificacionoficial' => $identificacionoficialdocs,
					'curp' => $curpdocs,
					'comprobante' => $comprobantedocs,
					'bajaplacas' => $bajaplacasdocs,
					'tenencias' => $tenenciasdocs,
					'tarjetacirculacion' => $tarjetacirculaciondocs,
					'responsivacompraventa' => $responsivacompraventadocs,
					'contratocompraventa' => $contratocompraventadocs,
					'avisoprivacidad' => $avisoprivacidaddocs,
					'checklistrevision' => $checklistrevisiondocs,
					'evaluacionmecanica' => $evaluacionmecanicadocs,
					'guiaautometrica' => $guiaautometricadocs,
					'polizaderecepcion' => $polizaderecepciondocs,
					'cuentasporpagar' => $cuentasporpagardocs,
					'formatodedacion' => $formatodedaciondocs,
					'reportederobo' => $reportederobodocs,
					'chequedeproveedor' => $chequedeproveedordocs,
					'solicitudcheque' => $solicitudchequedocs,
					'edocuenta' => $edocuentadocs,
					'calculoretencion' => $calculoretenciondocs,
					'autorizaciontoma' => $autorizaciontomadocs,
					'avaluorapido' => $avaluorapidodocs,
					'contratoadhesion' => $contratoadhesiondocs,
				);

				$datadates = array( 
					'checklist_id' => $this->db->insert_id(),
					'rfc' => $this->db->insert_id(),  
					'solicitudcfdi' => $solicitudcfdidates, 
					'cambioderol' => $cambioderoldates,  
					'cedulafiscal' => $cedulafiscaldates,
					'cfdisinactividad' => $cfdisinactividaddates,
					'xml' => $xmldates,
					'rfc' => $rfcdates,
					'validacioncfdi' => $validacioncfdidates,
					'verificacioncfdi' => $verificacioncfdidates,
					'autenticaciondoctos' => $autenticaciondoctosdates,
					'reportetransunion' => $reportetransuniondates,
					'noretencion' => $noretenciondates,
					'secuenciafacturas' => $secuenciafacturasdates,
					'identificacionoficial' => $identificacionoficialdates,
					'curp' => $curpdates,
					'comprobante' => $comprobantedates,
					'bajaplacas' => $bajaplacasdates,
					'tenencias' => $tenenciasdates,
					'tarjetacirculacion' => $tarjetacirculaciondates,
					'responsivacompraventa' => $responsivacompraventadates,
					'contratocompraventa' => $contratocompraventadates,
					'avisoprivacidad' => $avisoprivacidaddates,
					'checklistrevision' => $checklistrevisiondates,
					'evaluacionmecanica' => $evaluacionmecanicadates,
					'guiaautometrica' => $guiaautometricadates,
					'polizaderecepcion' => $polizaderecepciondates,
					'cuentasporpagar' => $cuentasporpagardates,
					'formatodedacion' => $formatodedaciondates,
					'reportederobo' => $reportederobodates,
					'chequedeproveedor' => $chequedeproveedordates,
					'solicitudcheque' => $solicitudchequedates,
					'edocuenta' => $edocuentadates,
					'calculoretencion' => $calculoretenciondates,
					'autorizaciontoma' => $autorizaciontomadates,
					'avaluorapido' => $avaluorapidodates,
					'contratoadhesion' => $contratoadhesiondates,
				);
				
				$datadocs = $this->security->xss_clean($datadocs);
				$resultdocs = $this->check_model->add_checkdocs($datadocs);

				$datadates = $this->security->xss_clean($datadates);
				$resultdates = $this->check_model->add_checkdates($datadates);


				if($result){
					// Registro de actividades
					$this->activity_model->add_log(28);
					$this->check_model->update($datag, $idg, 'seminuevostomados');
					$this->session->set_flashdata('success', 'El Checklist se ha agregado correctamente!');
					redirect(base_url('admin/checks'));
				}
			}
		}
		else{
			$this->load->view('admin/includes/_header', $data);
			$this->load->view('admin/checks/check_add');
			$this->load->view('admin/includes/_footer');
		}
		
	}

	//-----------------------------------------------------------
	//  Editar check
	//-------------------------------------------------------
	public function edit($id = 0){

		$this->rbac->check_operation_access(); //comprobar el permiso de operación

		$data['takes']=$this->check_model->get_all_takes_guides();

		if($this->input->post('submit')){

			$this->form_validation->set_rules('id', trans('take'), 'trim|required');
			$this->form_validation->set_rules('comentarios', 'Comentarios', 'trim|required');
			 
			if ($this->form_validation->run() == FALSE) {
				$data = array(
					'errors' => validation_errors()
				);
				$this->session->set_flashdata('errors', $data['errors']);
				redirect(base_url('admin/checks/edit/'.$id),'refresh');
			}
			else{

				if(!empty($this->input->post('solicitudcfdi'))){$solicitudcfdi = $this->input->post('solicitudcfdi', true);}
				else{$solicitudcfdi = 0;}

				if(!empty($this->input->post('cambioderol'))){$cambioderol = $this->input->post('cambioderol', true);}
				else{$cambioderol = 0;}

				if(!empty($this->input->post('cedulafiscal'))){$cedulafiscal = $this->input->post('cedulafiscal', true);}
				else{$cedulafiscal = 0;}

				if(!empty($this->input->post('cfdisinactividad'))){$cfdisinactividad = $this->input->post('cfdisinactividad', true);}
				else{$cfdisinactividad = 0;}

				if(!empty($this->input->post('cfdipersonamoral'))){$cfdipersonamoral = $this->input->post('cfdipersonamoral', true);}
				else{$cfdipersonamoral = 0;}

				if(!empty($this->input->post('xml'))){$xml = $this->input->post('xml', true);}
				else{$xml = 0;}

				if(!empty($this->input->post('rfc'))){$rfc = $this->input->post('rfc', true);}
				else{$rfc = 0;}

				if(!empty($this->input->post('validacioncfdi'))){$validacioncfdi = $this->input->post('validacioncfdi', true);}
				else{$validacioncfdi = 0;}

				if(!empty($this->input->post('verificacioncfdi'))){$verificacioncfdi = $this->input->post('verificacioncfdi', true);}
				else{$verificacioncfdi = 0;}

				if(!empty($this->input->post('autenticaciondoctos'))){$autenticaciondoctos = $this->input->post('autenticaciondoctos', true);}
				else{$autenticaciondoctos = 0;}

				if(!empty($this->input->post('reportetransunion'))){$reportetransunion = $this->input->post('reportetransunion', true);}
				else{$reportetransunion = 0;}

				if(!empty($this->input->post('noretencion'))){$noretencion = $this->input->post('noretencion', true);}
				else{$noretencion = 0;}

				if(!empty($this->input->post('secuenciafacturas'))){$secuenciafacturas = $this->input->post('secuenciafacturas', true);}
				else{$secuenciafacturas = 0;}

				if(!empty($this->input->post('identificacionoficial'))){$identificacionoficial = $this->input->post('identificacionoficial', true);}
				else{$identificacionoficial = 0;}

				if(!empty($this->input->post('curp'))){$curp = $this->input->post('curp', true);}
				else{$curp = 0;}

				if(!empty($this->input->post('comprobante'))){$comprobante = $this->input->post('comprobante', true);}
				else{$comprobante = 0;}

				if(!empty($this->input->post('bajaplacas'))){$bajaplacas = $this->input->post('bajaplacas', true);}
				else{$bajaplacas = 0;}

				if(!empty($this->input->post('tenencias'))){$tenencias = $this->input->post('tenencias', true);}
				else{$tenencias = 0;}

				if(!empty($this->input->post('tarjetacirculacion'))){$tarjetacirculacion = $this->input->post('tarjetacirculacion', true);}
				else{$tarjetacirculacion = 0;}

				if(!empty($this->input->post('responsivacompraventa'))){$responsivacompraventa = $this->input->post('responsivacompraventa', true);}
				else{$responsivacompraventa = 0;}

				if(!empty($this->input->post('contratocompraventa'))){$contratocompraventa = $this->input->post('contratocompraventa', true);}
				else{$contratocompraventa = 0;}

				if(!empty($this->input->post('avisoprivacidad'))){$avisoprivacidad = $this->input->post('avisoprivacidad', true);}
				else{$avisoprivacidad = 0;}

				if(!empty($this->input->post('checklistrevision'))){$checklistrevision = $this->input->post('checklistrevision', true);}
				else{$checklistrevision = 0;}

				if(!empty($this->input->post('evaluacionmecanica'))){$evaluacionmecanica = $this->input->post('evaluacionmecanica', true);}
				else{$evaluacionmecanica = 0;}

				if(!empty($this->input->post('guiaautometrica'))){$guiaautometrica = $this->input->post('guiaautometrica', true);}
				else{$guiaautometrica = 0;}

				if(!empty($this->input->post('polizaderecepcion'))){$polizaderecepcion = $this->input->post('polizaderecepcion', true);}
				else{$polizaderecepcion = 0;}

				if(!empty($this->input->post('cuentasporpagar'))){$cuentasporpagar = $this->input->post('cuentasporpagar', true);}
				else{$cuentasporpagar = 0;}

				if(!empty($this->input->post('formatodedacion'))){$formatodedacion = $this->input->post('formatodedacion', true);}
				else{$formatodedacion = 0;}

				if(!empty($this->input->post('reportederobo'))){$reportederobo = $this->input->post('reportederobo', true);}
				else{$reportederobo = 0;}

				if(!empty($this->input->post('chequedeproveedor'))){$chequedeproveedor = $this->input->post('chequedeproveedor', true);}
				else{$chequedeproveedor = 0;}

				if(!empty($this->input->post('solicitudcheque'))){$solicitudcheque = $this->input->post('solicitudcheque', true);}
				else{$solicitudcheque = 0;}

				if(!empty($this->input->post('edocuenta'))){$edocuenta = $this->input->post('edocuenta', true);}
				else{$edocuenta = 0;}

				if(!empty($this->input->post('calculoretencion'))){$calculoretencion = $this->input->post('calculoretencion', true);}
				else{$calculoretencion = 0;}

				if(!empty($this->input->post('autorizaciontoma'))){$autorizaciontoma = $this->input->post('autorizaciontoma', true);}
				else{$autorizaciontoma = 0;}

				if(!empty($this->input->post('avaluorapido'))){$avaluorapido = $this->input->post('avaluorapido', true);}
				else{$autorizaciontoma = 0;}

				if(!empty($this->input->post('contratoadhesion'))){$contratoadhesion = $this->input->post('contratoadhesion', true);}
				else{$contratoadhesion = 0;}

				$data = array(
					'solicitudcfdi' => $solicitudcfdi,
					'cambioderol' => $cambioderol,
					'cedulafiscal' => $cedulafiscal,
					'cfdisinactividad' => $cfdisinactividad,
					'xml' => $xml,
					'rfc' => $rfc,
					'validacioncfdi' => $validacioncfdi,
					'verificacioncfdi' => $verificacioncfdi,
					'autenticaciondoctos' => $autenticaciondoctos,
					'reportetransunion' => $reportetransunion,
					'noretencion' => $noretencion,
					'secuenciafacturas' => $secuenciafacturas,
					'identificacionoficial' => $identificacionoficial,
					'curp' => $curp,
					'comprobante' => $comprobante,
					'bajaplacas' => $bajaplacas,
					'tenencias' => $tenencias,
					'tarjetacirculacion' => $tarjetacirculacion,
					'responsivacompraventa' => $responsivacompraventa,
					'contratocompraventa' => $contratocompraventa,
					'avisoprivacidad' => $avisoprivacidad,
					'checklistrevision' => $checklistrevision,
					'evaluacionmecanica' => $evaluacionmecanica,
					'guiaautometrica' => $guiaautometrica,
					'polizaderecepcion' => $polizaderecepcion,
					'cuentasporpagar' => $cuentasporpagar,
					'formatodedacion' => $formatodedacion,
					'reportederobo' => $reportederobo,
					'chequedeproveedor' => $chequedeproveedor,
					'solicitudcheque' => $solicitudcheque,
					'edocuenta' => $edocuenta,
					'calculoretencion' => $calculoretencion,
					'autorizaciontoma' => $autorizaciontoma,
					'avaluorapido' => $avaluorapido,
					'contratoadhesion' => $contratoadhesion,
					'status' => $this->input->post('status'),
					'updated_at' => date('Y-m-d h:m:s'),
				);

				$datac=array(                
					'comentarios' => $this->input->post('comentarios', true),
					'checklist_id' => $this->input->post('id', true),
					'users_id' => $this->session->userdata('admin_id'),
					'created_at' => date('Y-m-d h:m:s')
				);

				$path="uploads/checklistdocs/".$folioenuso."/";

				//---- Archivo de 
			
				if(!empty($_FILES['solicitudcfdidocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'solicitudcfdidocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$solicitudcfdidocs = $path.$result['msg'];
						$solicitudcfdidates = date('Y-m-d h:m:s');
					}
					else{
						$solicitudcfdidates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}

				//---- Archivo de 

				if(!empty($_FILES['cambioderoldocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'cambioderoldocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$cambioderoldocs = $path.$result['msg'];
						$cambioderoldates = date('Y-m-d h:m:s'); 
					}
					else{
						$cambioderoldates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}

			   
			    //---- Archivo de 

				if(!empty($_FILES['cedulafiscaldocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'cedulafiscaldocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$cedulafiscaldocs = $path.$result['msg'];
						$cedulafiscaldates = date('Y-m-d h:m:s');
					}
					else{
						$cedulafiscaldates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}

				//---- Archivo de 
					 

					if(!empty($_FILES['cfdisinactividaddocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'cfdisinactividaddocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$cfdisinactividaddocs = $path.$result['msg'];
						$cfdisinactividaddates = date('Y-m-d h:m:s');
					}
					else{
						$cfdisinactividaddates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}
					 
					 //---- Archivo de 

					if(!empty($_FILES['xmldocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'xmldocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$xmldocs = $path.$result['msg'];
						$xmldates = date('Y-m-d h:m:s');
					}
					else{
						$xmldates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}
					 

					 //---- Archivo de 


					if(!empty($_FILES['rfcdocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'rfcdocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$rfcdocs = $path.$result['msg'];
						$rfcdates = date('Y-m-d h:m:s');
					}
					else{
						$rfcdates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}
					 

					 //---- Archivo de 


					if(!empty($_FILES['validacioncfdidocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'validacioncfdidocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$validacioncfdidocs = $path.$result['msg'];
						$validacioncfdidates = date('Y-m-d h:m:s');
					}
					else{
						$validacioncfdidates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}
					

					//---- Archivo de 


					if(!empty($_FILES['verificacioncfdidocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'verificacioncfdidocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$verificacioncfdidocs = $path.$result['msg'];
						$verificacioncfdidates = date('Y-m-d h:m:s');
					}
					else{
						$verificacioncfdidates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}
					

					//---- Archivo de 


					if(!empty($_FILES['autenticaciondoctosdocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'autenticaciondoctosdocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$autenticaciondoctosdocs = $path.$result['msg'];
						$autenticaciondoctosdates = date('Y-m-d h:m:s');
					}
					else{
						$autenticaciondoctosdates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}
					

					//---- Archivo de 


					if(!empty($_FILES['reportetransuniondocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'reportetransuniondocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$reportetransuniondocs = $path.$result['msg'];
						$reportetransuniondates = date('Y-m-d h:m:s');
					}
					else{
						$reportetransuniondates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}
					

					//---- Archivo de 


					if(!empty($_FILES['noretenciondocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'noretenciondocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$noretenciondocs = $path.$result['msg'];
						$noretenciondates = date('Y-m-d h:m:s');
					}
					else{
						$noretenciondates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}
					

					//---- Archivo de 


					if(!empty($_FILES['secuenciafacturasdocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'secuenciafacturasdocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$secuenciafacturasdocs = $path.$result['msg'];
						$secuenciafacturasdates = date('Y-m-d h:m:s');
					}
					else{
						$secuenciafacturasdates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}

					

					//---- Archivo de 


					if(!empty($_FILES['identificacionoficialdocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'identificacionoficialdocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$identificacionoficialdocs = $path.$result['msg'];
						$identificacionoficialdates = date('Y-m-d h:m:s');
					}
					else{
						$identificacionoficialdates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}

					

					//---- Archivo de 


					if(!empty($_FILES['curpdocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'curpdocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$curpdocs = $path.$result['msg'];
						$curpdates = date('Y-m-d h:m:s');
					}
					else{
						$curpdates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}

					

					//---- Archivo de 


					if(!empty($_FILES['comprobantedocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'comprobantedocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$comprobantedocs = $path.$result['msg'];
						$comprobantedates = date('Y-m-d h:m:s');
					}
					else{
						$comprobantedates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}

					

					//---- Archivo de 


					if(!empty($_FILES['bajaplacasdocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'bajaplacasdocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$bajaplacasdocs = $path.$result['msg'];
						$bajaplacasdates = date('Y-m-d h:m:s');
					}
					else{
						$bajaplacasdates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}

					

					//---- Archivo de 


					if(!empty($_FILES['tenenciasdocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'tenenciasdocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$tenenciasdocs = $path.$result['msg'];
						$tenenciasdates = date('Y-m-d h:m:s');
					}
					else{
						$tenenciasdates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}

					

					//---- Archivo de 


					if(!empty($_FILES['tarjetacirculaciondocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'tarjetacirculaciondocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$tarjetacirculaciondocs = $path.$result['msg'];
						$tarjetacirculaciondates = date('Y-m-d h:m:s');
					}
					else{
						$tarjetacirculaciondates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}

					

					//---- Archivo de 


					if(!empty($_FILES['responsivacompraventadocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'responsivacompraventadocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$responsivacompraventadocs = $path.$result['msg'];
						$responsivacompraventadates = date('Y-m-d h:m:s');
					}
					else{
						$responsivacompraventadates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}

					

					//---- Archivo de 


					if(!empty($_FILES['contratocompraventadocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'contratocompraventadocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$contratocompraventadocs = $path.$result['msg'];
						$contratocompraventadates = date('Y-m-d h:m:s');
					}
					else{
						$contratocompraventadates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}

					

					//---- Archivo de 


					if(!empty($_FILES['avisoprivacidaddocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'avisoprivacidaddocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$avisoprivacidaddocs = $path.$result['msg'];
						$avisoprivacidaddates = date('Y-m-d h:m:s');
					}
					else{
						$avisoprivacidaddates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}

					

					//---- Archivo de 


					if(!empty($_FILES['checklistrevisiondocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'checklistrevisiondocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$checklistrevisiondocs = $path.$result['msg'];
						$checklistrevisiondates = date('Y-m-d h:m:s');
					}
					else{
						$checklistrevisiondates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}

					

					//---- Archivo de 


					if(!empty($_FILES['evaluacionmecanicadocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'evaluacionmecanicadocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$evaluacionmecanicadocs = $path.$result['msg'];
						$evaluacionmecanicadates = date('Y-m-d h:m:s');
					}
					else{
						$evaluacionmecanicadates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}

					

					//---- Archivo de 


					if(!empty($_FILES['guiaautometricadocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'guiaautometricadocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$guiaautometricadocs = $path.$result['msg'];
						$guiaautometricadates = date('Y-m-d h:m:s');
					}
					else{
						$guiaautometricadates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}

					

					//---- Archivo de 


					if(!empty($_FILES['polizaderecepciondocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'polizaderecepciondocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$polizaderecepciondocs = $path.$result['msg'];
						$polizaderecepciondates = date('Y-m-d h:m:s');
					}
					else{
						$polizaderecepciondates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}

					

					//---- Archivo de 


					if(!empty($_FILES['cuentasporpagardocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'cuentasporpagardocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$cuentasporpagardocs = $path.$result['msg'];
						$cuentasporpagardates = date('Y-m-d h:m:s');
					}
					else{
						$cuentasporpagardates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}

					

					//---- Archivo de 


					if(!empty($_FILES['formatodedaciondocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'formatodedaciondocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$formatodedaciondocs = $path.$result['msg'];
						$formatodedaciondates = date('Y-m-d h:m:s');
					}
					else{
						$formatodedaciondates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}

					

					//---- Archivo de 


					if(!empty($_FILES['reportederobodocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'reportederobodocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$reportederobodocs = $path.$result['msg'];
						$reportederobodates = date('Y-m-d h:m:s');
					}
					else{
						$reportederobodates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}

					

					//---- Archivo de 


					if(!empty($_FILES['chequedeproveedordocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'chequedeproveedordocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$chequedeproveedordocs = $path.$result['msg'];
						$chequedeproveedordates = date('Y-m-d h:m:s');
					}
					else{
						$chequedeproveedordates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}

					

					//---- Archivo de 


					if(!empty($_FILES['solicitudchequedocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'solicitudchequedocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$solicitudchequedocs = $path.$result['msg'];
						$solicituddechequedates = date('Y-m-d h:m:s');
					}
					else{
						$solicituddechequedates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}

					

					//---- Archivo de 


					if(!empty($_FILES['edocuentadocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'edocuentadocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$edocuentadocs = $path.$result['msg'];
						$edocuentadates = date('Y-m-d h:m:s');
					}
					else{
						$edocuentadates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}

					

					//---- Archivo de 


					if(!empty($_FILES['calculoretenciondocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'calculoretenciondocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$calculoretenciondocs = $path.$result['msg'];
						$calculoretenciondates = date('Y-m-d h:m:s');
					}
					else{
						$calculoretenciondates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}
					

					//---- Archivo de 


					if(!empty($_FILES['autorizaciontomadocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'autorizaciontomadocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$autorizaciontomadocs = $path.$result['msg'];
						$autorizaciontomadates = date('Y-m-d h:m:s');
					}
					else{
						$autorizaciontomadates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}
					

					//---- Archivo de 


					if(!empty($_FILES['avaluorapidodocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'avaluorapidodocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$avaluorapidodocs = $path.$result['msg'];
						$avaluorapidodates = date('Y-m-d h:m:s');
					}
					else{
						$avaluorapidodates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}
					

					//---- Archivo de 


					if(!empty($_FILES['contratoadhesiondocs']['name']))
				{
					$result = $this->functions->file_insert($path, 'contratoadhesiondocs', 'pdf', '9097152');
					if($result['status'] == 1){
						$contratoadhesiondocs = $path.$result['msg'];
						$contratoadhesiondates = date('Y-m-d h:m:s');
					}
					else{
						$contratoadhesiondates = date('0000-00-00 00:');
						$this->session->set_flashdata('error', $result['msg']);
						redirect(base_url('admin/checks/'), 'refresh');
					}
				}

				$datadocs = array( 
					'rfc' => $rfcdocs,  
					'solicitudcfdi' => $solicitudcfdidocs,
					'cambioderol' => $cambioderoldocs,  
					'cedulafiscal' => $cedulafiscaldocs,
					'cfdisinactividad' => $cfdisinactividaddocs,
					'xml' => $xmldocs,
					'rfc' => $rfcdocs,
					'validacioncfdi' => $validacioncfdidocs,
					'verificacioncfdi' => $verificacioncfdidocs,
					'autenticaciondoctos' => $autenticaciondoctosdocs,
					'reportetransunion' => $reportetransuniondocs,
					'noretencion' => $noretenciondocs,
					'secuenciafacturas' => $secuenciafacturasdocs,
					'identificacionoficial' => $identificacionoficialdocs,
					'curp' => $curpdocs,
					'comprobante' => $comprobantedocs,
					'bajaplacas' => $bajaplacasdocs,
					'tenencias' => $tenenciasdocs,
					'tarjetacirculacion' => $tarjetacirculaciondocs,
					'responsivacompraventa' => $responsivacompraventadocs,
					'contratocompraventa' => $contratocompraventadocs,
					'avisoprivacidad' => $avisoprivacidaddocs,
					'checklistrevision' => $checklistrevisiondocs,
					'evaluacionmecanica' => $evaluacionmecanicadocs,
					'guiaautometrica' => $guiaautometricadocs,
					'polizaderecepcion' => $polizaderecepciondocs,
					'cuentasporpagar' => $cuentasporpagardocs,
					'formatodedacion' => $formatodedaciondocs,
					'reportederobo' => $reportederobodocs,
					'chequedeproveedor' => $chequedeproveedordocs,
					'solicitudcheque' => $solicitudchequedocs,
					'edocuenta' => $edocuentadocs,
					'calculoretencion' => $calculoretenciondocs,
					'autorizaciontoma' => $autorizaciontomadocs,
					'avaluorapido' => $avaluorapidodocs,
					'contratoadhesion' => $contratoadhesiondocs,
				);

				$datadates = array( 
					'rfc' => $rfcdates,  
					'solicitudcfdi' => $solicitudcfdidates, 
					'cambioderol' => $cambioderoldates,  
					'cedulafiscal' => $cedulafiscaldates,
					'cfdisinactividad' => $cfdisinactividaddates,
					'xml' => $xmldates,
					'rfc' => $rfcdates,
					'validacioncfdi' => $validacioncfdidates,
					'verificacioncfdi' => $verificacioncfdidates,
					'autenticaciondoctos' => $autenticaciondoctosdates,
					'reportetransunion' => $reportetransuniondates,
					'noretencion' => $noretenciondates,
					'secuenciafacturas' => $secuenciafacturasdates,
					'identificacionoficial' => $identificacionoficialdates,
					'curp' => $curpdates,
					'comprobante' => $comprobantedates,
					'bajaplacas' => $bajaplacasdates,
					'tenencias' => $tenenciasdates,
					'tarjetacirculacion' => $tarjetacirculaciondates,
					'responsivacompraventa' => $responsivacompraventadates,
					'contratocompraventa' => $contratocompraventadates,
					'avisoprivacidad' => $avisoprivacidaddates,
					'checklistrevision' => $checklistrevisiondates,
					'evaluacionmecanica' => $evaluacionmecanicadates,
					'guiaautometrica' => $guiaautometricadates,
					'polizaderecepcion' => $polizaderecepciondates,
					'cuentasporpagar' => $cuentasporpagardates,
					'formatodedacion' => $formatodedaciondates,
					'reportederobo' => $reportederobodates,
					'chequedeproveedor' => $chequedeproveedordates,
					'solicitudcheque' => $solicitudchequedates,
					'edocuenta' => $edocuentadates,
					'calculoretencion' => $calculoretenciondates,
					'autorizaciontoma' => $autorizaciontomadates,
					'avaluorapido' => $avaluorapidodates,
					'contratoadhesion' => $contratoadhesiondates,
				);

				$data = $this->security->xss_clean($data);
				$datac = $this->security->xss_clean($datac);
				$datadocs = $this->security->xss_clean($datadocs);
				$datadates = $this->security->xss_clean($datadates);

				$result = $this->check_model->edit_check($data, $id);

				$resultdocs = $this->check_model->edit_checkdocs($datadocs);
				$resultdates = $this->check_model->edit_checkdates($datadates);

				if($result){
					// Registro de actividades 
					$this->activity_model->add_log(29);
					$this->check_model->insert($datac, 'comentarios_has_checklist');
					$this->session->set_flashdata('success', 'La Marca ha sido actualizado con éxito!');
					redirect(base_url('admin/checks'));
				}
			}
		}
		else{
			$data['checks'] = $this->check_model->get_check_by_id($id);
			
			$this->load->view('admin/includes/_header');
			$this->load->view('admin/checks/check_edit', $data);
			$this->load->view('admin/includes/_footer');
		}
	}

	//-----------------------------------------------------------
	//  Ver check
	//-------------------------------------------------------
	public function view($id = 0){

		$this->rbac->check_operation_access(); //comprobar el permiso de operación
        
        $data['brands']=$this->check_model->get_all_simple_brands();
        $data['takes']=$this->check_model->get_all_takes_guides();
        $data['comments'] = $this->check_model->get_check_comments_by_id($id);
		 

		if($this->input->post('submit')){

			$this->form_validation->set_rules('folio', trans('folio'), 'trim|required');

			if ($this->form_validation->run() == FALSE) {
				$data = array(
					'errors' => validation_errors()
				);
				$this->session->set_flashdata('errors', $data['errors']);
				redirect(base_url('admin/guides/guide_edit/'.$id),'refresh');
			}
			else{
				$data = array(
					'valor' => $this->input->post('valor', true),
					'reacondicionamiento' => $this->input->post('reacondicionamiento', true),
					'premio' => $this->input->post('premio', true),
					'equipamiento' => $this->input->post('equipamiento', true),
					'compraautometrica' => $this->input->post('compraautometrica', true),
					'ventaautometrica' => $this->input->post('ventaautometrica', true),
					'km' => $this->input->post('km', true),
					'vin' => $this->input->post('vin', true),
					'folio' => $this->input->post('folio', true),
					'pdvapsr1' => $this->input->post('pdvapsr1a', true),
					'pdlttfac2' => $this->input->post('pdlttfac2a', true),
					'crpdltsi3' => $this->input->post('crpdltsi3a', true),
					'pdvapsi4' => $this->input->post('pdvapsi4a', true),
					'pdvapciyr5' => $this->input->post('pdvapciyr5a', true),
					'udlv6' => $this->input->post('udlv6a', true),
					'pdudlv7' => $this->input->post('pdudlv7', true),
					'pmdvap8' => $this->input->post('pmdvap8a', true),
					'cccr9' => $this->input->post('cccr9a', true),
					'pdvap10' => $this->input->post('pdvap10a', true),
					'ucidlu11' => $this->input->post('ucidlu11a', true),
					'idlu12' => $this->input->post('idlu12a', true),
					'fdume13' => $this->input->post('fdume13a', true),
					'models_id' => $this->input->post('modelo', true),
					'users_id' => $this->session->userdata('admin_id'),
					'tipoguias_id' => $this->input->post('tipoguia', true),
					'agencys_id' => $this->input->post('agencia', true),
					'status' => $this->input->post('status'),
					'updated_at' => date('Y-m-d h:m:s'),
				);
				$data = $this->security->xss_clean($data);
				$result = $this->guide_model->edit_model($data, $id);
				if($result){
					// Registro de actividades 
					$this->activity_model->add_log(26);
					$this->session->set_flashdata('success', 'La Guía ha sido actualizada con éxito!');
					redirect(base_url('admin/checks'));
				}
			}
		}
		else{
			$data['guide'] = $this->check_model->get_guide_by_id_business_view($id);
			$data['checks'] = $this->check_model->get_check_by_id($id);
			$data['comments'] = $this->check_model->get_check_comments_by_id($id);

			$data['all_users'] =  $this->check_model->get_all_simple_users();
			
			$this->load->view('admin/includes/_header');
			$this->load->view('admin/checks/check_view', $data);
			$this->load->view('admin/includes/_footer');
		}
	}

	//-----------------------------------------------------------
	//  Eliminar Check
	//-------------------------------------------------------
	public function delete($id = 0)
	{
		$this->rbac->check_operation_access(); // comprobar el permiso de operación
		$this->db->delete('anios', array('id' => $id));
		// Registro de actividades  
		$this->activity_model->add_log(30);
		$this->session->set_flashdata('success', 'La Marca se ha eliminado correctamente!');
		redirect(base_url('admin/checks'));
	}
	
	//-----------------------------------------------------------
	//  Exportar checks a CSV
	//-------------------------------------------------------
	public function export_csv(){ 
	   // nombre del archivo
		$filename = 'checks_'.date('Y-m-d').'.csv'; 
		header("Content-Description: File Transfer"); 
		header("Content-Disposition: attachment; filename=$filename"); 
		header("Content-Type: application/csv; ");

	   // obtener datos
		$model_data = $this->check_model->get_checks_for_csv();

	   // crear archivo
		$file = fopen('php://output', 'w');
		$header = array("ID", "Name", "Created Date"); 
		fputcsv($file, $header);
		foreach ($model_data as $key=>$line){ 
			fputcsv($file,$line); 
		}
		fclose($file); 
		exit; 
	}


	//---------------------------------------------------------------
	//  Exportar Check Individual a PDF
	//-------------------------------------------------------
	public function create_check_pdf($id = 0)
	{
		$this->load->helper('pdf_tcpdf_helper'); // loaded pdf helper
		$data['guide'] = $this->check_model->get_check_by_id_business_pdf($id);
		$this->load->view('admin/checks/check_pdf', $data);

	}


	//---------------------------------------------------------------
	//  Exportar Checklist a PDF
	//-------------------------------------------------------
	public function create_checks_pdf()
	{
		$this->load->helper('pdf_tcpdf_helper'); // loaded pdf helper
		$data['all_checks'] = $this->check_model->get_all_simple_checks();
		$this->load->view('admin/checks/check_tcpdf', $data);

	}

	//---------------------------------------------------------------
	//  Exportar Checklist Pendientes a PDF
	//-------------------------------------------------------
	public function create_checks_pdf_pendientes()
	{
		$this->load->helper('pdf_tcpdf_helper'); // loaded pdf helper
		$data['all_checks'] = $this->check_model->get_all_simple_checks_pendientes();
		$this->load->view('admin/checks/check_tcpdf', $data);

	}

	//---------------------------------------------------------------
	//  Exportar Checklist Proceso a PDF
	//-------------------------------------------------------
	public function create_checks_pdf_proceso()
	{
		$this->load->helper('pdf_tcpdf_helper'); // loaded pdf helper
		$data['all_checks'] = $this->check_model->get_all_simple_checks_proceso();
		$this->load->view('admin/checks/check_tcpdf', $data);

	}

	//---------------------------------------------------------------
	//  Exportar Checklist Terminados a PDF
	//-------------------------------------------------------
	public function create_checks_pdf_terminados()
	{
		$this->load->helper('pdf_tcpdf_helper'); // loaded pdf helper
		$data['all_checks'] = $this->check_model->get_all_simple_checks_terminados();
		$this->load->view('admin/checks/check_tcpdf', $data);

	}

	//---------------------------------------------------------------
	//  Exportar Checklist Pendientes a PDF
	//-------------------------------------------------------
	public function create_checks_pdf_libres()
	{
		$this->load->helper('pdf_tcpdf_helper'); // loaded pdf helper
		$data['all_checks'] = $this->check_model->get_all_simple_checks_libres();
		$this->load->view('admin/checks/check_tcpdf', $data);

	}



	//---------------------------------------------------
	// Busqueda Avanzada
	//-------------------------------------------------------
	public function advance_search(){

		$this->session->unset_modeldata('model_search_type');
		$this->session->unset_modeldata('model_search_from');
		$this->session->unset_modeldata('model_search_to');

		$data['title'] = 'Advanced Search with Datatable';

		$this->load->view('admin/includes/_header');
		$this->load->view('admin/checks/advance_search', $data);
		$this->load->view('admin/includes/_footer');
	}

	//---------------------------------------------------------------
	//  Funcion de Busquedas
	//-------------------------------------------------------
	function search(){

		$this->session->set_modeldata('model_search_type',$this->input->post('model_search_type'));
		$this->session->set_modeldata('model_search_from',$this->input->post('model_search_from'));
		$this->session->set_modeldata('model_search_to',$this->input->post('model_search_to'));
	}




}


?>