<?php
	class Brand_model extends CI_Model{

		public function add_brand($data){
			$this->db->insert('brands', $data);
			return true;
		}

		//---------------------------------------------------
		// obtener todas las marcas para el procesamiento de tablas de datos del lado del servidor (basado en ajax)
		//-----------------------------------------------------
		public function get_all_brands(){
			$this->db->select('*');
			//$this->db->where('status',1);
			return $this->db->get('brands')->result_array();
		}

		//---------------------------------------------------
		// Obtener información de marca por ID
		//-----------------------------------------------------
		public function get_brand_by_id($id){
			$query = $this->db->get_where('brands', array('id' => $id));
			return $result = $query->row_array();
		}

		//---------------------------------------------------
		// Editar registro de marca
		//-----------------------------------------------------
		public function edit_brand($data, $id){
			$this->db->where('id', $id);
			$this->db->update('brands', $data);
			return true;
		}

		//---------------------------------------------------
		// Cambiar status de marca
		//-----------------------------------------------------
		function change_status()
		{		
			$this->db->set('status', $this->input->post('status'));
			$this->db->where('id', $this->input->post('id'));
			$this->db->update('brands');
		} 

		//---------------------------------------------------
		// obtener marcas para exportar a csv
		//-----------------------------------------------------
		public function get_brands_for_csv(){
			$this->db->where('status', 1);
			$this->db->select('id, name, created_at');
			$this->db->from('brands');
			$query = $this->db->get();
			return $result = $query->result_array();
		}

		//---------------------------------------------------
		// obtener todos los registros de marcas 
		//-----------------------------------------------------
		public function get_all_simple_brands(){
			$this->db->where('status', 1);
			$this->db->order_by('created_at', 'desc');
			$query = $this->db->get('brands');
			return $result = $query->result_array();
		}

	}

?>