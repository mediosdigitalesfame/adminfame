  <!-- daterange picker -->

  <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/daterangepicker/daterangepicker-bs3.css">

  <!-- iCheck for checkboxes and radio inputs -->

  <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/iCheck/all.css">

  <!-- Bootstrap Color Picker -->

  <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/colorpicker/bootstrap-colorpicker.min.css">

  <!-- Bootstrap time Picker -->

  <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/timepicker/bootstrap-timepicker.min.css">

  <!-- Select2 -->

  <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/select2/select2.min.css">



  <!-- Content Wrapper. Contains page content -->

  <div class="content-wrapper">

    <!-- Main content -->

    <section class="content">

      <div class="card card-default">



        <div class="card-header">

          <div class="d-inline-block">

            <h3 class="card-title"> <i class="fa fa-plus"></i>

             <?= trans('add_new_inspect') ?> </h3>

           </div>

           <div class="d-inline-block float-right">

            <a href="<?= base_url('admin/inspects'); ?>" class="btn btn-success"><i class="fa fa-list"></i>  <?= trans('inspects_list') ?></a>

          </div>

        </div>



        <div class="card-body">



         <!-- For Messages -->

         <?php $this->load->view('admin/includes/_messages.php') ?>

         <?php echo form_open(base_url('admin/takes/add'), 'class="form-horizontal"');  ?> 

         

         <input type="hidden" id="identi" name="identi" value="">

         <input type="hidden" id="id" name="id" value="">

 



<div class="form-group">
           <div class="row">
            <div class="col-lg-3"></div>

            <div class="col-lg-2">
              <div class="input-group">
                <label for="firstname" alling="right" class="pull-right col-md-12 control-label">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Agencia:</label>  
              </div>
            </div>

            <div class="col-lg-3">
              <div class="input-group">
               <div class="col-md-12">
                <select name="agencia" id="agencia" class="form-control select2" onChange="cambiafolio();">
                  <option value=""><?= trans('select_agencys') ?></option>
                  <?php foreach($agencys as $agency): ?>
                    <option value="<?= $agency['id']; ?>"><?= $agency['name']; ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>
          </div>

          <div class="col-lg-1">
            <div class="input-group">
              <label for="firstname" alling="right" class="col-md-12 control-label">&nbsp;&nbsp;&nbsp;&nbsp;Folio:</label>
            </div>
          </div>
          <div class="col-lg-3">
            <div class="input-group">
              <div class="col-md-12">
                <input type="text" id="folio" name="folio" class="form-control" placeholder="" readonly required>
              </div>
            </div>
          </div>
        </div>
      </div>

 
      <div class="form-group">

       <div class="row">

        <div class="col-lg-6">
          <div class="input-group">
           <label for="vin" class="col-md-12 control-label">Nombre del Asesor</label>
           <div class="col-md-12">
            <input type="text" class="form-control" placeholder="" id="nombre" name="nombre" maxlength="17" onkeyup="javascript:this.value=this.value.toUpperCase();">
          </div>
        </div>
      </div>

<div class="col-lg-6">
          <div class="input-group">
           <label for="vin" class="col-md-12 control-label">Nombre del Cliente</label>
           <div class="col-md-12">
            <input type="text" class="form-control" placeholder="" id="nombre" name="nombre" maxlength="17" onkeyup="javascript:this.value=this.value.toUpperCase();">
          </div>
        </div>
      </div>

      
</div>
</div>



<div class="form-group">
 <div class="row">
  <div class="col-lg-2">
    <div class="input-group">
     <label for="urlp" class="col-md-12 control-label">Correo Electronico</label>
     <div class="col-md-12">
      <input type="text" class="form-control" placeholder="" name="numext" id="numext">
    </div>
  </div>
</div>
<div class="col-lg-2">
  <div class="input-group">
   <label for="urlp" class="col-md-12 control-label">Telefono</label>
   <div class="col-md-12">
    <input type="text" class="form-control" placeholder="" name="numint" id="numint">
  </div>
</div>
</div>

<div class="col-lg-4">
  <div class="input-group">
   <label for="vin" class="col-md-12 control-label">No. Orden Rep.</label>
   <div class="col-md-12">
    <input type="text" class="form-control" placeholder="" name="colonia" id="colonia" maxlength="17" onkeyup="javascript:this.value=this.value.toUpperCase();">
  </div>
</div>
</div>

<div class="col-lg-2">
  <div class="input-group">
   <label for="urlp" class="col-md-12 control-label">Año/Modelo</label>
   <div class="col-md-12">
    <input type="text" class="form-control" placeholder="" name="cp" id="cp">
  </div>
</div>
</div>

<div class="col-lg-2">
  <div class="input-group">
   <label for="urlp" class="col-md-12 control-label">Kilometraje</label>
   <div class="col-md-12">
    <input type="text" class="form-control" placeholder="" name="ciudad" id="ciudad">
  </div>
</div>

</div>
</div>
</div>

  

</div>
<!-- /.box-body -->
</div>

<div class="card card-default">
   <div class="card-header">
  <div class="d-inline-block">
    <h3 class="card-title"> Datos del Vehículo</h3>
  </div>
</div>



  <div class="card-body">
      
          <div class="form-group">
     <div class="row">
      <div class="col-lg-3">
        <div class="input-group">
         <label for="urlp" class="col-md-12 control-label">PUNTO DE REVISION </label>
      </div>
    </div>
        <div class="col-lg-2">
            <div class="input-group">
         <label for="urlp" class="col-md-12 control-label">ESTADO</label>
      </div>
    </div>
    <div class="col-lg-3">
        <div class="input-group">
         <label for="urlp" class="col-md-12 control-label">PRESUPUESTO</label>
      </div>
    </div>
</div>
</div>
      
 

    <div class="form-group">
     <div class="row">
      <div class="col-lg-3">
        <div class="input-group">
         <label for="urlp" class="col-md-12 control-label">1. Balatas Delanteras</label>
      </div>
    </div>
        <div class="col-lg-2">
        <div class="input-group">
         <div class="col-lg-12">
              <div class="input-group">
                       
                <select name="1a" id="1a" class="form-control <?php if(this.value == 1){echo 'bg-success color-palette';} ?> " >
                    <option value="0" class="bg-success color-palette">Funciona Correctamente</option>
                    <option value="1" class="bg-warning color-palette">Atencion Futura</option>
                    <option value="2" class="bg-danger color-palette">Atencion Inmediata</option>
                </select>
 
            </div>
          </div>      </div>
    </div>
    <div class="col-lg-3">
        <div class="input-group">
         <div class="col-md-12">
          <input type="text" class="form-control" placeholder="" name="1p" id="1p">
        </div>
      </div>
    </div>
</div>
</div>

<div class="form-group">
     <div class="row">
      <div class="col-lg-3">
        <div class="input-group">
         <label for="urlp" class="col-md-12 control-label">2. Balatas Traseras</label>
      </div>
    </div>
        <div class="col-lg-2">
        <div class="input-group">
         <div class="col-md-12">
           <select name="1a" id="1a" class="form-control" >
                    <option value="0" class="bg-success color-palette">Funciona Correctamente</option>
                    <option value="1" class="bg-warning color-palette">Atencion Futura</option>
                    <option value="2" class="bg-danger color-palette">Atencion Inmediata</option>
                </select>
        </div>
      </div>
    </div>
    <div class="col-lg-3">
        <div class="input-group">
         <div class="col-md-12">
          <input type="text" class="form-control" placeholder="" name="1p" id="1p">
        </div>
      </div>
    </div>
</div>
</div>

<div class="form-group">
     <div class="row">
      <div class="col-lg-3">
        <div class="input-group">
         <label for="urlp" class="col-md-12 control-label">3. Condiciones de Neumaticos</label>
      </div>
    </div>
        <div class="col-lg-2">
        <div class="input-group">
         <div class="col-md-12">
           <select name="1a" id="1a" class="form-control" >
                    <option value="0" class="bg-success color-palette">Funciona Correctamente</option>
                    <option value="1" class="bg-warning color-palette">Atencion Futura</option>
                    <option value="2" class="bg-danger color-palette">Atencion Inmediata</option>
                </select>
        </div>
      </div>
    </div>
    <div class="col-lg-3">
        <div class="input-group">
         <div class="col-md-12">
          <input type="text" class="form-control" placeholder="" name="1p" id="1p">
        </div>
      </div>
    </div>
</div>
</div>

<div class="form-group">
     <div class="row">
      <div class="col-lg-3">
        <div class="input-group">
         <label for="urlp" class="col-md-12 control-label">4. Llanta de Refaccion</label>
      </div>
    </div>
        <div class="col-lg-2">
        <div class="input-group">
         <div class="col-md-12">
           <select name="1a" id="1a" class="form-control" >
                    <option value="0" class="bg-success color-palette">Funciona Correctamente</option>
                    <option value="1" class="bg-warning color-palette">Atencion Futura</option>
                    <option value="2" class="bg-danger color-palette">Atencion Inmediata</option>
                </select>
        </div>
      </div>
    </div>
    <div class="col-lg-3">
        <div class="input-group">
         <div class="col-md-12">
          <input type="text" class="form-control" placeholder="" name="1p" id="1p">
        </div>
      </div>
    </div>
</div>
</div>

<div class="form-group">
     <div class="row">
      <div class="col-lg-3">
        <div class="input-group">
         <label for="urlp" class="col-md-12 control-label">5. Condicion de Carga de Bateria</label>
      </div>
    </div>
        <div class="col-lg-2">
        <div class="input-group">
         <div class="col-md-12">
           <select name="1a" id="1a" class="form-control" >
                    <option value="0" class="bg-success color-palette">Funciona Correctamente</option>
                    <option value="1" class="bg-warning color-palette">Atencion Futura</option>
                    <option value="2" class="bg-danger color-palette">Atencion Inmediata</option>
                </select>
        </div>
      </div>
    </div>
    <div class="col-lg-3">
        <div class="input-group">
         <div class="col-md-12">
          <input type="text" class="form-control" placeholder="" name="1p" id="1p">
        </div>
      </div>
    </div>
</div>
</div>

<div class="form-group">
     <div class="row">
      <div class="col-lg-3">
        <div class="input-group">
         <label for="urlp" class="col-md-12 control-label">6. Terminales de Bateria de Cables y Base</label>
      </div>
    </div>
        <div class="col-lg-2">
        <div class="input-group">
         <div class="col-md-12">
           <select name="1a" id="1a" class="form-control" >
                    <option value="0" class="bg-success color-palette">Funciona Correctamente</option>
                    <option value="1" class="bg-warning color-palette">Atencion Futura</option>
                    <option value="2" class="bg-danger color-palette">Atencion Inmediata</option>
                </select>
        </div>
      </div>
    </div>
    <div class="col-lg-3">
        <div class="input-group">
         <div class="col-md-12">
          <input type="text" class="form-control" placeholder="" name="1p" id="1p">
        </div>
      </div>
    </div>
</div>
</div>

<div class="form-group">
     <div class="row">
      <div class="col-lg-3">
        <div class="input-group">
         <label for="urlp" class="col-md-12 control-label">7. Faros, Direccionales Frontales, Luces Altas y Faros de Niebla</label>
      </div>
    </div>
        <div class="col-lg-2">
        <div class="input-group">
         <div class="col-md-12">
           <select name="1a" id="1a" class="form-control" >
                    <option value="0" class="bg-success color-palette">Funciona Correctamente</option>
                    <option value="1" class="bg-warning color-palette">Atencion Futura</option>
                    <option value="2" class="bg-danger color-palette">Atencion Inmediata</option>
                </select>
        </div>
      </div>
    </div>
    <div class="col-lg-3">
        <div class="input-group">
         <div class="col-md-12">
          <input type="text" class="form-control" placeholder="" name="1p" id="1p">
        </div>
      </div>
    </div>
</div>
</div>

<div class="form-group">
     <div class="row">
      <div class="col-lg-3">
        <div class="input-group">
         <label for="urlp" class="col-md-12 control-label">8. Luces Exteriores Traseras</label>
      </div>
    </div>
        <div class="col-lg-2">
        <div class="input-group">
         <div class="col-md-12">
           <select name="1a" id="1a" class="form-control" >
                    <option value="0" class="bg-success color-palette">Funciona Correctamente</option>
                    <option value="1" class="bg-warning color-palette">Atencion Futura</option>
                    <option value="2" class="bg-danger color-palette">Atencion Inmediata</option>
                </select>
        </div>
      </div>
    </div>
    <div class="col-lg-3">
        <div class="input-group">
         <div class="col-md-12">
          <input type="text" class="form-control" placeholder="" name="1p" id="1p">
        </div>
      </div>
    </div>
</div>
</div>

<div class="form-group">
     <div class="row">
      <div class="col-lg-3">
        <div class="input-group">
         <label for="urlp" class="col-md-12 control-label">9. Amortiguadores Delanteros y Traseros</label>
      </div>
    </div>
        <div class="col-lg-2">
        <div class="input-group">
         <div class="col-md-12">
           <select name="1a" id="1a" class="form-control" >
                    <option value="0" class="bg-success color-palette">Funciona Correctamente</option>
                    <option value="1" class="bg-warning color-palette">Atencion Futura</option>
                    <option value="2" class="bg-danger color-palette">Atencion Inmediata</option>
                </select>
        </div>
      </div>
    </div>
    <div class="col-lg-3">
        <div class="input-group">
         <div class="col-md-12">
          <input type="text" class="form-control" placeholder="" name="1p" id="1p">
        </div>
      </div>
    </div>
</div>
</div>

<div class="form-group">
     <div class="row">
      <div class="col-lg-3">
        <div class="input-group">
         <label for="urlp" class="col-md-12 control-label">10. Suspencion Delantera y Trasera</label>
      </div>
    </div>
        <div class="col-lg-2">
        <div class="input-group">
         <div class="col-md-12">
           <select name="1a" id="1a" class="form-control" >
                    <option value="0" class="bg-success color-palette">Funciona Correctamente</option>
                    <option value="1" class="bg-warning color-palette">Atencion Futura</option>
                    <option value="2" class="bg-danger color-palette">Atencion Inmediata</option>
                </select>
        </div>
      </div>
    </div>
    <div class="col-lg-3">
        <div class="input-group">
         <div class="col-md-12">
          <input type="text" class="form-control" placeholder="" name="1p" id="1p">
        </div>
      </div>
    </div>
</div>
</div>

<div class="form-group">
     <div class="row">
      <div class="col-lg-3">
        <div class="input-group">
         <label for="urlp" class="col-md-12 control-label">11. Operacion y Estado de Limpiaparabrisas</label>
      </div>
    </div>
        <div class="col-lg-2">
        <div class="input-group">
         <div class="col-md-12">
           <select name="1a" id="1a" class="form-control" >
                    <option value="0" class="bg-success color-palette">Funciona Correctamente</option>
                    <option value="1" class="bg-warning color-palette">Atencion Futura</option>
                    <option value="2" class="bg-danger color-palette">Atencion Inmediata</option>
                </select>
        </div>
      </div>
    </div>
    <div class="col-lg-3">
        <div class="input-group">
         <div class="col-md-12">
          <input type="text" class="form-control" placeholder="" name="1p" id="1p">
        </div>
      </div>
    </div>
</div>
</div>

<div class="form-group">
     <div class="row">
      <div class="col-lg-3">
        <div class="input-group">
         <label for="urlp" class="col-md-12 control-label">12. Operacion del Claxon</label>
      </div>
    </div>
        <div class="col-lg-2">
        <div class="input-group">
         <div class="col-md-12">
           <select name="1a" id="1a" class="form-control" >
                    <option value="0" class="bg-success color-palette">Funciona Correctamente</option>
                    <option value="1" class="bg-warning color-palette">Atencion Futura</option>
                    <option value="2" class="bg-danger color-palette">Atencion Inmediata</option>
                </select>
        </div>
      </div>
    </div>
    <div class="col-lg-3">
        <div class="input-group">
         <div class="col-md-12">
          <input type="text" class="form-control" placeholder="" name="1p" id="1p">
        </div>
      </div>
    </div>
</div>
</div>

<div class="form-group">
     <div class="row">
      <div class="col-lg-3">
        <div class="input-group">
         <label for="urlp" class="col-md-12 control-label">13. Inspeccion Visual de Fugas en Flechas de Velocidad Constante</label>
      </div>
    </div>
        <div class="col-lg-2">
        <div class="input-group">
         <div class="col-md-12">
           <select name="1a" id="1a" class="form-control" >
                    <option value="0" class="bg-success color-palette">Funciona Correctamente</option>
                    <option value="1" class="bg-warning color-palette">Atencion Futura</option>
                    <option value="2" class="bg-danger color-palette">Atencion Inmediata</option>
                </select>
        </div>
      </div>
    </div>
    <div class="col-lg-3">
        <div class="input-group">
         <div class="col-md-12">
          <input type="text" class="form-control" placeholder="" name="1p" id="1p">
        </div>
      </div>
    </div>
</div>
</div>

<div class="form-group">
     <div class="row">
      <div class="col-lg-3">
        <div class="input-group">
         <label for="urlp" class="col-md-12 control-label">14. Banda de Accesorios</label>
      </div>
    </div>
        <div class="col-lg-2">
        <div class="input-group">
         <div class="col-md-12">
           <select name="1a" id="1a" class="form-control" >
                    <option value="0" class="bg-success color-palette">Funciona Correctamente</option>
                    <option value="1" class="bg-warning color-palette">Atencion Futura</option>
                    <option value="2" class="bg-danger color-palette">Atencion Inmediata</option>
                </select>
        </div>
      </div>
    </div>
    <div class="col-lg-3">
        <div class="input-group">
         <div class="col-md-12">
          <input type="text" class="form-control" placeholder="" name="1p" id="1p">
        </div>
      </div>
    </div>
</div>
</div>

<div class="form-group">
     <div class="row">
      <div class="col-lg-3">
        <div class="input-group">
         <label for="urlp" class="col-md-12 control-label">15. Funcionamineto del Sistema de A/C, Mangueras y Conexiones del Sistema</label>
      </div>
    </div>
        <div class="col-lg-2">
        <div class="input-group">
         <div class="col-md-12">
           <select name="1a" id="1a" class="form-control" >
                    <option value="0" class="bg-success color-palette">Funciona Correctamente</option>
                    <option value="1" class="bg-warning color-palette">Atencion Futura</option>
                    <option value="2" class="bg-danger color-palette">Atencion Inmediata</option>
                </select>
        </div>
      </div>
    </div>
    <div class="col-lg-3">
        <div class="input-group">
         <div class="col-md-12">
          <input type="text" class="form-control" placeholder="" name="1p" id="1p">
        </div>
      </div>
    </div>
</div>
</div>

<div class="form-group">
     <div class="row">
      <div class="col-lg-3">
        <div class="input-group">
         <label for="urlp" class="col-md-12 control-label">16. Filtro de Aire Acondicionado</label>
      </div>
    </div>
        <div class="col-lg-2">
        <div class="input-group">
         <div class="col-md-12">
           <select name="1a" id="1a" class="form-control" >
                    <option value="0" class="bg-success color-palette">Funciona Correctamente</option>
                    <option value="1" class="bg-warning color-palette">Atencion Futura</option>
                    <option value="2" class="bg-danger color-palette">Atencion Inmediata</option>
                </select>
        </div>
      </div>
    </div>
    <div class="col-lg-3">
        <div class="input-group">
         <div class="col-md-12">
          <input type="text" class="form-control" placeholder="" name="1p" id="1p">
        </div>
      </div>
    </div>
</div>
</div>

<div class="form-group">
     <div class="row">
      <div class="col-lg-3">
        <div class="input-group">
         <label for="urlp" class="col-md-12 control-label">17. Radiador y Tapa de Radiador</label>
      </div>
    </div>
        <div class="col-lg-2">
        <div class="input-group">
         <div class="col-md-12">
           <select name="1a" id="1a" class="form-control" >
                    <option value="0" class="bg-success color-palette">Funciona Correctamente</option>
                    <option value="1" class="bg-warning color-palette">Atencion Futura</option>
                    <option value="2" class="bg-danger color-palette">Atencion Inmediata</option>
                </select>
        </div>
      </div>
    </div>
    <div class="col-lg-3">
        <div class="input-group">
         <div class="col-md-12">
          <input type="text" class="form-control" placeholder="" name="1p" id="1p">
        </div>
      </div>
    </div>
</div>
</div>

<div class="form-group">
     <div class="row">
      <div class="col-lg-3">
        <div class="input-group">
         <label for="urlp" class="col-md-12 control-label">18. Funcionamiento del Clutch (Si Aplica)</label>
      </div>
    </div>
        <div class="col-lg-2">
        <div class="input-group">
         <div class="col-md-12">
           <select name="1a" id="1a" class="form-control" >
                    <option value="0" class="bg-success color-palette">Funciona Correctamente</option>
                    <option value="1" class="bg-warning color-palette">Atencion Futura</option>
                    <option value="2" class="bg-danger color-palette">Atencion Inmediata</option>
                </select>
        </div>
      </div>
    </div>
    <div class="col-lg-3">
        <div class="input-group">
         <div class="col-md-12">
          <input type="text" class="form-control" placeholder="" name="1p" id="1p">
        </div>
      </div>
    </div>
</div>
</div>

<div class="form-group">
     <div class="row">
      <div class="col-lg-3">
        <div class="input-group">
         <label for="urlp" class="col-md-12 control-label">19. Nivel de Fluidos, Aceite, Liquido de Frenos y Anticongelante</label>
      </div>
    </div>
        <div class="col-lg-2">
        <div class="input-group">
         <div class="col-md-12">
           <select name="1a" id="1a" class="form-control" >
                    <option value="0" class="bg-success color-palette">Funciona Correctamente</option>
                    <option value="1" class="bg-warning color-palette">Atencion Futura</option>
                    <option value="2" class="bg-danger color-palette">Atencion Inmediata</option>
                </select>
        </div>
      </div>
    </div>
    <div class="col-lg-3">
        <div class="input-group">
         <div class="col-md-12">
          <input type="text" class="form-control" placeholder="" name="1p" id="1p">
        </div>
      </div>
    </div>
</div>
</div>

<div class="form-group">
     <div class="row">
      <div class="col-lg-3">
        <div class="input-group">
         <label for="urlp" class="col-md-12 control-label">20. Freno de Mano</label>
      </div>
    </div>
        <div class="col-lg-2">
        <div class="input-group">
         <div class="col-md-12">
           <select name="1a" id="1a" class="form-control" >
                    <option value="0" class="bg-success color-palette">Funciona Correctamente</option>
                    <option value="1" class="bg-warning color-palette">Atencion Futura</option>
                    <option value="2" class="bg-danger color-palette">Atencion Inmediata</option>
                </select>
        </div>
      </div>
    </div>
    <div class="col-lg-3">
        <div class="input-group">
         <div class="col-md-12">
          <input type="text" class="form-control" placeholder="" name="1p" id="1p">
        </div>
      </div>
    </div>
</div>
</div>

<div class="form-group">
     <div class="row">
      <div class="col-lg-3">
        <div class="input-group">
         <label for="urlp" class="col-md-12 control-label">21. Fuga de Liquidos (Liquido de Enfriamiento, Tansmicion y Aceite)</label>
      </div>
    </div>
        <div class="col-lg-2">
        <div class="input-group">
         <div class="col-md-12">
           <select name="1a" id="1a" class="form-control" >
                    <option value="0" class="bg-success color-palette">Funciona Correctamente</option>
                    <option value="1" class="bg-warning color-palette">Atencion Futura</option>
                    <option value="2" class="bg-danger color-palette">Atencion Inmediata</option>
                </select>
        </div>
      </div>
    </div>
    <div class="col-lg-3">
        <div class="input-group">
         <div class="col-md-12">
          <input type="text" class="form-control" placeholder="" name="1p" id="1p">
        </div>
      </div>
    </div>
</div>
</div>

<div class="form-group">
     <div class="row">
      <div class="col-lg-3">
        <div class="input-group">
         <label for="urlp" class="col-md-12 control-label">22. Mofle/Escape</label>
      </div>
    </div>
        <div class="col-lg-2">
        <div class="input-group">
         <div class="col-md-12">
           <select name="1a" id="1a" class="form-control" >
                    <option value="0" class="bg-success color-palette">Funciona Correctamente</option>
                    <option value="1" class="bg-warning color-palette">Atencion Futura</option>
                    <option value="2" class="bg-danger color-palette">Atencion Inmediata</option>
                </select>
        </div>
      </div>
    </div>
    <div class="col-lg-3">
        <div class="input-group">
         <div class="col-md-12">
          <input type="text" class="form-control" placeholder="" name="1p" id="1p">
        </div>
      </div>
    </div>
</div>
</div>

<div class="form-group">
     <div class="row">
      <div class="col-lg-3">
        <div class="input-group">
         <label for="urlp" class="col-md-12 control-label">23. Nivel de Fluidos</label>
      </div>
    </div>
        <div class="col-lg-2">
        <div class="input-group">
         <div class="col-md-12">
           <select name="1a" id="1a" class="form-control" >
                    <option value="0" class="bg-success color-palette">Funciona Correctamente</option>
                    <option value="1" class="bg-warning color-palette">Atencion Futura</option>
                    <option value="2" class="bg-danger color-palette">Atencion Inmediata</option>
                </select>
        </div>
      </div>
    </div>
    <div class="col-lg-3">
        <div class="input-group">
         <div class="col-md-12">
          <input type="text" class="form-control" placeholder="" name="1p" id="1p">
        </div>
      </div>
    </div>
</div>
</div>

<div class="form-group">
     <div class="row">
      <div class="col-lg-3">
        <div class="input-group">
         <label for="urlp" class="col-md-12 control-label">24. Flecha Propulsora</label>
      </div>
    </div>
        <div class="col-lg-2">
        <div class="input-group">
         <div class="col-md-12">
           <select name="1a" id="1a" class="form-control" >
                    <option value="0" class="bg-success color-palette">Funciona Correctamente</option>
                    <option value="1" class="bg-warning color-palette">Atencion Futura</option>
                    <option value="2" class="bg-danger color-palette">Atencion Inmediata</option>
                </select>
        </div>
      </div>
    </div>
    <div class="col-lg-3">
        <div class="input-group">
         <div class="col-md-12">
          <input type="text" class="form-control" placeholder="" name="1p" id="1p">
        </div>
      </div>
    </div>
</div>
</div>

<div class="form-group">
     <div class="row">
      <div class="col-lg-3">
        <div class="input-group">
         <label for="urlp" class="col-md-12 control-label">25. Lineas de Combustible y Conexiones</label>
      </div>
    </div>
        <div class="col-lg-2">
        <div class="input-group">
         <div class="col-md-12">
           <select name="1a" id="1a" class="form-control" >
                    <option value="0" class="bg-success color-palette">Funciona Correctamente</option>
                    <option value="1" class="bg-warning color-palette">Atencion Futura</option>
                    <option value="2" class="bg-danger color-palette">Atencion Inmediata</option>
                </select>
        </div>
      </div>
    </div>
    <div class="col-lg-3">
        <div class="input-group">
         <div class="col-md-12">
          <input type="text" class="form-control" placeholder="" name="1p" id="1p">
        </div>
      </div>
    </div>
</div>
</div>

<div class="form-group">
     <div class="row">
      <div class="col-lg-3">
        <div class="input-group">
         <label for="urlp" class="col-md-12 control-label">1. Balatas Delanteras</label>
      </div>
    </div>
        <div class="col-lg-2">
        <div class="input-group">
         <div class="col-md-12">
           <select name="1a" id="1a" class="form-control" >
                    <option value="0" class="bg-success color-palette">Funciona Correctamente</option>
                    <option value="1" class="bg-warning color-palette">Atencion Futura</option>
                    <option value="2" class="bg-danger color-palette">Atencion Inmediata</option>
                </select>
        </div>
      </div>
    </div>
    <div class="col-lg-3">
        <div class="input-group">
         <div class="col-md-12">
          <input type="text" class="form-control" placeholder="" name="1p" id="1p">
        </div>
      </div>
    </div>
</div>
</div>



  
 

</div>

</div>

 

<div class="card card-default">

  <div class="card-body">

    <div class="form-group">

      <div class="col-md-12">

        <input type="submit" name="submit" value="<?= trans('add_inspect') ?>" class="btn btn-primary pull-right">

      </div>

    </div>

    <?php echo form_close( ); ?>

  </div>

</div>



</section> 

</div>



<!-- Select2 -->

<script src="<?= base_url() ?>assets/plugins/select2/select2.full.min.js"></script>

<!-- InputMask -->

<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.js"></script>

<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>

<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>

<!-- date-range-picker -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>

<script src="<?= base_url() ?>assets/plugins/daterangepicker/daterangepicker.js"></script>



<!-- bootstrap color picker -->

<script src="<?= base_url() ?>assets/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>

<!-- bootstrap time picker -->

<script src="<?= base_url() ?>assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>

<!-- Sweet Alert -->

<script src="<?= base_url() ?>assets/dist/js/bootstrap-datepicker.min.js"></script>

<!-- Page script -->

<!-- iCheck 1.0.1 -->

<script src="<?= base_url() ?>assets/plugins/iCheck/icheck.min.js"></script>





<script language="javascript">



  jQuery('#fechaidenti, #fechafac, #fechabaja, #fechafacfinal, #fechatoma, #fechacontrato, #fechanaci').datepicker({

    format: 'yyyy-mm-dd'

  });



</script>



<script>

  $(function () {

    //Initialize Select2 Elements

    $('.select2').select2()



    //Datemask dd/mm/yyyy

    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })

    //Datemask2 mm/dd/yyyy

    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })

    //Datemask3 yyyy

    $('#datemask3').inputmask('yyyy', { 'placeholder': 'yyyy' })

    //Money Euro

    $('[data-mask]').inputmask()



 

    //Date range picker

    $('#reservation').daterangepicker()

    //Date range picker with time picker

    $('#reservationtime').daterangepicker({

      timePicker         : true,

      timePickerIncrement: 30,

      format             : 'MM/DD/YYYY h:mm A'

    })

    //Date range as a button

    $('#daterange-btn').daterangepicker(

    {

      ranges   : {

        'Today'       : [moment(), moment()],

        'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],

        'Last 7 Days' : [moment().subtract(6, 'days'), moment()],

        'Last 30 Days': [moment().subtract(29, 'days'), moment()],

        'This Month'  : [moment().startOf('month'), moment().endOf('month')],

        'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]

      },

      startDate: moment().subtract(29, 'days'),

      endDate  : moment()

    },

    function (start, end) {

      $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))

    }

    )



    //iCheck for checkbox and radio inputs

    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({

      checkboxClass: 'icheckbox_minimal-blue',

      radioClass   : 'iradio_minimal-blue'

    })

    //Red color scheme for iCheck

    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({

      checkboxClass: 'icheckbox_minimal-red',

      radioClass   : 'iradio_minimal-red'

    })

    //Flat red color scheme for iCheck

    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({

      checkboxClass: 'icheckbox_flat-green',

      radioClass   : 'iradio_flat-green'

    })



    //Colorpicker

    $('.my-colorpicker1').colorpicker()

    //color picker with addon

    $('.my-colorpicker2').colorpicker()



    //Timepicker

    $('.timepicker').timepicker({

      showInputs: false

    })

  })

</script>



<script>

  $("#forms").addClass('active');

  $("#advanced").addClass('active');

</script>  





