  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1> </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#"><?= trans('home') ?></a></li>
              <li class="breadcrumb-item active"><?= trans('guide') ?></li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

            <!-- Main content -->
            <div class="invoice p-3 mb-3">
              <!-- title row -->
              <div class="row">
                <div class="col-12">
                  <h4>
                    <i class="fa fa-clipboard"></i> Guía para la Toma de Seminuevos
                    <small class="float-right">Fecha de Creación: <?php echo $guide['created_at']; ?></small>
                  </h4>
                </div>
                <!-- /.col -->
              </div>
              <br>
              <!-- info row -->
              <div class="row invoice-info">
                <div class="col-sm-4 invoice-col">
                  <address>
                    <strong><?php echo $guide['agname']; ?></strong><br>
                    <?php echo $guide['agtel']; ?><br>
                    <?php echo $guide['folio']; ?><br>
                  </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                  <address>
                    <strong><?php echo $guide['version'].' '.$guide['persona'];; ?></strong><br>
                    % de Utilidad: <?php echo $guide['pdudlv7']; ?>%<br>
                  </address>
                </div>
                <div class="col-sm-4 invoice-col">
                  <address>
                    <strong>Status</strong><br>

                    <?php if($guide['status'] == 1): ?>
                      <span class="badge bg-info">Activa!</span>
                      <?php else: ?>
                       <span class=" badge bg-danger">Pendiente!</span>
                     <?php endif; ?>

                     <br>
                   </address>
                 </div>
                <!-- /.col 
                <div class="col-sm-4 invoice-col">
                  <b><?php echo $guide['brname']; ?></b><br>
                  <b>Año:</b><?php echo $guide['anname']; ?><br>
                  <b>Modelo:</b><?php echo $guide['moname']; ?><br>
                  <b>Versión:</b><?php echo $guide['moversion']; ?><br>
                </div>
                /.col -->
              </div>
              <!-- /.row -->

              <!-- Table row -->
              <div class="row">
                <div class="col-12 table-responsive">
                  <table class="table table-striped">
                    <thead>
                      <tr>
                        <th>Marca</th>
                        <th>Año</th>
                        <th>Modelo</th>
                         <th>Km</th>
                        <th>Modelo</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td><?php echo $guide['brname']; ?></td>
                        <td><?php echo $guide['anname']; ?></td>
                        <td><?php echo $guide['moname']; ?></td>
                        <td><?php echo $guide['km']; ?></td>
                        <td><?php echo $guide['moversion']; ?></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <div class="row">
                <!-- accepted payments column -->
                <div class="col-3">

                  <img src="<?= base_url($guide['image']); ?>" class="logo" width="250"></p>

                </div>
                <!-- /.col -->
                <div class="col-9">

                  <?php  

                

                  $fmt = new NumberFormatter('en_US', NumberFormatter::CURRENCY);


                  ?>

                  <div class="table-responsive">
                    <table class="table">

                      <?php if($guide['pmdvap8'] >= 1): ?>
                        <tr style="display: flex;">
                          <?php else: ?>
                           <tr style="display: none;">
                           <?php endif; ?>

                           <th style="width:70%"  >Precio Minimo de Venta al Publico Reacondicionado:</th>
                           <td><?php echo $fmt->formatCurrency($guide['pmdvap8'], "USD"); ?></td>
                         </tr>

                         <tr>
                           <th>Precio de la toma tope al cliente con iva:</th>
                           <td><?php echo  $fmt->formatCurrency($guide['pdlttfac2'], "USD"); ?></td>
                         </tr>

                         <tr>
                          <th>Precio de venta al publico con iva y reacondicionamiento:</th>
                          <td><?php echo $fmt->formatCurrency($guide['pdvapciyr5'], "USD"); ?></td>
                        </tr>

                      </table>
                    </div>
                  </div>
                  <!-- /.col -->
                </div>
                <!-- /.row -->

                <!-- this row will not appear when printing -->
                <div class="row no-print">
                  <div class="col-12">
                    <!--<a href="invoice-print.html" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>-->

                    <!--<button type="button" class="btn btn-primary float-right" style="margin-right: 5px;">
                      <i class="fa fa-pencil"></i> Editar
                    </button>-->
                  </div>
                </div>
              </div>
              <!-- /.invoice -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </section>
      <!-- /.content -->
    </div>
  <!-- /.content-wrapper -->