  <!-- daterange picker -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/daterangepicker/daterangepicker-bs3.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/colorpicker/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/select2/select2.min.css">


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      <div class="card card-default">
        <div class="card-header">
          <div class="d-inline-block">
            <h3 class="card-title"> <i class="fa fa-plus"></i>
             <?= trans('add_new_business') ?> </h3>
           </div>
           <div class="d-inline-block float-right">
            <a href="<?= base_url('admin/business'); ?>" class="btn btn-success"><i class="fa fa-list"></i>  <?= trans('business_list') ?></a>
          </div>
        </div>
        <div class="card-body">

         <!-- For Messages -->
         <?php $this->load->view('admin/includes/_messages.php') ?>
         <?php echo form_open(base_url('admin/business/add'), 'class="form-horizontal"');  ?> 

         <div class="form-group">
           <div class="row">
            <div class="col-lg-4">
              <div class="input-group">
                <label for="name" class="col-md-6 control-label"><?= trans('name') ?></label>
                <div class="col-md-12">
                  <input type="text" name="name" class="form-control" id="name" placeholder="" >
                </div>
              </div>
            </div>
            <div class="col-lg-5">
              <div class="input-group">
               <label for="rsocial" class="col-md-6 control-label"><?= trans('rsocial') ?></label>
               <div class="col-md-12">
                <input type="text" name="rsocial" class="form-control" id="rsocial" placeholder="">
              </div>
            </div>
          </div>
          <div class="col-lg-3">
            <div class="input-group">
              <label for="rfc" class="col-md-6 control-label"><?= trans('rfc') ?></label>
              <div class="col-md-12">
                <input type="text" name="rfc" class="form-control" id="rfc" placeholder="">
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="form-group">
       <div class="row">
        <div class="col-lg-3">
          <div class="input-group">
            <label for="tel" class="col-md-12 control-label"><?= trans('tel') ?></label>
            <div class="col-md-12">
              <input type="text" name="tel" class="form-control" id="tel" placeholder="">
            </div>
          </div>
        </div>
        <div class="col-lg-5">
          <div class="input-group">
            <label for="calle" class="col-md-12 control-label"><?= trans('street') ?></label>
            <div class="col-md-12">
              <input type="text" name="calle" class="form-control" id="calle" placeholder="">
            </div>
          </div>
        </div>
        <div class="col-lg-2">
          <div class="input-group">
           <label for="int" class="col-md-12 control-label"><?= trans('int') ?></label>
           <div class="col-md-12">
            <input type="text" name="int" class="form-control" id="int" placeholder="">
          </div>
        </div>
      </div>
      <div class="col-lg-2">
        <div class="input-group">
          <label for="ext" class="col-md-12 control-label"><?= trans('ext') ?></label>
          <div class="col-md-12">
            <input type="text" name="ext" class="form-control" id="ext" placeholder="">
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="form-group">
   <div class="row">
    <div class="col-lg-6">
      <div class="input-group">
       <label for="colonia" class="col-md-12 control-label"><?= trans('suburb') ?></label>
       <div class="col-md-12">
        <input type="text" name="colonia" class="form-control" id="colonia" placeholder="">
      </div>
    </div>
  </div>
  <div class="col-lg-2">
    <div class="input-group">
      <label for="cp" class="col-md-12 control-label"><?= trans('postcode') ?></label>
      <div class="col-md-12">
        <input type="text" name="cp" class="form-control" id="cp" placeholder="">
      </div>
    </div>
  </div>
  <div class="col-lg-2">
    <div class="input-group">
      <label for="ciudad" class="col-md-12 control-label"><?= trans('city') ?></label>
      <div class="col-md-12">
        <input type="text" name="ciudad" class="form-control" id="ciudad" placeholder="">
      </div>
    </div>
  </div>
  <div class="col-lg-2">
    <div class="input-group">
      <label for="estado" class="col-md-12 control-label"><?= trans('state') ?></label>
      <div class="col-md-12">
        <input type="text" name="estado" class="form-control" id="estado" placeholder="">
      </div>
    </div>
  </div>
</div>
</div>

<div class="form-group">
 <div class="row">
  <div class="col-lg-4">
    <div class="input-group">
      <label for="banco" class="col-md-12 control-label"><?= trans('bank') ?></label>
      <div class="col-md-12">
        <input type="text" name="banco" class="form-control" id="banco" placeholder="">
      </div>
    </div>
  </div>
  <div class="col-lg-4">
    <div class="input-group">
      <label for="cuenta" class="col-md-12 control-label"><?= trans('bill') ?></label>
      <div class="col-md-12">
        <input type="text" name="cuenta" class="form-control" id="rsocial" placeholder="">
      </div>
    </div>
  </div>
  <div class="col-lg-4">
    <div class="input-group">
     <label for="pago" class="col-md-12 control-label"><?= trans('pay') ?></label>
     <div class="col-md-12">
      <input type="text" name="pago" class="form-control" id="pago" placeholder="">
    </div>
  </div>
</div>
</div>
</div>

<div class="form-group">
  <div class="col-md-12">
    <input type="submit" name="submit" value="<?= trans('add_model') ?>" class="btn btn-primary pull-right">
  </div>
</div>
<?php echo form_close( ); ?>
</div>
<!-- /.box-body -->
</div>
</section> 
</div>


<!-- Select2 -->
<script src="<?= base_url() ?>assets/plugins/select2/select2.full.min.js"></script>
<!-- InputMask -->
<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src="<?= base_url() ?>assets/plugins/daterangepicker/daterangepicker.js"></script>
<!-- bootstrap color picker -->
<script src="<?= base_url() ?>assets/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="<?= base_url() ?>assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- Page script -->
<!-- iCheck 1.0.1 -->
<script src="<?= base_url() ?>assets/plugins/iCheck/icheck.min.js"></script>



<script>
  ! function(window, document, $) {
    "use strict";
    $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
  }(window, document, jQuery);
</script>




<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker         : true,
      timePickerIncrement: 30,
      format             : 'MM/DD/YYYY h:mm A'
    })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
    {
      ranges   : {
        'Today'       : [moment(), moment()],
        'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month'  : [moment().startOf('month'), moment().endOf('month')],
        'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      },
      startDate: moment().subtract(29, 'days'),
      endDate  : moment()
    },
    function (start, end) {
      $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
    }
    )

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>

<script>
  $("#forms").addClass('active');
  $("#advanced").addClass('active');
</script>  


