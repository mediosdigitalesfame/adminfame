<?php

date_default_timezone_set('America/Mexico_City');
   
$fecha = $take['fechacontrato']; 

$month = date('M', strtotime($fecha));
$year = date('Y', strtotime($fecha));
$day = date('d', strtotime($fecha));

$partes = explode('-', $month); 
$_fecha = "$year.$month.$day";


$fmt = new NumberFormatter('en_US', NumberFormatter::CURRENCY);
$formatterES = new NumberFormatter("es-ES", NumberFormatter::SPELLOUT);

$izquierda = intval(floor($take['pdlttfac2']));
$derecha = intval(($take['pdlttfac2'] - floor($take['pdlttfac2'])) * 100);



$html = '
<table border="0" style="width:100%">
	<tbody>

	    <table border="0">
			<tr align="center">
				<td><h1><>CARTA RESPONSIVA DE COMPRAVENTA</h1></td><br>
			</tr>
			<tr align="center">
				<td></td><br>
			</tr>
		</table>

		<table border="0">
			<tr align="right">
				<td>'.$take['agciudad'].', ' .$take['agestado'].' a: '.$day.' de ' .$month.' del ' .$year.'</td>
			</tr>
			<tr align="right">
				<td></td>
			</tr>
		</table>

		<table border="0" align="justify">
			<tr>
				<td>
					Reunidos por una parte el comprador <font color="#0A6ACF">'.$take['bursocial'].'</font> con domicilio fiscal <font color="#0A6ACF">'.$take['bucalle'].', '.$take['buint'].', Col.'.$take['bucolonia'].', C.P.'.$take['bucp'].', </font> en la ciudad de <font color="#0A6ACF">'.$take['buciudad'].'</font>. <br><br>

					Por otra parte el vendedor: <u><font color="#0A6ACF">'.$take['nombre'].'</font></u>, con domicilio en calle <u><font color="#0A6ACF">'.$take['calle'].'</font></u> numero <u><font color="#0A6ACF">'.$take['numext'].'</font></u>, colonia <u><font color="#0A6ACF">'.$take['colonia'].'</font>, <font color="#0A6ACF">'.$take['municipio'].'</font>, <font color="#0A6ACF">'.$take['ciudad'].'</font></u>, en el estado de <u><font color="#0A6ACF">'.$take['estado'].'</font></u>, código postal <u><font color="#0A6ACF">'.$take['cp'].'</font></u>. Se identificó con identificación oficial <u><font color="#0A6ACF">'.$take['tipoidenti'].'</font></u> número <u><font color="#0A6ACF">'.$take['nidenti'].'</font></u>. <br>Se acordo elaborar una carta responsiva al tenor de las siguientes clausulas.<br><br><br>

					1a. El vendedor entrega en este acto al comprador los documentos y el automóvil que a continuación se describe: <br>
				</td>
			</tr>
		</table>

		<table border="0">
			<tr>
				<td width="100">
					Marca:<br>
					Tipo:<br>
					Serie:<br>
					Placa:<br>
					Factura:<br>
				</td>
				<td width="150">
					<font color="#0A6ACF">'.$take['brname'].'</font><br>
					<font color="#0A6ACF">'.$take['versiontxt'].'</font><br>
					<font color="#0A6ACF">'.$take['nserie'].'</font><br>
					<font color="#0A6ACF">'.$take['placasbaja'].'</font><br>
					<font color="#0A6ACF">'.$take['facturavehi'].'</font><br>
				</td>
				<td>
					Modelo:<br>
					Color:<br>
					Motor:<br>
					Interior:<br><br>
				</td>
				<td>
					<font color="#0A6ACF">'.$take['modelotxt'].'</font><br>
					<font color="#0A6ACF">'.$take['colorext'].'</font><br>
					<font color="#0A6ACF">'.$take['nmotor'].'</font><br>
					<font color="#0A6ACF">'.$take['colorint'].'</font><br>
				</td>
			</tr>

			<tr>
				<td colspan="2">
					Expedida por: <font color="#0A6ACF">'.$take['expedida'].'</font><br> 
				</td>
			</tr>

		</table>

		<table border="0">
			<tr>
				<td>
					Tenencias por los años:  <u><font color="#0A6ACF">'.$take['tenencias'].'</font></u>  baja de placas, verificación.<br>Quien manifiesta que este automóviles de su propiedad y lo vende a <u><font color="#0A6ACF">'.$take['bursocial'].'</font></u>  en la cantidad de:  <u><font color="#0A6ACF">'.$fmt->formatCurrency($take['pdlttfac2'], "USD").'</font>   <font color="#0A6ACF">('.strtoupper($formatterES->format($izquierda)).' PESOS ' .$derecha.'/100 M.N.)</font> </u><br><br>
					Entregando la documentación antes descrita. <br> 
				</td>
			</tr>
		</table>

		<table border="0" align="justify">
			<tr>
				<td>
					2a. Declaro expresamente bajo propuesta de decir verdad, que el vehículo que les eh vendido lo adquiri legalmente y lo entrego completamente libre de cualquier gravamen o vicio, constituyéndome el único responsable de su saneamiento, solventaré todo problema que se presente respecto de su legítima propiedad, por su legal y definitiva estancia en el pais o por cualquier responsabilidad de carácter civil, penal o fiscal que por lo mismo resultare hasta la fecha, por lo que, en su caso me obligo expresamente a efectuar de inmediato la devolución del dinero que he recibido independientemente de la liquidación y reparación del monto de los daños y perjuicios que les causaren con tal motivo, asi como de las reparaciones hechas al vehículo.<br> <br> 

					3a. Las partes manifiestan su absoluta conformidad con las cláusulas que anteceden, firmando al calce para constancia.<br><br> 

					OBSERVACIÓN: <br><br><br><br><br><br><br><br><br><br>
				</td>
			</tr>
		</table>

		<table align="center">
			<tr>
				<td>
					___________________________<br>
					<font color="#0A6ACF">'.$take['nombrescfdi'].' '. $take['apellido1'].' '. $take['apellido2'].'</font><br>
					Vendedor
				</td>
				<td>
					___________________________<br>
					<font color="#0A6ACF">'.$take['ciadfirstnamegg'].' '.$take['ciadlastnamegg'].'</font><br>
					Gerente General
				</td>
				<td>
					___________________________<br>
					<font color="#0A6ACF">'.$take['ciadfirstnamegs'].' '.$take['ciadlastnamegs'].'</font><br>
					Gerente de Seminuevos
				</td>
			</tr>
		</table>';

$html .='</tbody> </table>';

tcpdf();
$obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
header('Content-type: application/pdf');
$obj_pdf->SetCreator(PDF_CREATOR);
$title = "Carta Responsiva de compra";
$obj_pdf->SetTitle($title);
$obj_pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $title, PDF_HEADER_STRING);
$obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$obj_pdf->SetDefaultMonospacedFont('helvetica');
$obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$obj_pdf->SetFont('helvetica', '', 9);
$obj_pdf->setFontSubsetting(false);
$obj_pdf->AddPage();
ob_start();
    // podemos tener cualquier parte de la vista aquí como HTML, PHP, etc.
$content = ob_get_contents();
ob_end_clean();
$obj_pdf->writeHTML($content, true, false, true, false, '');
$obj_pdf->writeHTML($html, true, false, true, false, '');
$filename = 'carta_responsiva';
$obj_pdf->Output($filename . '.pdf', 'D');
?>