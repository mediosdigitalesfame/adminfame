<?php

$fecha = $take['fechacontrato']; 

$month = date('M', strtotime($fecha));
$year = date('Y', strtotime($fecha));
$day = date('d', strtotime($fecha));

$partes = explode('-', $month); 
$_fecha = "$year.$month.$day";

$fmt = new NumberFormatter('en_US', NumberFormatter::CURRENCY);
$formatterES = new NumberFormatter("es-ES", NumberFormatter::SPELLOUT);

$izquierda = intval(floor($take['pdlttfac2']));
$derecha = intval(($take['pdlttfac2'] - floor($take['pdlttfac2'])) * 100);

$utilidad = ($take['pdlttfac2'])-($take['costoadquisicion']);
$retencion = ($take['pdlttfac2'])*0.2;

$uma= 89.62;
$umaanual = $uma*365;
$umaanualx3 = $umaanual*3;

if($utilidad>$umaanualx3) {$retencionreal = $retencion; } else {$retencionreal = 0;}

 

$html = '<table border="0" style="width:100%">
    <tbody>
	 
		 <table border="0">
			<tr align="center">
				<td><h1>CÁLCULO DE RETENCIÓN EN COMPRA DE <br> AUTOS USADOS DE PERSONAS FÍSICAS:</h1></td><br>
			</tr>
			<tr align="center">
				<td><h5> Fundamento: LISR. Art. 126, 124 y 93 fracc. XIX inciso b (No aplica Actualización del Costo de Adquisición y depreciación del 20% anual)</h5></td><br>
			</tr>
		</table>

		<table>

		    <tr>
				<td WIDTH="100%">
				</td>
			</tr>
		   
			<tr>
				<td WIDTH="100%">Nombre:<font color="#0A6ACF">' .$take['nombre'].'</font><br></td>
			</tr>
			<tr>
				<td WIDTH="100%">RFC:<font color="#0A6ACF">' .$take['rfc'].'</font> - <font color="#0A6ACF">'.$take['reginame'].'</font><br></td>
			</tr>
			<tr>
				<td WIDTH="100%">Domicilio:	<font color="#0A6ACF">Calle:' .$take['calle'].', Num. Ext: '.$take['numext'].', Num. Int:'.$take['numint'].', Colonia:'.$take['colonia'].', Mucicipio:'.$take['municipio'].', Ciudad:'.$take['ciudad'].', Estado:'.$take['estado'].', CP:'.$take['cp'].'</font><br></td>
			</tr>
			<tr>
				<td WIDTH="100%">Fecha Enajenacion:<font color="#0A6ACF">' .$take['fechacontrato'].'</font><br></td>
			</tr>
			<tr>
				<td WIDTH="100%">Fecha Adquisicion:<font color="#0A6ACF">' .$take['fechafac'].'</font><br></td>
			</tr>
			<tr>
				<td WIDTH="100%">Valor Enajenacion:<font color="#0A6ACF">' .$fmt->formatCurrency($take['pdlttfac2'], "USD").'</font><br></td>
			</tr>
			<tr>
				<td WIDTH="100%">Costo Adquisicion:<font color="#0A6ACF">' .$fmt->formatCurrency($take['costoadquisicion'], "USD").'</font><br></td>
			</tr>
			<tr>
				<td WIDTH="100%">Retencion 20%:<font color="#0A6ACF">' .$fmt->formatCurrency($retencion, "USD").'</font><br></td>
			</tr>
			<tr>
				<td WIDTH="100%">Utilidad:<font color="#0A6ACF">' .$fmt->formatCurrency($utilidad, "USD").'</font><br></td>
			</tr>
			<tr>
				<td WIDTH="100%">Parte Exenta:<font color="#0A6ACF">'.$fmt->formatCurrency($umaanualx3, "USD").'</font><br></td>
			</tr>
			<tr>
				<td WIDTH="100%">Retencion:<font color="#0A6ACF">' .$fmt->formatCurrency($retencionreal, "USD").'</font><br></td>
			</tr>
			<tr>
				<td WIDTH="100%">El cálculo de Retención en Compra de Autos Usados de Personas Físicas sólo se realiza en el caso de que el valor de la unidad sea igual o mayor a: $ 227,400.01 <br></td>
			</tr>
		


	    </table>
		 
    ';
$html.='</tbody> </table>';

tcpdf();
$obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
header('Content-type: application/pdf');
$obj_pdf->SetCreator(PDF_CREATOR);
$title = "Calculo de retención";
$obj_pdf->SetTitle($title);
$obj_pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $title, PDF_HEADER_STRING);
$obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$obj_pdf->SetDefaultMonospacedFont('helvetica');
$obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$obj_pdf->SetFont('helvetica', '', 9);
$obj_pdf->setFontSubsetting(false);
$obj_pdf->AddPage();
ob_start();
    // podemos tener cualquier parte de la vista aquí como HTML, PHP, etc.
$content = ob_get_contents();
ob_end_clean();
$obj_pdf->writeHTML($content, true, false, true, false, '');
$obj_pdf->writeHTML($html, true, false, true, false, '');


$filename = 'calculo_retencion';

$obj_pdf->Output($filename . '.pdf', 'D');




?>