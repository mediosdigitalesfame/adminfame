<?php

$fecha = $take['fechacontrato']; 

$month = date('M', strtotime($fecha));
$year = date('Y', strtotime($fecha));
$day = date('d', strtotime($fecha));

$partes = explode('-', $month); 
$_fecha = "$year.$month.$day";

$fmt = new NumberFormatter('en_US', NumberFormatter::CURRENCY);
$formatterES = new NumberFormatter("es-ES", NumberFormatter::SPELLOUT);

$izquierda = intval(floor($take['pdlttfac2']));
$derecha = intval(($take['pdlttfac2'] - floor($take['pdlttfac2'])) * 100);

$html = '
<table border="0" style="width:100%">
	<tbody>

	    <table border="0">
			<tr align="center">
				<td><h1>SOLICITUD DE CHEQUE</h1></td><br>
			</tr>
			<tr align="right">
				<td> </td><br>
			</tr>
		</table>

		 <table border="0">
			<tr align="center">
				<td> </td> 
			</tr>
			<tr align="right">
				<td>Folio:'.$take['folio'].'</td><br>
			</tr>

			<tr>
               <td align="right" > Marca:'.$take['brname'].' </td>
            </tr>
            <tr>
		       <td align="right"> Version:'.$take['modelotxt'].' - '.$take['versiontxt'].' </td> 
	        </tr>
	        <tr>
		       <td align="right"> Año Modelo: '.$take['anname'].'</td> 
	        </tr>


			<tr align="right">
				<td>'.$take['agciudad'].', ' .$take['agestado'].' a: '.$day.' de ' .$month.' del ' .$year.'</td><br>
			</tr>
		</table>

		<table>
			<tr>
				<td WIDTH="100">
					<b>Nombre:<br><br>
						Importe:<br><br>
						Letra:<br><br>
					</b>
				</td>
				<td WIDTH="600" >
					<font color="#0A6ACF">'.$take['nombre'].'</font><br><br>
					<font color="#0A6ACF">'.$fmt->formatCurrency($take['pdlttfac2'], "USD").'</font><br><br>
					<font color="#0A6ACF">('.strtoupper($formatterES->format($izquierda)).' PESOS ' .$derecha.'/100 M.N.)</font><br><br>
				</td>
			</tr>
			<table>
				<table>
					<tr>
						<td WIDTH="100">
							<b>Concepto:<br><br>
								Folios Docto:<br><br>
							</b>
				        </td>
				        <td WIDTH="400" >
							<font color="#0A6ACF"> PAGO DE UNIDAD USADA </font><br><br>
							<font color="#0A6ACF"> 0 </font><br><br><br><br>
				        </td>
				    </tr>
				<table>
				<table>
					<tr>
						<td>
							<b>Banco:<br><br>
								Cuenta:<br><br>
								Clabe:
							</b>
						</td>

						<td>
							<font color="#0A6ACF">'.$take['banco'].'</font><br><br>
							<font color="#0A6ACF">'.$take['cuenta'].'</font><br><br>
							<font color="#0A6ACF">'.$take['clabe'].'</font><br><br>
						</td>

						<td>
							<b>Sucursal:<br><br>
								Convenio:<br><br>
								Referencia:
							</b>
						</td>
						<td>
							<font color="#0A6ACF">'.$take['sucursal'].'</font><br><br>
							<font color="#0A6ACF">'.$take['convenio'].'</font><br><br>
							<font color="#0A6ACF">'.$take['referencia'].'</font><br><br>
						</td>
					</tr>
				<table>

				<table>
					<tr>
						<td WIDTH="100">
							<b>Area:</b>
						</td>
						<td WIDTH="400">
							<font color="#0A6ACF"> SEMINUEVOS </font><br><br>
							<br><br><br><br><br><br><br>
							<br><br><br><br><br><br><br>
						</td>
					</tr>
				</table>



				<table border="0" class="table table-bordered">
  <tbody>

    

  </tbody>
</table>





				<table>
					<tr>
						<td WIDTH="200">
							<label align="center">
								___________________________________<br>
								<b>
									Nombre y firma<br>
									Coordinador
								</b>
							</label>
						</td>
						<td WIDTH="35"></td>
						<td WIDTH="35"></td>
						<td WIDTH="200">
							<label align="center">
								___________________________________<br>
								<b>'.$take['ciadfirstnamegg'].' '.$take['ciadlastnamegg'].'<br>
								Gerente General</b>
							</label>
						</td>
				    </tr>
		    ';

$html.='</tbody></table>';

tcpdf();
$obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
header('Content-type: application/pdf');
$obj_pdf->SetCreator(PDF_CREATOR);
$title = "Solicitud de Cheque";
$obj_pdf->SetTitle($title);
$obj_pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $title, PDF_HEADER_STRING);
$obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$obj_pdf->SetDefaultMonospacedFont('helvetica');
$obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$obj_pdf->SetFont('helvetica', '', 9);
$obj_pdf->setFontSubsetting(false);
$obj_pdf->AddPage();
ob_start();
    // podemos tener cualquier parte de la vista aquí como HTML, PHP, etc.
$content = ob_get_contents();
ob_end_clean();
$obj_pdf->writeHTML($content, true, false, true, false, '');
$obj_pdf->writeHTML($html, true, false, true, false, '');


$filename = 'solicitud_cheque';

$obj_pdf->Output($filename . '.pdf', 'D');




?>