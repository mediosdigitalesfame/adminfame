<!-- daterange picker -->
<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/daterangepicker/daterangepicker-bs3.css">
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/iCheck/all.css">
<!-- Bootstrap Color Picker -->
<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/colorpicker/bootstrap-colorpicker.min.css">
<!-- Bootstrap time Picker -->
<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/timepicker/bootstrap-timepicker.min.css">
<!-- Select2 -->
<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/select2/select2.min.css">

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Main content -->
  <section class="content">
    <div class="card card-default">
      <div class="card-header">
        <div class="d-inline-block">
          <h3 class="card-title"> <i class="fa fa-pencil"></i>
            &nbsp; <?= trans('edit_checks') ?> </h3>
          </div>
          <div class="d-inline-block float-right">
            <a href="<?= base_url('admin/checks'); ?>" class="btn btn-success"><i class="fa fa-list"></i> <?= trans('checks_list') ?></a>
            <a href="<?= base_url('admin/checks/add'); ?>" class="btn btn-success"><i class="fa fa-plus"></i> <?= trans('add_new_check') ?></a>
          </div>
        </div>
        <div class="card-body">

         <!-- For Messages -->
         <?php $this->load->view('admin/includes/_messages.php') ?>

         <?php echo form_open(base_url('admin/checks/edit/'.$checks['id']), 'class="form-horizontal"' )?> 

         <input type="hidden" name="id" value="<?php echo html_escape($checks['id']); ?>">

         <div class="form-group">
          <div class="row">
            <div class="col-lg-4">
              <div class="input-group">
                <label for="tomasel" class="col-md-12 control-label"><?= trans('select_take') ?>*</label>
                <div class="col-md-12">
                  <select id="tomasel" name="tomasel" class="form-control select2" disabled>
                    <option value=""><?= trans('select_take') ?></option>
                    <?php foreach($takes as $take): ?>
                      <?php if($takes['id'] == $check['seminuevostomados_id']): ?>
                        <option value="<?= $take['id']; ?>" selected><?= $take['folio']; ?></option>
                        <?php else: ?>
                          <option value="<?= $take['id']; ?>"><?= $take['folio']; ?></option>
                        <?php endif; ?>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="row">
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">Solicitud CFDI</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="solicitudcfdi" id="solicitudcfdi" type="checkbox" value="1"  <?php if($checks['solicitudcfdi'] == 1){echo 'checked';} ?> > 
                    <label for="solicitudcfdi"></label>
                  </div>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">Cambio de Rol</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="cambioderol" id="cambioderol" type="checkbox" value="1" <?php if($checks['cambioderol'] == 1){echo 'checked';} ?> > <label for="cambioderol"></label>
                  </div>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">Cédula de Id. Fiscal</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="cedulafiscal" id="cedulafiscal" type="checkbox" value="1" <?php if($checks['cedulafiscal'] == 1){echo 'checked';} ?> > <label for="cedulafiscal"></label>
                  </div>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">CFDI Sin Act. Emp.</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="cfdisinactividad" id="cfdisinactividad" type="checkbox" value="1" <?php if($checks['cfdisinactividad'] == 1){echo 'checked';} ?> > <label for="cfdisinactividad"></label>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="row">
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">XML de DFDI</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="xml" id="xml" type="checkbox" value="1" <?php if($checks['xml'] == 1){echo 'checked';} ?> > <label for="xml"></label>
                  </div>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">Ratificación de RFC</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="rfc" id="rfc" type="checkbox" value="1" <?php if($checks['rfc'] == 1){echo 'checked';} ?> > <label for="rfc"></label>
                  </div>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">Validación Fiscal de CFDI</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="validacioncfdi" id="validacioncfdi" type="checkbox" value="1" <?php if($checks['validacioncfdi'] == 1){echo 'checked';} ?> > <label for="validacioncfdi"></label>
                  </div>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">Verifiación Fiscal de CFDI</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="verificacioncfdi" id="verificacioncfdi" type="checkbox" value="1" <?php if($checks['verificacioncfdi'] == 1){echo 'checked';} ?> > <label for="verificacioncfdi"></label>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="row">
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">Autenticacion de Doctos.</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="autenticaciondoctos" id="autenticaciondoctos" type="checkbox" value="1" <?php if($checks['autenticaciondoctos'] == 1){echo 'checked';} ?> > <label for="autenticaciondoctos"></label>
                  </div>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">Reporte de Transunion</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="reportetransunion" id="reportetransunion" type="checkbox" value="1" <?php if($checks['reportetransunion'] == 1){echo 'checked';} ?> > <label for="reportetransunion"></label>
                  </div>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">Cer. No Retencion de ISR</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="noretencion" id="noretencion" type="checkbox" value="1" <?php if($checks['noretencion'] == 1){echo 'checked';} ?> > <label for="noretencion"></label>
                  </div>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">Facturas del Auto</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="secuenciafacturas" id="secuenciafacturas" type="checkbox" value="1" <?php if($checks['secuenciafacturas'] == 1){echo 'checked';} ?> > <label for="secuenciafacturas"></label>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="row">
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">Identificación</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="identificacionoficial" id="identificacionoficial" type="checkbox" value="1" <?php if($checks['identificacionoficial'] == 1){echo 'checked';} ?> > <label for="identificacionoficial"></label>
                  </div>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">CURP</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="curp" id="curp" type="checkbox" value="1" <?php if($checks['curp'] == 1){echo 'checked';} ?> > <label for="curp"></label>
                  </div>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">Comprobante Domicilio</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="comprobante" id="comprobante" type="checkbox" value="1" <?php if($checks['comprobante'] == 1){echo 'checked';} ?> > <label for="comprobante"></label>
                  </div>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">Baja de Placas</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="bajaplacas" id="bajaplacas" type="checkbox" value="1" <?php if($checks['bajaplacas'] == 1){echo 'checked';} ?> > <label for="bajaplacas"></label>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="row">
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">Historial de Tenencias</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="tenencias" id="tenencias" type="checkbox" value="1" <?php if($checks['tenencias'] == 1){echo 'checked';} ?> > <label for="tenencias"></label>
                  </div>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">Tarjeta de Circulación</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="tarjetacirculacion" id="tarjetacirculacion" type="checkbox" value="1" <?php if($checks['tarjetacirculacion'] == 1){echo 'checked';} ?> > <label for="tarjetacirculacion"></label>
                  </div>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">Carta Res. Compraventa</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="responsivacompraventa" id="responsivacompraventa" type="checkbox" value="1" <?php if($checks['responsivacompraventa'] == 1){echo 'checked';} ?> > <label for="responsivacompraventa"></label>
                  </div>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">Contrato Compraventa</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="contratocompraventa" id="contratocompraventa" type="checkbox" value="1" <?php if($checks['contratocompraventa'] == 1){echo 'checked';} ?> > <label for="contratocompraventa"></label>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="row">
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">Aviso de Privasidad</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="avisoprivacidad" id="avisoprivacidad" type="checkbox" value="1" <?php if($checks['avisoprivacidad'] == 1){echo 'checked';} ?> > <label for="avisoprivacidad"></label>
                  </div>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">Check de Rev. y Cert.</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="checklistrevision" id="checklistrevision" type="checkbox" value="1" <?php if($checks['checklistrevision'] == 1){echo 'checked';} ?> > <label for="checklistrevision"></label>
                  </div>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">Evaluación Mecánica</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="evaluacionmecanica" id="evaluacionmecanica" type="checkbox" value="1" <?php if($checks['evaluacionmecanica'] == 1){echo 'checked';} ?> > <label for="evaluacionmecanica"></label>
                  </div>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">Guía Autométrica</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="guiaautometrica" id="guiaautometrica" type="checkbox" value="1" <?php if($checks['guiaautometrica'] == 1){echo 'checked';} ?> > <label for="guiaautometrica"></label>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="row">
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">Póliza de Recepción</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="polizaderecepcion" id="polizaderecepcion" type="checkbox" value="1" <?php if($checks['polizaderecepcion'] == 1){echo 'checked';} ?> > <label for="polizaderecepcion"></label>
                  </div>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">Cuentas pos Pagar</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="cuentasporpagar" id="cuentasporpagar" type="checkbox" value="1" <?php if($checks['cuentasporpagar'] == 1){echo 'checked';} ?> > <label for="cuentasporpagar"></label>
                  </div>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">Formato Dación en Pago</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="formatodedacion" id="formatodedacion" type="checkbox" value="1" <?php if($checks['formatodedacion'] == 1){echo 'checked';} ?> > <label for="formatodedacion"></label>
                  </div>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">Consulta R. de Robo</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="reportederobo" id="reportederobo" type="checkbox" value="1" <?php if($checks['reportederobo'] == 1){echo 'checked';} ?> > <label for="reportederobo"></label>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="row">
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">Cheque de Proveedor</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="chequedeproveedor" id="chequedeproveedor" type="checkbox" value="1" <?php if($checks['chequedeproveedor'] == 1){echo 'checked';} ?> > <label for="chequedeproveedor"></label>
                  </div>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">Solicitud de Cheque</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="solicitudcheque" id="solicitudcheque" type="checkbox" value="1" <?php if($checks['solicitudcheque'] == 1){echo 'checked';} ?> > <label for="solicitudcheque"></label>
                  </div>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">Estado de Cuenta</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="edocuenta" id="edocuenta" type="checkbox" value="1" <?php if($checks['edocuenta'] == 1){echo 'checked';} ?> > <label for="edocuenta"></label>
                  </div>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">Calculo de Retención</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="calculoretencion" id="calculoretencion" type="checkbox" value="1" <?php if($checks['calculoretencion'] == 1){echo 'checked';} ?> > <label for="calculoretencion"></label>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="row">
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="gg" class="col-md-12 control-label">Autorizacion P/Toma</label>
                  <div class="col-md-12">
                    <input class="tgl_checkbox tgl-ios" name="autorizaciontoma" id="autorizaciontoma" type="checkbox" value="1" <?php if($checks['autorizaciontoma'] == 1){echo 'checked';} ?> > <label for="autorizaciontoma"></label>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <label for="role" class="col-md-2 control-label"><?= trans('status') ?></label>
            <div class="col-md-12">
              <select name="status" class="form-control">
                <option value=""><?= trans('select_status') ?></option>
                <option value="0" <?= ($checks['status'] == 0)?'selected': '' ?>>Libre</option>
                <option value="1" <?= ($checks['status'] == 1)?'selected': '' ?>>Pendiente</option>
                <option value="2" <?= ($checks['status'] == 2)?'selected': '' ?>>En Proceso</option>
                <option value="3" <?= ($checks['status'] == 3)?'selected': '' ?>>Terminado</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-lg-12">
                <div class="input-group">
                 <label for="comentarios" class="col-md-12 control-label"><?= trans('comments') ?></label>
                 <div class="col-md-12">
                  <input type="text" name="comentarios" class="form-control" id="comentarios" placeholder="">
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="form-group">
          <div class="col-md-12">
            <input type="submit" name="submit" value="<?= trans('update_check') ?>" class="btn btn-primary pull-right">
          </div>
        </div>
        <?php echo form_close(); ?>
      </div>
      <!-- /.box-body -->
    </div> 

    <div class="card card-default">
      <div class="card-body">

      </div>
    </div>
 
    </section> 
  </div>



  <!-- Select2 -->
  <script src="<?= base_url() ?>assets/plugins/select2/select2.full.min.js"></script>
  <!-- InputMask -->
  <script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.js"></script>
  <script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
  <script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>
  <!-- date-range-picker -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
  <script src="<?= base_url() ?>assets/plugins/daterangepicker/daterangepicker.js"></script>
  <!-- bootstrap color picker -->
  <script src="<?= base_url() ?>assets/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
  <!-- bootstrap time picker -->
  <script src="<?= base_url() ?>assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>
  <!-- Page script -->
  <!-- iCheck 1.0.1 -->
  <script src="<?= base_url() ?>assets/plugins/iCheck/icheck.min.js"></script>
  <script>
    $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker         : true,
      timePickerIncrement: 30,
      format             : 'MM/DD/YYYY h:mm A'
    })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
    {
      ranges   : {
        'Today'       : [moment(), moment()],
        'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month'  : [moment().startOf('month'), moment().endOf('month')],
        'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      },
      startDate: moment().subtract(29, 'days'),
      endDate  : moment()
    },
    function (start, end) {
      $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
    }
    )

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>

<script>
  $("#forms").addClass('active');
  $("#advanced").addClass('active');
</script>  
