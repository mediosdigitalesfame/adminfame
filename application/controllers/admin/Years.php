<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Years extends MY_Controller 
{
	public function __construct(){

		parent::__construct();
		auth_check(); // comprobar la autenticación de inicio de sesión
		$this->rbac->check_module_access();

		$this->load->model('admin/year_model', 'year_model');
		$this->load->model('admin/Activity_model', 'activity_model');
	}

	//-----------------------------------------------------------
	public function index(){

		$this->load->view('admin/includes/_header');
		$this->load->view('admin/years/year_list');
		$this->load->view('admin/includes/_footer');
	}
	
	public function datatable_json(){				   					   
		$records['data'] = $this->year_model->get_all_years();
		$data = array();

		$i=0;
		foreach ($records['data']   as $row) 
		{  
			$status = ($row['status'] == 1)? 'checked': '';

			$data[]= array(
				++$i,
				$row['name'],
				date("d-m-Y", strtotime($row['created_at'])),
				'<input class="tgl_checkbox tgl-ios"data-id="'.$row['id'].'"id="cb_'.$row['id'].'"type="checkbox"	'.$status.'><label for="cb_'.$row['id'].'"></label>',	
				'<a title="View" class="view btn btn-sm btn-info" href="'.base_url('admin/years/edit/'.$row['id']).'"> <i class="fa fa-eye"></i></a>
				<a title="Edit" class="update btn btn-sm btn-warning" href="'.base_url('admin/years/edit/'.$row['id']).'"> <i class="fa fa-pencil-square-o"></i></a>
				<a title="Delete" class="delete btn btn-sm btn-danger" href='.base_url("admin/years/delete/".$row['id']).' title="Delete" onclick="return confirm(\'Do you want to delete ?\')"> <i class="fa fa-trash-o"></i></a>'	 		
			);
		}
		$records['data']=$data;
		echo json_encode($records);						   
	}

	//-----------------------------------------------------------
	function change_status()
	{   
		$this->rbac->check_operation_access('change_status'); //comprobar el permiso de operación
		$this->year_model->change_status();
	}

	public function add(){
		
		$this->rbac->check_operation_access(); //comprobar el permiso de operación

		if($this->input->post('submit')){
			$this->form_validation->set_rules('name', 'Name', 'trim|required');
			 

			if ($this->form_validation->run() == FALSE) {
				$data = array(
					'errors' => validation_errors()
				);
				$this->session->set_flashdata('errors', $data['errors']);
				redirect(base_url('admin/years/add'),'refresh');
			}
			else{
				$data = array(
					'name' => $this->input->post('name'),
					'created_at' => date('Y-m-d : h:m:s'),
					'updated_at' => date('Y-m-d : h:m:s'),
				);
				$data = $this->security->xss_clean($data);
				$result = $this->year_model->add_year($data);
				if($result){

					// Registro de actividades
					$this->activity_model->add_log(13);
					$this->session->set_flashdata('success', 'La Marca se ha agregado correctamente!');
					redirect(base_url('admin/years'));
				}
			}
		}
		else{
			$this->load->view('admin/includes/_header');
			$this->load->view('admin/years/year_add');
			$this->load->view('admin/includes/_footer');
		}
		
	}

	public function edit($id = 0){

		$this->rbac->check_operation_access(); //comprobar el permiso de operación

		if($this->input->post('submit')){
			$this->form_validation->set_rules('name', 'Name', 'trim|required');
			$this->form_validation->set_rules('status', 'Status', 'trim|required');
			if ($this->form_validation->run() == FALSE) {
				$data = array(
					'errors' => validation_errors()
				);
				$this->session->set_flashdata('errors', $data['errors']);
				redirect(base_url('admin/years/year_edit/'.$id),'refresh');
			}
			else{
				$data = array(
					'name' => $this->input->post('name'),
					'status' => $this->input->post('status'),
					'updated_at' => date('Y-m-d : h:m:s'),
				);
				$data = $this->security->xss_clean($data);
				$result = $this->year_model->edit_year($data, $id);
				if($result){
					// Registro de actividades 
					$this->activity_model->add_log(14);
					$this->session->set_flashdata('success', 'La Marca ha sido actualizado con éxito!');
					redirect(base_url('admin/years'));
				}
			}
		}
		else{
			$data['year'] = $this->year_model->get_year_by_id($id);
			
			$this->load->view('admin/includes/_header');
			$this->load->view('admin/years/year_edit', $data);
			$this->load->view('admin/includes/_footer');
		}
	}

	public function delete($id = 0)
	{
		$this->rbac->check_operation_access(); // comprobar el permiso de operación
		$this->db->delete('anios', array('id' => $id));
		// Registro de actividades  
		$this->activity_model->add_log(15);
		$this->session->set_flashdata('success', 'La Marca se ha eliminado correctamente!');
		redirect(base_url('admin/years'));
	}


	public function export_csv(){ 
	   // nombre del archivo
		$filename = 'years_'.date('Y-m-d').'.csv'; 
		header("Content-Description: File Transfer"); 
		header("Content-Disposition: attachment; filename=$filename"); 
		header("Content-Type: application/csv; ");

	   // obtener datos
		$year_data = $this->year_model->get_years_for_csv();

	   // crear archivo
		$file = fopen('php://output', 'w');
		$header = array("ID", "Name", "Created Date"); 
		fputcsv($file, $header);
		foreach ($year_data as $key=>$line){ 
			fputcsv($file,$line); 
		}
		fclose($file); 
		exit; 
	}


	//---------------------------------------------------------------
	//  Exportar Marcas a PDF
	public function create_years_pdf()
	{
		$this->load->helper('pdf_tcpdf_helper'); // loaded pdf helper
		$data['all_years'] = $this->year_model->get_all_simple_years();
		$this->load->view('admin/years/years_tcpdf', $data);

	}




}


?>