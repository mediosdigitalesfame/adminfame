  <!-- daterange picker -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/daterangepicker/daterangepicker-bs3.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/colorpicker/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/select2/select2.min.css">

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">

      <div class="card card-default">

        <div class="card-header">
          <div class="d-inline-block">
            <h3 class="card-title"> <i class="fa fa-plus"></i>
             <?= trans('add_new_take') ?> </h3>
           </div>
           <div class="d-inline-block float-right">
            <a href="<?= base_url('admin/takes'); ?>" class="btn btn-success"><i class="fa fa-list"></i>  <?= trans('takes_list') ?></a>
          </div>
        </div>

        <div class="card-body">

         <!-- For Messages -->
         <?php $this->load->view('admin/includes/_messages.php') ?>
         <?php echo form_open(base_url('admin/takes/add'), 'class="form-horizontal"');  ?> 
         
         <input type="hidden" id="identi" name="identi" value="">
         <input type="hidden" id="id" name="id" value="">

         <div class="form-group">
           <div class="row">

            <div class="col-lg-3">
              <div class="input-group">
               <label for="regimen" class="col-md-12 control-label">Régimen*</label>
               <div class="col-md-12">
                <select name="regimen" id="regimen" class="form-control select2" >
                  <option value=""><?= trans('select_guide') ?></option>
                  <?php foreach($regis as $regi): ?>
                    <option value="<?= $regi['id']; ?>"><?= $regi['name']; ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>
          </div>

          <div class="col-lg-3">
            <div class="input-group">
             <label for="guia" class="col-md-12 control-label"><?= trans('select_guide') ?>*</label>
             <div class="col-md-12">
              <select name="guia" id="guia" class="form-control select2" >
                <option value=""><?= trans('select_guide') ?></option>
                <?php foreach($guides as $guide): ?>
                  <option value="<?= $guide['id']; ?>"><?= $guide['folio']; ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>
        </div>

        <div class="col-lg-3">
          <div class="input-group">
           <label for="tipodetoma" class="col-md-12 control-label">Tipo de Toma*</label>
           <div class="col-md-12">
            <select name="tipodetoma" id="tipodetoma" class="form-control select2" >
              <option value="">Seleccionar Tipo de Toma</option>
              <option value="0">Pago Directo</option>
              <option value="1">Dación en Pago</option>
            </select>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
<!-- /.box-body -->
</div>

<div class="card card-default">

  <div class="card-header">
    <div class="d-inline-block">
      <h3 class="card-title">Proveedor </h3>
    </div>
  </div>

  <div class="card-body">



    <div class="form-group">
     <div class="row">
      <div class="col-lg-4">
        <div class="input-group">
         <label for="vin" class="col-md-12 control-label">Nombre Completo</label>
         <div class="col-md-12">
          <input type="text" class="form-control" placeholder="" id="nombre" name="nombre" maxlength="70" onkeyup="javascript:this.value=this.value.toUpperCase();">
        </div>
      </div>
    </div>
    <div class="col-lg-3">
      <div class="input-group">
       <label for="url" class="col-md-12 control-label">Fecha Contrato</label>
       <div class="col-md-12">
        <div class="input-group">
          <div class="input-group-prepend">
            <span class="input-group-text"><i class="fa fa-calendar"></i></span>
          </div>
          <input type="text" class="form-control" name="fechacontrato" id="fechacontrato" >
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-5">
    <div class="input-group">
     <label for="urlp" class="col-md-12 control-label">Calle</label>
     <div class="col-md-12">
      <input type="text" class="form-control" placeholder="" name="calle" id="calle" style="text-transform:uppercase;">
    </div>
  </div>
</div>
</div>
</div>

<div class="form-group">
 <div class="row">
  <div class="col-lg-2">
    <div class="input-group">
     <label for="urlp" class="col-md-12 control-label">No. Ext</label>
     <div class="col-md-12">
      <input type="text" class="form-control" placeholder="" name="numext" id="numext">
    </div>
  </div>
</div>
<div class="col-lg-2">
  <div class="input-group">
   <label for="urlp" class="col-md-12 control-label">No. Int</label>
   <div class="col-md-12">
    <input type="text" class="form-control" placeholder="" name="numint" id="numint">
  </div>
</div>
</div>
<div class="col-lg-4">
  <div class="input-group">
   <label for="vin" class="col-md-12 control-label">Colonia</label>
   <div class="col-md-12">
    <input type="text" class="form-control" placeholder="" name="colonia" id="colonia" maxlength="65" onkeyup="javascript:this.value=this.value.toUpperCase();">
  </div>
</div>
</div>
<div class="col-lg-2">
  <div class="input-group">
   <label for="urlp" class="col-md-12 control-label">CP</label>
   <div class="col-md-12">
    <input type="text" class="form-control" placeholder="" name="cp" id="cp">
  </div>
</div>
</div>
<div class="col-lg-2">
  <div class="input-group">
   <label for="urlp" class="col-md-12 control-label">Ciudad</label>
   <div class="col-md-12">
    <input type="text" class="form-control" placeholder="" name="ciudad" id="ciudad" style="text-transform:uppercase;">
  </div>
</div>
</div>
</div>
</div>

<div class="form-group">
 <div class="row">
  <div class="col-lg-2">
    <div class="input-group">
     <label for="urlp" class="col-md-12 control-label">Municipio</label>
     <div class="col-md-12">
      <input type="text" class="form-control" placeholder="" name="municipio" id="municipio" style="text-transform:uppercase;">
    </div>
  </div>
</div>
<div class="col-lg-2">
  <div class="input-group">
   <label for="urlp" class="col-md-12 control-label">Estado</label>
   <div class="col-md-12">
    <input type="text" class="form-control" placeholder="" name="estado" id="estado" style="text-transform:uppercase;">
  </div>
</div>
</div>
<div class="col-lg-3">
  <div class="input-group">
   <label for="vin" class="col-md-12 control-label">RFC</label>
   <div class="col-md-12">
    <input type="text" class="form-control" placeholder="" name="rfc" id="rfc" maxlength="13" onkeyup="javascript:this.value=this.value.toUpperCase();">
  </div>
</div>
</div>
<div class="col-lg-3">
  <div class="input-group">
   <label for="urlp" class="col-md-12 control-label">No. Identificación</label>
   <div class="col-md-12">
    <input type="text" class="form-control" placeholder="" id="nidenti" name="nidenti" style="text-transform:uppercase;">
  </div>
</div>
</div>
<div class="col-lg-2">
  <div class="input-group">
   <label for="urlp" class="col-md-12 control-label">Tipo Identificación</label>
   <div class="col-md-12">
    <input type="text" class="form-control" placeholder="" id="tipoidenti" name="tipoidenti" style="text-transform:uppercase;">
  </div>
</div>
</div>
</div>
</div>

<div class="form-group">
 <div class="row">
  <!--
  <div class="col-lg-3">
    <div class="input-group">
     <label for="url" class="col-md-12 control-label">Fecha Identificación</label>
     <div class="col-md-12">
      <div class="input-group">
        <div class="input-group-prepend">
          <span class="input-group-text"><i class="fa fa-calendar"></i></span>
        </div>
        <input type="text" class="form-control" name="fechaidenti" id="fechaidenti" >
      </div>
    </div>
  </div>
</div>
-->
<div class="col-lg-2">
  <div class="input-group">
   <label for="urlp" class="col-md-12 control-label">Mes del Recibo</label>
   <div class="col-md-12">
    <input type="text" class="form-control" placeholder="" id="mesrecibo" name="mesrecibo" style="text-transform:uppercase;">
  </div>
</div>
</div>

<div class="col-lg-2">
  <div class="input-group">
   <label for="urlp" class="col-md-12 control-label">Teléfono</label>
   <div class="col-md-12">
    <input type="text" class="form-control" placeholder="" id="tel" name="tel" style="text-transform:uppercase;">
  </div>
</div>
</div>

 

</div>
</div>

</div>
<!-- /.box-body -->
</div>

<div class="card card-default">

 <div class="card-header">
  <div class="d-inline-block">
    <h3 class="card-title"> Datos del Vehículo</h3>
  </div>
</div>

<div class="card-body">

  <div class="form-group">
   <div class="row">
    <div class="col-lg-2">
      <div class="input-group">
       <label for="urlp" class="col-md-12 control-label">No.Factura Vehiculo</label>
       <div class="col-md-12">
        <input type="text" class="form-control" placeholder="" name="facturavehi" id="facturavehi" style="text-transform:uppercase;">
      </div>
    </div>
  </div>
  <div class="col-lg-3">
    <div class="input-group">
     <label for="urlp" class="col-md-12 control-label">Expedida Por</label>
     <div class="col-md-12">
      <input type="text" class="form-control" placeholder="" id="expedida" name="expedida" style="text-transform:uppercase;">
    </div>
  </div>
</div>
<div class="col-lg-3">
  <div class="input-group">
   <label for="url" class="col-md-12 control-label">Fecha Factura</label>
   <div class="col-md-12">
    <div class="input-group">
      <div class="input-group-prepend">
        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
      </div>
      <input type="text" class="form-control" name="fechafac" id="fechafac" >
    </div>
  </div>
</div>
</div>

<div class="col-lg-2">
  <div class="input-group">
   <label for="vin" class="col-md-12 control-label">Color Ext</label>
   <div class="col-md-12">
    <input type="text" class="form-control" placeholder="" id="colorext" name="colorext" maxlength="17" onkeyup="javascript:this.value=this.value.toUpperCase();">
  </div>
</div>
</div>
<div class="col-lg-2">
  <div class="input-group">
   <label for="urlp" class="col-md-12 control-label">Color Int</label>
   <div class="col-md-12">
    <input type="text" class="form-control" placeholder="" id="colorint" name="colorint" style="text-transform:uppercase;">
  </div>
</div>
</div>


<!--
<div class="col-lg-3">
  <div class="input-group">
   <label for="vin" class="col-md-12 control-label">Marca Vehiculo</label>
   <div class="col-md-12">
    <input type="text" class="form-control" placeholder="" id="marcavehi" name="marcavehi" maxlength="17" onkeyup="javascript:this.value=this.value.toUpperCase();">
  </div>
</div>
</div>
-->
</div>
</div>

<div class="form-group">
 <div class="row">

<!--  <div class="col-lg-3">
    <div class="input-group">
     <label for="urlp" class="col-md-12 control-label">Modelo Vehiculo</label>
     <div class="col-md-12">
      <input type="text" class="form-control" placeholder="" id="modelovehi" name="modelovehi">
    </div>
  </div>
</div>
<div class="col-lg-3">
  <div class="input-group">
   <label for="url" class="col-md-12 control-label">Año Modelo</label>
   <div class="col-md-12">
    <div class="input-group">
      <div class="input-group-prepend">
        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
      </div>
      <input type="text" class="form-control" id="aniomodelo" name="aniomodelo" data-inputmask='"mask": "9999"' data-mask>
    </div>
  </div>
</div>
</div>
-->

<!--
<div class="col-lg-2">
  <div class="input-group">
   <label for="urlp" class="col-md-12 control-label">Version</label>
   <div class="col-md-12">
    <input type="text" class="form-control" placeholder="" id="versionvehi" name="versionvehi">
  </div>
</div>
</div>
-->
</div>
</div>

<div class="form-group">
 <div class="row">
  <div class="col-lg-3">
    <div class="input-group">
     <label for="urlp" class="col-md-12 control-label">Clave Vehicular</label>
     <div class="col-md-12">
      <input type="text" class="form-control" placeholder="" id="clavevehi" name="clavevehi" style="text-transform:uppercase;">
    </div>
  </div>
</div>
<div class="col-lg-3">
  <div class="input-group">
   <label for="vin" class="col-md-12 control-label">No. de Serie</label>
   <div class="col-md-12">
    <input type="text" class="form-control" placeholder="" id="nserie" name="nserie" maxlength="17" onkeyup="javascript:this.value=this.value.toUpperCase();">
  </div>
</div>
</div>
<div class="col-lg-3">
  <div class="input-group">
   <label for="urlp" class="col-md-12 control-label">No. de Motor</label>
   <div class="col-md-12">
    <input type="text" class="form-control" placeholder="" id="nmotor" name="nmotor" style="text-transform:uppercase;">
  </div>
</div>
</div>
<div class="col-lg-3">
  <div class="input-group">
   <label for="urlp" class="col-md-12 control-label">No. de Baja</label>
   <div class="col-md-12">
    <input type="text" class="form-control" placeholder=""  id="nbaja" name="nbaja" style="text-transform:uppercase;">
  </div>
</div>
</div>
</div>
</div>

<div class="form-group">
 <div class="row">
  <div class="col-lg-3">
    <div class="input-group">
     <label for="urlp" class="col-md-12 control-label">Edo Emisor Baja</label>
     <div class="col-md-12">
      <input type="text" class="form-control" placeholder="" id="edoemisor" name="edoemisor" style="text-transform:uppercase;">
    </div>
  </div>
</div>
<div class="col-lg-3">
  <div class="input-group">
   <label for="url" class="col-md-12 control-label">Fecha de Baja</label>
   <div class="col-md-12">
    <div class="input-group">
      <div class="input-group-prepend">
        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
      </div>
      <input type="text" class="form-control" name="fechabaja" id="fechabaja" style="text-transform:uppercase;">
    </div>
  </div>
</div>
</div>
<div class="col-lg-3">
  <div class="input-group">
   <label for="urlp" class="col-md-12 control-label">Placas de Baja</label>
   <div class="col-md-12">
    <input type="text" class="form-control" placeholder="" id="placasbaja" name="placasbaja" style="text-transform:uppercase;">
  </div>
</div>
</div>
<div class="col-lg-3">
  <div class="input-group">
   <label for="urlp" class="col-md-12 control-label">No. Verificación</label>
   <div class="col-md-12">
    <input type="text" class="form-control" placeholder="" id="nverificacion" name="nverificacion" style="text-transform:uppercase;">
  </div>
</div>
</div>
</div>
</div>

<div class="form-group">
 <div class="row">
  <div class="col-lg-6">
    <div class="input-group">
     <label for="urlp" class="col-md-12 control-label">Nombre a Quien se Vende la Unidad</label>
     <div class="col-md-12">
      <input type="text" class="form-control" placeholder="" id="aquiensevende" name="aquiensevende" style="text-transform:uppercase;">
    </div>
  </div>
</div>
<div class="col-lg-3">
  <div class="input-group">
   <label for="url" class="col-md-12 control-label">Fecha Fac. Final</label>
   <div class="col-md-12">
    <div class="input-group">
      <div class="input-group-prepend">
        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
      </div>
      <input type="text" class="form-control" name="fechafacfinal" id="fechafacfinal" style="text-transform:uppercase;">
    </div>
  </div>
</div>
</div>
<div class="col-lg-3">
  <div class="input-group">
   <label for="urlp" class="col-md-12 control-label">Costo de Adquisición(Fac.Origen)</label>
   <div class="col-md-12">
    <input type="text" class="form-control" placeholder="" id="costoadquisicion" name="costoadquisicion">
  </div>
</div>
</div>
<!--
<div class="col-lg-3">
  <div class="input-group">
   <label for="url" class="col-md-12 control-label">Fecha de Toma</label>
   <div class="col-md-12">
    <div class="input-group">
      <div class="input-group-prepend">
        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
      </div>
      <input type="text" class="form-control" name="fechatoma" id="fechatoma" >
    </div>
  </div>
</div>
</div>
-->
</div>
</div>

<div class="form-group">
 <div class="row">

  <div class="col-lg-6">
    <div class="input-group">
     <label for="urlp" class="col-md-12 control-label">Años de Tenencias</label>
     <div class="col-md-12">
      <input type="text" data-role="tagsinput" class="form-control" placeholder="" id="tenencias" name="tenencias">
    </div>
  </div>
</div>

<div class="col-lg-4">
    <div class="input-group">
      <label class="col-md-12 control-label">PDF de las Tenencias</label> 
      <div class="form-group">
        <td>
         <input type="file" name="image" accept=".png, .jpg, .jpeg, .gif, .svg, .pdf">
         <p><small class="text-success"><?= trans('allowed_types') ?>:  .pdf</small></p>
       </td>
     </div>
   </div>
 </div>

</div>
</div>


</div>
</div>

<div class="card card-default">

 <div class="card-header">
  <div class="d-inline-block">
    <h3 class="card-title"> Solicitud CFDI</h3>
  </div>
</div>

<div class="card-body">

  <div class="form-group">
   <div class="row">
    <div class="col-lg-3">
      <div class="input-group">
       <label for="urlp" class="col-md-12 control-label">Nombres</label>
       <div class="col-md-12">
        <input type="text" class="form-control" placeholder="" id="nombrescfdi" name="nombrescfdi" style="text-transform:uppercase;">
      </div>
    </div>
  </div>
  <div class="col-lg-3">
    <div class="input-group">
     <label for="urlp" class="col-md-12 control-label">Apellido Paterno</label>
     <div class="col-md-12">
      <input type="text" data-role="tagsinput" class="form-control" placeholder="" id="apellido1" name="apellido1" style="text-transform:uppercase;">
    </div>
  </div>
</div>
<div class="col-lg-3">
  <div class="input-group">
   <label for="urlp" class="col-md-12 control-label">Apellido Materno</label>
   <div class="col-md-12">
    <input type="text" data-role="tagsinput" class="form-control" placeholder="" id="apellido2" name="apellido2" style="text-transform:uppercase;">
  </div>
</div>
</div>
<div class="col-lg-3">
  <div class="input-group">
   <label for="urlp" class="col-md-12 control-label">eMail</label>
   <div class="col-md-12">
    <input type="text" data-role="tagsinput" class="form-control" placeholder="" id="email" name="email" style="text-transform:lowercase;">
  </div>
</div>
</div>
</div>
</div>

<div class="form-group">
 <div class="row">
  <div class="col-lg-3">
    <div class="input-group">
     <label for="urlp" class="col-md-12 control-label">Teléfono</label>
     <div class="col-md-12">
      <input type="text" class="form-control" placeholder="" id="tel" name="tel" style="text-transform:uppercase;">
    </div>
  </div>
</div>
<div class="col-lg-3">
  <div class="input-group">
   <label for="urlp" class="col-md-12 control-label">Celular</label>
   <div class="col-md-12">
    <input type="text" data-role="tagsinput" class="form-control" placeholder="" id="cel" name="cel">
  </div>
</div>
</div>
<div class="col-lg-3">
  <div class="input-group">
   <label for="url" class="col-md-12 control-label">Fecha de Nacimiento</label>
   <div class="col-md-12">
    <div class="input-group">
      <div class="input-group-prepend">
        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
      </div>
      <input type="text" class="form-control" id="fechanaci" name="fechanaci" >
    </div>
  </div>
</div>
</div>
<div class="col-lg-3">
  <div class="input-group">
   <label for="urlp" class="col-md-12 control-label">Entidad Nacimiento</label>
   <div class="col-md-12">
    <input type="text" data-role="tagsinput" class="form-control" placeholder="" id="entidadnaci" name="entidadnaci" style="text-transform:uppercase;">
  </div>
</div>
</div>
</div>
</div>

<div class="form-group">
 <div class="row">
  <div class="col-lg-3">
    <div class="input-group">
     <label for="urlp" class="col-md-12 control-label">Actividad</label>
     <div class="col-md-12">
      <input type="text" class="form-control" placeholder="" id="actividad" name="actividad" style="text-transform:uppercase;">
    </div>
  </div>
</div>
<div class="col-lg-3">
  <div class="input-group">
   <label for="urlp" class="col-md-12 control-label">CURP</label>
   <div class="col-md-12">
    <input type="text" data-role="tagsinput" class="form-control" placeholder="" id="curp" name="curp" style="text-transform:uppercase;">
  </div>
</div>
</div>
<div class="col-lg-3">
  <div class="input-group">
   <label for="urlp" class="col-md-12 control-label">País</label>
   <div class="col-md-12">
    <input type="text" data-role="tagsinput" class="form-control" placeholder="" id="pais" name="pais" style="text-transform:uppercase;">
  </div>
</div>
</div>
</div>
</div>

</div>
</div>

<div class="card card-default">

 <div class="card-header">
  <div class="d-inline-block">
    <h3 class="card-title"> Solicitud de Cheque</h3>
  </div>
</div>

<div class="card-body">

  <div class="form-group">
   <div class="row">
    <div class="col-lg-3">
      <div class="input-group">
       <label for="urlp" class="col-md-12 control-label">Banco</label>
       <div class="col-md-12">
        <input type="text" class="form-control" placeholder="" id="banco" name="banco" style="text-transform:uppercase;">
      </div>
    </div>
  </div>
  <div class="col-lg-3">
    <div class="input-group">
     <label for="urlp" class="col-md-12 control-label">Cuenta</label>
     <div class="col-md-12">
      <input type="text" data-role="tagsinput" class="form-control" placeholder="" id="cuenta" name="cuenta" style="text-transform:uppercase;">
    </div>
  </div>
</div>
<div class="col-lg-3">
  <div class="input-group">
   <label for="urlp" class="col-md-12 control-label">Clabe</label>
   <div class="col-md-12">
    <input type="text" data-role="tagsinput" class="form-control" placeholder="" id="clabe" name="clabe" style="text-transform:uppercase;">
  </div>
</div>
</div>
<div class="col-lg-3">
  <div class="input-group">
   <label for="urlp" class="col-md-12 control-label">Sucursal</label>
   <div class="col-md-12">
    <input type="text" data-role="tagsinput" class="form-control" placeholder="" id="sucursal" name="sucursal" style="text-transform:uppercase;">
  </div>
</div>
</div>
</div>
</div>

<div class="form-group">
 <div class="row">
  <div class="col-lg-3">
    <div class="input-group">
     <label for="urlp" class="col-md-12 control-label">Convenio</label>
     <div class="col-md-12">
      <input type="text" class="form-control" placeholder="" id="convenio" name="convenio" style="text-transform:uppercase;">
    </div>
  </div>
</div>
<div class="col-lg-3">
  <div class="input-group">
   <label for="urlp" class="col-md-12 control-label">Referencia</label>
   <div class="col-md-12">
    <input type="text" class="form-control" placeholder="" id="referencia" name="referencia" style="text-transform:uppercase;">
  </div>
</div>
</div>
<div class="col-lg-3">
  <div class="input-group">
   <label for="urlp" class="col-md-12 control-label">Folio CFDI</label>
   <div class="col-md-12">
    <input type="text" class="form-control" placeholder="" id="foliocfdi" name="foliocfdi" style="text-transform:uppercase;">
  </div>
</div>
</div>

</div>
</div>

</div>
</div>

<div class="card card-default">
  <div class="card-body">
    <div class="form-group">
      <div class="col-md-12">
        <input type="submit" name="submit" value="<?= trans('add_take') ?>" class="btn btn-primary pull-right">
      </div>
    </div>
    <?php echo form_close( ); ?>
  </div>
</div>

</section> 
</div>

<!-- Select2 -->
<script src="<?= base_url() ?>assets/plugins/select2/select2.full.min.js"></script>
<!-- InputMask -->
<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src="<?= base_url() ?>assets/plugins/daterangepicker/daterangepicker.js"></script>

<!-- bootstrap color picker -->
<script src="<?= base_url() ?>assets/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="<?= base_url() ?>assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- Sweet Alert -->
<script src="<?= base_url() ?>assets/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Page script -->
<!-- iCheck 1.0.1 -->
<script src="<?= base_url() ?>assets/plugins/iCheck/icheck.min.js"></script>


<script language="javascript">

  jQuery('#fechaidenti, #fechafac, #fechabaja, #fechafacfinal, #fechatoma, #fechacontrato, #fechanaci').datepicker({
    format: 'yyyy-mm-dd'
  });

</script>

<!-- Prevenir envio de formulario con enter -->
<script>
  document.addEventListener('DOMContentLoaded', () => {
    document.querySelectorAll('input[type=text]').forEach( node => node.addEventListener('keypress', e => {
      if(e.keyCode == 13) {
        e.preventDefault();
      }
    }))
  });
</script>

<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Datemask3 yyyy
    $('#datemask3').inputmask('yyyy', { 'placeholder': 'yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()


    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker         : true,
      timePickerIncrement: 30,
      format             : 'MM/DD/YYYY h:mm A'
    })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
    {
      ranges   : {
        'Today'       : [moment(), moment()],
        'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month'  : [moment().startOf('month'), moment().endOf('month')],
        'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      },
      startDate: moment().subtract(29, 'days'),
      endDate  : moment()
    },
    function (start, end) {
      $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
    }
    )

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>

<script>
  $("#forms").addClass('active');
  $("#advanced").addClass('active');
</script>  


