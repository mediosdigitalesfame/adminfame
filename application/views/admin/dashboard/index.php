  <!-- Content Wrapper. Contains page content -->

  <div class="content-wrapper">

    <!-- Content Header (Page header) -->

    <div class="content-header">

      <div class="container-fluid">

        <div class="row mb-2">

          <div class="col-sm-6">

            <h1 class="m-0 text-dark">Bienvenido  - <?= trans('dashboard') ?></h1>

          </div><!-- /.col -->

          <div class="col-sm-6">

            <ol class="breadcrumb float-sm-right">

              <li class="breadcrumb-item"><a href="#"><?= trans('home') ?></a></li>

              <li class="breadcrumb-item active"><?= trans('dashboard') ?></li>

            </ol>

          </div><!-- /.col -->

        </div><!-- /.row -->

      </div><!-- /.container-fluid -->

    </div>

    <!-- /.content-header -->



    <!-- Main content -->

    <section class="content">

 

      <?php if($this->rbac->check_operation_permission('data')): ?>

              

      <div class="container-fluid">



        <!--<div class="row">

          <div class="col-12 col-sm-6 col-md-3">

            <div class="info-box">

              <span class="info-box-icon bg-info elevation-1"><i class="fa fa-car"></i></span>

              <div class="info-box-content">

                <span class="info-box-text">Marcas</span>

                <span class="info-box-number">

                  <?= $all_brands; ?>

                </span>

              </div>

            </div>

          </div>

          <div class="col-12 col-sm-6 col-md-3">

            <div class="info-box mb-3">

              <span class="info-box-icon bg-danger elevation-1"><i class="fa fa-google-plus"></i></span>

              <div class="info-box-content">

                <span class="info-box-text">Modelos</span>

                <span class="info-box-number"><?= $all_models; ?></span>

              </div>

            </div>

          </div>

          <div class="clearfix hidden-md-up"></div>

          <div class="col-12 col-sm-6 col-md-3">

            <div class="info-box mb-3">

              <span class="info-box-icon bg-success elevation-1"><i class="fa fa-shopping-cart"></i></span>

              <div class="info-box-content">

                <span class="info-box-text">Negocios</span>

                <span class="info-box-number"><?= $all_business; ?></span>

              </div>

            </div>

          </div>

          <div class="col-12 col-sm-6 col-md-3">

            <div class="info-box mb-3">

              <span class="info-box-icon bg-warning elevation-1"><i class="fa fa-users"></i></span>

              <div class="info-box-content">

                <span class="info-box-text">agencias</span>

                <span class="info-box-number"><?= $all_agencys; ?></span>

              </div>

            </div>

          </div>

        </div>-->



        



        <div class="row">

          <div class="col-lg-3 col-6">

            <div class="small-box bg-warning">

              <div class="inner">

                <h3><?= $all_guides; ?></h3>

                <p>Guías Registradas</p>

              </div>

              <div class="icon">

                <i class="ion ion-document-text"></i>  

              </div>

              <a href="<?= base_url('admin/guides'); ?>" class="small-box-footer"><?= trans('more_info') ?> <i class="fa fa-arrow-circle-right"></i></a>

            </div>

          </div>

          <div class="col-lg-3 col-6">

            <div class="small-box bg-warning">

              <div class="inner">

                <h3><?= $active_guides; ?></h3>



                <p>Guías Autorizadas</p>

              </div>

              <div class="icon">

                <i class="ion ion-document-text"></i>

              </div>

              <a href="<?= base_url('admin/guides/index_autorizadas'); ?>" class="small-box-footer"><?= trans('more_info') ?> <i class="fa fa-arrow-circle-right"></i></a>

            </div>

          </div>

          <div class="col-lg-3 col-6">

            <div class="small-box bg-warning">

              <div class="inner">

                <h3><?= $deactive_guides; ?></h3>



                <p>Guías Pendientes</p>

              </div>

              <div class="icon">

                <i class="ion ion-document-text"></i>

              </div>

              <a href="<?= base_url('admin/guides/index_pendientes'); ?>" class="small-box-footer"><?= trans('more_info') ?> <i class="fa fa-arrow-circle-right"></i></a>

            </div>

          </div>

          <div class="col-lg-3 col-6">

            <div class="small-box bg-warning">

              <div class="inner">

                <h3><?= $take_guides; ?></h3>



                <p>Guías Tomadas</p>

              </div>

              <div class="icon">

                <i class="ion ion-document-text"></i>

              </div>

              <a href="<?= base_url('admin/guides/index_tomadas'); ?>" class="small-box-footer"><?= trans('more_info') ?> <i class="fa fa-arrow-circle-right"></i></a>

            </div>

          </div>

        </div>



        <div class="row">

          <div class="col-lg-3 col-6">

            <div class="small-box bg-info">

              <div class="inner">

                <h3><?= $all_takes; ?></h3>

                <p>Tomas Registradas</p>

              </div>

              <div class="icon">

                <i class="ion ion-document-text"></i>  

              </div>

              <a href="<?= base_url('admin/takes'); ?>" class="small-box-footer"><?= trans('more_info') ?> <i class="fa fa-arrow-circle-right"></i></a>

            </div>

          </div>

          <div class="col-lg-3 col-6">

            <div class="small-box bg-info">

              <div class="inner">

                <h3><?= $active_takes; ?></h3>

                <p>Tomas Autorizadas</p>

              </div>

              <div class="icon">

                <i class="ion ion-bag"></i>

              </div>

              <a href="<?= base_url('admin/takes/index_autorizadas'); ?>" class="small-box-footer"><?= trans('more_info') ?> <i class="fa fa-arrow-circle-right"></i></a>

            </div>

          </div>

          <div class="col-lg-3 col-6">

            <div class="small-box bg-info">

              <div class="inner">

                <h3><?= $deactive_takes; ?></h3>

                <p>Tomas Pendientes</p>

              </div>

              <div class="icon">

                <i class="ion ion-stats-bars"></i>

              </div>

              <a href="<?= base_url('admin/takes/index_pendientes'); ?>" class="small-box-footer"><?= trans('more_info') ?> <i class="fa fa-arrow-circle-right"></i></a>

            </div>

          </div>

          <div class="col-lg-3 col-6">

            <div class="small-box bg-info">

              <div class="inner">

                <h3><?= $take_checks; ?></h3>

                <p>Tomas con Checklist</p>

              </div>

              <div class="icon">

                <i class="ion ion-pie-graph"></i>

              </div>

              <a href="<?= base_url('admin/takes/index_checks'); ?>" class="small-box-footer"><?= trans('more_info') ?> <i class="fa fa-arrow-circle-right"></i></a>

            </div>

          </div>

        </div>



        <div class="row">

          <div class="col-lg-3 col-6">

            <div class="small-box bg-warning">

              <div class="inner">

                <h3><?= $libre_checks; ?></h3>

                <p>Checklist Libres</p>

              </div>

              <div class="icon">

                <i class="ion ion-document-text"></i>  

              </div>

              <a href="<?= base_url('admin/checks/index_libres'); ?>" class="small-box-footer"><?= trans('more_info') ?> <i class="fa fa-arrow-circle-right"></i></a>

            </div>

          </div>

 

          <div class="col-lg-3 col-6">

            <div class="small-box bg-info">

              <div class="inner">

                <h3><?= $pendiente_checks; ?></h3>

                <p>Checklist Pendientes</p>

              </div>

              <div class="icon">

                <i class="ion ion-bag"></i>

              </div>

              <a href="<?= base_url('admin/checks/index_pendientes'); ?>" class="small-box-footer"><?= trans('more_info') ?> <i class="fa fa-arrow-circle-right"></i></a>

            </div>

          </div>



          <div class="col-lg-3 col-6">

            <div class="small-box bg-success">

              <div class="inner">

                <h3><?= $proceso_checks; ?></h3>

                <p>Checklist en Proceso</p>

              </div>

              <div class="icon">

                <i class="ion ion-stats-bars"></i>

              </div>

              <a href="<?= base_url('admin/checks/index_proceso'); ?>" class="small-box-footer"><?= trans('more_info') ?> <i class="fa fa-arrow-circle-right"></i></a>

            </div>

          </div>

          

          <div class="col-lg-3 col-6">

            <div class="small-box bg-danger">

              <div class="inner">

                <h3><?= $terminado_checks; ?></h3>

                <p>Checklist Terminados</p>

              </div>

              <div class="icon">

                <i class="ion ion-pie-graph"></i>

              </div>

              <a href="<?= base_url('admin/checks/index_terminados'); ?>" class="small-box-footer"><?= trans('more_info') ?> <i class="fa fa-arrow-circle-right"></i></a>

            </div>

          </div>

        </div>





      </div><!--/. container-fluid -->



      <?php endif; ?>  








 <div class="card-body">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                   
                  <div class="carousel-inner">
                    <div class="carousel-item active">
                      <img class="d-block w-100" src="<?= base_url()?>assets/img/bannerlogo.png" alt="Banner Logo">
                    </div>
                    
                   
                  </div>
                 
                </div>
              </div>





    </section>

    <!-- /.content -->

  </div>

  <!-- /.content-wrapper -->



  <!-- PAGE PLUGINS -->

  <!-- SparkLine -->

  <script src="<?= base_url() ?>assets/plugins/sparkline/jquery.sparkline.min.js"></script>

  <!-- jVectorMap -->

  <script src="<?= base_url() ?>assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>

  <script src="<?= base_url() ?>assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>

  <!-- SlimScroll 1.3.0 -->

  <script src="<?= base_url() ?>assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>

  <!-- ChartJS 1.0.2 -->

  <script src="<?= base_url() ?>assets/plugins/chartjs-old/Chart.min.js"></script>



  <!-- PAGE SCRIPTS -->

  <script src="<?= base_url() ?>assets/dist/js/pages/dashboard2.js"></script>

















  <!-- Morris.js charts -->

  <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>

  <script src="<?= base_url() ?>assets/plugins/morris/morris.min.js"></script>





  <!-- jQuery Knob Chart -->

  <script src="<?= base_url() ?>assets/plugins/knob/jquery.knob.js"></script>

  <!-- daterangepicker -->

  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>

  <script src="<?= base_url() ?>assets/plugins/daterangepicker/daterangepicker.js"></script>



  <!-- Bootstrap WYSIHTML5 -->

  <script src="<?= base_url() ?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>





  <!-- AdminLTE dashboard demo (This is only for demo purposes) -->

  <script src="<?= base_url() ?>assets/dist/js/pages/dashboard.js"></script>















