<?php
switch ($take['idbus'])
{
   	case '1': 
   	$empresa = "Grupo FAME";
   	$planta = "Grupo FAME"; 
   	break;	//Grupo FAME
	case '2': 
	$empresa = "Automotriz Acueducto"; 
	$planta = "Nissan Mexicana, S.A. de C.V. "; 
	break;	//Automotriz Acueducto
	case '3': 
	$empresa = "Autos FAME"; 
	$planta = "Volkswagen de México, S.A. de C.V."; 
	break;	//Autos FAME
	case '4': 
	$empresa = "Comercializadora Automotriz"; 
	$planta = " Grupo FAME"; 
	break;	//Comercializadora
	case '5': 
	$empresa = "FAME Automotriz"; 
	$planta = "FCA México, S.A. de C.V"; 
	break;	//FAME Automotriz
	case '6': 
	$empresa = "FAME Camiones"; 
	$planta = "Isuzu Motors de México, S. de R.L. "; 
	break;	//FAME Camiones
	case '7': 
	$empresa = "FAME Corregidora"; 
	$planta = "Honda de México S.A. de C.V. "; 
	break;	//FAME Corregidora
	case '8': 
	$empresa = "FAME Manantiales"; 
	$planta = "General Motors de México S. de R.L. de C.V."; 
	break;	//FAME Manantiales
	case '9': 
	$empresa = "FAME Morelia"; 
	$planta = "General Motors de México S. de R.L. de C.V. "; 
	break;	//FAME Morelia
	case '10': 
	$empresa = "FAME Motors"; 
	$planta = "Kia Mexico, S.A. DE C.V."; 
	break;	//FAME Motors
	case '11': 
	$empresa = "FAME Perisur"; 
	$planta = "Toyota Motor Sales de México, S. de R.L. de C.V. "; 
	break;	//FAME Perisur
	case '12': 
	$empresa = "Servicios Metropolitanos"; 
	$planta = "Grupo FAME"; 
	break;	//Servicios Metropolitanos
	case '13': 
	$empresa = "Servicios Profesionales"; 
	$planta = "Grupo FAME"; 
	break;	//Servicios Profesionales
	case '14': 
	$empresa = "Talisman Automotriz"; 
	$planta = "BMW de México, S. A. de C. V. "; 
	break;	//Talisman Automotriz
	case '15': 
	$empresa = "FAME Altozano"; 
	$planta = "Mitsubishi Motors de México S.A. de C.V. "; 
	break;	//FAME Altozano
	case '16': 
	$empresa = "FAME Star"; 
	$planta = "Grupo FAME"; 
	break;	//FAME Star
}


$html ='
<table border="0" style="width:100%">
	<tbody>
		<table>
			<tr bgcolor="black">
				<td align="center"> <div style="vertical-align: middle;"><h1><font color="#ffffff">AVISO DE PRIVACIDAD</font></h1></div></td>
			</tr>
		</table>
	</tbody>
</table>
<table border="0" style="width:100%">
	<tbody>
		<table>
			<tr>
				<td>
					<label align="justify">
						<b>IDENTIFICACIÓN DEL RESPONSABLE</b>
						<br> 
						En <font color="#0A6ACF">'.$take['bursocial'].'</font> en lo sucesivo <font color="#0A6ACF">'.$take['agname'].'</font>, con domicilio en <font color="#0A6ACF">'.$take['bucalle'].', '.$take['buint'].', Col.'.$take['bucolonia'].', C.P.'.$take['bucp'].', '.$take['buciudad'].'</font>, le comunicamos que los datos personales de nuestros clientes y clientes potenciales son tratados en atención a lo que dispone la legislación en la materia y de manera confidencial y segura.
						<br>
						<b>I. I.¿A QUIÉN SE DIRIGE EL PRESENTE AVISO?</b>
						<br>
						<b>A.</b> A las personas físicas que proporcionen a <font color="#0A6ACF">'.$take['agname'].'</font>, datos personales que estén sujetos al ámbito de aplicación de la legislación en materia de protección de datos personales, con el objeto de solicitar, avalar, o bien, iniciar y obtener un Producto y/o Servicio de los ofrecido por <font color="#0A6ACF">'.$take['agname'].'</font>.<br>
						<b>B.</b>	Los accionistas de las personas morales que sean personas físicas (en lo subsecuente los "Accionistas").<br>
						<b>C.</b>	Los representantes legales de los clientes o adquirentes de nuestros productos y/o servicios (en lo subsecuente los "Representantes Legales").<br>
						<b>D.</b>	Las personas físicas y/o morales que funjan como clientes beneficiarios y aquellos proveedores de recursos.<br>
						<b>E.</b>	Las personas físicas que sean cónyuges de los Clientes.<br>
						<b>F.</b>	Las personas físicas que obtengan los beneficios, ejerciendo los derechos de uso, disfrute, aprovechamiento o disposición del Producto y/o Servicio de los ofrecidos por <font color="#0A6ACF">'.$take['agname'].'</font>. Identificándose como verdaderos dueños; también comprende a aquella persona o grupo de personas que ejerzan el control sobre una persona moral (en adelante el "Propietario Real").<br>
						<b>II. MEDIOS DE OBTENCIÓN Y DATOS PERSONALES QUE SE RECABAN</b>
						<br>
						<font color="#0A6ACF">'.$take['agname'].'</font>, recaba los siguientes datos personales necesarios para dar cumplimiento a las finalidades del presente Aviso de Privacidad, dependiendo de la relación que con usted exista.<br>	';

						switch ($take['agid'])
						{
							case '1': $html .='
							<b>A.</b> Datos personales que recabamos de manera personal: Datos personales de identificación: nombre, fecha de nacimiento, domicilio, teléfonos (celular y fijo), correo electrónico Registro Federal de Contribuyentes y clase y tipo de licencia de conducir, laborales, patrimoniales, financieros, de salud necesarios para la prevención o el diagnóstico médico, asistencia sanitaria o tratamiento médico, así como datos de terceros relacionados (al proporcionar los datos personales de terceros, se entiende que cuenta con el consentimiento del titular de los datos personales).
							<br>
							<b>B.</b> Datos personales que recabamos de manera directa y personal y/o a través de página web, correo electrónico y vía telefónica: Datos personales de identificación.<br>
							<b>C.</b>	Datos personales que recabamos de manera indirecta a través de transferencias de terceros y fuentes de acceso público que están permitidas por la Ley: Datos personales de identificación.
							<br>';
							break;

							case '2': $html .=' 
							<b>A.</b> Datos personales que recabamos de manera personal: Datos personales de identificación: nombre, fecha de nacimiento, domicilio, teléfonos (celular y fijo), correo electrónico Registro Federal de Contribuyentes y clase y tipo de licencia de conducir, laborales, patrimoniales, financieros, de salud necesarios para la prevención o el diagnóstico médico, asistencia sanitaria o tratamiento médico, así como datos de terceros relacionados (al proporcionar los datos personales de terceros, se entiende que cuenta con el consentimiento del titular de los datos personales).
							<br>
							<b>B.</b> Datos personales que recabamos de manera directa y personal y/o a través de página web, correo electrónico y vía telefónica: Datos personales de identificación.
							<br>
							<b>C.</b>	Datos personales que recabamos de manera indirecta a través de transferencias de terceros y fuentes de acceso público que están permitidas por la Ley: Datos personales de identificación.
							<br>';
							break;	//Nissan Altozano

							case '3': $html .='
							<b>A.</b> Datos personales que recabamos de manera personal: Datos personales de identificación: nombre, fecha de nacimiento, domicilio, teléfonos (celular y fijo), correo electrónico Registro Federal de Contribuyentes y clase y tipo de licencia de conducir, laborales, patrimoniales, financieros, de salud necesarios para la prevención o el diagnóstico médico, asistencia sanitaria o tratamiento médico, así como datos de terceros relacionados (al proporcionar los datos personales de terceros, se entiende que cuenta con el consentimiento del titular de los datos personales).
							<br>
							<b>B.</b> Datos personales que recabamos de manera directa y personal y/o a través de página web, correo electrónico y vía telefónica: Datos personales de identificación.
							<br>
							<b>C.</b> Datos personales que recabamos de manera indirecta a través de transferencias de terceros y fuentes de acceso público que están permitidas por la Ley: Datos personales de identificación.
							<br>';
							break;	//Nissan Acueducto

							case '4': $html .='
							<b>A.</b> Datos personales que recabamos de manera directa y personal y/o a través de página web, correo electrónico y vía telefónica: Datos personales de identificación.
							<br>
							<b>B.</b> Datos personales que recabamos de manera indirecta a través de transferencias de terceros y fuentes de acceso público que están permitidas por la Ley: Datos personales de identificación.
							<br>';
							break;	//Honda Manantiales

							case '5': $html .='
							<b>A.</b> Datos personales que recabamos de manera directa y personal y/o a través de página web, correo electrónico y vía telefónica: Datos personales de identificación.
							<br>
							<b>B.</b> Datos personales que recabamos de manera indirecta a través de transferencias de terceros y fuentes de acceso público que están permitidas por la Ley: Datos personales de identificación.
							<br>';
							break;	//Honda Atizapan

							case '6': $html .=' ';
							break;	//Honda Monarca DF

							case '7': $html .='
						    <b>A.</b> Datos personales que recabamos de manera directa y personal y/o a través de página web, correo electrónico y vía telefónica: Datos personales de identificación.
						    <br>
							<b>B.</b> Datos personales que recabamos de manera indirecta a través de transferencias de terceros y fuentes de acceso público que están permitidas por la Ley: Datos personales de identificación.
							<br>';
							break;	//Honda Altozano

						    case '8': $html .='
							<b>A.</b> Datos personales que recabamos de manera directa y personal y/o a través de página web, correo electrónico y vía telefónica: Datos personales de identificación.
							<br>
							<b>B.</b> Datos personales que recabamos de manera indirecta a través de transferencias de terceros y fuentes de acceso público que están permitidas por la Ley: Datos personales de identificación.
							<br>

							Si deseo ser contactado                               No deseo ser contactado	    Si deseo la trasferencia de mis datos                                      No deseo la transferencia de mis datos	<br>
							Estoy enterado del tratamiento que recibirán mis datos personales en términos de lo establecido en la Ley Federal de Protección de Datos Personales en Posesión de Particulares, en mi carácter de titular de los “Datos” proporcionados para los propósitos y bajo las condiciones establecidas en el presente Aviso de Privacidad y por la “Ley”.';
							break;	//Honda Uruapan

							case '9': $html .='
							<b>A.<b> Datos personales que recabamos de manera directa y personal y/o a través de página web, correo electrónico y vía telefónica: Datos personales de identificación.
							<br>
							<b>B.<b> Datos personales que recabamos de manera indirecta a través de transferencias de terceros y fuentes de acceso público que están permitidas por la Ley: Datos personales de identificación.
							<br>

							Si deseo ser contactado                               No deseo ser contactado	    Si deseo la trasferencia de mis datos                                      No deseo la transferencia de mis datos	<br>
							Estoy enterado del tratamiento que recibirán mis datos personales en términos de lo establecido en la Ley Federal de Protección de Datos Personales en Posesión de Particulares, en mi carácter de titular de los “Datos” proporcionados para los propósitos y bajo las condiciones establecidas en el presente Aviso de Privacidad y por la “Ley”.';
							break;	//Honda Monarca Morelia

							case '10': $html .='
							<b>A.</b> Datos personales que recabamos de manera directa y personal y/o a través de página web, correo electrónico y vía telefónica: Datos personales de identificación.
							<br>
							<b>B.</b> Datos personales que recabamos de manera indirecta a través de transferencias de terceros y fuentes de acceso público que están permitidas por la Ley: Datos personales de identificación.
							<br>';
						    break;	//Honda Corregidora

							case '11': $html .='
							<b>A.</b> Datos personales que recabamos de manera directa y personal y/o a través de página web, correo electrónico y vía telefónica: Datos personales de identificación.
							<br>
							<b>B.</b> Datos personales que recabamos de manera indirecta a través de transferencias de terceros y fuentes de acceso público que están permitidas por la Ley: Datos personales de identificación.
							<br>';
							break;	//Honda Marquesa

							case '12': $html .='
							<b>A.</b> Datos personales que recabamos de manera directa y personal y/o a través de página web, correo electrónico y vía telefónica: Datos personales de identificación.
							<br>
							<b>B.</b> Datos personales que recabamos de manera indirecta a través de transferencias de terceros y fuentes de acceso público que están permitidas por la Ley: Datos personales de identificación.
							<br>';
							break;	//Honda San Juan del Rio

							case '13': $html .='
							Datos personales que recabamos de manera personal: Datos personales de identificación: nombre completo, fecha de nacimiento, edad, género, domicilio, teléfonos casa, y/o trabajo, teléfono celular, correo electrónico Registro Federal de Contribuyentes, documentos de identificación como lo es credencial para votar, pasaporte, cedula profesional, licencia de conducir, así como datos laborales, patrimoniales, financieros, de salud necesarios para la prevención o el diagnóstico médico, asistencia sanitaria o tratamiento médico, así como datos de terceros relacionados (al proporcionar los datos personales de terceros, se entiende que cuenta con el consentimiento del titular de los datos personales).
							<br>
							<b>A.</b> Datos personales que recabamos de manera directa y personal y/o a través de página web, correo electrónico y vía telefónica: Datos personales de identificación.
							<br>
							<b>B.</b> Datos personales que recabamos de manera indirecta a través de transferencias de terceros y fuentes de acceso público que están permitidas por la Ley: Datos personales de identificación.
							<br>';
						    break;	//Kia Pedregal

						    case '14': $html .='
						    Datos personales que recabamos de manera personal: Datos personales de identificación: nombre completo, fecha de nacimiento, edad, género, domicilio, teléfonos casa, y/o trabajo, teléfono celular, correo electrónico Registro Federal de Contribuyentes, documentos de identificación como lo es credencial para votar, pasaporte, cedula profesional, licencia de conducir, así como datos laborales, patrimoniales, financieros, de salud necesarios para la prevención o el diagnóstico médico, asistencia sanitaria o tratamiento médico, así como datos de terceros relacionados (al proporcionar los datos personales de terceros, se entiende que cuenta con el consentimiento del titular de los datos personales).
						    <br>
							<b>A.</b> Datos personales que recabamos de manera directa y personal y/o a través de página web, correo electrónico y vía telefónica: Datos personales de identificación.
							<br>
							<b>B.</b> Datos personales que recabamos de manera indirecta a través de transferencias de terceros y fuentes de acceso público que están permitidas por la Ley: Datos personales de identificación.
							<br>';
							break;	//Kia Paricutin

						    case '15': 
							$html .='Datos personales que recabamos de manera personal: Datos personales de identificación: nombre completo, fecha de nacimiento, edad, género, domicilio, teléfonos casa, y/o trabajo, teléfono celular, correo electrónico Registro Federal de Contribuyentes, documentos de identificación como lo es credencial para votar, pasaporte, cedula profesional, licencia de conducir, así como datos laborales, patrimoniales, financieros, de salud necesarios para la prevención o el diagnóstico médico, asistencia sanitaria o tratamiento médico, así como datos de terceros relacionados (al proporcionar los datos personales de terceros, se entiende que cuenta con el consentimiento del titular de los datos personales).
                            <br>
							<b>A.</b> Datos personales que recabamos de manera directa y personal y/o a través de página web, correo electrónico y vía telefónica: Datos personales de identificación.
							<br>
							<b>B.</b> Datos personales que recabamos de manera indirecta a través de transferencias de terceros y fuentes de acceso público que están permitidas por la Ley: Datos personales de identificación.
							<br>';
							break;	//Kia Mil Cumbres

							case '16': 
							$html .='Datos personales que recabamos de manera personal: Datos personales de identificación: nombre completo, fecha de nacimiento, edad, género, domicilio, teléfonos casa, y/o trabajo, teléfono celular, correo electrónico Registro Federal de Contribuyentes, documentos de identificación como lo es credencial para votar, pasaporte, cedula profesional, licencia de conducir, así como datos laborales, patrimoniales, financieros, de salud necesarios para la prevención o el diagnóstico médico, asistencia sanitaria o tratamiento médico, así como datos de terceros relacionados (al proporcionar los datos personales de terceros, se entiende que cuenta con el consentimiento del titular de los datos personales).
                            <br> 
							<b>A.</b> Datos personales que recabamos de manera directa y personal y/o a través de página web, correo electrónico y vía telefónica: Datos personales de identificación.
							<b>B.</b> Datos personales que recabamos de manera indirecta a través de transferencias de terceros y fuentes de acceso público que están permitidas por la Ley: Datos personales de identificación.';
							break;	//Kia del Duero

							case '17': 
							$html .='Datos personales que recabamos de manera personal: Datos personales de identificación: nombre, fecha de nacimiento, domicilio, teléfonos (celular y fijo), correo electrónico Registro Federal de Contribuyentes y clase y tipo de licencia de conducir, laborales, patrimoniales, financieros, de salud necesarios para la prevención o el diagnóstico médico, asistencia sanitaria o tratamiento médico, así como datos de terceros relacionados (al proporcionar los datos personales de terceros, se entiende que cuenta con el consentimiento del titular de los datos personales).
							<br>
							<b>A.</b>	Datos personales que recabamos de manera directa y personal y/o a través de página web, correo electrónico y vía telefónica: Datos personales de identificación.
							<br>
							<b>B.</b>	Datos personales que recabamos de manera indirecta a través de transferencias de terceros y fuentes de acceso público que están permitidas por la Ley: Datos personales de identificación.
							<br>';
							break;	//Chevrolet Morelia

							case '18': 
							$html .='Datos personales que recabamos de manera personal: Datos personales de identificación: nombre, fecha de nacimiento, domicilio, teléfonos (celular y fijo), correo electrónico Registro Federal de Contribuyentes y clase y tipo de licencia de conducir, laborales, patrimoniales, financieros, de salud necesarios para la prevención o el diagnóstico médico, asistencia sanitaria o tratamiento médico, así como datos de terceros relacionados (al proporcionar los datos personales de terceros, se entiende que cuenta con el consentimiento del titular de los datos personales).
							<br>
							<b>A.</b>	Datos personales que recabamos de manera directa y personal y/o a través de página web, correo electrónico y vía telefónica: Datos personales de identificación.
							<br>
							<b>B.</b>	Datos personales que recabamos de manera indirecta a través de transferencias de terceros y fuentes de acceso público que están permitidas por la Ley: Datos personales de identificación.
							<br>


							Si deseo ser contactado                               No deseo ser contactado	    Si deseo la trasferencia de mis datos                                      No deseo la transferencia de mis datos<br>	


							Estoy enterado del tratamiento que recibirán mis datos personales en términos de lo establecido en la Ley Federal de Protección de Datos Personales en Posesión de Particulares, en mi carácter de titular de los “Datos” proporcionados para los propósitos y bajo las condiciones establecidas en el presente Aviso de Privacidad y por la “Ley”.


							Nombre del Titular 		Firma ó huella dactilar		Fecha ';
							break;	//Chevrolet Lazaro Cardenas

							case '19': 
							$html .='Datos personales que recabamos de manera personal: Datos personales de identificación: nombre, fecha de nacimiento, domicilio, teléfonos (celular y fijo), correo electrónico Registro Federal de Contribuyentes y clase y tipo de licencia de conducir, laborales, patrimoniales, financieros, de salud necesarios para la prevención o el diagnóstico médico, asistencia sanitaria o tratamiento médico, así como datos de terceros relacionados (al proporcionar los datos personales de terceros, se entiende que cuenta con el consentimiento del titular de los datos personales).

							<b>A.</b> Datos personales que recabamos de manera directa y personal y/o a través de página web, correo electrónico y vía telefónica: Datos personales de identificación.
							<br>
							<b>B.</b> Datos personales que recabamos de manera indirecta a través de transferencias de terceros y fuentes de acceso público que están permitidas por la Ley: Datos personales de identificación.
							<br>';
							break;	//Chevrolet Uruapan

							case '20': 
							$html .=' 
							Datos personales que recabamos de manera personal: Datos personales de identificación: nombre, fecha de nacimiento, domicilio, teléfonos (celular y fijo), correo electrónico Registro Federal de Contribuyentes y clase y tipo de licencia de conducir, laborales, patrimoniales, financieros, de salud necesarios para la prevención o el diagnóstico médico, asistencia sanitaria o tratamiento médico, así como datos de terceros relacionados (al proporcionar los datos personales de terceros, se entiende que cuenta con el consentimiento del titular de los datos personales).
                            <br>
							<b>A.</b> Datos personales que recabamos de manera directa y personal y/o a través de página web, correo electrónico y vía telefónica: Datos personales de identificación.
							<br>
							<b>B.</b> Datos personales que recabamos de manera indirecta a través de transferencias de terceros y fuentes de acceso público que están permitidas por la Ley: Datos personales de identificación.
							<br>';
							break;	//Chevrolet Apatzingan

							case '21': 
							$html .=' 
							Datos personales que recabamos de manera personal: Datos personales de identificación: nombre, fecha de nacimiento, domicilio, teléfonos (celular y fijo), correo electrónico Registro Federal de Contribuyentes y clase y tipo de licencia de conducir, laborales, patrimoniales, financieros, de salud necesarios para la prevención o el diagnóstico médico, asistencia sanitaria o tratamiento médico, así como datos de terceros relacionados (al proporcionar los datos personales de terceros, se entiende que cuenta con el consentimiento del titular de los datos personales).
							<br>
							<b>A.</b> Datos personales que recabamos de manera directa y personal y/o a través de página web, correo electrónico y vía telefónica: Datos personales de identificación.
							<br>
							<b>B.</b> Datos personales que recabamos de manera indirecta a través de transferencias de terceros y fuentes de acceso público que están permitidas por la Ley: Datos personales de identificación.
							<br>';
							break;	//Chevrolet Zamora

							case '22': 
							$html .='
							<b>A.</b> Datos personales que recabamos de manera personal: Datos personales de identificación: nombre, fecha de nacimiento, domicilio, teléfonos (celular y fijo), correo electrónico Registro Federal de Contribuyentes y clase y tipo de licencia de conducir, laborales, patrimoniales, financieros, de salud necesarios para la prevención o el diagnóstico médico, asistencia sanitaria o tratamiento médico, así como datos de terceros relacionados (al proporcionar los datos personales de terceros, se entiende que cuenta con el consentimiento del titular de los datos personales).
							<br>
							<b>B.</b> Datos personales que recabamos de manera directa y personal y/o a través de página web, correo electrónico y vía telefónica: Datos personales de identificación.
							<br>
							<b>C.</b> Datos personales que recabamos de manera indirecta a través de transferencias de terceros y fuentes de acceso público que están permitidas por la Ley: Datos personales de identificación.
							<br>';
							break;	//Toyota Uruapan

							case '23': 
							$html .='
							<b>A.<b>	Datos personales que recabamos de manera personal: Datos personales de identificación: nombre, fecha de nacimiento, domicilio, teléfonos (celular y fijo), correo electrónico Registro Federal de Contribuyentes y clase y tipo de licencia de conducir, laborales, patrimoniales, financieros, de salud necesarios para la prevención o el diagnóstico médico, asistencia sanitaria o tratamiento médico, así como datos de terceros relacionados (al proporcionar los datos personales de terceros, se entiende que cuenta con el consentimiento del titular de los datos personales).
							<br>
							<b>B.</b> Datos personales que recabamos de manera directa y personal y/o a través de página web, correo electrónico y vía telefónica: Datos personales de identificación.
							<br>
							<b>C.</b> Datos personales que recabamos de manera indirecta a través de transferencias de terceros y fuentes de acceso público que están permitidas por la Ley: Datos personales de identificación.
							<br>';
							break;	//Toyota Altozano

							case '24': 
							$html .='
							<b>A.</b> Datos personales que recabamos de manera personal: Datos personales de identificación: nombre, fecha de nacimiento, domicilio, teléfonos (celular y fijo), correo electrónico Registro Federal de Contribuyentes y clase y tipo de licencia de conducir, laborales, patrimoniales, financieros, de salud necesarios para la prevención o el diagnóstico médico, asistencia sanitaria o tratamiento médico, así como datos de terceros relacionados (al proporcionar los datos personales de terceros, se entiende que cuenta con el consentimiento del titular de los datos personales).
							<br>
							<b>B.</b> Datos personales que recabamos de manera directa y personal y/o a través de página web, correo electrónico y vía telefónica: Datos personales de identificación.
							<br>
							<b>C.</b> Datos personales que recabamos de manera indirecta a través de transferencias de terceros y fuentes de acceso público que están permitidas por la Ley: Datos personales de identificación.
							<br>';
							break;	//Toyota Valladolid

							case '25': 
							$html .='
							<b>A.</b>	Datos personales que recabamos de manera personal: Datos personales de identificación: nombre, fecha de nacimiento, domicilio, teléfonos (celular y fijo), correo electrónico Registro Federal de Contribuyentes y clase y tipo de licencia de conducir, laborales, patrimoniales, financieros, de salud necesarios para la prevención o el diagnóstico médico, asistencia sanitaria o tratamiento médico, así como datos de terceros relacionados (al proporcionar los datos personales de terceros, se entiende que cuenta con el consentimiento del titular de los datos personales).
							<br>
							<b>B.</b> Datos personales que recabamos de manera directa y personal y/o a través de página web, correo electrónico y vía telefónica: Datos personales de identificación.
							<br>
							<b>C.</b> Datos personales que recabamos de manera indirecta a través de transferencias de terceros y fuentes de acceso público que están permitidas por la Ley: Datos personales de identificación.
							<br>';
							break;	//Toyota Perisur

							case '26': 
							$html .='
							Datos personales que recabamos de manera personal: Datos personales de identificación: nombre, fecha de nacimiento, domicilio, teléfonos (celular y fijo), correo electrónico Registro Federal de Contribuyentes y clase y tipo de licencia de conducir, laborales, patrimoniales, financieros, de salud necesarios para la prevención o el diagnóstico médico, asistencia sanitaria o tratamiento médico, así como datos de terceros relacionados (al proporcionar los datos personales de terceros, se entiende que cuenta con el consentimiento del titular de los datos personales).
							<br>							
							<b>A.</b> Datos personales que recabamos de manera directa y personal y/o a través de página web, correo electrónico y vía telefónica: Datos personales de identificación.
							<br>
							<b>B.</b> Datos personales que recabamos de manera indirecta a través de transferencias de terceros y fuentes de acceso público que están permitidas por la Ley: Datos personales de identificación.
							<br>';
							break;	//Mitsubishi Queretaro

							case '27': 
							$html .='
							Datos personales que recabamos de manera personal: Datos personales de identificación: nombre, fecha de nacimiento, domicilio, teléfonos (celular y fijo), correo electrónico Registro Federal de Contribuyentes y clase y tipo de licencia de conducir, laborales, patrimoniales, financieros, de salud necesarios para la prevención o el diagnóstico médico, asistencia sanitaria o tratamiento médico, así como datos de terceros relacionados (al proporcionar los datos personales de terceros, se entiende que cuenta con el consentimiento del titular de los datos personales).
							<br>							
							<b>A.</b> Datos personales que recabamos de manera directa y personal y/o a través de página web, correo electrónico y vía telefónica: Datos personales de identificación.
							<br>
							<br>
							<b>B.</b> Datos personales que recabamos de manera indirecta a través de transferencias de terceros y fuentes de acceso público que están permitidas por la Ley: Datos personales de identificación.';
							break;	//Mitsubishi Uruapan

							case '28':  
							$html .='Datos personales que recabamos de manera personal: Datos personales de identificación: nombre, fecha de nacimiento, domicilio, teléfonos (celular y fijo), correo electrónico Registro Federal de Contribuyentes y clase y tipo de licencia de conducir, laborales, patrimoniales, financieros, de salud necesarios para la prevención o el diagnóstico médico, asistencia sanitaria o tratamiento médico, así como datos de terceros relacionados (al proporcionar los datos personales de terceros, se entiende que cuenta con el consentimiento del titular de los datos personales).
							<br>
							<b>A.</b> Datos personales que recabamos de manera directa y personal y/o a través de página web, correo electrónico y vía telefónica: Datos personales de identificación.
							<br>
							<b>B.</b> Datos personales que recabamos de manera indirecta a través de transferencias de terceros y fuentes de acceso público que están permitidas por la Ley: Datos personales de identificación.
							<br>';
							break;	//GMC Morelia

							case '29': 
							$html .='Datos personales que recabamos de manera personal: Datos personales de identificación: nombre, fecha de nacimiento, domicilio, teléfonos (celular y fijo), correo electrónico Registro Federal de Contribuyentes y clase y tipo de licencia de conducir, laborales, patrimoniales, financieros, de salud necesarios para la prevención o el diagnóstico médico, asistencia sanitaria o tratamiento médico, así como datos de terceros relacionados (al proporcionar los datos personales de terceros, se entiende que cuenta con el consentimiento del titular de los datos personales).
							<br>
							<b>A.</b> Datos personales que recabamos de manera directa y personal y/o a través de página web, correo electrónico y vía telefónica: Datos personales de identificación.
							<br>
							<b>B.</b> Datos personales que recabamos de manera indirecta a través de transferencias de terceros y fuentes de acceso público que están permitidas por la Ley: Datos personales de identificación.
							<br>';
							break;	//GMC Uruapan

							case '30': 
							$html .='
							Datos personales que recabamos de manera personal: Datos personales de identificación: nombre, fecha de nacimiento, domicilio, teléfonos (celular y fijo), correo electrónico Registro Federal de Contribuyentes y clase y tipo de licencia de conducir, laborales, patrimoniales, financieros, de salud necesarios para la prevención o el diagnóstico médico, asistencia sanitaria o tratamiento médico, así como datos de terceros relacionados (al proporcionar los datos personales de terceros, se entiende que cuenta con el consentimiento del titular de los datos personales).
							<br>
							<b>A.</b> Datos personales que recabamos de manera directa y personal y/o a través de página web, correo electrónico y vía telefónica: Datos personales de identificación.
							<br>
							<b>B.</b> Datos personales que recabamos de manera indirecta a través de transferencias de terceros y fuentes de acceso público que están permitidas por la Ley: Datos personales de identificación.
							<br>';
							break;	//BMW Talisman

							case '31': 
							$html .=' 
							Datos personales que recabamos de manera personal: Datos personales de identificación: nombre, fecha de nacimiento, domicilio, teléfonos (celular y fijo), correo electrónico Registro Federal de Contribuyentes y clase y tipo de licencia de conducir, laborales, patrimoniales, financieros, de salud necesarios para la prevención o el diagnóstico médico, asistencia sanitaria o tratamiento médico, así como datos de terceros relacionados (al proporcionar los datos personales de terceros, se entiende que cuenta con el consentimiento del titular de los datos personales).
							<br>
							<b>A.</b> Datos personales que recabamos de manera directa y personal y/o a través de página web, correo electrónico y vía telefónica: Datos personales de identificación.
							<br>
							<b>B.</b> Datos personales que recabamos de manera indirecta a través de transferencias de terceros y fuentes de acceso público que están permitidas por la Ley: Datos personales de identificación.
							<br>';
							break;	//Motorrad Morelia

							case '32': 
							$html .='
							Datos personales que recabamos de manera personal: Datos personales de identificación: nombre, fecha de nacimiento, domicilio, teléfonos (celular y fijo), correo electrónico Registro Federal de Contribuyentes y clase y tipo de licencia de conducir, laborales, patrimoniales, financieros, de salud necesarios para la prevención o el diagnóstico médico, asistencia sanitaria o tratamiento médico, así como datos de terceros relacionados (al proporcionar los datos personales de terceros, se entiende que cuenta con el consentimiento del titular de los datos personales).
							<br>
							<b>A.</b> Datos personales que recabamos de manera directa y personal y/o a través de página web, correo electrónico y vía telefónica: Datos personales de identificación.
							<br>
							<b>B.</b> Datos personales que recabamos de manera indirecta a través de transferencias de terceros y fuentes de acceso público que están permitidas por la Ley: Datos personales de identificación.
							<br>';
							break;	//Mini Talisman

							case '33': 
							$html .=' ';
							break;	//Chrysler Queretaro

							case '34': 
							$html .=' ';
							break;	//Chrysler Uruapan

							case '35': 
							$html .=' ';
							break;	//Audi Juriquilla

							case '36': 
							$html .='Datos personales que recabamos de manera personal: Datos personales de identificación: nombre, fecha de nacimiento, domicilio, teléfonos (celular y fijo), correo electrónico Registro Federal de Contribuyentes y clase y tipo de licencia de conducir, laborales, patrimoniales, financieros, de salud necesarios para la prevención o el diagnóstico médico, asistencia sanitaria o tratamiento médico, así como datos de terceros relacionados (al proporcionar los datos personales de terceros, se entiende que cuenta con el consentimiento del titular de los datos personales).
							<br>
							<b>A.</b>	Datos personales que recabamos de manera directa y personal y/o a través de página web, correo electrónico y vía telefónica: Datos personales de identificación.
							<br>
							<b>B.</b>	Datos personales que recabamos de manera indirecta a través de transferencias de terceros y fuentes de acceso público que están permitidas por la Ley: Datos personales de identificación.
							<br>';
							break;	//Isuzu Camiones

							case '37': 
							$html .=' ';
							break;	//Volkswagen

							case '38': 
							$html .=' ';
							break;	//Das Welt

							case '39': 
							$html .=' ';
							break;	//Salon del Auto

							case '40': 
							$html .=' ';
							break;	//Multimarca

							case '41': 
							$html .=' ';
							break;	//Noti FAME

							case '42': 
							$html .=' ';
							break;	//Audi Queretaro
						}


						$html .='Se le informa que las imágenes y sonidos captados a través del sistema de video vigilancia, serán utilizadas para su seguridad y de las personas que nos visitan. Si usted participa en eventos y reportajes, se le comunica que las imágenes y sonidos captados, podrán ser difundidas a través de medios de comunicación interna y las distintas redes sociales. Asimismo, los sonidos relativos a su voz, recabados a través de los distintos medios utilizados, serán tratados como soporte del seguimiento de la calidad de nuestros servicios.<br>

						<b>Datos personales sensibles.</b><br>
						Aquellos que refieren a características personales, como huella digital, género, laborales, patrimoniales, financieros, de salud, y demás datos personales que estén sujetos al ámbito de aplicación de la legislación en materia de protección de datos personales.<br>

						<b>III.	FINALIDADES</b><br>

						En términos de lo establecido por La Ley Federal de Protección de Datos Personales en Posesión de los Particulares (La Ley) y su Reglamento, los datos personales que nos proporcione serán utilizados para las siguientes finalidades:<br> ';


						switch ($take['agid'])
						{
							case '1': $html .='
							<b>A.</b>	Necesarias para la prestación del servicio: <br>
							<b>I.</b>	Realizar los trámites, procedimientos y gestiones para la adquisición de bienes, productos y servicios para nuestros clientes y clientes potenciales.
							<br><b>II.</b>	Comercialización de vehículos automotores nuevos y seminuevos, así como toda clase de partes, refacciones y accesorios de vehículos automotores en sus diversas modalidades: contado, crédito y autofi¬nanciamiento.
							<br><b>III.</b>	Otorgarle toda clase de bienes, productos y servicios para el mantenimiento de su automóvil (refacciones, hojalatería y pintura, servicios varios relacionados con vehículos automotores).
							<br><b>IV.</b>	Realizar los diversos trámites para gestionar ante las autoridades competentes, el alta de los vehículos automotores en los padrones vehiculares correspondientes, así como llevar a cabo la verificación ambiental en su caso.
							<br><b>V.</b>	Consultar el historial crediticio ante las sociedades de información crediticia.
							<br><b>VI.</b>	Cumplimiento de las obligaciones en materia de garantías en vehículos automotores nuevos, seminuevos y en servicio de reparación y/o mantenimiento.
							<br><b>VII.</b>	Informar sobre las campañas de seguridad o de servicio de los vehículos automotores que promueva el fabricante de los productos (llamada/recall), así como de actualizaciones técnicas.
							<br><b>VIII.</b>	Tramitar y formalizar un crédito para la adquisición de un vehículo nuevo y/o seminuevo, así como la adquisición de seguros automotrices y extensiones de garantía conforme los contratos previamente establecidos por las partes.
							<br><b>IX.</b>	Cumplir con todas las actividades complementarias y/o auxiliares necesarias para la comercialización de vehículos automotores nuevos, seminuevos y la prestación de servicios y productos para el manteni¬miento de vehículos.
							<br><b>X.</b>	Procesar solicitudes, realizar actividades de cobranza, facturación y aclaraciones.
							<br><b>XI.</b>	Cumplir con las leyes, reglamentos y demás disposiciones legales aplicables.
							<br><b>XII.</b>	Mantener actualizados nuestros registros para poder responder a sus consultas.
							<br><b>XIII.</b>	Promociones, recordatorios y citas de servicio, carta de bienvenida, información sobre campañas preventivas de servicio y/o de seguridad, citas de prueba de manejo, seguimiento de comentarios, consultas, quejas y reclamaciones; controles estadísticos, controles administrativos y encuestas.
							<br><b>B.</b>	No necesarias para la prestación el servicio:
							<br><b>1)</b>	Para ofrecer bienes y servicios adicionales a los contratados.
							<br><b>2)</b>	Realizar estudios internos sobre hábitos de consumo, mercadotecnia, publicidad y prospección comercial relacionada con los bienes, productos y servicios, así como las inscripciones a programas de lealtad que ofrece el fabricante de los productos.
							<br><b>3)</b>	Dar seguimiento a nuestra relación comercial y solicitar la evaluación y el seguimiento para conocer el nivel de satisfacción de nuestros productos y servicios.
							<br><b>4)</b>	Análisis estadísticos / Fines estadísticos
							<br><b>5)</b>	Envió de formulario de encuestas para seguimiento de los servicios ofrecidos / Realizar encuestas / Realizar estudios de mercado
							<br><b>6)</b>	Análisis de información / Analizar hábitos de consumo
							<br><b>7)</b>	Contacto con el titular
							<br><b>8)</b>	Determinar la satisfacción / evaluar la calidad del servicio
							<br><b>9)</b>	Para invitarlo a eventos o enviar información sobre promocionales, productos y servicios adicionales que consideremos pueden ser de su interés.
							<br><b>10)</b>	Para contactarlo, enviarle notificaciones y uso de las diversas aplicaciones digitales administradas por nosotros y/o terceros que designemos; y que usted decida utilizar para mantenerse informado y/o dar seguimiento a sus adquisiciones de producto y servicio de postventa.';	
							break; //Grupo FAME 
							 

							case '2': $html .='
							<b>A.</b>	Necesarias para la prestación del servicio: <br>
							<b>I.</b>	Realizar los trámites, procedimientos y gestiones para la adquisición de bienes, productos y servicios para nuestros clientes y clientes potenciales.
							<br><b>II.</b>	Comercialización de vehículos automotores nuevos y seminuevos, así como toda clase de partes, refacciones y accesorios de vehículos automotores en sus diversas modalidades: contado, crédito y autofi¬nanciamiento.
							<br><b>III.</b>	Otorgarle toda clase de bienes, productos y servicios para el mantenimiento de su automóvil (refacciones, hojalatería y pintura, servicios varios relacionados con vehículos automotores).
							<br><b>IV.</b>	Realizar los diversos trámites para gestionar ante las autoridades competentes, el alta de los vehículos automotores en los padrones vehiculares correspondientes, así como llevar a cabo la verificación ambiental en su caso.
							<br><b>V.</b>	Consultar el historial crediticio ante las sociedades de información crediticia.
							<br><b>VI.</b>	Cumplimiento de las obligaciones en materia de garantías en vehículos automotores nuevos, seminuevos y en servicio de reparación y/o mantenimiento.
							<br><b>VII.</b>	Informar sobre las campañas de seguridad o de servicio de los vehículos automotores que promueva el fabricante de los productos (llamada/recall), así como de actualizaciones técnicas.
							<br><b>VIII.</b>	Tramitar y formalizar un crédito para la adquisición de un vehículo nuevo y/o seminuevo, así como la adquisición de seguros automotrices y extensiones de garantía conforme los contratos previamente establecidos por las partes.
							<br><b>IX.</b>	Cumplir con todas las actividades complementarias y/o auxiliares necesarias para la comercialización de vehículos automotores nuevos, seminuevos y la prestación de servicios y productos para el manteni¬miento de vehículos.
							<br><b>X.</b>	Procesar solicitudes, realizar actividades de cobranza, facturación y aclaraciones.
							<br><b>XI.</b>	Cumplir con las leyes, reglamentos y demás disposiciones legales aplicables.
							<br><b>XII.</b>	Mantener actualizados nuestros registros para poder responder a sus consultas.
							<br><b>XIII.</b>	Promociones, recordatorios y citas de servicio, carta de bienvenida, información sobre campañas preventivas de servicio y/o de seguridad, citas de prueba de manejo, seguimiento de comentarios, consultas, quejas y reclamaciones; controles estadísticos, controles administrativos y encuestas.
							<br><b>B.</b>	No necesarias para la prestación el servicio:
							<br><b>1)</b>	Para ofrecer bienes y servicios adicionales a los contratados.
							<br><b>2)</b>	Realizar estudios internos sobre hábitos de consumo, mercadotecnia, publicidad y prospección comercial relacionada con los bienes, productos y servicios, así como las inscripciones a programas de lealtad que ofrece el fabricante de los productos.
							<br><b>3)</b>	Dar seguimiento a nuestra relación comercial y solicitar la evaluación y el seguimiento para conocer el nivel de satisfacción de nuestros productos y servicios.
							<br><b>4)</b>	Análisis estadísticos / Fines estadísticos
							<br><b>5)</b>	Envió de formulario de encuestas para seguimiento de los servicios ofrecidos / Realizar encuestas / Realizar estudios de mercado
							<br><b>6)</b>	Análisis de información / Analizar hábitos de consumo
							<br><b>7)</b>	Contacto con el titular
							<br><b>8)</b>	Determinar la satisfacción / evaluar la calidad del servicio
							<br><b>9)</b>	Para invitarlo a eventos o enviar información sobre promocionales, productos y servicios adicionales que consideremos pueden ser de su interés.
							<br><b>10)</b>	Para contactarlo, enviarle notificaciones y uso de las diversas aplicaciones digitales administradas por nosotros y/o terceros que designemos; y que usted decida utilizar para mantenerse informado y/o dar seguimiento a sus adquisiciones de producto y servicio de postventa.';	
							break; //Nissan Altozano
							 

							case '3': $html .='
							<b>A.</b>	Necesarias para la prestación del servicio: <br> <br>

							<b>I.</b>	Realizar los trámites, procedimientos y gestiones para la adquisición de bienes, productos y servicios para nuestros clientes y clientes potenciales.
							<br><b>II.</b>	Comercialización de vehículos automotores nuevos y seminuevos, así como toda clase de partes, refacciones y accesorios de vehículos automotores en sus diversas modalidades: contado, crédito y autofi¬nanciamiento.
							<br><b>III.</b>	Otorgarle toda clase de bienes, productos y servicios para el mantenimiento de su automóvil (refacciones, hojalatería y pintura, servicios varios relacionados con vehículos automotores).
							<br><b>IV.</b>	Realizar los diversos trámites para gestionar ante las autoridades competentes, el alta de los vehículos automotores en los padrones vehiculares correspondientes, así como llevar a cabo la verificación ambiental en su caso.
							<br><b>V.</b>	Consultar el historial crediticio ante las sociedades de información crediticia.
							<br><b>VI.</b>	Cumplimiento de las obligaciones en materia de garantías en vehículos automotores nuevos, seminuevos y en servicio de reparación y/o mantenimiento.
							<br><b>VII.</b>	Informar sobre las campañas de seguridad o de servicio de los vehículos automotores que promueva el fabricante de los productos (llamada/recall), así como de actualizaciones técnicas.
							<br><b>VIII.</b>	Tramitar y formalizar un crédito para la adquisición de un vehículo nuevo y/o seminuevo, así como la adquisición de seguros automotrices y extensiones de garantía conforme los contratos previamente establecidos por las partes.
							<br><b>IX.</b>	Cumplir con todas las actividades complementarias y/o auxiliares necesarias para la comercialización de vehículos automotores nuevos, seminuevos y la prestación de servicios y productos para el manteni¬miento de vehículos.
							<br><b>X.</b>	Procesar solicitudes, realizar actividades de cobranza, facturación y aclaraciones.
							<br><b>XI.</b>	Cumplir con las leyes, reglamentos y demás disposiciones legales aplicables.
							<br><b>XII.</b>	Mantener actualizados nuestros registros para poder responder a sus consultas.
							<br><b>XIII.</b>	Promociones, recordatorios y citas de servicio, carta de bienvenida, información sobre campañas preventivas de servicio y/o de seguridad, citas de prueba de manejo, seguimiento de comentarios, consultas, quejas y reclamaciones; controles estadísticos, controles administrativos y encuestas.
							<br><b>B.</b>	No necesarias para la prestación el servicio:
							<br><b>1)</b>	Para ofrecer bienes y servicios adicionales a los contratados.
							<br><b>2)</b>	Realizar estudios internos sobre hábitos de consumo, mercadotecnia, publicidad y prospección comercial relacionada con los bienes, productos y servicios, así como las inscripciones a programas de lealtad que ofrece el fabricante de los productos.
							<br><b>3)</b>	Dar seguimiento a nuestra relación comercial y solicitar la evaluación y el seguimiento para conocer el nivel de satisfacción de nuestros productos y servicios.
							<br><b>4)</b>	Análisis estadísticos / Fines estadísticos
							<br><b>5)</b>	Envió de formulario de encuestas para seguimiento de los servicios ofrecidos / Realizar encuestas / Realizar estudios de mercado
							<br><b>6)</b>	Análisis de información / Analizar hábitos de consumo
							<br><b>7)</b>	Contacto con el titular
							<br><b>8)</b>	Determinar la satisfacción / evaluar la calidad del servicio
							<br><b>9)</b>	Para invitarlo a eventos o enviar información sobre promocionales, productos y servicios adicionales que consideremos pueden ser de su interés.
							<br><b>10)</b>	Para contactarlo, enviarle notificaciones y uso de las diversas aplicaciones digitales administradas por nosotros y/o terceros que designemos; y que usted decida utilizar para mantenerse informado y/o dar seguimiento a sus adquisiciones de producto y servicio de postventa.';	
							break; //Nissan Acueducto

							case '4': $html .='
							<b>A.</b>	Necesarias para la prestación del servicio: <br>
							<b>I.</b>	Realizar los trámites, procedimientos y gestiones para la adquisición de bienes, productos y servicios para nuestros clientes y clientes potenciales.
							<br><b>II.</b>	Comercialización de vehículos automotores nuevos y seminuevos, así como toda clase de partes, refacciones y accesorios de vehículos automotores en sus diversas modalidades: contado, crédito y autofi¬nanciamiento.
							<br><b>III.</b>	Otorgarle toda clase de bienes, productos y servicios para el mantenimiento de su automóvil (refacciones, hojalatería y pintura, servicios varios relacionados con vehículos automotores).
							<br><b>IV.</b>	Realizar los diversos trámites para gestionar ante las autoridades competentes, el alta de los vehículos automotores en los padrones vehiculares correspondientes, así como llevar a cabo la verificación ambiental en su caso.
							<br><b>V.</b>	Consultar el historial crediticio ante las sociedades de información crediticia.
							<br><b>VI.</b>	Cumplimiento de las obligaciones en materia de garantías en vehículos automotores nuevos, seminuevos y en servicio de reparación y/o mantenimiento.
							<br><b>VII.</b>	Informar sobre las campañas de seguridad o de servicio de los vehículos automotores que promueva el fabricante de los productos (llamada/recall), así como de actualizaciones técnicas.
							<br><b>VIII.</b>	Tramitar y formalizar un crédito para la adquisición de un vehículo nuevo y/o seminuevo, así como la adquisición de seguros automotrices y extensiones de garantía conforme los contratos previamente establecidos por las partes.
							<br><b>IX.</b>	Cumplir con todas las actividades complementarias y/o auxiliares necesarias para la comercialización de vehículos automotores nuevos, seminuevos y la prestación de servicios y productos para el manteni¬miento de vehículos.
							<br><b>X.</b>	Procesar solicitudes, realizar actividades de cobranza, facturación y aclaraciones.
							<br><b>XI.</b>	Cumplir con las leyes, reglamentos y demás disposiciones legales aplicables.
							<br><b>XII.</b>	Mantener actualizados nuestros registros para poder responder a sus consultas.
							<br><b>XIII.</b>	Promociones, recordatorios y citas de servicio, carta de bienvenida, información sobre campañas preventivas de servicio y/o de seguridad, citas de prueba de manejo, seguimiento de comentarios, consultas, quejas y reclamaciones; controles estadísticos, controles administrativos y encuestas.
							<br><b>B.</b>	No necesarias para la prestación el servicio:
							<br><b>1)</b>	Para ofrecer bienes y servicios adicionales a los contratados.
							<br><b>2)</b>	Realizar estudios internos sobre hábitos de consumo, mercadotecnia, publicidad y prospección comercial relacionada con los bienes, productos y servicios, así como las inscripciones a programas de lealtad que ofrece el fabricante de los productos.
							<br><b>3)</b>	Dar seguimiento a nuestra relación comercial y solicitar la evaluación y el seguimiento para conocer el nivel de satisfacción de nuestros productos y servicios.
							<br><b>4)</b>	Análisis estadísticos / Fines estadísticos
							<br><b>5)</b>	Envió de formulario de encuestas para seguimiento de los servicios ofrecidos / Realizar encuestas / Realizar estudios de mercado
							<br><b>6)</b>	Análisis de información / Analizar hábitos de consumo
							<br><b>7)</b>	Contacto con el titular
							<br><b>8)</b>	Determinar la satisfacción / evaluar la calidad del servicio
							<br><b>9)</b>	Para invitarlo a eventos o enviar información sobre promocionales, productos y servicios adicionales que consideremos pueden ser de su interés.
							<br><b>10)</b>	Para contactarlo, enviarle notificaciones y uso de las diversas aplicaciones digitales administradas por nosotros y/o terceros que designemos; y que usted decida utilizar para mantenerse informado y/o dar seguimiento a sus adquisiciones de producto y servicio de postventa.';	
							break; //Honda 

							case '5': $html .='
							<b>A.</b>	Necesarias para la prestación del servicio: <br>
							<b>I.</b>	Realizar los trámites, procedimientos y gestiones para la adquisición de bienes, productos y servicios para nuestros clientes y clientes potenciales.
							<br><b>II.</b>	Comercialización de vehículos automotores nuevos y seminuevos, así como toda clase de partes, refacciones y accesorios de vehículos automotores en sus diversas modalidades: contado, crédito y autofi¬nanciamiento.
							<br><b>III.</b>	Otorgarle toda clase de bienes, productos y servicios para el mantenimiento de su automóvil (refacciones, hojalatería y pintura, servicios varios relacionados con vehículos automotores).
							<br><b>IV.</b>	Realizar los diversos trámites para gestionar ante las autoridades competentes, el alta de los vehículos automotores en los padrones vehiculares correspondientes, así como llevar a cabo la verificación ambiental en su caso.
							<br><b>V.</b>	Consultar el historial crediticio ante las sociedades de información crediticia.
							<br><b>VI.</b>	Cumplimiento de las obligaciones en materia de garantías en vehículos automotores nuevos, seminuevos y en servicio de reparación y/o mantenimiento.
							<br><b>VII.</b>	Informar sobre las campañas de seguridad o de servicio de los vehículos automotores que promueva el fabricante de los productos (llamada/recall), así como de actualizaciones técnicas.
							<br><b>VIII.</b>	Tramitar y formalizar un crédito para la adquisición de un vehículo nuevo y/o seminuevo, así como la adquisición de seguros automotrices y extensiones de garantía conforme los contratos previamente establecidos por las partes.
							<br><b>IX.</b>	Cumplir con todas las actividades complementarias y/o auxiliares necesarias para la comercialización de vehículos automotores nuevos, seminuevos y la prestación de servicios y productos para el manteni¬miento de vehículos.
							<br><b>X.</b>	Procesar solicitudes, realizar actividades de cobranza, facturación y aclaraciones.
							<br><b>XI.</b>	Cumplir con las leyes, reglamentos y demás disposiciones legales aplicables.
							<br><b>XII.</b>	Mantener actualizados nuestros registros para poder responder a sus consultas.
							<br><b>XIII.</b>	Promociones, recordatorios y citas de servicio, carta de bienvenida, información sobre campañas preventivas de servicio y/o de seguridad, citas de prueba de manejo, seguimiento de comentarios, consultas, quejas y reclamaciones; controles estadísticos, controles administrativos y encuestas.
							<br><b>B.</b>	No necesarias para la prestación el servicio:
							<br><b>1)</b>	Para ofrecer bienes y servicios adicionales a los contratados.
							<br><b>2)</b>	Realizar estudios internos sobre hábitos de consumo, mercadotecnia, publicidad y prospección comercial relacionada con los bienes, productos y servicios, así como las inscripciones a programas de lealtad que ofrece el fabricante de los productos.
							<br><b>3)</b>	Dar seguimiento a nuestra relación comercial y solicitar la evaluación y el seguimiento para conocer el nivel de satisfacción de nuestros productos y servicios.
							<br><b>4)</b>	Análisis estadísticos / Fines estadísticos
							<br><b>5)</b>	Envió de formulario de encuestas para seguimiento de los servicios ofrecidos / Realizar encuestas / Realizar estudios de mercado
							<br><b>6)</b>	Análisis de información / Analizar hábitos de consumo
							<br><b>7)</b>	Contacto con el titular
							<br><b>8)</b>	Determinar la satisfacción / evaluar la calidad del servicio
							<br><b>9)</b>	Para invitarlo a eventos o enviar información sobre promocionales, productos y servicios adicionales que consideremos pueden ser de su interés.
							<br><b>10)</b>	Para contactarlo, enviarle notificaciones y uso de las diversas aplicaciones digitales administradas por nosotros y/o terceros que designemos; y que usted decida utilizar para mantenerse informado y/o dar seguimiento a sus adquisiciones de producto y servicio de postventa.';	//Honda Atizapan
							case '6': $html .='';	
							break;//Honda Monarca DF

							case '7': $html .='
							<b>A.</b>	Necesarias para la prestación del servicio: <br>
							<b>I.</b>	Realizar los trámites, procedimientos y gestiones para la adquisición de bienes, productos y servicios para nuestros clientes y clientes potenciales.
							<br><b>II.</b>	Comercialización de vehículos automotores nuevos y seminuevos, así como toda clase de partes, refacciones y accesorios de vehículos automotores en sus diversas modalidades: contado, crédito y autofi¬nanciamiento.
							<br><b>III.</b>	Otorgarle toda clase de bienes, productos y servicios para el mantenimiento de su automóvil (refacciones, hojalatería y pintura, servicios varios relacionados con vehículos automotores).
							<br><b>IV.</b>	Realizar los diversos trámites para gestionar ante las autoridades competentes, el alta de los vehículos automotores en los padrones vehiculares correspondientes, así como llevar a cabo la verificación ambiental en su caso.
							<br><b>V.</b>	Consultar el historial crediticio ante las sociedades de información crediticia.
							<br><b>VI.</b>	Cumplimiento de las obligaciones en materia de garantías en vehículos automotores nuevos, seminuevos y en servicio de reparación y/o mantenimiento.
							<br><b>VII.</b>	Informar sobre las campañas de seguridad o de servicio de los vehículos automotores que promueva el fabricante de los productos (llamada/recall), así como de actualizaciones técnicas.
							<br><b>VIII.</b>	Tramitar y formalizar un crédito para la adquisición de un vehículo nuevo y/o seminuevo, así como la adquisición de seguros automotrices y extensiones de garantía conforme los contratos previamente establecidos por las partes.
							<br><b>IX.</b>	Cumplir con todas las actividades complementarias y/o auxiliares necesarias para la comercialización de vehículos automotores nuevos, seminuevos y la prestación de servicios y productos para el manteni¬miento de vehículos.
							<br><b>X.</b>	Procesar solicitudes, realizar actividades de cobranza, facturación y aclaraciones.
							<br><b>XI.</b>	Cumplir con las leyes, reglamentos y demás disposiciones legales aplicables.
							<br><b>XII.</b>	Mantener actualizados nuestros registros para poder responder a sus consultas.
							<br><b>XIII.</b>	Promociones, recordatorios y citas de servicio, carta de bienvenida, información sobre campañas preventivas de servicio y/o de seguridad, citas de prueba de manejo, seguimiento de comentarios, consultas, quejas y reclamaciones; controles estadísticos, controles administrativos y encuestas.
							<br><b>B.</b>	No necesarias para la prestación el servicio:
							<br><b>1)</b>	Para ofrecer bienes y servicios adicionales a los contratados.
							<br><b>2)</b>	Realizar estudios internos sobre hábitos de consumo, mercadotecnia, publicidad y prospección comercial relacionada con los bienes, productos y servicios, así como las inscripciones a programas de lealtad que ofrece el fabricante de los productos.
							<br><b>3)</b>	Dar seguimiento a nuestra relación comercial y solicitar la evaluación y el seguimiento para conocer el nivel de satisfacción de nuestros productos y servicios.
							<br><b>4)</b>	Análisis estadísticos / Fines estadísticos
							<br><b>5)</b>	Envió de formulario de encuestas para seguimiento de los servicios ofrecidos / Realizar encuestas / Realizar estudios de mercado
							<br><b>6)</b>	Análisis de información / Analizar hábitos de consumo
							<br><b>7)</b>	Contacto con el titular
							<br><b>8)</b>	Determinar la satisfacción / evaluar la calidad del servicio
							<br><b>9)</b>	Para invitarlo a eventos o enviar información sobre promocionales, productos y servicios adicionales que consideremos pueden ser de su interés.
							<br><b>10)</b>	Para contactarlo, enviarle notificaciones y uso de las diversas aplicaciones digitales administradas por nosotros y/o terceros que designemos; y que usted decida utilizar para mantenerse informado y/o dar seguimiento a sus adquisiciones de producto y servicio de postventa.';	
							break; //Honda Altozano

							case '8': $html .='
							<b>A.</b>	Necesarias para la prestación del servicio: <br>
							<b>I.</b>	Realizar los trámites, procedimientos y gestiones para la adquisición de bienes, productos y servicios para nuestros clientes y clientes potenciales.
							<br><b>II.</b>	Comercialización de vehículos automotores nuevos y seminuevos, así como toda clase de partes, refacciones y accesorios de vehículos automotores en sus diversas modalidades: contado, crédito y autofi¬nanciamiento.
							<br><b>III.</b>	Otorgarle toda clase de bienes, productos y servicios para el mantenimiento de su automóvil (refacciones, hojalatería y pintura, servicios varios relacionados con vehículos automotores).
							<br><b>IV.</b>	Realizar los diversos trámites para gestionar ante las autoridades competentes, el alta de los vehículos automotores en los padrones vehiculares correspondientes, así como llevar a cabo la verificación ambiental en su caso.
							<br><b>V.</b>	Consultar el historial crediticio ante las sociedades de información crediticia.
							<br><b>VI.</b>	Cumplimiento de las obligaciones en materia de garantías en vehículos automotores nuevos, seminuevos y en servicio de reparación y/o mantenimiento.
							<br><b>VII.</b>	Informar sobre las campañas de seguridad o de servicio de los vehículos automotores que promueva el fabricante de los productos (llamada/recall), así como de actualizaciones técnicas.
							<br><b>VIII.</b>	Tramitar y formalizar un crédito para la adquisición de un vehículo nuevo y/o seminuevo, así como la adquisición de seguros automotrices y extensiones de garantía conforme los contratos previamente establecidos por las partes.
							<br><b>IX.</b>	Cumplir con todas las actividades complementarias y/o auxiliares necesarias para la comercialización de vehículos automotores nuevos, seminuevos y la prestación de servicios y productos para el manteni¬miento de vehículos.
							<br><b>X.</b>	Procesar solicitudes, realizar actividades de cobranza, facturación y aclaraciones.
							<br><b>XI.</b>	Cumplir con las leyes, reglamentos y demás disposiciones legales aplicables.
							<br><b>XII.</b>	Mantener actualizados nuestros registros para poder responder a sus consultas.
							<br><b>XIII.</b>	Promociones, recordatorios y citas de servicio, carta de bienvenida, información sobre campañas preventivas de servicio y/o de seguridad, citas de prueba de manejo, seguimiento de comentarios, consultas, quejas y reclamaciones; controles estadísticos, controles administrativos y encuestas.
							<br><b>B.</b>	No necesarias para la prestación el servicio:
							<br><b>1)</b>	Realizar estudios internos sobre hábitos de consumo, mercadotecnia, publicidad y prospección comercial relacionada con los bienes, productos y servicios, así como las inscripciones a programas de lealtad que ofrece el fabricante de los productos.
							<br><b>2)</b>	Dar seguimiento a nuestra relación comercial y solicitar la evaluación y el seguimiento para conocer el nivel de satisfacción de nuestros productos y servicios.
							<br><b>3)</b>	Análisis estadísticos / Fines estadísticos
							<br><b>4)</b>	Envió de formulario de encuestas para seguimiento de los servicios ofrecidos / Realizar encuestas / Realizar estudios de mercado
							<br><b>5)</b>	Análisis de información / Analizar hábitos de consumo
							<br><b>6)</b>	Contacto con el titular
							<br><b>7)</b>	Determinar la satisfacción / evaluar la calidad del servicio.
							<br><b>8)</b>	Para contactarlo, enviarle notificaciones y uso de las diversas aplicaciones digitales administradas por nosotros y/o terceros que designemos; y que usted decida utilizar para mantenerse informado y/o dar seguimiento a sus adquisiciones de producto y servicio de postventa.';	
							break; //Honda Uruapan

							case '9': $html .='
							<b>A.</b>	Necesarias para la prestación del servicio: <br>
							<b>I.</b>	Realizar los trámites, procedimientos y gestiones para la adquisición de bienes, productos y servicios para nuestros clientes y clientes potenciales.
							<br><b>II.</b>	Comercialización de vehículos automotores nuevos y seminuevos, así como toda clase de partes, refacciones y accesorios de vehículos automotores en sus diversas modalidades: contado, crédito y autofi¬nanciamiento.
							<br><b>III.</b>	Otorgarle toda clase de bienes, productos y servicios para el mantenimiento de su automóvil (refacciones, hojalatería y pintura, servicios varios relacionados con vehículos automotores).
							<br><b>IV.</b>	Realizar los diversos trámites para gestionar ante las autoridades competentes, el alta de los vehículos automotores en los padrones vehiculares correspondientes, así como llevar a cabo la verificación ambiental en su caso.
							<br><b>V.</b>	Consultar el historial crediticio ante las sociedades de información crediticia.
							<br><b>VI.</b>	Cumplimiento de las obligaciones en materia de garantías en vehículos automotores nuevos, seminuevos y en servicio de reparación y/o mantenimiento.
							<br><b>VII.</b>	Informar sobre las campañas de seguridad o de servicio de los vehículos automotores que promueva el fabricante de los productos (llamada/recall), así como de actualizaciones técnicas.
							<br><b>VIII.</b>	Tramitar y formalizar un crédito para la adquisición de un vehículo nuevo y/o seminuevo, así como la adquisición de seguros automotrices y extensiones de garantía conforme los contratos previamente establecidos por las partes.
							<br><b>IX.</b>	Cumplir con todas las actividades complementarias y/o auxiliares necesarias para la comercialización de vehículos automotores nuevos, seminuevos y la prestación de servicios y productos para el manteni¬miento de vehículos.
							<br><b>X.</b>	Procesar solicitudes, realizar actividades de cobranza, facturación y aclaraciones.
							<br><b>XI.</b>	Cumplir con las leyes, reglamentos y demás disposiciones legales aplicables.
							<br><b>XII.</b>	Mantener actualizados nuestros registros para poder responder a sus consultas.
							<br><b>XIII.</b>	Promociones, recordatorios y citas de servicio, carta de bienvenida, información sobre campañas preventivas de servicio y/o de seguridad, citas de prueba de manejo, seguimiento de comentarios, consultas, quejas y reclamaciones; controles estadísticos, controles administrativos y encuestas.
							<br><b>B.</b>	No necesarias para la prestación el servicio:
							<br><b>1)</b>	Para ofrecer bienes y servicios adicionales a los contratados.
							<br><b>2)</b>	Realizar estudios internos sobre hábitos de consumo, mercadotecnia, publicidad y prospección comercial relacionada con los bienes, productos y servicios, así como las inscripciones a programas de lealtad que ofrece el fabricante de los productos.
							<br><b>3)</b>	Dar seguimiento a nuestra relación comercial y solicitar la evaluación y el seguimiento para conocer el nivel de satisfacción de nuestros productos y servicios.
							<br><b>4)</b>	Análisis estadísticos / Fines estadísticos
							<br><b>5)</b>	Envió de formulario de encuestas para seguimiento de los servicios ofrecidos / Realizar encuestas / Realizar estudios de mercado
							<br><b>6)</b>	Análisis de información / Analizar hábitos de consumo
							<br><b>7)</b>	Contacto con el titular
							<br><b>8)</b>	Determinar la satisfacción / evaluar la calidad del servicio
							<br><b>9)</b>	Para invitarlo a eventos o enviar información sobre promocionales, productos y servicios adicionales que consideremos pueden ser de su interés.
							<br><b>10)</b>	Para contactarlo, enviarle notificaciones y uso de las diversas aplicaciones digitales administradas por nosotros y/o terceros que designemos; y que usted decida utilizar para mantenerse informado y/o dar seguimiento a sus adquisiciones de producto y servicio de postventa.';	
							break; //Honda Monarca Morelia

							case '10': $html .='
							<b>A.</b>	Necesarias para la prestación del servicio: <br>
							<b>I.</b>	Realizar los trámites, procedimientos y gestiones para la adquisición de bienes, productos y servicios para nuestros clientes y clientes potenciales.
							<br><b>II.</b>	Comercialización de vehículos automotores nuevos y seminuevos, así como toda clase de partes, refacciones y accesorios de vehículos automotores en sus diversas modalidades: contado, crédito y autofi¬nanciamiento.
							<br><b>III.</b>	Otorgarle toda clase de bienes, productos y servicios para el mantenimiento de su automóvil (refacciones, hojalatería y pintura, servicios varios relacionados con vehículos automotores).
							<br><b>IV.</b>	Realizar los diversos trámites para gestionar ante las autoridades competentes, el alta de los vehículos automotores en los padrones vehiculares correspondientes, así como llevar a cabo la verificación ambiental en su caso.
							<br><b>V.</b>	Consultar el historial crediticio ante las sociedades de información crediticia.
							<br><b>VI.</b>	Cumplimiento de las obligaciones en materia de garantías en vehículos automotores nuevos, seminuevos y en servicio de reparación y/o mantenimiento.
							<br><b>VII.</b>	Informar sobre las campañas de seguridad o de servicio de los vehículos automotores que promueva el fabricante de los productos (llamada/recall), así como de actualizaciones técnicas.
							<br><b>VIII.</b>	Tramitar y formalizar un crédito para la adquisición de un vehículo nuevo y/o seminuevo, así como la adquisición de seguros automotrices y extensiones de garantía conforme los contratos previamente establecidos por las partes.
							<br><b>IX.</b>	Cumplir con todas las actividades complementarias y/o auxiliares necesarias para la comercialización de vehículos automotores nuevos, seminuevos y la prestación de servicios y productos para el manteni¬miento de vehículos.
							<br><b>X.</b>	Procesar solicitudes, realizar actividades de cobranza, facturación y aclaraciones.
							<br><b>XI.</b>	Cumplir con las leyes, reglamentos y demás disposiciones legales aplicables.
							<br><b>XII.</b>	Mantener actualizados nuestros registros para poder responder a sus consultas.
							<br><b>XIII.</b>	Promociones, recordatorios y citas de servicio, carta de bienvenida, información sobre campañas preventivas de servicio y/o de seguridad, citas de prueba de manejo, seguimiento de comentarios, consultas, quejas y reclamaciones; controles estadísticos, controles administrativos y encuestas.
							<br><b>B.</b>	No necesarias para la prestación el servicio:
							<br><b>1)</b>	Para ofrecer bienes y servicios adicionales a los contratados.
							<br><b>2)</b>	Realizar estudios internos sobre hábitos de consumo, mercadotecnia, publicidad y prospección comercial relacionada con los bienes, productos y servicios, así como las inscripciones a programas de lealtad que ofrece el fabricante de los productos.
							<br><b>3)</b>	Dar seguimiento a nuestra relación comercial y solicitar la evaluación y el seguimiento para conocer el nivel de satisfacción de nuestros productos y servicios.
							<br><b>4)</b>	Análisis estadísticos / Fines estadísticos
							<br><b>5)</b>	Envió de formulario de encuestas para seguimiento de los servicios ofrecidos / Realizar encuestas / Realizar estudios de mercado
							<br><b>6)</b>	Análisis de información / Analizar hábitos de consumo
							<br><b>7)</b>	Contacto con el titular
							<br><b>8)</b>	Determinar la satisfacción / evaluar la calidad del servicio
							<br><b>9)</b>	Para invitarlo a eventos o enviar información sobre promocionales, productos y servicios adicionales que consideremos pueden ser de su interés.
							<br><b>10)</b>	Para contactarlo, enviarle notificaciones y uso de las diversas aplicaciones digitales administradas por nosotros y/o terceros que designemos; y que usted decida utilizar para mantenerse informado y/o dar seguimiento a sus adquisiciones de producto y servicio de postventa.';	
							break;//Honda Corregidora

							case '11': $html .='
							<b>A.</b>	Necesarias para la prestación del servicio: <br>
							<b>I.</b>	Realizar los trámites, procedimientos y gestiones para la adquisición de bienes, productos y servicios para nuestros clientes y clientes potenciales.
							<br><b>II.</b>	Comercialización de vehículos automotores nuevos y seminuevos, así como toda clase de partes, refacciones y accesorios de vehículos automotores en sus diversas modalidades: contado, crédito y autofi¬nanciamiento.
							<br><b>III.</b>	Otorgarle toda clase de bienes, productos y servicios para el mantenimiento de su automóvil (refacciones, hojalatería y pintura, servicios varios relacionados con vehículos automotores).
							<br><b>IV.</b>	Realizar los diversos trámites para gestionar ante las autoridades competentes, el alta de los vehículos automotores en los padrones vehiculares correspondientes, así como llevar a cabo la verificación ambiental en su caso.
							<br><b>V.</b>	Consultar el historial crediticio ante las sociedades de información crediticia.
							<br><b>VI.</b>	Cumplimiento de las obligaciones en materia de garantías en vehículos automotores nuevos, seminuevos y en servicio de reparación y/o mantenimiento.
							<br><b>VII.</b>	Informar sobre las campañas de seguridad o de servicio de los vehículos automotores que promueva el fabricante de los productos (llamada/recall), así como de actualizaciones técnicas.
							<br><b>VIII.</b>	Tramitar y formalizar un crédito para la adquisición de un vehículo nuevo y/o seminuevo, así como la adquisición de seguros automotrices y extensiones de garantía conforme los contratos previamente establecidos por las partes.
							<br><b>IX.</b>	Cumplir con todas las actividades complementarias y/o auxiliares necesarias para la comercialización de vehículos automotores nuevos, seminuevos y la prestación de servicios y productos para el manteni¬miento de vehículos.
							<br><b>X.</b>	Procesar solicitudes, realizar actividades de cobranza, facturación y aclaraciones.
							<br><b>XI.</b>	Cumplir con las leyes, reglamentos y demás disposiciones legales aplicables.
							<br><b>XII.</b>	Mantener actualizados nuestros registros para poder responder a sus consultas.
							<br><b>XIII.</b>	Promociones, recordatorios y citas de servicio, carta de bienvenida, información sobre campañas preventivas de servicio y/o de seguridad, citas de prueba de manejo, seguimiento de comentarios, consultas, quejas y reclamaciones; controles estadísticos, controles administrativos y encuestas.
							<br><b>B.</b>	No necesarias para la prestación el servicio:
							<br><b>1)</b>	Para ofrecer bienes y servicios adicionales a los contratados.
							<br><b>2)</b>	Realizar estudios internos sobre hábitos de consumo, mercadotecnia, publicidad y prospección comercial relacionada con los bienes, productos y servicios, así como las inscripciones a programas de lealtad que ofrece el fabricante de los productos.
							<br><b>3)</b>	Dar seguimiento a nuestra relación comercial y solicitar la evaluación y el seguimiento para conocer el nivel de satisfacción de nuestros productos y servicios.
							<br><b>4)</b>	Análisis estadísticos / Fines estadísticos
							<br><b>5)</b>	Envió de formulario de encuestas para seguimiento de los servicios ofrecidos / Realizar encuestas / Realizar estudios de mercado
							<br><b>6)</b>	Análisis de información / Analizar hábitos de consumo
							<br><b>7)</b>	Contacto con el titular
							<br><b>8)</b>	Determinar la satisfacción / evaluar la calidad del servicio
							<br><b>9)</b>	Para invitarlo a eventos o enviar información sobre promocionales, productos y servicios adicionales que consideremos pueden ser de su interés.
							<br><b>10)</b>	Para contactarlo, enviarle notificaciones y uso de las diversas aplicaciones digitales administradas por nosotros y/o terceros que designemos; y que usted decida utilizar para mantenerse informado y/o dar seguimiento a sus adquisiciones de producto y servicio de postventa.';	
							break; //Honda Marquesa

							case '12': $html .='
							<b>A.</b>	Necesarias para la prestación del servicio: <br>
							<b>I.</b>	Realizar los trámites, procedimientos y gestiones para la adquisición de bienes, productos y servicios para nuestros clientes y clientes potenciales.
							<br><b>II.</b>	Comercialización de vehículos automotores nuevos y seminuevos, así como toda clase de partes, refacciones y accesorios de vehículos automotores en sus diversas modalidades: contado, crédito y autofi¬nanciamiento.
							<br><b>III.</b>	Otorgarle toda clase de bienes, productos y servicios para el mantenimiento de su automóvil (refacciones, hojalatería y pintura, servicios varios relacionados con vehículos automotores).
							<br><b>IV.</b>	Realizar los diversos trámites para gestionar ante las autoridades competentes, el alta de los vehículos automotores en los padrones vehiculares correspondientes, así como llevar a cabo la verificación ambiental en su caso.
							<br><b>V.</b>	Consultar el historial crediticio ante las sociedades de información crediticia.
							<br><b>VI.</b>	Cumplimiento de las obligaciones en materia de garantías en vehículos automotores nuevos, seminuevos y en servicio de reparación y/o mantenimiento.
							<br><b>VII.</b>	Informar sobre las campañas de seguridad o de servicio de los vehículos automotores que promueva el fabricante de los productos (llamada/recall), así como de actualizaciones técnicas.
							<br><b>VIII.</b>	Tramitar y formalizar un crédito para la adquisición de un vehículo nuevo y/o seminuevo, así como la adquisición de seguros automotrices y extensiones de garantía conforme los contratos previamente establecidos por las partes.
							<br><b>IX.</b>	Cumplir con todas las actividades complementarias y/o auxiliares necesarias para la comercialización de vehículos automotores nuevos, seminuevos y la prestación de servicios y productos para el manteni¬miento de vehículos.
							<br><b>X.</b>	Procesar solicitudes, realizar actividades de cobranza, facturación y aclaraciones.
							<br><b>XI.</b>	Cumplir con las leyes, reglamentos y demás disposiciones legales aplicables.
							<br><b>XII.</b>	Mantener actualizados nuestros registros para poder responder a sus consultas.
							<br><b>XIII.</b>	Promociones, recordatorios y citas de servicio, carta de bienvenida, información sobre campañas preventivas de servicio y/o de seguridad, citas de prueba de manejo, seguimiento de comentarios, consultas, quejas y reclamaciones; controles estadísticos, controles administrativos y encuestas.
							<br><b>B.</b>	No necesarias para la prestación el servicio:
							<br><b>1)</b>	Para ofrecer bienes y servicios adicionales a los contratados.
							<br><b>2)</b>	Realizar estudios internos sobre hábitos de consumo, mercadotecnia, publicidad y prospección comercial relacionada con los bienes, productos y servicios, así como las inscripciones a programas de lealtad que ofrece el fabricante de los productos.
							<br><b>3)</b>	Dar seguimiento a nuestra relación comercial y solicitar la evaluación y el seguimiento para conocer el nivel de satisfacción de nuestros productos y servicios.
							<br><b>4)</b>	Análisis estadísticos / Fines estadísticos
							<br><b>5)</b>	Envió de formulario de encuestas para seguimiento de los servicios ofrecidos / Realizar encuestas / Realizar estudios de mercado
							<br><b>6)</b>	Análisis de información / Analizar hábitos de consumo
							<br><b>7)</b>	Contacto con el titular
							<br><b>8)</b>	Determinar la satisfacción / evaluar la calidad del servicio
							<br><b>9)</b>	Para invitarlo a eventos o enviar información sobre promocionales, productos y servicios adicionales que consideremos pueden ser de su interés.
							<br><b>10)</b>	Para contactarlo, enviarle notificaciones y uso de las diversas aplicaciones digitales administradas por nosotros y/o terceros que designemos; y que usted decida utilizar para mantenerse informado y/o dar seguimiento a sus adquisiciones de producto y servicio de postventa.';	
							break; //Honda San Juan del Rio

							case '13': $html .='
							<b>A.</b>	Necesarias para la prestación del servicio: <br>
							<b>I.</b>	Para identificación.
							<br><b>II.</b>	Realizar los trámites, procedimientos y gestiones para la adquisición de bienes, productos y servicios para nuestros clientes y clientes potenciales.
							<br><b>III.</b>	Comercialización de vehículos automotores nuevos y seminuevos, así como toda clase de partes, refacciones y accesorios de vehículos automotores en sus diversas modalidades: contado, crédito y autofi¬nanciamiento.
							<br><b>IV.</b>	Otorgarle toda clase de bienes, productos y servicios para el mantenimiento de su automóvil (refacciones, hojalatería y pintura, servicios varios relacionados con vehículos automotores).
							<br><b>V.</b>	Realizar los diversos trámites para gestionar ante las autoridades competentes, el alta de los vehículos automotores en los padrones vehiculares correspondientes, así como llevar a cabo la verificación ambiental en su caso.
							<br><b>VI.</b>	Consultar el historial crediticio ante las sociedades de información crediticia.
							<br><b>VII.</b>	Cumplimiento de las obligaciones en materia de garantías en vehículos automotores nuevos, seminuevos y en servicio de reparación y/o mantenimiento.
							<br><b>VIII.</b>	Informar sobre las campañas de seguridad o de servicio de los vehículos automotores que promueva el fabricante de los productos (llamada/recall), así como de actualizaciones técnicas de los bienes o servicios que se hubiese adquirido.
							<br><b>IX.</b>	Tramitar y formalizar un crédito para la adquisición de un vehículo nuevo y/o seminuevo, así como la adquisición de seguros automotrices y extensiones de garantía conforme los contratos previamente establecidos por las partes.
							<br><b>X.</b>	Cumplir con todas las actividades complementarias y/o auxiliares necesarias para la comercialización de vehículos automotores nuevos, seminuevos y la prestación de servicios y productos para recordarle los servicios de manteni¬miento de su vehículo.
							<br><b>XI.</b>	Procesar solicitudes, realizar actividades de cobranza, facturación y aclaraciones.
							<br><b>XII.</b>	Cumplir con las leyes, reglamentos y demás disposiciones legales aplicables.
							<br><b>XIII.</b>	Mantener actualizados nuestros registros para poder responder a sus consultas.
							<br><b>XIV.</b>	Promociones, recordatorios y citas de servicio, carta de bienvenida, información sobre campañas preventivas de servicio y/o de seguridad, citas de prueba de manejo, seguimiento de comentarios, consultas, quejas y reclamaciones; controles estadísticos, controles administrativos y encuestas.
							<br><b>XV.</b>	Para hacer válida la garantía de su vehículo.
							<br><b>XVI.</b>	Para dar seguimiento a solicitudes, consultas o reclamaciones, participación en actividades y dinámicas, así como, dar seguimiento a nuestra relación comercial.
							<br><b>B.</b>	No necesarias para la prestación el servicio:
							<br><b>1)</b>	Para ofrecer bienes y servicios.
							<br><b>2)</b>	Realizar estudios cuantitativos y cualitativos sobre hábitos de consumo, mercadotecnia, publicidad y prospección comercial relacionada con los bienes, productos y servicios, así como las inscripciones a programas de lealtad que ofrece el fabricante de los productos.
							<br><b>3)</b>	Dar seguimiento a nuestra relación comercial y solicitar la evaluación y el seguimiento para conocer el nivel de satisfacción de nuestros productos y servicios.
							<br><b>4)</b>	Análisis estadísticos / Fines estadísticos
							<br><b>5)</b>	Envió de formulario de encuestas para seguimiento de los servicios ofrecidos / Realizar encuestas / Realizar estudios de mercado
							<br><b>6)</b>	Análisis de información / Analizar hábitos de consumo
							<br><b>7)</b>	Contacto con el titular
							<br><b>8)</b>	Para realizar encuestas de satisfacción de bienes adquiridos y evaluar la calidad del servicio. 
							<br><b>9)</b>	Para invitarlo a eventos o enviar información sobre promocionales, productos y servicios adicionales que consideremos pueden ser de su interés.
							<br><b>10)</b>	Para contactarlo, enviarle notificaciones y uso de las diversas aplicaciones digitales administradas por nosotros y/o terceros que designemos; y que usted decida utilizar para mantenerse informado y/o dar seguimiento a sus adquisiciones de producto y servicio de postventa.';	
							break; //Kia Pedregal

							case '14': $html .='
							<b>A.</b>	Necesarias para la prestación del servicio: <br>
							<b>I.</b>	Para identificación.
							<br><b>II.</b>	Realizar los trámites, procedimientos y gestiones para la adquisición de bienes, productos y servicios para nuestros clientes y clientes potenciales.
							<br><b>III.</b>	Comercialización de vehículos automotores nuevos y seminuevos, así como toda clase de partes, refacciones y accesorios de vehículos automotores en sus diversas modalidades: contado, crédito y autofi¬nanciamiento.
							<br><b>IV.</b>	Otorgarle toda clase de bienes, productos y servicios para el mantenimiento de su automóvil (refacciones, hojalatería y pintura, servicios varios relacionados con vehículos automotores).
							<br><b>V.</b>	Realizar los diversos trámites para gestionar ante las autoridades competentes, el alta de los vehículos automotores en los padrones vehiculares correspondientes, así como llevar a cabo la verificación ambiental en su caso.
							<br><b>VI.</b>	Consultar el historial crediticio ante las sociedades de información crediticia.
							<br><b>VII.</b>	Cumplimiento de las obligaciones en materia de garantías en vehículos automotores nuevos, seminuevos y en servicio de reparación y/o mantenimiento.
							<br><b>VIII.</b>	Informar sobre las campañas de seguridad o de servicio de los vehículos automotores que promueva el fabricante de los productos (llamada/recall), así como de actualizaciones técnicas de los bienes o servicios que se hubiese adquirido.
							<br><b>IX.</b>	Tramitar y formalizar un crédito para la adquisición de un vehículo nuevo y/o seminuevo, así como la adquisición de seguros automotrices y extensiones de garantía conforme los contratos previamente establecidos por las partes.
							<br><b>X.</b>	Cumplir con todas las actividades complementarias y/o auxiliares necesarias para la comercialización de vehículos automotores nuevos, seminuevos y la prestación de servicios y productos para recordarle los servicios de manteni¬miento de su vehículo.
							<br><b>XI.</b>	Procesar solicitudes, realizar actividades de cobranza, facturación y aclaraciones.
							<br><b>XII.</b>	Cumplir con las leyes, reglamentos y demás disposiciones legales aplicables.
							<br><b>XIII.</b>	Mantener actualizados nuestros registros para poder responder a sus consultas.
							<br><b>XIV.</b>	Promociones, recordatorios y citas de servicio, carta de bienvenida, información sobre campañas preventivas de servicio y/o de seguridad, citas de prueba de manejo, seguimiento de comentarios, consultas, quejas y reclamaciones; controles estadísticos, controles administrativos y encuestas.
							<br><b>XV.</b>	Para hacer válida la garantía de su vehículo.
							<br><b>XVI.</b>	Para dar seguimiento a solicitudes, consultas o reclamaciones, participación en actividades y dinámicas, así como, dar seguimiento a nuestra relación comercial.
							<br><b>B.</b>	No necesarias para la prestación el servicio:
							<br><b>1)</b>	Para ofrecer bienes y servicios adicionales a los contratados.
							<br><b>2)</b>	Realizar estudios internos sobre hábitos de consumo, mercadotecnia, publicidad y prospección comercial relacionada con los bienes, productos y servicios, así como las inscripciones a programas de lealtad que ofrece el fabricante de los productos.
							<br><b>3)</b>	Dar seguimiento a nuestra relación comercial y solicitar la evaluación y el seguimiento para conocer el nivel de satisfacción de nuestros productos y servicios.
							<br><b>4)</b>	Análisis estadísticos / Fines estadísticos
							<br><b>5)</b>	Envió de formulario de encuestas para seguimiento de los servicios ofrecidos / Realizar encuestas / Realizar estudios de mercado
							<br><b>6)</b>	Análisis de información / Analizar hábitos de consumo
							<br><b>7)</b>	Contacto con el titular
							<br><b>8)</b>	Determinar la satisfacción / evaluar la calidad del servicio
							<br><b>9)</b>	Para invitarlo a eventos o enviar información sobre promocionales, productos y servicios adicionales que consideremos pueden ser de su interés.
							<br><b>10)</b>	Para contactarlo, enviarle notificaciones y uso de las diversas aplicaciones digitales administradas por nosotros y/o terceros que designemos; y que usted decida utilizar para mantenerse informado y/o dar seguimiento a sus adquisiciones de producto y servicio de postventa.';	
							break; //Kia Paricutin

							case '15': $html .='
							<b>A.</b>	Necesarias para la prestación del servicio: <br>
							<b>I.</b>	Para identificación.
							<br><b>II.</b>	Realizar los trámites, procedimientos y gestiones para la adquisición de bienes, productos y servicios para nuestros clientes y clientes potenciales.
							<br><b>III.</b>	Comercialización de vehículos automotores nuevos y seminuevos, así como toda clase de partes, refacciones y accesorios de vehículos automotores en sus diversas modalidades: contado, crédito y autofi¬nanciamiento.
							<br><b>IV.</b>	Otorgarle toda clase de bienes, productos y servicios para el mantenimiento de su automóvil (refacciones, hojalatería y pintura, servicios varios relacionados con vehículos automotores).
							<br><b>V.</b>	Realizar los diversos trámites para gestionar ante las autoridades competentes, el alta de los vehículos automotores en los padrones vehiculares correspondientes, así como llevar a cabo la verificación ambiental en su caso.
							<br><b>VI.</b>	Consultar el historial crediticio ante las sociedades de información crediticia.
							<br><b>VII.</b>	Cumplimiento de las obligaciones en materia de garantías en vehículos automotores nuevos, seminuevos y en servicio de reparación y/o mantenimiento.
							<br><b>VIII.</b>	Informar sobre las campañas de seguridad o de servicio de los vehículos automotores que promueva el fabricante de los productos (llamada/recall), así como de actualizaciones técnicas de los bienes o servicios que se hubiese adquirido.
							<br><b>IX.</b>	Tramitar y formalizar un crédito para la adquisición de un vehículo nuevo y/o seminuevo, así como la adquisición de seguros automotrices y extensiones de garantía conforme los contratos previamente establecidos por las partes.
							<br><b>X.</b>	Cumplir con todas las actividades complementarias y/o auxiliares necesarias para la comercialización de vehículos automotores nuevos, seminuevos y la prestación de servicios y productos para recordarle los servicios de manteni¬miento de su vehículo.
							<br><b>XI.</b>	Procesar solicitudes, realizar actividades de cobranza, facturación y aclaraciones.
							<br><b>XII.</b>	Cumplir con las leyes, reglamentos y demás disposiciones legales aplicables.
							<br><b>XIII.</b>	Mantener actualizados nuestros registros para poder responder a sus consultas.
							<br><b>XIV.</b>	Promociones, recordatorios y citas de servicio, carta de bienvenida, información sobre campañas preventivas de servicio y/o de seguridad, citas de prueba de manejo, seguimiento de comentarios, consultas, quejas y reclamaciones; controles estadísticos, controles administrativos y encuestas.
							<br><b>XV.</b>	Para hacer válida la garantía de su vehículo.
							<br><b>XVI.</b>	Para dar seguimiento a solicitudes, consultas o reclamaciones, participación en actividades y dinámicas, así como, dar seguimiento a nuestra relación comercial.
							<br><b>B.</b>	No necesarias para la prestación el servicio:
							<br><b>1)</b>	Para ofrecer bienes y servicios.
							<br><b>2)</b>	Realizar estudios cuantitativos y cualitativos sobre hábitos de consumo, mercadotecnia, publicidad y prospección comercial relacionada con los bienes, productos y servicios, así como las inscripciones a programas de lealtad que ofrece el fabricante de los productos.
							<br><b>3)</b>	Dar seguimiento a nuestra relación comercial y solicitar la evaluación y el seguimiento para conocer el nivel de satisfacción de nuestros productos y servicios.
							<br><b>4)</b>	Análisis estadísticos / Fines estadísticos
							<br><b>5)</b>	Envió de formulario de encuestas para seguimiento de los servicios ofrecidos / Realizar encuestas / Realizar estudios de mercado
							<br><b>6)</b>	Análisis de información / Analizar hábitos de consumo
							<br><b>7)</b>	Contacto con el titular
							<br><b>8)</b>	Para realizar encuestas de satisfacción de bienes adquiridos y evaluar la calidad del servicio. 
							<br><b>9)</b>	Para invitarlo a eventos o enviar información sobre promocionales, productos y servicios adicionales que consideremos pueden ser de su interés.
							<br><b>10)</b>	Para contactarlo, enviarle notificaciones y uso de las diversas aplicaciones digitales administradas por nosotros y/o terceros que designemos; y que usted decida utilizar para mantenerse informado y/o dar seguimiento a sus adquisiciones de producto y servicio de postventa.';	
							break; //Kia Mil Cumbres

							case '16': $html .='
							<b>A.</b>	Necesarias para la prestación del servicio: <br>
							<b>I.</b>	Para identificación.
							<br><b>II.</b>	Realizar los trámites, procedimientos y gestiones para la adquisición de bienes, productos y servicios para nuestros clientes y clientes potenciales.
							<br><b>III.</b>	Comercialización de vehículos automotores nuevos y seminuevos, así como toda clase de partes, refacciones y accesorios de vehículos automotores en sus diversas modalidades: contado, crédito y autofi¬nanciamiento.
							<br><b>IV.</b>	Otorgarle toda clase de bienes, productos y servicios para el mantenimiento de su automóvil (refacciones, hojalatería y pintura, servicios varios relacionados con vehículos automotores).
							<br><b>V.</b>	Realizar los diversos trámites para gestionar ante las autoridades competentes, el alta de los vehículos automotores en los padrones vehiculares correspondientes, así como llevar a cabo la verificación ambiental en su caso.
							<br><b>VI.</b>	Consultar el historial crediticio ante las sociedades de información crediticia.
							<br><b>VII.</b>	Cumplimiento de las obligaciones en materia de garantías en vehículos automotores nuevos, seminuevos y en servicio de reparación y/o mantenimiento.
							<br><b>VIII.</b>	Informar sobre las campañas de seguridad o de servicio de los vehículos automotores que promueva el fabricante de los productos (llamada/recall), así como de actualizaciones técnicas de los bienes o servicios que se hubiese adquirido.
							<br><b>IX.</b>	Tramitar y formalizar un crédito para la adquisición de un vehículo nuevo y/o seminuevo, así como la adquisición de seguros automotrices y extensiones de garantía conforme los contratos previamente establecidos por las partes.
							<br><b>X.</b>	Cumplir con todas las actividades complementarias y/o auxiliares necesarias para la comercialización de vehículos automotores nuevos, seminuevos y la prestación de servicios y productos para recordarle los servicios de manteni¬miento de su vehículo.
							<br><b>XI.</b>	Procesar solicitudes, realizar actividades de cobranza, facturación y aclaraciones.
							<br><b>XII.</b>	Cumplir con las leyes, reglamentos y demás disposiciones legales aplicables.
							<br><b>XIII.</b>	Mantener actualizados nuestros registros para poder responder a sus consultas.
							<br><b>XIV.</b>	Promociones, recordatorios y citas de servicio, carta de bienvenida, información sobre campañas preventivas de servicio y/o de seguridad, citas de prueba de manejo, seguimiento de comentarios, consultas, quejas y reclamaciones; controles estadísticos, controles administrativos y encuestas.
							<br><b>XV.</b>	Para hacer válida la garantía de su vehículo.
							<br><b>XVI.</b>	Para dar seguimiento a solicitudes, consultas o reclamaciones, participación en actividades y dinámicas, así como, dar seguimiento a nuestra relación comercial.
							<br><b>B.</b>	No necesarias para la prestación el servicio:
							<br><b>1)</b>	Para ofrecer bienes y servicios adicionales a los contratados.
							<br><b>2)</b>	Realizar estudios internos sobre hábitos de consumo, mercadotecnia, publicidad y prospección comercial relacionada con los bienes, productos y servicios, así como las inscripciones a programas de lealtad que ofrece el fabricante de los productos.
							<br><b>3)</b>	Dar seguimiento a nuestra relación comercial y solicitar la evaluación y el seguimiento para conocer el nivel de satisfacción de nuestros productos y servicios.
							<br><b>4)</b>	Análisis estadísticos / Fines estadísticos
							<br><b>5)</b>	Envió de formulario de encuestas para seguimiento de los servicios ofrecidos / Realizar encuestas / Realizar estudios de mercado
							<br><b>6)</b>	Análisis de información / Analizar hábitos de consumo
							<br><b>7)</b>	Contacto con el titular
							<br><b>8)</b>	Determinar la satisfacción / evaluar la calidad del servicio
							<br><b>9)</b>	Para invitarlo a eventos o enviar información sobre promocionales, productos y servicios adicionales que consideremos pueden ser de su interés.
							<br><b>10)</b>	Para contactarlo, enviarle notificaciones y uso de las diversas aplicaciones digitales administradas por nosotros y/o terceros que designemos; y que usted decida utilizar para mantenerse informado y/o dar seguimiento a sus adquisiciones de producto y servicio de postventa.';	
							break; //Kia del Duero

							case '17': $html .='
							<b>A.</b>	Necesarias para la prestación del servicio: <br>
							<b>I.</b>	Realizar los trámites, procedimientos y gestiones para la adquisición de bienes, productos y servicios para nuestros clientes y clientes potenciales.
							<br><b>II.</b>	Comercialización de vehículos automotores nuevos y seminuevos, así como toda clase de partes, refacciones y accesorios de vehículos automotores en sus diversas modalidades: contado, crédito y autofi¬nanciamiento.
							<br><b>III.</b>	Otorgarle toda clase de bienes, productos y servicios para el mantenimiento de su automóvil (refacciones, hojalatería y pintura, servicios varios relacionados con vehículos automotores).
							<br><b>IV.</b>	Realizar los diversos trámites para gestionar ante las autoridades competentes, el alta de los vehículos automotores en los padrones vehiculares correspondientes, así como llevar a cabo la verificación ambiental en su caso.
							<br><b>V.</b>	Consultar el historial crediticio ante las sociedades de información crediticia.
							<br><b>VI.</b>	Cumplimiento de las obligaciones en materia de garantías en vehículos automotores nuevos, seminuevos y en servicio de reparación y/o mantenimiento.
							<br><b>VII.</b>	Informar sobre las campañas de seguridad o de servicio de los vehículos automotores que promueva el fabricante de los productos (llamada/recall), así como de actualizaciones técnicas.
							<br><b>VIII.</b>	Tramitar y formalizar un crédito para la adquisición de un vehículo nuevo y/o seminuevo, así como la adquisición de seguros automotrices y extensiones de garantía conforme los contratos previamente establecidos por las partes.
							<br><b>IX.</b>	Cumplir con todas las actividades complementarias y/o auxiliares necesarias para la comercialización de vehículos automotores nuevos, seminuevos y la prestación de servicios y productos para el manteni¬miento de vehículos.
							<br><b>X.</b>	Procesar solicitudes, realizar actividades de cobranza, facturación y aclaraciones.
							<br><b>XI.</b>	Cumplir con las leyes, reglamentos y demás disposiciones legales aplicables.
							<br><b>XII.</b>	Mantener actualizados nuestros registros para poder responder a sus consultas.
							<br><b>XIII.</b>	Promociones, recordatorios y citas de servicio, carta de bienvenida, información sobre campañas preventivas de servicio y/o de seguridad, citas de prueba de manejo, seguimiento de comentarios, consultas, quejas y reclamaciones; controles estadísticos, controles administrativos y encuestas.
							<br><b>B.</b>	No necesarias para la prestación el servicio:
							<br><b>1)</b>	Para ofrecer bienes y servicios adicionales a los contratados.
							<br><b>2)</b>	Realizar estudios internos sobre hábitos de consumo, mercadotecnia, publicidad y prospección comercial relacionada con los bienes, productos y servicios, así como las inscripciones a programas de lealtad que ofrece el fabricante de los productos.
							<br><b>3)</b>	Dar seguimiento a nuestra relación comercial y solicitar la evaluación y el seguimiento para conocer el nivel de satisfacción de nuestros productos y servicios.
							<br><b>4)</b>	Análisis estadísticos / Fines estadísticos
							<br><b>5)</b>	Envió de formulario de encuestas para seguimiento de los servicios ofrecidos / Realizar encuestas / Realizar estudios de mercado
							<br><b>6)</b>	Análisis de información / Analizar hábitos de consumo
							<br><b>7)</b>	Contacto con el titular
							<br><b>8)</b>	Determinar la satisfacción / evaluar la calidad del servicio
							<br><b>9)</b>	Para invitarlo a eventos o enviar información sobre promocionales, productos y servicios adicionales que consideremos pueden ser de su interés.
							<br><b>10)</b>	Para contactarlo, enviarle notificaciones y uso de las diversas aplicaciones digitales administradas por nosotros y/o terceros que designemos; y que usted decida utilizar para mantenerse informado y/o dar seguimiento a sus adquisiciones de producto y servicio de postventa.';	
							break; //Chevrolet Morelia

							case '18': $html .='
							<b>A.</b>	Necesarias para la prestación del servicio: <br>
							<b>I.</b>	Realizar los trámites, procedimientos y gestiones para la adquisición de bienes, productos y servicios para nuestros clientes y clientes potenciales.
							<br><b>II.</b>	Comercialización de vehículos automotores nuevos y seminuevos, así como toda clase de partes, refacciones y accesorios de vehículos automotores en sus diversas modalidades: contado, crédito y autofi¬nanciamiento.
							<br><b>III.</b>	Otorgarle toda clase de bienes, productos y servicios para el mantenimiento de su automóvil (refacciones, hojalatería y pintura, servicios varios relacionados con vehículos automotores).
							<br><b>IV.</b>	Realizar los diversos trámites para gestionar ante las autoridades competentes, el alta de los vehículos automotores en los padrones vehiculares correspondientes, así como llevar a cabo la verificación ambiental en su caso.
							<br><b>V.</b>	Consultar el historial crediticio ante las sociedades de información crediticia.
							<br><b>VI.</b>	Cumplimiento de las obligaciones en materia de garantías en vehículos automotores nuevos, seminuevos y en servicio de reparación y/o mantenimiento.
							<br><b>VII.</b>	Informar sobre las campañas de seguridad o de servicio de los vehículos automotores que promueva el fabricante de los productos (llamada/recall), así como de actualizaciones técnicas.
							<br><b>VIII.</b>	Tramitar y formalizar un crédito para la adquisición de un vehículo nuevo y/o seminuevo, así como la adquisición de seguros automotrices y extensiones de garantía conforme los contratos previamente establecidos por las partes.
							<br><b>IX.</b>	Cumplir con todas las actividades complementarias y/o auxiliares necesarias para la comercialización de vehículos automotores nuevos, seminuevos y la prestación de servicios y productos para el manteni¬miento de vehículos.
							<br><b>X.</b>	Procesar solicitudes, realizar actividades de cobranza, facturación y aclaraciones.
							<br><b>XI.</b>	Cumplir con las leyes, reglamentos y demás disposiciones legales aplicables.
							<br><b>XII.</b>	Mantener actualizados nuestros registros para poder responder a sus consultas.
							<br><b>XIII.</b>	Promociones, recordatorios y citas de servicio, carta de bienvenida, información sobre campañas preventivas de servicio y/o de seguridad, citas de prueba de manejo, seguimiento de comentarios, consultas, quejas y reclamaciones; controles estadísticos, controles administrativos y encuestas.
							<br><b>B.</b>	No necesarias para la prestación el servicio:
							<br><b>1)</b>	Para ofrecer bienes y servicios adicionales a los contratados.
							<br><b>2)</b>	Realizar estudios internos sobre hábitos de consumo, mercadotecnia, publicidad y prospección comercial relacionada con los bienes, productos y servicios, así como las inscripciones a programas de lealtad que ofrece el fabricante de los productos.
							<br><b>3)</b>	Dar seguimiento a nuestra relación comercial y solicitar la evaluación y el seguimiento para conocer el nivel de satisfacción de nuestros productos y servicios.
							<br><b>4)</b>	Análisis estadísticos / Fines estadísticos
							<br><b>5)</b>	Envió de formulario de encuestas para seguimiento de los servicios ofrecidos / Realizar encuestas / Realizar estudios de mercado
							<br><b>6)</b>	Análisis de información / Analizar hábitos de consumo
							<br><b>7)</b>	Contacto con el titular
							<br><b>8)</b>	Determinar la satisfacción / evaluar la calidad del servicio
							<br><b>9)</b>	Para invitarlo a eventos o enviar información sobre promocionales, productos y servicios adicionales que consideremos pueden ser de su interés.
							<br><b>10)</b>	Para contactarlo, enviarle notificaciones y uso de las diversas aplicaciones digitales administradas por nosotros y/o terceros que designemos; y que usted decida utilizar para mantenerse informado y/o dar seguimiento a sus adquisiciones de producto y servicio de postventa.';	
							break; //Chevrolet Lazaro Cardenas

							case '19': $html .='
							<b>A.</b>	Necesarias para la prestación del servicio: <br>
							<b>I.</b>	Realizar los trámites, procedimientos y gestiones para la adquisición de bienes, productos y servicios para nuestros clientes y clientes potenciales.
							<br><b>II.</b>	Comercialización de vehículos automotores nuevos y seminuevos, así como toda clase de partes, refacciones y accesorios de vehículos automotores en sus diversas modalidades: contado, crédito y autofi¬nanciamiento.
							<br><b>III.</b>	Otorgarle toda clase de bienes, productos y servicios para el mantenimiento de su automóvil (refacciones, hojalatería y pintura, servicios varios relacionados con vehículos automotores).
							<br><b>IV.</b>	Realizar los diversos trámites para gestionar ante las autoridades competentes, el alta de los vehículos automotores en los padrones vehiculares correspondientes, así como llevar a cabo la verificación ambiental en su caso.
							<br><b>V.</b>	Consultar el historial crediticio ante las sociedades de información crediticia.
							<br><b>VI.</b>	Cumplimiento de las obligaciones en materia de garantías en vehículos automotores nuevos, seminuevos y en servicio de reparación y/o mantenimiento.
							<br><b>VII.</b>	Informar sobre las campañas de seguridad o de servicio de los vehículos automotores que promueva el fabricante de los productos (llamada/recall), así como de actualizaciones técnicas.
							<br><b>VIII.</b>	Tramitar y formalizar un crédito para la adquisición de un vehículo nuevo y/o seminuevo, así como la adquisición de seguros automotrices y extensiones de garantía conforme los contratos previamente establecidos por las partes.
							<br><b>IX.</b>	Cumplir con todas las actividades complementarias y/o auxiliares necesarias para la comercialización de vehículos automotores nuevos, seminuevos y la prestación de servicios y productos para el manteni¬miento de vehículos.
							<br><b>X.</b>	Procesar solicitudes, realizar actividades de cobranza, facturación y aclaraciones.
							<br><b>XI.</b>	Cumplir con las leyes, reglamentos y demás disposiciones legales aplicables.
							<br><b>XII.</b>	Mantener actualizados nuestros registros para poder responder a sus consultas.
							<br><b>XIII.</b>	Promociones, recordatorios y citas de servicio, carta de bienvenida, información sobre campañas preventivas de servicio y/o de seguridad, citas de prueba de manejo, seguimiento de comentarios, consultas, quejas y reclamaciones; controles estadísticos, controles administrativos y encuestas.
							<br><b>B.</b>	No necesarias para la prestación el servicio:
							<br><b>1)</b>	Para ofrecer bienes y servicios adicionales a los contratados.
							<br><b>2)</b>	Realizar estudios internos sobre hábitos de consumo, mercadotecnia, publicidad y prospección comercial relacionada con los bienes, productos y servicios, así como las inscripciones a programas de lealtad que ofrece el fabricante de los productos.
							<br><b>3)</b>	Dar seguimiento a nuestra relación comercial y solicitar la evaluación y el seguimiento para conocer el nivel de satisfacción de nuestros productos y servicios.
							<br><b>4)</b>	Análisis estadísticos / Fines estadísticos
							<br><b>5)</b>	Envió de formulario de encuestas para seguimiento de los servicios ofrecidos / Realizar encuestas / Realizar estudios de mercado
							<br><b>6)</b>	Análisis de información / Analizar hábitos de consumo
							<br><b>7)</b>	Contacto con el titular
							<br><b>8)</b>	Determinar la satisfacción / evaluar la calidad del servicio
							<br><b>9)</b>	Para invitarlo a eventos o enviar información sobre promocionales, productos y servicios adicionales que consideremos pueden ser de su interés.
							<br><b>10)</b>	Para contactarlo, enviarle notificaciones y uso de las diversas aplicaciones digitales administradas por nosotros y/o terceros que designemos; y que usted decida utilizar para mantenerse informado y/o dar seguimiento a sus adquisiciones de producto y servicio de postventa.';	
							break; //Chevrolet Uruapan

							case '20': $html .='
							<b>A.</b>	Necesarias para la prestación del servicio: <br>
							<b>I.</b>	Realizar los trámites, procedimientos y gestiones para la adquisición de bienes, productos y servicios para nuestros clientes y clientes potenciales.
							<br><b>II.</b>	Comercialización de vehículos automotores nuevos y seminuevos, así como toda clase de partes, refacciones y accesorios de vehículos automotores en sus diversas modalidades: contado, crédito y autofi¬nanciamiento.
							<br><b>III.</b>	Otorgarle toda clase de bienes, productos y servicios para el mantenimiento de su automóvil (refacciones, hojalatería y pintura, servicios varios relacionados con vehículos automotores).
							<br><b>IV.</b>	Realizar los diversos trámites para gestionar ante las autoridades competentes, el alta de los vehículos automotores en los padrones vehiculares correspondientes, así como llevar a cabo la verificación ambiental en su caso.
							<br><b>V.</b>	Consultar el historial crediticio ante las sociedades de información crediticia.
							<br><b>VI.</b>	Cumplimiento de las obligaciones en materia de garantías en vehículos automotores nuevos, seminuevos y en servicio de reparación y/o mantenimiento.
							<br><b>VII.</b>	Informar sobre las campañas de seguridad o de servicio de los vehículos automotores que promueva el fabricante de los productos (llamada/recall), así como de actualizaciones técnicas.
							<br><b>VIII.</b>	Tramitar y formalizar un crédito para la adquisición de un vehículo nuevo y/o seminuevo, así como la adquisición de seguros automotrices y extensiones de garantía conforme los contratos previamente establecidos por las partes.
							<br><b>IX.</b>	Cumplir con todas las actividades complementarias y/o auxiliares necesarias para la comercialización de vehículos automotores nuevos, seminuevos y la prestación de servicios y productos para el manteni¬miento de vehículos.
							<br><b>X.</b>	Procesar solicitudes, realizar actividades de cobranza, facturación y aclaraciones.
							<br><b>XI.</b>	Cumplir con las leyes, reglamentos y demás disposiciones legales aplicables.
							<br><b>XII.</b>	Mantener actualizados nuestros registros para poder responder a sus consultas.
							<br><b>XIII.</b>	Promociones, recordatorios y citas de servicio, carta de bienvenida, información sobre campañas preventivas de servicio y/o de seguridad, citas de prueba de manejo, seguimiento de comentarios, consultas, quejas y reclamaciones; controles estadísticos, controles administrativos y encuestas.
							<br><b>B.</b>	No necesarias para la prestación el servicio:
							<br><b>1)</b>	Para ofrecer bienes y servicios adicionales a los contratados.
							<br><b>2)</b>	Realizar estudios internos sobre hábitos de consumo, mercadotecnia, publicidad y prospección comercial relacionada con los bienes, productos y servicios, así como las inscripciones a programas de lealtad que ofrece el fabricante de los productos.
							<br><b>3)</b>	Dar seguimiento a nuestra relación comercial y solicitar la evaluación y el seguimiento para conocer el nivel de satisfacción de nuestros productos y servicios.
							<br><b>4)</b>	Análisis estadísticos / Fines estadísticos
							<br><b>5)</b>	Envió de formulario de encuestas para seguimiento de los servicios ofrecidos / Realizar encuestas / Realizar estudios de mercado
							<br><b>6)</b>	Análisis de información / Analizar hábitos de consumo
							<br><b>7)</b>	Contacto con el titular
							<br><b>8)</b>	Determinar la satisfacción / evaluar la calidad del servicio
							<br><b>9)</b>	Para invitarlo a eventos o enviar información sobre promocionales, productos y servicios adicionales que consideremos pueden ser de su interés.
							<br><b>10)</b>	Para contactarlo, enviarle notificaciones y uso de las diversas aplicaciones digitales administradas por nosotros y/o terceros que designemos; y que usted decida utilizar para mantenerse informado y/o dar seguimiento a sus adquisiciones de producto y servicio de postventa.';	
							break; //Chevrolet Apatzingan

							case '21': $html .='
							<b>A.</b>	Necesarias para la prestación del servicio: <br>
							<b>I.</b>	Realizar los trámites, procedimientos y gestiones para la adquisición de bienes, productos y servicios para nuestros clientes y clientes potenciales.
							<br><b>II.</b>	Comercialización de vehículos automotores nuevos y seminuevos, así como toda clase de partes, refacciones y accesorios de vehículos automotores en sus diversas modalidades: contado, crédito y autofi¬nanciamiento.
							<br><b>III.</b>	Otorgarle toda clase de bienes, productos y servicios para el mantenimiento de su automóvil (refacciones, hojalatería y pintura, servicios varios relacionados con vehículos automotores).
							<br><b>IV.</b>	Realizar los diversos trámites para gestionar ante las autoridades competentes, el alta de los vehículos automotores en los padrones vehiculares correspondientes, así como llevar a cabo la verificación ambiental en su caso.
							<br><b>V.</b>	Consultar el historial crediticio ante las sociedades de información crediticia.
							<br><b>VI.</b>	Cumplimiento de las obligaciones en materia de garantías en vehículos automotores nuevos, seminuevos y en servicio de reparación y/o mantenimiento.
							<br><b>VII.</b>	Informar sobre las campañas de seguridad o de servicio de los vehículos automotores que promueva el fabricante de los productos (llamada/recall), así como de actualizaciones técnicas.
							<br><b>VIII.</b>	Tramitar y formalizar un crédito para la adquisición de un vehículo nuevo y/o seminuevo, así como la adquisición de seguros automotrices y extensiones de garantía conforme los contratos previamente establecidos por las partes.
							<br><b>IX.</b>	Cumplir con todas las actividades complementarias y/o auxiliares necesarias para la comercialización de vehículos automotores nuevos, seminuevos y la prestación de servicios y productos para el manteni¬miento de vehículos.
							<br><b>X.</b>	Procesar solicitudes, realizar actividades de cobranza, facturación y aclaraciones.
							<br><b>XI.</b>	Cumplir con las leyes, reglamentos y demás disposiciones legales aplicables.
							<br><b>XII.</b>	Mantener actualizados nuestros registros para poder responder a sus consultas.
							<br><b>XIII.</b>	Promociones, recordatorios y citas de servicio, carta de bienvenida, información sobre campañas preventivas de servicio y/o de seguridad, citas de prueba de manejo, seguimiento de comentarios, consultas, quejas y reclamaciones; controles estadísticos, controles administrativos y encuestas.
							<br><b>B.</b>	No necesarias para la prestación el servicio:
							<br><b>1)</b>	Para ofrecer bienes y servicios adicionales a los contratados.
							<br><b>2)</b>	Realizar estudios internos sobre hábitos de consumo, mercadotecnia, publicidad y prospección comercial relacionada con los bienes, productos y servicios, así como las inscripciones a programas de lealtad que ofrece el fabricante de los productos.
							<br><b>3)</b>	Dar seguimiento a nuestra relación comercial y solicitar la evaluación y el seguimiento para conocer el nivel de satisfacción de nuestros productos y servicios.
							<br><b>4)</b>	Análisis estadísticos / Fines estadísticos
							<br><b>5)</b>	Envió de formulario de encuestas para seguimiento de los servicios ofrecidos / Realizar encuestas / Realizar estudios de mercado
							<br><b>6)</b>	Análisis de información / Analizar hábitos de consumo
							<br><b>7)</b>	Contacto con el titular
							<br><b>8)</b>	Determinar la satisfacción / evaluar la calidad del servicio
							<br><b>9)</b>	Para invitarlo a eventos o enviar información sobre promocionales, productos y servicios adicionales que consideremos pueden ser de su interés.
							<br><b>10)</b>	Para contactarlo, enviarle notificaciones y uso de las diversas aplicaciones digitales administradas por nosotros y/o terceros que designemos; y que usted decida utilizar para mantenerse informado y/o dar seguimiento a sus adquisiciones de producto y servicio de postventa.';	
							break; //Chevrolet Zamora

							case '22': $html .='
							<b>A.</b>	Necesarias para la prestación del servicio: <br>
							<b>I.</b>	Realizar los trámites, procedimientos y gestiones para la adquisición de bienes, productos y servicios para nuestros clientes y clientes potenciales.
							<br><b>II.</b>	Comercialización de vehículos automotores nuevos y seminuevos, así como toda clase de partes, refacciones y accesorios de vehículos automotores en sus diversas modalidades: contado, crédito y autofi¬nanciamiento.
							<br><b>III.</b>	Otorgarle toda clase de bienes, productos y servicios para el mantenimiento de su automóvil (refacciones, hojalatería y pintura, servicios varios relacionados con vehículos automotores).
							<br><b>IV.</b>	Realizar los diversos trámites para gestionar ante las autoridades competentes, el alta de los vehículos automotores en los padrones vehiculares correspondientes, así como llevar a cabo la verificación ambiental en su caso.
							<br><b>V.</b>	Consultar el historial crediticio ante las sociedades de información crediticia.
							<br><b>VI.</b>	Cumplimiento de las obligaciones en materia de garantías en vehículos automotores nuevos, seminuevos y en servicio de reparación y/o mantenimiento.
							<br><b>VII.</b>	Informar sobre las campañas de seguridad o de servicio de los vehículos automotores que promueva el fabricante de los productos (llamada/recall), así como de actualizaciones técnicas.
							<br><b>VIII.</b>	Tramitar y formalizar un crédito para la adquisición de un vehículo nuevo y/o seminuevo, así como la adquisición de seguros automotrices y extensiones de garantía conforme los contratos previamente establecidos por las partes.
							<br><b>IX.</b>	Cumplir con todas las actividades complementarias y/o auxiliares necesarias para la comercialización de vehículos automotores nuevos, seminuevos y la prestación de servicios y productos para el manteni¬miento de vehículos.
							<br><b>X.</b>	Procesar solicitudes, realizar actividades de cobranza, facturación y aclaraciones.
							<br><b>XI.</b>	Cumplir con las leyes, reglamentos y demás disposiciones legales aplicables.
							<br><b>XII.</b>	Mantener actualizados nuestros registros para poder responder a sus consultas.
							<br><b>XIII.</b>	Promociones, recordatorios y citas de servicio, carta de bienvenida, información sobre campañas preventivas de servicio y/o de seguridad, citas de prueba de manejo, seguimiento de comentarios, consultas, quejas y reclamaciones; controles estadísticos, controles administrativos y encuestas.
							<br><b>B.</b>	No necesarias para la prestación el servicio:
							<br><b>1)</b>	Para ofrecer bienes y servicios adicionales a los contratados.
							<br><b>2)</b>	Realizar estudios internos sobre hábitos de consumo, mercadotecnia, publicidad y prospección comercial relacionada con los bienes, productos y servicios, así como las inscripciones a programas de lealtad que ofrece el fabricante de los productos.
							<br><b>3)</b>	Dar seguimiento a nuestra relación comercial y solicitar la evaluación y el seguimiento para conocer el nivel de satisfacción de nuestros productos y servicios.
							<br><b>4)</b>	Análisis estadísticos / Fines estadísticos
							<br><b>5)</b>	Envió de formulario de encuestas para seguimiento de los servicios ofrecidos / Realizar encuestas / Realizar estudios de mercado
							<br><b>6)</b>	Análisis de información / Analizar hábitos de consumo
							<br><b>7)</b>	Contacto con el titular
							<br><b>8)</b>	Determinar la satisfacción / evaluar la calidad del servicio
							<br><b>9)</b>	Para invitarlo a eventos o enviar información sobre promocionales, productos y servicios adicionales que consideremos pueden ser de su interés.
							<br><b>10)</b>	Para contactarlo, enviarle notificaciones y uso de las diversas aplicaciones digitales administradas por nosotros y/o terceros que designemos; y que usted decida utilizar para mantenerse informado y/o dar seguimiento a sus adquisiciones de producto y servicio de postventa.';	
							break; //Toyota Uruapan

							case '23': $html .='
							<b>A.</b>	Necesarias para la prestación del servicio: <br>
							<b>I.</b>	Realizar los trámites, procedimientos y gestiones para la adquisición de bienes, productos y servicios para nuestros clientes y clientes potenciales.
							<br><b>II.</b>	Comercialización de vehículos automotores nuevos y seminuevos, así como toda clase de partes, refacciones y accesorios de vehículos automotores en sus diversas modalidades: contado, crédito y autofi¬nanciamiento.
							<br><b>III.</b>	Otorgarle toda clase de bienes, productos y servicios para el mantenimiento de su automóvil (refacciones, hojalatería y pintura, servicios varios relacionados con vehículos automotores).
							<br><b>IV.</b>	Realizar los diversos trámites para gestionar ante las autoridades competentes, el alta de los vehículos automotores en los padrones vehiculares correspondientes, así como llevar a cabo la verificación ambiental en su caso.
							<br><b>V.</b>	Consultar el historial crediticio ante las sociedades de información crediticia.
							<br><b>VI.</b>	Cumplimiento de las obligaciones en materia de garantías en vehículos automotores nuevos, seminuevos y en servicio de reparación y/o mantenimiento.
							<br><b>VII.</b>	Informar sobre las campañas de seguridad o de servicio de los vehículos automotores que promueva el fabricante de los productos (llamada/recall), así como de actualizaciones técnicas.
							<br><b>VIII.</b>	Tramitar y formalizar un crédito para la adquisición de un vehículo nuevo y/o seminuevo, así como la adquisición de seguros automotrices y extensiones de garantía conforme los contratos previamente establecidos por las partes.
							<br><b>IX.</b>	Cumplir con todas las actividades complementarias y/o auxiliares necesarias para la comercialización de vehículos automotores nuevos, seminuevos y la prestación de servicios y productos para el manteni¬miento de vehículos.
							<br><b>X.</b>	Procesar solicitudes, realizar actividades de cobranza, facturación y aclaraciones.
							<br><b>XI.</b>	Cumplir con las leyes, reglamentos y demás disposiciones legales aplicables.
							<br><b>XII.</b>	Mantener actualizados nuestros registros para poder responder a sus consultas.
							<br><b>XIII.</b>	Promociones, recordatorios y citas de servicio, carta de bienvenida, información sobre campañas preventivas de servicio y/o de seguridad, citas de prueba de manejo, seguimiento de comentarios, consultas, quejas y reclamaciones; controles estadísticos, controles administrativos y encuestas.
							<br><b>B.</b>	No necesarias para la prestación el servicio:
							<br><b>1)</b>	Para ofrecer bienes y servicios adicionales a los contratados.
							<br><b>2)</b>	Realizar estudios internos sobre hábitos de consumo, mercadotecnia, publicidad y prospección comercial relacionada con los bienes, productos y servicios, así como las inscripciones a programas de lealtad que ofrece el fabricante de los productos.
							<br><b>3)</b>	Dar seguimiento a nuestra relación comercial y solicitar la evaluación y el seguimiento para conocer el nivel de satisfacción de nuestros productos y servicios.
							<br><b>4)</b>	Análisis estadísticos / Fines estadísticos
							<br><b>5)</b>	Envió de formulario de encuestas para seguimiento de los servicios ofrecidos / Realizar encuestas / Realizar estudios de mercado
							<br><b>6)</b>	Análisis de información / Analizar hábitos de consumo
							<br><b>7)</b>	Contacto con el titular
							<br><b>8)</b>	Determinar la satisfacción / evaluar la calidad del servicio
							<br><b>9)</b>	Para invitarlo a eventos o enviar información sobre promocionales, productos y servicios adicionales que consideremos pueden ser de su interés.
							<br><b>10)</b>	Para contactarlo, enviarle notificaciones y uso de las diversas aplicaciones digitales administradas por nosotros y/o terceros que designemos; y que usted decida utilizar para mantenerse informado y/o dar seguimiento a sus adquisiciones de producto y servicio de postventa.';	
							break; //Toyota Altozano

							case '24': $html .='
							<b>A.</b>	Necesarias para la prestación del servicio: <br>
							<b>I.</b>	Realizar los trámites, procedimientos y gestiones para la adquisición de bienes, productos y servicios para nuestros clientes y clientes potenciales.
							<br><b>II.</b>	Comercialización de vehículos automotores nuevos y seminuevos, así como toda clase de partes, refacciones y accesorios de vehículos automotores en sus diversas modalidades: contado, crédito y autofi¬nanciamiento.
							<br><b>III.</b>	Otorgarle toda clase de bienes, productos y servicios para el mantenimiento de su automóvil (refacciones, hojalatería y pintura, servicios varios relacionados con vehículos automotores).
							<br><b>IV.</b>	Realizar los diversos trámites para gestionar ante las autoridades competentes, el alta de los vehículos automotores en los padrones vehiculares correspondientes, así como llevar a cabo la verificación ambiental en su caso.
							<br><b>V.</b>	Consultar el historial crediticio ante las sociedades de información crediticia.
							<br><b>VI.</b>	Cumplimiento de las obligaciones en materia de garantías en vehículos automotores nuevos, seminuevos y en servicio de reparación y/o mantenimiento.
							<br><b>VII.</b>	Informar sobre las campañas de seguridad o de servicio de los vehículos automotores que promueva el fabricante de los productos (llamada/recall), así como de actualizaciones técnicas.
							<br><b>VIII.</b>	Tramitar y formalizar un crédito para la adquisición de un vehículo nuevo y/o seminuevo, así como la adquisición de seguros automotrices y extensiones de garantía conforme los contratos previamente establecidos por las partes.
							<br><b>IX.</b>	Cumplir con todas las actividades complementarias y/o auxiliares necesarias para la comercialización de vehículos automotores nuevos, seminuevos y la prestación de servicios y productos para el manteni¬miento de vehículos.
							<br><b>X.</b>	Procesar solicitudes, realizar actividades de cobranza, facturación y aclaraciones.
							<br><b>XI.</b>	Cumplir con las leyes, reglamentos y demás disposiciones legales aplicables.
							<br><b>XII.</b>	Mantener actualizados nuestros registros para poder responder a sus consultas.
							<br><b>XIII.</b>	Promociones, recordatorios y citas de servicio, carta de bienvenida, información sobre campañas preventivas de servicio y/o de seguridad, citas de prueba de manejo, seguimiento de comentarios, consultas, quejas y reclamaciones; controles estadísticos, controles administrativos y encuestas.
							<br><b>B.</b>	No necesarias para la prestación el servicio:
							<br><b>1)</b>	Para ofrecer bienes y servicios adicionales a los contratados.
							<br><b>2)</b>	Realizar estudios internos sobre hábitos de consumo, mercadotecnia, publicidad y prospección comercial relacionada con los bienes, productos y servicios, así como las inscripciones a programas de lealtad que ofrece el fabricante de los productos.
							<br><b>3)</b>	Dar seguimiento a nuestra relación comercial y solicitar la evaluación y el seguimiento para conocer el nivel de satisfacción de nuestros productos y servicios.
							<br><b>4)</b>	Análisis estadísticos / Fines estadísticos
							<br><b>5)</b>	Envió de formulario de encuestas para seguimiento de los servicios ofrecidos / Realizar encuestas / Realizar estudios de mercado
							<br><b>6)</b>	Análisis de información / Analizar hábitos de consumo
							<br><b>7)</b>	Contacto con el titular
							<br><b>8)</b>	Determinar la satisfacción / evaluar la calidad del servicio
							<br><b>9)</b>	Para invitarlo a eventos o enviar información sobre promocionales, productos y servicios adicionales que consideremos pueden ser de su interés.
							<br><b>10)</b>	Para contactarlo, enviarle notificaciones y uso de las diversas aplicaciones digitales administradas por nosotros y/o terceros que designemos; y que usted decida utilizar para mantenerse informado y/o dar seguimiento a sus adquisiciones de producto y servicio de postventa.';	
							break; //Toyota Valladolid

							case '25': $html .='
							<b>A.</b>	Necesarias para la prestación del servicio: <br>
							<b>I.</b>	Realizar los trámites, procedimientos y gestiones para la adquisición de bienes, productos y servicios para nuestros clientes y clientes potenciales.
							<br><b>II.</b>	Comercialización de vehículos automotores nuevos y seminuevos, así como toda clase de partes, refacciones y accesorios de vehículos automotores en sus diversas modalidades: contado, crédito y autofi¬nanciamiento.
							<br><b>III.</b>	Otorgarle toda clase de bienes, productos y servicios para el mantenimiento de su automóvil (refacciones, hojalatería y pintura, servicios varios relacionados con vehículos automotores).
							<br><b>IV.</b>	Realizar los diversos trámites para gestionar ante las autoridades competentes, el alta de los vehículos automotores en los padrones vehiculares correspondientes, así como llevar a cabo la verificación ambiental en su caso.
							<br><b>V.</b>	Consultar el historial crediticio ante las sociedades de información crediticia.
							<br><b>VI.</b>	Cumplimiento de las obligaciones en materia de garantías en vehículos automotores nuevos, seminuevos y en servicio de reparación y/o mantenimiento.
							<br><b>VII.</b>	Informar sobre las campañas de seguridad o de servicio de los vehículos automotores que promueva el fabricante de los productos (llamada/recall), así como de actualizaciones técnicas.
							<br><b>VIII.</b>	Tramitar y formalizar un crédito para la adquisición de un vehículo nuevo y/o seminuevo, así como la adquisición de seguros automotrices y extensiones de garantía conforme los contratos previamente establecidos por las partes.
							<br><b>IX.</b>	Cumplir con todas las actividades complementarias y/o auxiliares necesarias para la comercialización de vehículos automotores nuevos, seminuevos y la prestación de servicios y productos para el manteni¬miento de vehículos.
							<br><b>X.</b>	Procesar solicitudes, realizar actividades de cobranza, facturación y aclaraciones.
							<br><b>XI.</b>	Cumplir con las leyes, reglamentos y demás disposiciones legales aplicables.
							<br><b>XII.</b>	Mantener actualizados nuestros registros para poder responder a sus consultas.
							<br><b>XIII.</b>	Promociones, recordatorios y citas de servicio, carta de bienvenida, información sobre campañas preventivas de servicio y/o de seguridad, citas de prueba de manejo, seguimiento de comentarios, consultas, quejas y reclamaciones; controles estadísticos, controles administrativos y encuestas.
							<br><b>B.</b>	No necesarias para la prestación el servicio:
							<br><b>1)</b>	Para ofrecer bienes y servicios adicionales a los contratados.
							<br><b>2)</b>	Realizar estudios cuantitativos y cualitativos sobre hábitos de consumo, mercadotecnia, publicidad y prospección comercial relacionada con los bienes, productos y servicios, así como las inscripciones a programas de lealtad que ofrece el fabricante de los productos.
							<br><b>3)</b>	Dar seguimiento a nuestra relación comercial y solicitar la evaluación y el seguimiento para conocer el nivel de satisfacción de nuestros productos y servicios.
							<br><b>4)</b>	Análisis estadísticos / Fines estadísticos
							<br><b>5)</b>	Envió de formulario de encuestas para seguimiento de los servicios ofrecidos / Realizar encuestas / Realizar estudios de mercado
							<br><b>6)</b>	Análisis de información / Analizar hábitos de consumo
							<br><b>7)</b>	Contacto con el titular
							<br><b>8)</b>	Para realizar encuestas de satisfacción de bienes adquiridos y evaluar la calidad del servicio. 
							<br><b>9)</b>	Para invitarlo a eventos o enviar información sobre promocionales, productos y servicios adicionales que consideremos pueden ser de su interés.
							<br><b>10)</b>	Para contactarlo, enviarle notificaciones y uso de las diversas aplicaciones digitales administradas por nosotros y/o terceros que designemos; y que usted decida utilizar para mantenerse informado y/o dar seguimiento a sus adquisiciones de producto y servicio de postventa.';	
							break; //Toyota Perisur

							case '26': $html .='
							<b>A.</b>	Necesarias para la prestación del servicio: <br>
							<b>I.</b>	Realizar los trámites, procedimientos y gestiones para la adquisición de bienes, productos y servicios para nuestros clientes y clientes potenciales.
							<br><b>II.</b>	Comercialización de vehículos automotores nuevos y seminuevos, así como toda clase de partes, refacciones y accesorios de vehículos automotores en sus diversas modalidades: contado, crédito y autofi¬nanciamiento.
							<br><b>III.</b>	Otorgarle toda clase de bienes, productos y servicios para el mantenimiento de su automóvil (refacciones, hojalatería y pintura, servicios varios relacionados con vehículos automotores).
							<br><b>IV.</b>	Realizar los diversos trámites para gestionar ante las autoridades competentes, el alta de los vehículos automotores en los padrones vehiculares correspondientes, así como llevar a cabo la verificación ambiental en su caso.
							<br><b>V.</b>	Consultar el historial crediticio ante las sociedades de información crediticia.
							<br><b>VI.</b>	Cumplimiento de las obligaciones en materia de garantías en vehículos automotores nuevos, seminuevos y en servicio de reparación y/o mantenimiento.
							<br><b>VII.</b>	Informar sobre las campañas de seguridad o de servicio de los vehículos automotores que promueva el fabricante de los productos (llamada/recall), así como de actualizaciones técnicas.
							<br><b>VIII.</b>	Tramitar y formalizar un crédito para la adquisición de un vehículo nuevo y/o seminuevo, así como la adquisición de seguros automotrices y extensiones de garantía conforme los contratos previamente establecidos por las partes.
							<br><b>IX.</b>	Cumplir con todas las actividades complementarias y/o auxiliares necesarias para la comercialización de vehículos automotores nuevos, seminuevos y la prestación de servicios y productos para el manteni¬miento de vehículos.
							<br><b>X.</b>	Procesar solicitudes, realizar actividades de cobranza, facturación y aclaraciones.
							<br><b>XI.</b>	Cumplir con las leyes, reglamentos y demás disposiciones legales aplicables.
							<br><b>XII.</b>	Mantener actualizados nuestros registros para poder responder a sus consultas.
							<br><b>XIII.</b>	Promociones, recordatorios y citas de servicio, carta de bienvenida, información sobre campañas preventivas de servicio y/o de seguridad, citas de prueba de manejo, seguimiento de comentarios, consultas, quejas y reclamaciones; controles estadísticos, controles administrativos y encuestas.
							<br><b>B.</b>	No necesarias para la prestación el servicio:
							<br><b>1)</b>	Para ofrecer bienes y servicios adicionales a los contratados.
							<br><b>2)</b>	Realizar estudios internos sobre hábitos de consumo, mercadotecnia, publicidad y prospección comercial relacionada con los bienes, productos y servicios, así como las inscripciones a programas de lealtad que ofrece el fabricante de los productos.
							<br><b>3)</b>	Dar seguimiento a nuestra relación comercial y solicitar la evaluación y el seguimiento para conocer el nivel de satisfacción de nuestros productos y servicios.
							<br><b>4)</b>	Análisis estadísticos / Fines estadísticos
							<br><b>5)</b>	Envió de formulario de encuestas para seguimiento de los servicios ofrecidos / Realizar encuestas / Realizar estudios de mercado
							<br><b>6)</b>	Análisis de información / Analizar hábitos de consumo
							<br><b>7)</b>	Contacto con el titular
							<br><b>8)</b>	Determinar la satisfacción / evaluar la calidad del servicio
							<br><b>9)</b>	Para invitarlo a eventos o enviar información sobre promocionales, productos y servicios adicionales que consideremos pueden ser de su interés.
							<br><b>10)</b>	Para contactarlo, enviarle notificaciones y uso de las diversas aplicaciones digitales administradas por nosotros y/o terceros que designemos; y que usted decida utilizar para mantenerse informado y/o dar seguimiento a sus adquisiciones de producto y servicio de postventa.';	
							break; //Mitsubishi Queretaro

							case '27': $html .='
							<b>A.</b>	Necesarias para la prestación del servicio: <br>
							<b>I.</b>	Realizar los trámites, procedimientos y gestiones para la adquisición de bienes, productos y servicios para nuestros clientes y clientes potenciales.
							<br><b>II.</b>	Comercialización de vehículos automotores nuevos y seminuevos, así como toda clase de partes, refacciones y accesorios de vehículos automotores en sus diversas modalidades: contado, crédito y autofi¬nanciamiento.
							<br><b>III.</b>	Otorgarle toda clase de bienes, productos y servicios para el mantenimiento de su automóvil (refacciones, hojalatería y pintura, servicios varios relacionados con vehículos automotores).
							<br><b>IV.</b>	Realizar los diversos trámites para gestionar ante las autoridades competentes, el alta de los vehículos automotores en los padrones vehiculares correspondientes, así como llevar a cabo la verificación ambiental en su caso.
							<br><b>V.</b>	Consultar el historial crediticio ante las sociedades de información crediticia.
							<br><b>VI.</b>	Cumplimiento de las obligaciones en materia de garantías en vehículos automotores nuevos, seminuevos y en servicio de reparación y/o mantenimiento.
							<br><b>VII.</b>	Informar sobre las campañas de seguridad o de servicio de los vehículos automotores que promueva el fabricante de los productos (llamada/recall), así como de actualizaciones técnicas.
							<br><b>VIII.</b>	Tramitar y formalizar un crédito para la adquisición de un vehículo nuevo y/o seminuevo, así como la adquisición de seguros automotrices y extensiones de garantía conforme los contratos previamente establecidos por las partes.
							<br><b>IX.</b>	Cumplir con todas las actividades complementarias y/o auxiliares necesarias para la comercialización de vehículos automotores nuevos, seminuevos y la prestación de servicios y productos para el manteni¬miento de vehículos.
							<br><b>X.</b>	Procesar solicitudes, realizar actividades de cobranza, facturación y aclaraciones.
							<br><b>XI.</b>	Cumplir con las leyes, reglamentos y demás disposiciones legales aplicables.
							<br><b>XII.</b>	Mantener actualizados nuestros registros para poder responder a sus consultas.
							<br><b>XIII.</b>	Promociones, recordatorios y citas de servicio, carta de bienvenida, información sobre campañas preventivas de servicio y/o de seguridad, citas de prueba de manejo, seguimiento de comentarios, consultas, quejas y reclamaciones; controles estadísticos, controles administrativos y encuestas.
							<br><b>B.</b>	No necesarias para la prestación el servicio:
							<br><b>1)</b>	Para ofrecer bienes y servicios adicionales a los contratados.
							<br><b>2)</b>	Realizar estudios cuantitativos y cualitativos sobre hábitos de consumo, mercadotecnia, publicidad y prospección comercial relacionada con los bienes, productos y servicios, así como las inscripciones a programas de lealtad que ofrece el fabricante de los productos.
							<br><b>3)</b>	Dar seguimiento a nuestra relación comercial y solicitar la evaluación y el seguimiento para conocer el nivel de satisfacción de nuestros productos y servicios.
							<br><b>4)</b>	Análisis estadísticos / Fines estadísticos
							<br><b>5)</b>	Envió de formulario de encuestas para seguimiento de los servicios ofrecidos / Realizar encuestas / Realizar estudios de mercado
							<br><b>6)</b>	Análisis de información / Analizar hábitos de consumo
							<br><b>7)</b>	Contacto con el titular
							<br><b>8)</b>	Para realizar encuestas de satisfacción de bienes adquiridos y evaluar la calidad del servicio. 
							<br><b>9)</b>	Para invitarlo a eventos o enviar información sobre promocionales, productos y servicios adicionales que consideremos pueden ser de su interés.
							<br><b>10)</b>	Para contactarlo, enviarle notificaciones y uso de las diversas aplicaciones digitales administradas por nosotros y/o terceros que designemos; y que usted decida utilizar para mantenerse informado y/o dar seguimiento a sus adquisiciones de producto y servicio de postventa. '; 
							break; //Mitsubishi Uruapan

							case '28': $html .='
							<b>A.</b>	Necesarias para la prestación del servicio: <br>
							<b>I.</b>	Realizar los trámites, procedimientos y gestiones para la adquisición de bienes, productos y servicios para nuestros clientes y clientes potenciales.
							<br><b>II.</b>	Comercialización de vehículos automotores nuevos y seminuevos, así como toda clase de partes, refacciones y accesorios de vehículos automotores en sus diversas modalidades: contado, crédito y autofi¬nanciamiento.
							<br><b>III.</b>	Otorgarle toda clase de bienes, productos y servicios para el mantenimiento de su automóvil (refacciones, hojalatería y pintura, servicios varios relacionados con vehículos automotores).
							<br><b>IV.</b>	Realizar los diversos trámites para gestionar ante las autoridades competentes, el alta de los vehículos automotores en los padrones vehiculares correspondientes, así como llevar a cabo la verificación ambiental en su caso.
							<br><b>V.</b>	Consultar el historial crediticio ante las sociedades de información crediticia.
							<br><b>VI.</b>	Cumplimiento de las obligaciones en materia de garantías en vehículos automotores nuevos, seminuevos y en servicio de reparación y/o mantenimiento.
							<br><b>VII.</b>	Informar sobre las campañas de seguridad o de servicio de los vehículos automotores que promueva el fabricante de los productos (llamada/recall), así como de actualizaciones técnicas.
							<br><b>VIII.</b>	Tramitar y formalizar un crédito para la adquisición de un vehículo nuevo y/o seminuevo, así como la adquisición de seguros automotrices y extensiones de garantía conforme los contratos previamente establecidos por las partes.
							<br><b>IX.</b>	Cumplir con todas las actividades complementarias y/o auxiliares necesarias para la comercialización de vehículos automotores nuevos, seminuevos y la prestación de servicios y productos para el manteni¬miento de vehículos.
							<br><b>X.</b>	Procesar solicitudes, realizar actividades de cobranza, facturación y aclaraciones.
							<br><b>XI.</b>	Cumplir con las leyes, reglamentos y demás disposiciones legales aplicables.
							<br><b>XII.</b>	Mantener actualizados nuestros registros para poder responder a sus consultas.
							<br><b>XIII.</b>	Promociones, recordatorios y citas de servicio, carta de bienvenida, información sobre campañas preventivas de servicio y/o de seguridad, citas de prueba de manejo, seguimiento de comentarios, consultas, quejas y reclamaciones; controles estadísticos, controles administrativos y encuestas.
							<br><b>B.</b>	No necesarias para la prestación el servicio:
							<br><b>1)</b>	Para ofrecer bienes y servicios adicionales a los contratados.
							<br><b>2)</b>	Realizar estudios internos sobre hábitos de consumo, mercadotecnia, publicidad y prospección comercial relacionada con los bienes, productos y servicios, así como las inscripciones a programas de lealtad que ofrece el fabricante de los productos.
							<br><b>3)</b>	Dar seguimiento a nuestra relación comercial y solicitar la evaluación y el seguimiento para conocer el nivel de satisfacción de nuestros productos y servicios.
							<br><b>4)</b>	Análisis estadísticos / Fines estadísticos
							<br><b>5)</b>	Envió de formulario de encuestas para seguimiento de los servicios ofrecidos / Realizar encuestas / Realizar estudios de mercado
							<br><b>6)</b>	Análisis de información / Analizar hábitos de consumo
							<br><b>7)</b>	Contacto con el titular
							<br><b>8)</b>	Determinar la satisfacción / evaluar la calidad del servicio
							<br><b>9)</b>	Para invitarlo a eventos o enviar información sobre promocionales, productos y servicios adicionales que consideremos pueden ser de su interés.
							<br><b>10)</b>	Para contactarlo, enviarle notificaciones y uso de las diversas aplicaciones digitales administradas por nosotros y/o terceros que designemos; y que usted decida utilizar para mantenerse informado y/o dar seguimiento a sus adquisiciones de producto y servicio de postventa.';	
							break; //GMC Morelia

							case '29': $html .='
							<b>A.</b>	Necesarias para la prestación del servicio: <br>
							<b>I.</b>	Realizar los trámites, procedimientos y gestiones para la adquisición de bienes, productos y servicios para nuestros clientes y clientes potenciales.
							<br><b>II.</b>	Comercialización de vehículos automotores nuevos y seminuevos, así como toda clase de partes, refacciones y accesorios de vehículos automotores en sus diversas modalidades: contado, crédito y autofi¬nanciamiento.
							<br><b>III.</b>	Otorgarle toda clase de bienes, productos y servicios para el mantenimiento de su automóvil (refacciones, hojalatería y pintura, servicios varios relacionados con vehículos automotores).
							<br><b>IV.</b>	Realizar los diversos trámites para gestionar ante las autoridades competentes, el alta de los vehículos automotores en los padrones vehiculares correspondientes, así como llevar a cabo la verificación ambiental en su caso.
							<br><b>V.</b>	Consultar el historial crediticio ante las sociedades de información crediticia.
							<br><b>VI.</b>	Cumplimiento de las obligaciones en materia de garantías en vehículos automotores nuevos, seminuevos y en servicio de reparación y/o mantenimiento.
							<br><b>VII.</b>	Informar sobre las campañas de seguridad o de servicio de los vehículos automotores que promueva el fabricante de los productos (llamada/recall), así como de actualizaciones técnicas.
							<br><b>VIII.</b>	Tramitar y formalizar un crédito para la adquisición de un vehículo nuevo y/o seminuevo, así como la adquisición de seguros automotrices y extensiones de garantía conforme los contratos previamente establecidos por las partes.
							<br><b>IX.</b>	Cumplir con todas las actividades complementarias y/o auxiliares necesarias para la comercialización de vehículos automotores nuevos, seminuevos y la prestación de servicios y productos para el manteni¬miento de vehículos.
							<br><b>X.</b>	Procesar solicitudes, realizar actividades de cobranza, facturación y aclaraciones.
							<br><b>XI.</b>	Cumplir con las leyes, reglamentos y demás disposiciones legales aplicables.
							<br><b>XII.</b>	Mantener actualizados nuestros registros para poder responder a sus consultas.
							<br><b>XIII.</b>	Promociones, recordatorios y citas de servicio, carta de bienvenida, información sobre campañas preventivas de servicio y/o de seguridad, citas de prueba de manejo, seguimiento de comentarios, consultas, quejas y reclamaciones; controles estadísticos, controles administrativos y encuestas.
							<br><b>B.</b>	No necesarias para la prestación el servicio:
							<br><b>1)</b>	Para ofrecer bienes y servicios adicionales a los contratados.
							<br><b>2)</b>	Realizar estudios internos sobre hábitos de consumo, mercadotecnia, publicidad y prospección comercial relacionada con los bienes, productos y servicios, así como las inscripciones a programas de lealtad que ofrece el fabricante de los productos.
							<br><b>3)</b>	Dar seguimiento a nuestra relación comercial y solicitar la evaluación y el seguimiento para conocer el nivel de satisfacción de nuestros productos y servicios.
							<br><b>4)</b>	Análisis estadísticos / Fines estadísticos
							<br><b>5)</b>	Envió de formulario de encuestas para seguimiento de los servicios ofrecidos / Realizar encuestas / Realizar estudios de mercado
							<br><b>6)</b>	Análisis de información / Analizar hábitos de consumo
							<br><b>7)</b>	Contacto con el titular
							<br><b>8)</b>	Determinar la satisfacción / evaluar la calidad del servicio
							<br><b>9)</b>	Para invitarlo a eventos o enviar información sobre promocionales, productos y servicios adicionales que consideremos pueden ser de su interés.
							<br><b>10)</b>	Para contactarlo, enviarle notificaciones y uso de las diversas aplicaciones digitales administradas por nosotros y/o terceros que designemos; y que usted decida utilizar para mantenerse informado y/o dar seguimiento a sus adquisiciones de producto y servicio de postventa.';	
							break; //GMC Uruapan

							case '30': $html .='
							<b>A.</b>	Necesarias para la prestación del servicio: <br>
							<b>I.</b>	Realizar los trámites, procedimientos y gestiones para la adquisición de bienes, productos y servicios para nuestros clientes y clientes potenciales.
							<br><b>II.</b>	Comercialización de vehículos automotores nuevos y seminuevos, así como toda clase de partes, refacciones y accesorios de vehículos automotores en sus diversas modalidades: contado, crédito y autofi¬nanciamiento.
							<br><b>III.</b>	Otorgarle toda clase de bienes, productos y servicios para el mantenimiento de su automóvil (refacciones, hojalatería y pintura, servicios varios relacionados con vehículos automotores).
							<br><b>IV.</b>	Realizar los diversos trámites para gestionar ante las autoridades competentes, el alta de los vehículos automotores en los padrones vehiculares correspondientes, así como llevar a cabo la verificación ambiental en su caso.
							<br><b>V.</b>	Consultar el historial crediticio ante las sociedades de información crediticia.
							<br><b>VI.</b>	Cumplimiento de las obligaciones en materia de garantías en vehículos automotores nuevos, seminuevos y en servicio de reparación y/o mantenimiento.
							<br><b>VII.</b>	Informar sobre las campañas de seguridad o de servicio de los vehículos automotores que promueva el fabricante de los productos (llamada/recall), así como de actualizaciones técnicas.
							<br><b>VIII.</b>	Tramitar y formalizar un crédito para la adquisición de un vehículo nuevo y/o seminuevo, así como la adquisición de seguros automotrices y extensiones de garantía conforme los contratos previamente establecidos por las partes.
							<br><b>IX.</b>	Cumplir con todas las actividades complementarias y/o auxiliares necesarias para la comercialización de vehículos automotores nuevos, seminuevos y la prestación de servicios y productos para el manteni¬miento de vehículos.
							<br><b>X.</b>	Procesar solicitudes, realizar actividades de cobranza, facturación y aclaraciones.
							<br><b>XI.</b>	Cumplir con las leyes, reglamentos y demás disposiciones legales aplicables.
							<br><b>XII.</b>	Mantener actualizados nuestros registros para poder responder a sus consultas.
							<br><b>XIII.</b>	Promociones, recordatorios y citas de servicio, carta de bienvenida, información sobre campañas preventivas de servicio y/o de seguridad, citas de prueba de manejo, seguimiento de comentarios, consultas, quejas y reclamaciones; controles estadísticos, controles administrativos y encuestas.
							<br><b>B.</b>	No necesarias para la prestación el servicio:
							<br><b>1)</b>	Para ofrecer bienes y servicios adicionales a los contratados.
							<br><b>2)</b>	Realizar estudios cuantitativos y cualitativos sobre hábitos de consumo, mercadotecnia, publicidad y prospección comercial relacionada con los bienes, productos y servicios, así como las inscripciones a programas de lealtad que ofrece el fabricante de los productos.
							<br><b>3)</b>	Dar seguimiento a nuestra relación comercial y solicitar la evaluación y el seguimiento para conocer el nivel de satisfacción de nuestros productos y servicios.
							<br><b>4)</b>	Análisis estadísticos / Fines estadísticos
							<br><b>5)</b>	Envió de formulario de encuestas para seguimiento de los servicios ofrecidos / Realizar encuestas / Realizar estudios de mercado
							<br><b>6)</b>	Análisis de información / Analizar hábitos de consumo
							<br><b>7)</b>	Contacto con el titular
							<br><b>8)</b>	Para realizar encuestas de satisfacción de bienes adquiridos y evaluar la calidad del servicio. 
							<br><b>9)</b>	Para invitarlo a eventos o enviar información sobre promocionales, productos y servicios adicionales que consideremos pueden ser de su interés.
							<br><b>10)</b>	Para contactarlo, enviarle notificaciones y uso de las diversas aplicaciones digitales administradas por nosotros y/o terceros que designemos; y que usted decida utilizar para mantenerse informado y/o dar seguimiento a sus adquisiciones de producto y servicio de postventa.';	
							break; //BMW Talisman

							case '31': $html .='
							<b>A.</b>	Necesarias para la prestación del servicio: <br>
							<b>I.</b>	Realizar los trámites, procedimientos y gestiones para la adquisición de bienes, productos y servicios para nuestros clientes y clientes potenciales.
							<br><b>II.</b>	Comercialización de vehículos automotores nuevos y seminuevos, así como toda clase de partes, refacciones y accesorios de vehículos automotores en sus diversas modalidades: contado, crédito y autofi¬nanciamiento.
							<br><b>III.</b>	Otorgarle toda clase de bienes, productos y servicios para el mantenimiento de su automóvil (refacciones, hojalatería y pintura, servicios varios relacionados con vehículos automotores).
							<br><b>IV.</b>	Realizar los diversos trámites para gestionar ante las autoridades competentes, el alta de los vehículos automotores en los padrones vehiculares correspondientes, así como llevar a cabo la verificación ambiental en su caso.
							<br><b>V.</b>	Consultar el historial crediticio ante las sociedades de información crediticia.
							<br><b>VI.</b>	Cumplimiento de las obligaciones en materia de garantías en vehículos automotores nuevos, seminuevos y en servicio de reparación y/o mantenimiento.
							<br><b>VII.</b>	Informar sobre las campañas de seguridad o de servicio de los vehículos automotores que promueva el fabricante de los productos (llamada/recall), así como de actualizaciones técnicas.
							<br><b>VIII.</b>	Tramitar y formalizar un crédito para la adquisición de un vehículo nuevo y/o seminuevo, así como la adquisición de seguros automotrices y extensiones de garantía conforme los contratos previamente establecidos por las partes.
							<br><b>IX.</b>	Cumplir con todas las actividades complementarias y/o auxiliares necesarias para la comercialización de vehículos automotores nuevos, seminuevos y la prestación de servicios y productos para el manteni¬miento de vehículos.
							<br><b>X.</b>	Procesar solicitudes, realizar actividades de cobranza, facturación y aclaraciones.
							<br><b>XI.</b>	Cumplir con las leyes, reglamentos y demás disposiciones legales aplicables.
							<br><b>XII.</b>	Mantener actualizados nuestros registros para poder responder a sus consultas.
							<br><b>XIII.</b>	Promociones, recordatorios y citas de servicio, carta de bienvenida, información sobre campañas preventivas de servicio y/o de seguridad, citas de prueba de manejo, seguimiento de comentarios, consultas, quejas y reclamaciones; controles estadísticos, controles administrativos y encuestas.
							<br><b>B.</b>	No necesarias para la prestación el servicio:
							<br><b>1)</b>	Para ofrecer bienes y servicios adicionales a los contratados.
							<br><b>2)</b>	Realizar estudios cuantitativos y cualitativos sobre hábitos de consumo, mercadotecnia, publicidad y prospección comercial relacionada con los bienes, productos y servicios, así como las inscripciones a programas de lealtad que ofrece el fabricante de los productos.
							<br><b>3)</b>	Dar seguimiento a nuestra relación comercial y solicitar la evaluación y el seguimiento para conocer el nivel de satisfacción de nuestros productos y servicios.
							<br><b>4)</b>	Análisis estadísticos / Fines estadísticos
							<br><b>5)</b>	Envió de formulario de encuestas para seguimiento de los servicios ofrecidos / Realizar encuestas / Realizar estudios de mercado
							<br><b>6)</b>	Análisis de información / Analizar hábitos de consumo
							<br><b>7)</b>	Contacto con el titular
							<br><b>8)</b>	Para realizar encuestas de satisfacción de bienes adquiridos y evaluar la calidad del servicio. 
							<br><b>9)</b>	Para invitarlo a eventos o enviar información sobre promocionales, productos y servicios adicionales que consideremos pueden ser de su interés.
							<br><b>10)</b>	Para contactarlo, enviarle notificaciones y uso de las diversas aplicaciones digitales administradas por nosotros y/o terceros que designemos; y que usted decida utilizar para mantenerse informado y/o dar seguimiento a sus adquisiciones de producto y servicio de postventa.';	
							break; //Motorrad Morelia

							case '32': $html .='
							<b>A.</b>	Necesarias para la prestación del servicio: <br>
							<b>I.</b>	Realizar los trámites, procedimientos y gestiones para la adquisición de bienes, productos y servicios para nuestros clientes y clientes potenciales.
							<br><b>II.</b>	Comercialización de vehículos automotores nuevos y seminuevos, así como toda clase de partes, refacciones y accesorios de vehículos automotores en sus diversas modalidades: contado, crédito y autofi¬nanciamiento.
							<br><b>III.</b>	Otorgarle toda clase de bienes, productos y servicios para el mantenimiento de su automóvil (refacciones, hojalatería y pintura, servicios varios relacionados con vehículos automotores).
							<br><b>IV.</b>	Realizar los diversos trámites para gestionar ante las autoridades competentes, el alta de los vehículos automotores en los padrones vehiculares correspondientes, así como llevar a cabo la verificación ambiental en su caso.
							<br><b>V.</b>	Consultar el historial crediticio ante las sociedades de información crediticia.
							<br><b>VI.</b>	Cumplimiento de las obligaciones en materia de garantías en vehículos automotores nuevos, seminuevos y en servicio de reparación y/o mantenimiento.
							<br><b>VII.</b>	Informar sobre las campañas de seguridad o de servicio de los vehículos automotores que promueva el fabricante de los productos (llamada/recall), así como de actualizaciones técnicas.
							<br><b>VIII.</b>	Tramitar y formalizar un crédito para la adquisición de un vehículo nuevo y/o seminuevo, así como la adquisición de seguros automotrices y extensiones de garantía conforme los contratos previamente establecidos por las partes.
							<br><b>IX.</b>	Cumplir con todas las actividades complementarias y/o auxiliares necesarias para la comercialización de vehículos automotores nuevos, seminuevos y la prestación de servicios y productos para el manteni¬miento de vehículos.
							<br><b>X.</b>	Procesar solicitudes, realizar actividades de cobranza, facturación y aclaraciones.
							<br><b>XI.</b>	Cumplir con las leyes, reglamentos y demás disposiciones legales aplicables.
							<br><b>XII.</b>	Mantener actualizados nuestros registros para poder responder a sus consultas.
							<br><b>XIII.</b>	Promociones, recordatorios y citas de servicio, carta de bienvenida, información sobre campañas preventivas de servicio y/o de seguridad, citas de prueba de manejo, seguimiento de comentarios, consultas, quejas y reclamaciones; controles estadísticos, controles administrativos y encuestas.
							<br><b>B.</b>	No necesarias para la prestación el servicio:
							<br><b>1)</b>	Para ofrecer bienes y servicios adicionales a los contratados.
							<br><b>2)</b>	Realizar estudios cuantitativos y cualitativos sobre hábitos de consumo, mercadotecnia, publicidad y prospección comercial relacionada con los bienes, productos y servicios, así como las inscripciones a programas de lealtad que ofrece el fabricante de los productos.
							<br><b>3)</b>	Dar seguimiento a nuestra relación comercial y solicitar la evaluación y el seguimiento para conocer el nivel de satisfacción de nuestros productos y servicios.
							<br><b>4)</b>	Análisis estadísticos / Fines estadísticos
							<br><b>5)</b>	Envió de formulario de encuestas para seguimiento de los servicios ofrecidos / Realizar encuestas / Realizar estudios de mercado
							<br><b>6)</b>	Análisis de información / Analizar hábitos de consumo
							<br><b>7)</b>	Contacto con el titular
							<br><b>8)</b>	Para realizar encuestas de satisfacción de bienes adquiridos y evaluar la calidad del servicio. 
							<br><b>9)</b>	Para invitarlo a eventos o enviar información sobre promocionales, productos y servicios adicionales que consideremos pueden ser de su interés.
							<br><b>10)</b>	Para contactarlo, enviarle notificaciones y uso de las diversas aplicaciones digitales administradas por nosotros y/o terceros que designemos; y que usted decida utilizar para mantenerse informado y/o dar seguimiento a sus adquisiciones de producto y servicio de postventa. ';	
							break; //Mini Talisman

							case '33': $html .='';	
							break; //Chrysler Queretaro

							case '34': $html .='';	
							break; //Chrysler Uruapan

							case '35': $html .='';	
							break; //Audi Juriquilla

							case '36': $html .='					
							<b>A.</b>	Necesarias para la prestación del servicio: <br>
							<b>I.</b>	Realizar los trámites, procedimientos y gestiones para la adquisición de bienes, productos y servicios para nuestros clientes y clientes potenciales.
							<br><b>II.</b>	Comercialización de vehículos automotores nuevos y seminuevos, así como toda clase de partes, refacciones y accesorios de vehículos automotores en sus diversas modalidades: contado, crédito y autofi¬nanciamiento.
							<br><b>III.</b>	Otorgarle toda clase de bienes, productos y servicios para el mantenimiento de su automóvil (refacciones, hojalatería y pintura, servicios varios relacionados con vehículos automotores).
							<br><b>IV.</b>	Realizar los diversos trámites para gestionar ante las autoridades competentes, el alta de los vehículos automotores en los padrones vehiculares correspondientes, así como llevar a cabo la verificación ambiental en su caso.
							<br><b>V.</b>	Consultar el historial crediticio ante las sociedades de información crediticia.
							<br><b>VI.</b>	Cumplimiento de las obligaciones en materia de garantías en vehículos automotores nuevos, seminuevos y en servicio de reparación y/o mantenimiento.
							<br><b>VII.</b>	Informar sobre las campañas de seguridad o de servicio de los vehículos automotores que promueva el fabricante de los productos (llamada/recall), así como de actualizaciones técnicas.
							<br><b>VIII.</b>	Tramitar y formalizar un crédito para la adquisición de un vehículo nuevo y/o seminuevo, así como la adquisición de seguros automotrices y extensiones de garantía conforme los contratos previamente establecidos por las partes.
							<br><b>IX.</b>	Cumplir con todas las actividades complementarias y/o auxiliares necesarias para la comercialización de vehículos automotores nuevos, seminuevos y la prestación de servicios y productos para el manteni¬miento de vehículos.
							<br><b>X.</b>	Procesar solicitudes, realizar actividades de cobranza, facturación y aclaraciones.
							<br><b>XI.</b>	Cumplir con las leyes, reglamentos y demás disposiciones legales aplicables.
							<br><b>XII.</b>	Mantener actualizados nuestros registros para poder responder a sus consultas.
							<br><b>XIII.</b>	Promociones, recordatorios y citas de servicio, carta de bienvenida, información sobre campañas preventivas de servicio y/o de seguridad, citas de prueba de manejo, seguimiento de comentarios, consultas, quejas y reclamaciones; controles estadísticos, controles administrativos y encuestas.
							<br><b>B.</b>	No necesarias para la prestación el servicio:
							<br><b>1)</b>	Para ofrecer bienes y servicios adicionales a los contratados.
							<br><b>2)</b>	Realizar estudios internos sobre hábitos de consumo, mercadotecnia, publicidad y prospección comercial relacionada con los bienes, productos y servicios, así como las inscripciones a programas de lealtad que ofrece el fabricante de los productos.
							<br><b>3)</b>	Dar seguimiento a nuestra relación comercial y solicitar la evaluación y el seguimiento para conocer el nivel de satisfacción de nuestros productos y servicios.
							<br><b>4)</b>	Análisis estadísticos / Fines estadísticos
							<br><b>5)</b>	Envió de formulario de encuestas para seguimiento de los servicios ofrecidos / Realizar encuestas / Realizar estudios de mercado
							<br><b>6)</b>	Análisis de información / Analizar hábitos de consumo
							<br><b>7)</b>	Contacto con el titular
							<br><b>8)</b>	Determinar la satisfacción / evaluar la calidad del servicio
							<br><b>9)</b>	Para invitarlo a eventos o enviar información sobre promocionales, productos y servicios adicionales que consideremos pueden ser de su interés.
							<br><b>10)</b>	Para contactarlo, enviarle notificaciones y uso de las diversas aplicaciones digitales administradas por nosotros y/o terceros que designemos; y que usted decida utilizar para mantenerse informado y/o dar seguimiento a sus adquisiciones de producto y servicio de postventa.';	
							break; //Isuzu Camiones

							case '37': $html .='';	
							break; //Volkswagen

							case '38': $html .='';	
							break; //Das Welt

							case '39': $html .='';	
							break; //Salon del Auto

							case '40': $html .='';	
							break; //Multimarca

							case '41': $html .='';	
							break; //Noti FAME

							case '42': $html .='';	
							break; //Audi Queretaro
							}


							$html .='
							Es importante mencionar que las finalidades (I), (II), (III) (IV), (V), (VI), (VII), (VIII), (IX), (X), (XI) Y (XII) y dan origen y son necesarias.
							Las finalidades (1) y (2) no son necesarias, pero son importantes para ofrecerle a través de campañas de mercadotecnia, publicidad y prospección comercial, bienes, productos y servicios exclusivos, por lo que usted tiene derecho a oponerse, o bien, a revocar su consentimiento para que <font color="#0A6ACF">'.$take['agname'].'</font> deje de tratar sus datos personales para dichas finalidades, de acuerdo al procedimiento señalado en el aparatado IV o V del presente Aviso de Privacidad.
							<br>
							<b>IV.	OPCIONES PARA LIMITAR EL USO O DIVULGACIÓN DE SUS DATOS PERSONALES.</b>
							<br>
							Si usted desea dejar de recibir mensajes de mercadotecnia, publicidad o de prospección comercial, puede hacerlo valer a través del correo electrónico avisodeprivacidad@grupofame.com o si lo prefiere, accediendo a nuestro sitio WEB <font color="#0A6ACF">'.$take['agurl'].'</font> sección Aviso de Privacidad o al teléfono 800 670 8386.
							<br>
							<b>V.	SOLICITUD DE ACCESO, RECTIFICACIÓN, CANCELACIÓN U OPOSICIÓN DE DATOS PERSONALES (DERECHOS ARCO) </b>
							<br>
							De conformidad con la Ley Federal de Protección de Datos Personales en Posesión de los Particulares, usted tiene el derecho de Acceder (“Acceso”) a los datos que posee el responsable y a los detalles del tratamiento de los mismos, así como a Rectificarlos en caso de ser inexactos o incompletos (“Rectificación”); Cancelarlos en los términos de la Ley (“Cancelación”) u Oponerse al tratamiento de los mismos para fines específicos (“Oposición”).
							<br>
							Usted o su representante legal podrán ejercer los derechos descritos anteriormente (Acceder, Rectificar, Cancelar u Oponerse) enviando un correo electrónico a la siguiente dirección electrónica: avisodeprivacidad@grupofame.com, para que le sea proporcionado el formato de Solicitud de Derechos ARCO, el cual deberá contener y acompañar lo siguiente: I. El nombre del titular y domicilio u otro medio para comunicarle la respuesta a su solicitud; II. Los documentos que acrediten la identidad o, en su caso, la representación legal del titular; III. La descripción clara y precisa de los datos personales respecto de los que se busca ejercer alguno de los derechos antes mencionados, y IV. Cualquier otro elemento o documento que facilite la localización de los datos personales; solicitud que deberá presentar en el domicilio del Responsable, debiendo adjuntar una copia de su identificación oficial y los documentos correspondientes para acreditar su titularidad. La respuesta a su solicitud de Derechos ARCO se le hará llegar al correo electrónico que haya proporcionado, dentro del término de 20 días hábiles contados a partir de la recepción de dicha solicitud, si <font color="#0A6ACF">'.$take['agname'].'</font>, resuelve que su solicitud es procedente, se hará efectiva dentro de los 15 días siguientes a la fecha en que se le comunicó la resolución, en caso que  se resuelva negar a Usted el acceso a sus datos personales o el ejercicio de los demás Derechos ARCO, deberá informarle el motivo de la negativa dentro en un plazo máximo de 20 días contados desde la fecha en que recibió su solicitud, acompañando las pruebas que crea pertinentes. Los periodos de comunicación de la resolución y en el que se haga efectiva su solicitud podrán ampliarse una sola vez por un periodo igual, siempre y cuando así lo justifiquen las circunstancias
							El ejercicio de los Derechos ARCO, será gratuito, en su caso, el titular debe de cubrir los gastos de envío, reproducción y/o certificación de documentos, sin embargo, si el titular ejerce el mismo derecho en un periodo no mayor de 12 meses, la respuesta a la solicitud tendrá un costo que no excederá de 3 unidades de cuenta vigente en la Ciudad de México.
							<br>
							<font color="#0A6ACF">'.$take['agname'].'</font>, podrá negar el acceso a los datos personales, o a realizar la rectificación o cancelación o conceder la oposición al tratamiento de los mismos, en los supuestos establecidos en el artículo 34 de la Ley, así mismo <font color="#0A6ACF">'.$take['agname'].'</font> no estará obligado a cancelar los datos personales del titular, bajo los supuestos establecidos en el artículo 26 de la Ley.
							<br>
							<br>
							<b>VI.	REVOCACIÓN DE CONSENTIMIENTO.</b>
							<br>
							El titular podrá revocar el consentimiento que nos ha otorgado para el tratamiento de sus datos personales en cualquier momento, en los términos de lo establecido en el artículo 8 de la Ley Federal de Protección de Datos Personales en Posesión de Particulares y salvo las excepciones previstas en la misma. En caso de no estar de acuerdo en el tratamiento de sus datos personales enviando un correo electrónico a la siguiente dirección electrónica: avisodeprivacidad@grupofame.com, donde se le informará el procedimiento a seguir y en  caso de que su Solicitud sea procedente se le registrará en el listado de exclusión de <font color="#0A6ACF">'.$take['agname'].'</font>. 
							En caso de considerarlo necesario usted puede acudir ante la autoridad competente que es el Instituto Nacional de Transparencia, Acceso a la Información y Protección de Datos Personales (INAI), o a través de su sitio web: www.inai.org.mx, para mayores informes o para hacer valer sus derechos que en materia correspondan.
							<br>
							<b>VII.	LIMITE AL TRATAMIENTO DE DATOS PERSONALES:</b>
							<br>
							El tratamiento de sus datos personales se encuentra limitado al cumplimiento de las finalidades previstas en el presente aviso de privacidad. Para el caso que se requiera tratar sus datos para un fin distinto que no resulte compatible o análogo a los fines establecidos en el presente aviso de privacidad, se requerirá obtener nuevamente el consentimiento de su titular. 
							Para el caso que el titular deseé limitar el consentimiento de sus datos, deberá enviar un correo electrónico a la siguiente dirección electrónica: avisodeprivacidad@grupofame.com, en el que deberá señalar: I. El nombre del titular y domicilio u otro medio para comunicarle la respuesta a su solicitud; II. Los documentos que acrediten la identidad (identificación oficial) o, en su caso, la representación legal del titular; III. La descripción clara y precisa de los datos personales respecto de los que se busca ejercer la limitante, los alcances y temporalidad de la limitante; y IV. Cualquier otro elemento o documento que facilite la localización de los datos personales que se desea limitar el consentimiento. . La respuesta a su solicitud de límite de tratamiento de datos se le hará llegar al correo electrónico que haya proporcionado para el efecto, dentro del término de 20 días hábiles contados a partir de la recepción de dicha solicitud, si <font color="#0A6ACF">'.$take['agname'].'</font>, resuelve que su solicitud es procedente, se hará efectiva dentro de los 15 días siguientes a la fecha en que se le comunicó la resolución
							<br>
							<b>VIII. TRANSFERENCIAS DE DATOS PERSONALES.</b> 
							<br>
							Transferencias sin necesidad de consentimiento:
							Se le informa que a fin de dar cumplimiento a las finalidades establecidas en el inciso (a), apartado III del presente Aviso de Privacidad, sus datos personales pueden ser transferidos y tratados dentro y fuera de los Estados Unidos Mexicanos por personas distintas a <font color="#0A6ACF">'.$planta.'</font>. En este sentido y con fundamento en La Ley y su Reglamento, sus datos personales podrán ser transferidos sin necesidad de su consen¬timiento a: (i) A terceros proveedores de servicios, única y exclusivamente para el cumplimiento de las finalidades establecidas en el inciso (a) del apartado III del presente Aviso de Privacidad; (ii) A terceros para realizar gestiones de cobranza extrajudicial y judicial en caso de incumplimiento, (iii) A terceros proveedores de servicios para la investigación y aprobación, en su caso, del crédito correspondiente para el cumplimiento de las finalidades establecidas en el inciso (a) del apartado III del presente Aviso de Privacidad; (iv) A <font color="#0A6ACF">'.$planta.'</font>, y filiales, subsidiarias, controladas y controladoras para el establecimiento de las condiciones de garantía, campañas de seguridad y servicio (llamada/recall) y actualizaciones técnicas; (v) A las Autoridades competentes en materia de control vehicular; (vi) A instituciones financieras, bancarias y crediticias, para la solicitud de otorgamiento de créditos por el servicio contratado por el cliente; (vii) A aseguradoras para el otorgamiento de pólizas de seguros por el servicio contratado por el cliente; (viii) A <font color="#0A6ACF">'.$planta.'</font> y filiales subsidiarias, controladas y controladoras para cumplir con las obligaciones de extensión de garantía. (ix) A empresas filiales, subsidiarias, controladas y controladoras de <font color="#0A6ACF">'.$take['agname'].'</font> que ofrezcan productos y servicios del portafolio de marcas del conjunto de empresas que integran a Grupo Fame.
							Transferencias con consentimiento:
							<font color="#0A6ACF">'.$take['agname'].'</font>, podrá transferir sus datos personales de identificación a <font color="#0A6ACF">'.$planta.'</font> para administrar el programa de lealtad propio de la marca; (ii) A <font color="#0A6ACF">'.$planta.'</font> para la evaluación y seguimiento del nivel de satisfacción de nuestros clientes. (iii) Departamento de Contact y Call Center de <font color="#0A6ACF">'.$planta.'</font> que considere para la realización de estudios internos sobre hábitos de consmo, mercadotecnia, publicidad y prospección comercial relacionada con los bienes, productos y servicios; dar seguimiento a la relación comercial y solicitar la evaluación y el seguimiento para conocer el nivel de satisfacción de los productos y servicios. A la firma del presente Aviso de Privacidad nos otorga su consentimiento para utransferir sus datos personales de identificación. Si usted no desea que sus datos personales sean transferidos a dichos terceros, puede manifestar su negativa conforme al procedimiento establecido en el apartado IV del presente Aviso de Privacidad.
							<br>
							<b>IX.	USO DE COOKIES.</b>
							<br>
							<b>X.</b><font color="#0A6ACF">'.$take['agname'].'</font>, le informa que utiliza varias tecnologías para mejorar la eficiencia de sus sitios web, entre estas tecnologías se incluye el uso de cookies de terceros (como socios comerciales, filiales o empresas subsidiarias, afiliada, partes relacionadas, controladas, controladoras o bajo control Común). Las cookies son pequeñas cantidades de información que se almacenan en el navegador utilizado por cada usuario para que el servidor recuerde cierta información que posteriormente pueda utilizar. Esta información permite identificarle y guardar sus preferencias personales para brindarle una mejor experiencia de navegación. Le recordamos que usted puede deshabilitar o ajustar el uso de cookies siguiendo los procedimientos del navegador de internet que utiliza. Nuestras páginas de internet no usan o guardan cookies para obtener datos de identificación personal de la computadora del Titular que no se hayan enviado originalmente como parte de la cookie. Los datos personales que recabamos de manera electrónica, así como las finalidades del tratamiento se encuentran establecidos en el presente Aviso de Privacidad. 
							<br>
							<br>
							<br>
							<br>

							<b>XI.	MODIFICACIONES AL AVISO DE PRIVACIDAD.</b>
							<br>
							Este Aviso de Privacidad podrá ser modificado; dichas modificaciones podrán consultarse a través de los siguientes medios:
							<br>
							<b>1.</b> Nuestra página de internet <font color="#0A6ACF">'.$take['agurl'].'</font>
							<br>
							<b>2.</b> Avisos visibles en nuestras instalaciones; o,
							<br>
							<b>3.</b> Cualquier otro medio de comunicación oral, impreso o electrónico que se determine para tal efecto.
							<br>
                            ';


                            switch ($take['agid'])
						{
							case '1': $html .='';
							break;

							case '2': $html .='';
							break;	//Nissan Altozano

							case '3': $html .='';
							break;	//Nissan Acueducto

							case '4': $html .='';
							break;	//Honda Manantiales

							case '5': $html .='';
							break;	//Honda Atizapan

							case '6': $html .=' ';
							break;	//Honda Monarca DF

							case '7': $html .='';
							break;	//Honda Altozano

						    case '8': $html .='';
							break;	//Honda Uruapan

							case '9': $html .=' ';
							break;	//Honda Monarca Morelia

							case '10': $html .='';
						    break;	//Honda Corregidora

							case '11': $html .='';
							break;	//Honda Marquesa

							case '12': $html .='';
							break;	//Honda San Juan del Rio

							case '13': $html .='';
						    break;	//Kia Pedregal

						    case '14': $html .='';
							break;	//Kia Paricutin

						    case '15': $html .=' ';
							break;	//Kia Mil Cumbres

							case '16': $html .=' ';
							break;	//Kia del Duero

							case '17': $html .=' ';
							break;	//Chevrolet Morelia

							case '18': $html .='  ';
							break;	//Chevrolet Lazaro Cardenas

							case '19': $html .=' ';
							break;	//Chevrolet Uruapan

							case '20': $html .='';
							break;	//Chevrolet Apatzingan

							case '21': $html .='';
							break;	//Chevrolet Zamora

							case '22': $html .='';
							break;	//Toyota Uruapan

							case '23': $html .='';
							break;	//Toyota Altozano

							case '24': $html .='';
							break;	//Toyota Valladolid

							case '25': $html .='';
							break;	//Toyota Perisur

							case '26': $html .='';
							break;	//Mitsubishi Queretaro

							case '27': $html .='';
							break;	//Mitsubishi Uruapan

							case '28': $html .=' ';
							break;	//GMC Morelia

							case '29': $html .=' ';
							break;	//GMC Uruapan

							case '30': $html .='';
							break;	//BMW Talisman

							case '31': $html .='';
							break;	//Motorrad Morelia

							case '32': $html .='';
							break;	//Mini Talisman

							case '33': $html .=' ';
							break;	//Chrysler Queretaro

							case '34': $html .=' ';
							break;	//Chrysler Uruapan

							case '35': $html .=' ';
							break;	//Audi Juriquilla

							case '36': $html .='';
							break;	//Isuzu Camiones

							case '37': $html .=' ';
							break;	//Volkswagen

							case '38': $html .=' ';
							break;	//Das Welt

							case '39': $html .=' ';
							break;	//Salon del Auto

							case '40': $html .=' ';
							break;	//Multimarca

							case '41': $html .=' ';
							break;	//Noti FAME

							case '42': $html .=' ';
							break;	//Audi Queretaro
						}

                        $html .='

                        ';

		
$html.='
                    </label>
                </td>
            </tr>    
        </table>

        <br>

         <table align="center" border="0" >
			<tr>
				<td> <input type="checkbox" name="check_list[]" style="width:20px;height:20px;" value="to1" > SI deseo ser contactado <br> </td>
				<td> <input type="checkbox" name="check_list[]" style="width:20px;height:20px;" value="to1" > NO deseo ser contactado <br> </td>
			</tr>
			<tr>
				<td> <input type="checkbox" name="check_list[]" style="width:20px;height:20px;" value="to1" > SI deseo la trasferencia de mis datos <br> </td>
				<td> <input type="checkbox" name="check_list[]" style="width:20px;height:20px;" value="to1" > NO deseo la trasferencia de mis datos <br> </td>
			</tr>
		</table>

		<br>
		<br>

		<label align="justify">Estoy enterado del tratamiento que recibirán mis datos personales en términos de lo establecido en la Ley Federal de Protección de Datos Personales en Posesión de Particulares, en mi carácter de titular de los “Datos” proporcionados para los propósitos y bajo las condiciones establecidas en el presente Aviso de Privacidad y por la “Ley” </label>

		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>

        <table align="center">
			<tr>
				<td>
					<b>'.$take['nombre'].'</b><br>
					_________________________________<br>
					Nombre del titular
				</td>
				<td>
					<br><br><br>
					_________________________________<br>
					Firma o huella dactilar
				</td>
				<td>
					<b>'.$take['fechacontrato'].'</b><br>
					_________________________________<br>
					Fecha(AAAA-M-D)
				</td>
			</tr>
		</table>
        <br>
        Última fecha de actualización 05 de Julio del 2021


    </tbody> 
</table>';

tcpdf();
$obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
header('Content-type: application/pdf');
$obj_pdf->SetCreator(PDF_CREATOR);
$title = "Aviso de privacidad";
$obj_pdf->SetTitle($title);
$obj_pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $title, PDF_HEADER_STRING);
$obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$obj_pdf->SetDefaultMonospacedFont('helvetica');
$obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$obj_pdf->SetFont('helvetica', '', 9);
$obj_pdf->setFontSubsetting(false);
$obj_pdf->AddPage();
ob_start();
    // podemos tener cualquier parte de la vista aquí como HTML, PHP, etc.
$content = ob_get_contents();
ob_end_clean();
$obj_pdf->writeHTML($content, true, false, true, false, '');
$obj_pdf->writeHTML($html, true, false, true, false, '');


$filename = 'aviso_privacidad';

$obj_pdf->Output($filename . '.pdf', 'D');




?>