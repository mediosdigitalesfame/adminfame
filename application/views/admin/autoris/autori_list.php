<!-- DataTables -->
<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/datatables/dataTables.bootstrap4.css"> 

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <section class="content">
    <!-- For Messages -->
    <?php $this->load->view('admin/includes/_messages.php') ?>
    <div class="card">
      <div class="card-header">
        <div class="d-inline-block">
          <h3 class="card-title"><i class="fa fa-list"></i>&nbsp; <?= trans('checks_list') ?></h3>
        </div>
        <div class="d-inline-block float-right">
          <?php if($this->rbac->check_operation_permission('add')): ?>
            <a href="<?= base_url('admin/checks/add'); ?>" class="btn btn-success"><i class="fa fa-plus"></i> <?= trans('add_new_check') ?></a>
          <?php endif; ?>  
          <?php if($this->rbac->check_operation_permission('pdf')): ?>  
            <a href="<?= base_url('admin/checks/create_checks_pdf') ?>" class="btn btn-success"><?= trans('export_as_pdf') ?></a>
          <?php endif; ?>
          <?php if($this->rbac->check_operation_permission('csv')): ?>  
            <a href="<?= base_url('admin/checks/export_csv') ?>" class="btn btn-success"><?= trans('export_as_csv') ?></a>
          <?php endif; ?>
        </div>
      </div>
    </div>

    <!--<div class="card">
      <div class="card-body table-responsive"> 
        <?php echo form_open("/",'id="model_search"') ?>
        <div class="row">
          <div class="col-md-4">
            <label><?= trans('model_type') ?></label><hr style="margin:5px 0px;" />
            <input checked="checked" onchange="user_filter()" name="user_search_type" value="" type="radio"  /> <?= trans('all') ?>&nbsp;&nbsp;&nbsp;
            <input onchange="model_filter()" name="model_search_type" value="1" type="radio"  /> <?= trans('active') ?>&nbsp;&nbsp;&nbsp;
            <input onchange="model_filter()" name="model_search_type" value="0" type="radio"  /> <?= trans('inactive') ?>
          </div>
          <div class="col-md-3">
            <label>Date From:</label><input name="user_search_from" type="text" class="form-control form-control-inline input-medium datepicker" id="" />
          </div>
          <div class="col-md-3"> 
            <label>Date To:</label><input name="user_search_to" type="text" class="form-control form-control-inline input-medium datepicker" id="" /> 
          </div>
          <div class="col-md-2"> 
            <button type="button" style="margin-top:20px;" onclick="user_filter()" class="btn btn-info">Submit</button>
            <a href="<?= base_url('admin/checks/advance_search'); ?>" class="btn btn-danger" style="margin-top:20px;">
              <i class="fa fa-repeat"></i>
            </a>
          </div>
        </div>
        <?php echo form_close(); ?>
      </div>
    </div>-->  


    <div class="card">
      <div class="card-body table-responsive">
        <table id="na_datatable" class="table table-bordered table-striped" width="100%">
          <thead>
            <tr>
              <th>#<?= trans('id') ?></th>
              <th>#<?= trans('agency') ?></th>
              <th><?= trans('folio') ?></th>
              <th><?= trans('created_date') ?></th>
              <th><?= trans('status') ?></th>
              <th width="150px" class="text-right"><?= trans('action') ?></th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </section>  
</div>

<!-- bootstrap datepicker -->
<!-- datepicker -->
<script src="<?= base_url() ?>assets/plugins/datepicker/bootstrap-datepicker.js"></script>
<script>
  $('.datepicker').datepicker({
    autoclose: true
  });
</script>
<!-- DataTables -->
<script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?= base_url() ?>assets/plugins/datatables/dataTables.bootstrap4.js"></script>

<script>
  //---------------------------------------------------
  var table = $('#na_datatable').DataTable( {
    "processing": true,
    "serverSide": false,
    "ajax": "<?=base_url('admin/checks/datatable_json')?>",
    "order": [[0,'desc']],
    "columnDefs": [
    { "targets": 0, "name": "id", 'searchable':true, 'orderable':true},
    { "targets": 1, "name": "agname", 'searchable':true, 'orderable':true},
    { "targets": 2, "name": "folio", 'searchable':true, 'orderable':true},
    { "targets": 3, "name": "created_at", 'searchable':false, 'orderable':false},
    { "targets": 4, "name": "status", 'searchable':true, 'orderable':true},
    { "targets": 5, "name": "Action", 'searchable':false, 'orderable':false,'width':'100px'}
    ]
  });
</script>

<script>
  //---------------------------------------------------
 function model_filter()
  {
    var _form = $("#model_search").serialize();
    $.ajax({
      data: _form,
      type: 'post',
      url: '<?php echo base_url();?>admin/checks/search',
      async: true,
      success: function(output){
        table.ajax.reload( null, false );
      }
    });
  }
</script>

<script type="text/javascript">
  $("body").on("change",".tgl_checkbox",function(){
    console.log('checked');
    $.post('<?=base_url("admin/checks/change_status")?>',
    {
      '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>',
      id : $(this).data('id'),
      status : $(this).is(':checked') == true?1:0
    },
    <?php if($this->rbac->check_operation_permission('change_status')): ?>
      function(data){$.notify("El Status cambió con éxito", "success");}
      <?php else: ?>
        function(data){$.notify("No Tienes Permisos", "warn");}
      <?php endif; ?>
      );
  });
</script>


