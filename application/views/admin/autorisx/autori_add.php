  <!-- daterange picker -->

  <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/daterangepicker/daterangepicker-bs3.css">

  <!-- iCheck for checkboxes and radio inputs -->

  <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/iCheck/all.css">

  <!-- Bootstrap Color Picker -->

  <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/colorpicker/bootstrap-colorpicker.min.css">

  <!-- Bootstrap time Picker -->

  <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/timepicker/bootstrap-timepicker.min.css">

  <!-- Select2 -->

  <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/select2/select2.min.css">



  <!-- Content Wrapper. Contains page content -->

  <div class="content-wrapper">

    <!-- Main content -->

    <section class="content">

      <div class="card card-default">

        <div class="card-header">

          <div class="d-inline-block">

            <h3 class="card-title"> <i class="fa fa-plus"></i>

             <?= trans('add_new_authorization') ?> </h3>

           </div>

           <div class="d-inline-block float-right">

            <a href="<?= base_url('admin/autoris'); ?>" class="btn btn-success"><i class="fa fa-list"></i>  <?= trans('checks_list') ?></a>

          </div>

        </div>



        <div class="card-body">



         <!-- For Messages -->

         <?php $this->load->view('admin/includes/_messages.php') ?>

         <?php echo form_open(base_url('admin/authorizations/add'), 'class="form-horizontal"');  ?> 



         <input type="hidden" id="identi" name="identi" value="">

         <input type="hidden" id="id" name="id" value="">





         <div class="form-group">

          <div class="row">

            <div class="col-lg-3">

              <div class="input-group">

                <label for="tomasel" class="col-md-12 control-label"><?= trans('select_take') ?>*</label>

                <div class="col-md-12">

                  <select id="tomasel" name="tomasel" class="form-control select2">

                    <option value=""><?= trans('select_take') ?></option>

                    <?php foreach($takes as $take): ?>

                      <option value="<?= $take['id']; ?>"><?= $take['folio']; ?></option>

                    <?php endforeach; ?>

                  </select>

                </div>

              </div>

            </div>

          </div>

        </div>



        

<div class="col-lg-5">



  <div class="input-group">



    <label class="col-md-12 control-label">Imágen Autometrica</label> 



    <div class="form-group">



      <td>



       <input type="file" name="image" accept=".png, .jpg, .jpeg, .gif, .svg">



       <p><small class="text-success"><?= trans('allowed_types') ?>: gif, jpg, png, jpeg</small></p>







     </td>



   </div>



 </div>



</div>



         

        <div class="form-group">

          <div class="row">

            <div class="col-lg-3">

              <div class="input-group">

                <label class="col-md-12 control-label">Condiciones Generales</label> 

                <div class="col-md-12">

                 <input type="file" name="cgenerales" accept=".png, .jpg, .jpeg, .gif, .svg, .pdf">

                 <p><small class="text-success"><?= trans('allowed_types') ?>: gif, jpg, png, jpeg, pdf</small></p>

               </div>

             </div>

           </div>

           <div class="col-lg-3">

            <div class="input-group">

             <label class="col-md-12 control-label">Reporte Diagnostico</label> 

             <div class="col-md-12">

              <input type="file" name="rdiagnostico" accept=".png, .jpg, .jpeg, .gif, .svg, .pdf">

              <p><small class="text-success"><?= trans('allowed_types') ?>: gif, jpg, png, jpeg, pdf</small></p>

            </div>

          </div>

        </div>

        <div class="col-lg-3">

          <div class="input-group">

           <label class="col-md-12 control-label">Reporte Transunion</label> 

           <div class="col-md-12">

            <input type="file" name="rtrans" accept=".png, .jpg, .jpeg, .gif, .svg, .pdf">

            <p><small class="text-success"><?= trans('allowed_types') ?>: gif, jpg, png, jpeg, pdf</small></p>

          </div>

        </div>

      </div>

      <div class="col-lg-3">

        <div class="input-group">

          <label class="col-md-12 control-label">Situacion  Fiscal</label> 

          <div class="col-md-12">

            <input type="file" name="sfiscal" accept=".png, .jpg, .jpeg, .gif, .svg, .pdf">

            <p><small class="text-success"><?= trans('allowed_types') ?>: gif, jpg, png, jpeg, pdf</small></p>

          </div>

        </div>

      </div>

    </div>



    <div class="row">

      <div class="col-lg-6">

        <div class="input-group">

          <label for="modelo" class="col-md-12 control-label">Observaciones</label>

          <div class="col-md-12">

            <input onkeyup="javascript:this.value=this.value.toUpperCase();" type="text" value="<?= old("modelo"); ?>" class="form-control " name="observaciones" id="observaciones">

            <div class="input-group">

            </div>

          </div>

        </div>

      </div>

    </div>



  </div>

</div>

</div>   







<div class="card card-default">



 <div class="card-header">

  <div class="d-inline-block">

    <h3 class="card-title">Importes en Guia Autometrica</h3>

  </div>

</div>



<div class="card-body">



  <div class="d-inline-block">

    <h3 class="card-title">COMPRA</h3>

  </div>

  <br>



  <div class="form-group">

   <div class="row">

    <div class="col-lg-3">

      <div class="input-group">

       <label for="urlp" class="col-md-12 control-label">Precio Compra Libro</label>

       <div class="col-md-12">

        <input type="text" class="form-control" placeholder="" id="pcompra" name="pcompra">

      </div>

    </div>

  </div>

  <div class="col-lg-3">

    <div class="input-group">

     <label for="urlp" class="col-md-12 control-label">Bono x Equipamiento</label>

     <div class="col-md-12">

      <input type="text" data-role="tagsinput" class="form-control" placeholder="" id="bonoc" name="bonoc">

    </div>

  </div>

</div>

<div class="col-lg-3">

  <div class="input-group">

   <label for="urlp" class="col-md-12 control-label">Premio / Castigo</label>

   <div class="col-md-12">

    <input type="text" data-role="tagsinput" class="form-control" placeholder="" id="premioc" name="premioc">

  </div>

</div>

</div>

<div class="col-lg-3">

  <div class="input-group">

   <label for="urlp" class="col-md-12 control-label">Precio de Compra</label>

   <div class="col-md-12">

    <input type="text" data-role="tagsinput" class="form-control" placeholder="" id="precioc" name="precioc">

  </div>

</div>

</div>

</div>

</div>



<br>



<div class="d-inline-block">

    <h3 class="card-title">VENTA</h3>

  </div>

  <br>



<div class="form-group">

 <div class="row">

  <div class="col-lg-3">

    <div class="input-group">

     <label for="urlp" class="col-md-12 control-label">Precio de Venta</label>

     <div class="col-md-12">

      <input type="text" class="form-control" placeholder="" id="pventa" name="pventa">

    </div>

  </div>

</div>

<div class="col-lg-3">

  <div class="input-group">

   <label for="urlp" class="col-md-12 control-label">Bono x Equipamiento</label>

   <div class="col-md-12">

    <input type="text" class="form-control" placeholder="" id="bonov" name="bonov">

  </div>

</div>

</div>

<div class="col-lg-3">

  <div class="input-group">

   <label for="urlp" class="col-md-12 control-label">Premio / Castigo</label>

   <div class="col-md-12">

    <input type="text" class="form-control" placeholder="" id="premiov" name="premiov">

  </div>

</div>

</div>

<div class="col-lg-3">

  <div class="input-group">

   <label for="urlp" class="col-md-12 control-label">Precio de Venta</label>

   <div class="col-md-12">

    <input type="text" class="form-control" placeholder="" id="preciov" name="preciov">

  </div>

</div>

</div>



</div>

</div>



</div>

</div>    







<div class="card card-default">

  <div class="card-body">

    <div class="form-group">

      <div class="col-md-12">

        <input type="submit" name="submit" value="<?= trans('add_take') ?>" class="btn btn-primary pull-right">

      </div>

    </div>

    <?php echo form_close( ); ?>

  </div>

</div>





</section> 

</div>







<!-- Select2 -->

<script src="<?= base_url() ?>assets/plugins/select2/select2.full.min.js"></script>

<!-- InputMask -->

<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.js"></script>

<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>

<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>

<!-- date-range-picker -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>

<script src="<?= base_url() ?>assets/plugins/daterangepicker/daterangepicker.js"></script>

<!-- bootstrap color picker -->

<script src="<?= base_url() ?>assets/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>

<!-- bootstrap time picker -->

<script src="<?= base_url() ?>assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>

<!-- Page script -->

<!-- iCheck 1.0.1 -->

<script src="<?= base_url() ?>assets/plugins/iCheck/icheck.min.js"></script>

<script>

  $(function () {

    //Initialize Select2 Elements

    $('.select2').select2()



    //Datemask dd/mm/yyyy

    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })

    //Datemask2 mm/dd/yyyy

    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })

    //Money Euro

    $('[data-mask]').inputmask()



    //Date range picker

    $('#reservation').daterangepicker()

    //Date range picker with time picker

    $('#reservationtime').daterangepicker({

      timePicker         : true,

      timePickerIncrement: 30,

      format             : 'MM/DD/YYYY h:mm A'

    })

    //Date range as a button

    $('#daterange-btn').daterangepicker(

    {

      ranges   : {

        'Today'       : [moment(), moment()],

        'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],

        'Last 7 Days' : [moment().subtract(6, 'days'), moment()],

        'Last 30 Days': [moment().subtract(29, 'days'), moment()],

        'This Month'  : [moment().startOf('month'), moment().endOf('month')],

        'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]

      },

      startDate: moment().subtract(29, 'days'),

      endDate  : moment()

    },

    function (start, end) {

      $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))

    }

    )



    //iCheck for checkbox and radio inputs

    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({

      checkboxClass: 'icheckbox_minimal-blue',

      radioClass   : 'iradio_minimal-blue'

    })

    //Red color scheme for iCheck

    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({

      checkboxClass: 'icheckbox_minimal-red',

      radioClass   : 'iradio_minimal-red'

    })

    //Flat red color scheme for iCheck

    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({

      checkboxClass: 'icheckbox_flat-green',

      radioClass   : 'iradio_flat-green'

    })



    //Colorpicker

    $('.my-colorpicker1').colorpicker()

    //color picker with addon

    $('.my-colorpicker2').colorpicker()



    //Timepicker

    $('.timepicker').timepicker({

      showInputs: false

    })

  })

</script>



<script>

  $("#forms").addClass('active');

  $("#advanced").addClass('active');

</script>  

