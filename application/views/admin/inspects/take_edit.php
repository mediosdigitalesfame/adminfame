    <!-- daterange picker -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/daterangepicker/daterangepicker-bs3.css">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/iCheck/all.css">
    <!-- Bootstrap Color Picker -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/colorpicker/bootstrap-colorpicker.min.css">
    <!-- Bootstrap time Picker -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/timepicker/bootstrap-timepicker.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/select2/select2.min.css">

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Main content -->
      <section class="content">
        <div class="card card-default">
          <div class="card-header">
            <div class="d-inline-block">
              <h3 class="card-title"> <i class="fa fa-pencil"></i>
                &nbsp; <?= trans('edit_take') ?> </h3>
              </div>
              <div class="d-inline-block float-right">
                <a href="<?= base_url('admin/takes'); ?>" class="btn btn-success"><i class="fa fa-list"></i> <?= trans('takes_list') ?></a>
                <a href="<?= base_url('admin/takes/add'); ?>" class="btn btn-success"><i class="fa fa-plus"></i> <?= trans('add_new_take') ?></a>
              </div>
            </div>
            <div class="card-body">

             <!-- For Messages -->
             <?php $this->load->view('admin/includes/_messages.php') ?>
             <?php echo form_open(base_url('admin/takes/edit/'.$take['id']), 'class="form-horizontal"' )?> 

             <input type="hidden" id="identi" name="identi" >
             <input type="hidden" id="id" name="id" value="<?= $take['id']; ?>">
             <input type="hidden" id="guiaactual" name="guiaactual" value="<?= $take['guias_id']; ?>">

             <div class="form-group">
               <div class="row">
                <div class="col-lg-3">
                  <div class="input-group">
                   <label for="guia" class="col-md-12 control-label"><?= trans('select_guide') ?>*</label>
                   <div class="col-md-12">
                    <select name="guia" id="guia" class="form-control select2" >
                      <option value=""><?= trans('select_guide') ?></option>
                      <?php foreach($guides as $guide): ?>
                        <?php if($guide['id'] == $take['guias_id']): ?>
                          <option value="<?= $guide['id']; ?>" selected><?= $guide['folio']; ?></option>
                          <?php else: ?>
                            <option value="<?= $guide['id']; ?>"><?= $guide['folio']; ?></option>
                          <?php endif; ?>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="form-group">
             <div class="row">
              <div class="col-lg-4">
                <div class="input-group">
                 <label for="vin" class="col-md-12 control-label">Nombre Completo</label>
                 <div class="col-md-12">
                  <input type="text" class="form-control" placeholder="" value="<?= $take['nombre']; ?>" id="nombre" name="nombre" maxlength="17" onkeyup="javascript:this.value=this.value.toUpperCase();">
                </div>
              </div>
            </div>
            <div class="col-lg-3">
              <div class="input-group">
               <label for="url" class="col-md-12 control-label">Fecha Contrato</label>
               <div class="col-md-12">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                  </div>
                  <input type="text" class="form-control" value="<?= $take['fechacontrato']; ?>" name="fechacontrato" id="fechacontrato" >
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-5">
            <div class="input-group">
             <label for="urlp" class="col-md-12 control-label">Calle</label>
             <div class="col-md-12">
              <input type="text" class="form-control" placeholder="" value="<?= $take['calle']; ?>" name="calle" id="calle">
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="form-group">
 <div class="row">
  <div class="col-lg-2">
    <div class="input-group">
     <label for="urlp" class="col-md-12 control-label">No. Ext</label>
     <div class="col-md-12">
      <input type="text" class="form-control" placeholder="" value="<?= $take['numext']; ?>"name="numext" id="numext">
    </div>
  </div>
</div>
<div class="col-lg-2">
  <div class="input-group">
   <label for="urlp" class="col-md-12 control-label">No. Int</label>
   <div class="col-md-12">
    <input type="text" class="form-control" placeholder="" value="<?= $take['numint']; ?>" name="numint" id="numint">
  </div>
</div>
</div>
<div class="col-lg-4">
  <div class="input-group">
   <label for="vin" class="col-md-12 control-label">Colonia</label>
   <div class="col-md-12">
    <input type="text" class="form-control" placeholder="" value="<?= $take['colonia']; ?>" name="colonia" id="colonia" maxlength="17" onkeyup="javascript:this.value=this.value.toUpperCase();">
  </div>
</div>
</div>
<div class="col-lg-2">
  <div class="input-group">
   <label for="urlp" class="col-md-12 control-label">CP</label>
   <div class="col-md-12">
    <input type="text" class="form-control" placeholder="" value="<?= $take['cp']; ?>" name="cp" id="cp">
  </div>
</div>
</div>
<div class="col-lg-2">
  <div class="input-group">
   <label for="urlp" class="col-md-12 control-label">Ciudad</label>
   <div class="col-md-12">
    <input type="text" class="form-control" placeholder="" value="<?= $take['ciudad']; ?>" name="ciudad" id="ciudad">
  </div>
</div>
</div>
</div>
</div>

<div class="form-group">
 <div class="row">
  <div class="col-lg-2">
    <div class="input-group">
     <label for="urlp" class="col-md-12 control-label">Municipio</label>
     <div class="col-md-12">
      <input type="text" class="form-control" placeholder="" value="<?= $take['municipio']; ?>"name="municipio" id="municipio">
    </div>
  </div>
</div>
<div class="col-lg-2">
  <div class="input-group">
   <label for="urlp" class="col-md-12 control-label">Estado</label>
   <div class="col-md-12">
    <input type="text" class="form-control" placeholder="" value="<?= $take['estado']; ?>" name="estado" id="estado">
  </div>
</div>
</div>
<div class="col-lg-3">
  <div class="input-group">
   <label for="vin" class="col-md-12 control-label">RFC</label>
   <div class="col-md-12">
    <input type="text" class="form-control" placeholder="" value="<?= $take['rfc']; ?>" name="rfc" id="rfc" maxlength="13" onkeyup="javascript:this.value=this.value.toUpperCase();">
  </div>
</div>
</div>
<div class="col-lg-3">
  <div class="input-group">
   <label for="urlp" class="col-md-12 control-label">No. Identificación</label>
   <div class="col-md-12">
    <input type="text" class="form-control" placeholder="" value="<?= $take['nidenti']; ?>" id="nidenti" name="nidenti">
  </div>
</div>
</div>
<div class="col-lg-2">
  <div class="input-group">
   <label for="urlp" class="col-md-12 control-label">Tipo Identificación</label>
   <div class="col-md-12">
    <input type="text" class="form-control" placeholder="" value="<?= $take['tipoidenti']; ?>" id="tipoidenti" name="tipoidenti">
  </div>
</div>
</div>
</div>
</div>

<div class="form-group">
 <div class="row">
  <div class="col-lg-3">
    <div class="input-group">
     <label for="url" class="col-md-12 control-label">Fecha Identificación</label>
     <div class="col-md-12">
      <div class="input-group">
        <div class="input-group-prepend">
          <span class="input-group-text"><i class="fa fa-calendar"></i></span>
        </div>
        <input type="text" class="form-control" value="<?= $take['fechaidenti']; ?>" name="fechaidenti" id="fechaidenti" >
      </div>
    </div>
  </div>
</div>
<div class="col-lg-2">
  <div class="input-group">
   <label for="urlp" class="col-md-12 control-label">Mes del Recibo</label>
   <div class="col-md-12">
    <input type="text" class="form-control" placeholder="" value="<?= $take['mesrecibo']; ?>" id="mesrecibo" name="mesrecibo">
  </div>
</div>
</div>
</div>
</div>

<!--<div class="form-group">
  <label for="role" class="col-md-2 control-label"><?= trans('status') ?></label>

  <div class="col-md-12">
    <select name="status" class="form-control">
      <option value=""><?= trans('select_status') ?></option>
      <option value="1" <?= ($guide['status'] == 1)?'selected': '' ?> ><?= trans('active') ?></option>
      <option value="0" <?= ($guide['status'] == 0)?'selected': '' ?>><?= trans('inactive') ?></option>
    </select>
  </div>
</div>-->
 
</div>
<!-- /.box-body -->
</div>

<div class="card card-default">

   <div class="card-header">
  <div class="d-inline-block">
    <h3 class="card-title"> Datos del Vehículo</h3>
  </div>
</div>

  <div class="card-body">

    <div class="form-group">
     <div class="row">
      <div class="col-lg-3">
        <div class="input-group">
         <label for="urlp" class="col-md-12 control-label">Factura Vehiculo</label>
         <div class="col-md-12">
          <input type="text" class="form-control" placeholder="" value="<?= $take['facturavehi']; ?>" name="facturavehi" id="facturavehi">
        </div>
      </div>
    </div>
    <div class="col-lg-3">
      <div class="input-group">
       <label for="urlp" class="col-md-12 control-label">Expedida Por</label>
       <div class="col-md-12">
        <input type="text" class="form-control" placeholder="" value="<?= $take['expedida']; ?>" id="expedida" name="expedida">
      </div>
    </div>
  </div>
  <div class="col-lg-3">
    <div class="input-group">
     <label for="url" class="col-md-12 control-label">Fecha Factura</label>
     <div class="col-md-12">
      <div class="input-group">
        <div class="input-group-prepend">
          <span class="input-group-text"><i class="fa fa-calendar"></i></span>
        </div>
        <input type="text" class="form-control" value="<?= $take['fechafac']; ?>" name="fechafac" id="fechafac" >
      </div>
    </div>
  </div>
</div>
<div class="col-lg-3">
  <div class="input-group">
   <label for="vin" class="col-md-12 control-label">Marca Vehiculo</label>
   <div class="col-md-12">
    <input type="text" class="form-control" placeholder="" value="<?= $take['marcavehi']; ?>" id="marcavehi" name="marcavehi" maxlength="17" onkeyup="javascript:this.value=this.value.toUpperCase();">
  </div>
</div>
</div>
</div>
</div>

<div class="form-group">
 <div class="row">
  <div class="col-lg-3">
    <div class="input-group">
     <label for="urlp" class="col-md-12 control-label">Modelo Vehiculo</label>
     <div class="col-md-12">
      <input type="text" class="form-control" placeholder="" value="<?= $take['modelovehi']; ?>" id="modelovehi" name="modelovehi">
    </div>
  </div>
</div>
<div class="col-lg-3">
  <div class="input-group">
   <label for="url" class="col-md-12 control-label">Año Modelo</label>
   <div class="col-md-12">
    <div class="input-group">
      <div class="input-group-prepend">
        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
      </div>
      <input type="text" class="form-control" id="aniomodelo" value="<?= $take['aniomodelo']; ?>" name="aniomodelo" data-inputmask='"mask": "9999"' data-mask>
    </div>
  </div>
</div>
</div>
<div class="col-lg-2">
  <div class="input-group">
   <label for="vin" class="col-md-12 control-label">Color Ext</label>
   <div class="col-md-12">
    <input type="text" class="form-control" placeholder="" value="<?= $take['colorext']; ?>" id="colorext" name="colorext" maxlength="17" onkeyup="javascript:this.value=this.value.toUpperCase();">
  </div>
</div>
</div>
<div class="col-lg-2">
  <div class="input-group">
   <label for="urlp" class="col-md-12 control-label">Color Int</label>
   <div class="col-md-12">
    <input type="text" class="form-control" placeholder="" value="<?= $take['colorint']; ?>" id="colorint" name="colorint">
  </div>
</div>
</div>
<div class="col-lg-2">
  <div class="input-group">
   <label for="urlp" class="col-md-12 control-label">Version</label>
   <div class="col-md-12">
    <input type="text" class="form-control" placeholder="" value="<?= $take['versionvehi']; ?>" id="versionvehi" name="versionvehi">
  </div>
</div>
</div>
</div>
</div>

<div class="form-group">
 <div class="row">
  <div class="col-lg-3">
    <div class="input-group">
     <label for="urlp" class="col-md-12 control-label">Clave Vehicular</label>
     <div class="col-md-12">
      <input type="text" class="form-control" placeholder="" value="<?= $take['clavevehi']; ?>" id="clavevehi" name="clavevehi">
    </div>
  </div>
</div>
<div class="col-lg-3">
  <div class="input-group">
   <label for="vin" class="col-md-12 control-label">No. de Serie</label>
   <div class="col-md-12">
    <input type="text" class="form-control" placeholder="" value="<?= $take['nserie']; ?>" id="nserie" name="nserie" maxlength="17" onkeyup="javascript:this.value=this.value.toUpperCase();">
  </div>
</div>
</div>
<div class="col-lg-3">
  <div class="input-group">
   <label for="urlp" class="col-md-12 control-label">No. de Motor</label>
   <div class="col-md-12">
    <input type="text" class="form-control" placeholder="" value="<?= $take['nmotor']; ?>" id="nmotor" name="nmotor">
  </div>
</div>
</div>
<div class="col-lg-3">
  <div class="input-group">
   <label for="urlp" class="col-md-12 control-label">No. de Baja</label>
   <div class="col-md-12">
    <input type="text" class="form-control" placeholder=""  value="<?= $take['nbaja']; ?>" id="nbaja" name="nbaja">
  </div>
</div>
</div>
</div>
</div>

<div class="form-group">
 <div class="row">
  <div class="col-lg-3">
    <div class="input-group">
     <label for="urlp" class="col-md-12 control-label">Edo Emisor Baja</label>
     <div class="col-md-12">
      <input type="text" class="form-control" placeholder="" value="<?= $take['edoemisor']; ?>" id="edoemisor" name="edoemisor">
    </div>
  </div>
</div>
<div class="col-lg-3">
  <div class="input-group">
   <label for="url" class="col-md-12 control-label">Fecha de Baja</label>
   <div class="col-md-12">
    <div class="input-group">
      <div class="input-group-prepend">
        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
      </div>
      <input type="text" class="form-control" value="<?= $take['fechabaja']; ?>" name="fechabaja" id="fechabaja" >
    </div>
  </div>
</div>
</div>
<div class="col-lg-3">
  <div class="input-group">
   <label for="urlp" class="col-md-12 control-label">Placas de Baja</label>
   <div class="col-md-12">
    <input type="text" class="form-control" placeholder="" value="<?= $take['placasbaja']; ?>" id="placasbaja" name="placasbaja">
  </div>
</div>
</div>
<div class="col-lg-3">
  <div class="input-group">
   <label for="urlp" class="col-md-12 control-label">No. Verificación</label>
   <div class="col-md-12">
    <input type="text" class="form-control" placeholder="" value="<?= $take['nverificacion']; ?>" id="nverificacion" name="nverificacion">
  </div>
</div>
</div>
</div>
</div>

<div class="form-group">
 <div class="row">
  <div class="col-lg-6">
    <div class="input-group">
     <label for="urlp" class="col-md-12 control-label">Nombre a Quien se Vende la Unidad</label>
     <div class="col-md-12">
      <input type="text" class="form-control" placeholder="" value="<?= $take['aquiensevende']; ?>" id="aquiensevende" name="aquiensevende">
    </div>
  </div>
</div>
<div class="col-lg-3">
  <div class="input-group">
   <label for="url" class="col-md-12 control-label">Fecha Fac. Final</label>
   <div class="col-md-12">
    <div class="input-group">
      <div class="input-group-prepend">
        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
      </div>
      <input type="text" class="form-control" value="<?= $take['fechafacfinal']; ?>" name="fechafacfinal" id="fechafacfinal" >
    </div>
  </div>
</div>
</div>
<div class="col-lg-3">
  <div class="input-group">
   <label for="url" class="col-md-12 control-label">Fecha de Toma</label>
   <div class="col-md-12">
    <div class="input-group">
      <div class="input-group-prepend">
        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
      </div>
      <input type="text" class="form-control" value="<?= $take['fechatoma']; ?>" name="fechatoma" id="fechatoma" >
    </div>
  </div>
</div>
</div>
</div>
</div>

<div class="form-group">
 <div class="row">
  <div class="col-lg-3">
    <div class="input-group">
     <label for="urlp" class="col-md-12 control-label">Costo de Adquisición</label>
     <div class="col-md-12">
      <input type="text" class="form-control" placeholder="" value="<?= $take['costoadquisicion']; ?>" id="costoadquisicion" name="costoadquisicion">
    </div>
  </div>
</div>
<div class="col-lg-6">
  <div class="input-group">
   <label for="urlp" class="col-md-12 control-label">Años de Tenencias</label>
   <div class="col-md-12">
    <input type="text" data-role="tagsinput" class="form-control" value="<?= $take['tenencias']; ?>" placeholder="" id="tenencias" name="tenencias">
  </div>
</div>
</div>
</div>
</div>
</div>
</div>

<div class="card card-default">

 <div class="card-header">
  <div class="d-inline-block">
    <h3 class="card-title"> Solicitud CFDI</h3>
  </div>
</div>

<div class="card-body">

  <div class="form-group">
   <div class="row">
    <div class="col-lg-3">
      <div class="input-group">
       <label for="urlp" class="col-md-12 control-label">Nombres</label>
       <div class="col-md-12">
        <input type="text" class="form-control" placeholder="" value="<?= $take['nombrescfdi']; ?>"id="nombrescfdi" name="nombrescfdi">
      </div>
    </div>
  </div>
  <div class="col-lg-3">
    <div class="input-group">
     <label for="urlp" class="col-md-12 control-label">Apellido Paterno</label>
     <div class="col-md-12">
      <input type="text" data-role="tagsinput" class="form-control" placeholder="" value="<?= $take['apellido1']; ?>" id="apellido1" name="apellido1">
    </div>
  </div>
</div>
<div class="col-lg-3">
  <div class="input-group">
   <label for="urlp" class="col-md-12 control-label">Apellido Materno</label>
   <div class="col-md-12">
    <input type="text" data-role="tagsinput" class="form-control" placeholder="" value="<?= $take['apellido2']; ?>" id="apellido2" name="apellido2">
  </div>
</div>
</div>
<div class="col-lg-3">
  <div class="input-group">
   <label for="urlp" class="col-md-12 control-label">eMail</label>
   <div class="col-md-12">
    <input type="text" data-role="tagsinput" class="form-control" placeholder="" value="<?= $take['email']; ?>" id="email" name="email">
  </div>
</div>
</div>
</div>
</div>

<div class="form-group">
   <div class="row">
    <div class="col-lg-3">
      <div class="input-group">
       <label for="urlp" class="col-md-12 control-label">Teléfono</label>
       <div class="col-md-12">
        <input type="text" class="form-control" placeholder="" value="<?= $take['tel']; ?>" id="tel" name="tel">
      </div>
    </div>
  </div>
  <div class="col-lg-3">
    <div class="input-group">
     <label for="urlp" class="col-md-12 control-label">Celular</label>
     <div class="col-md-12">
      <input type="text" data-role="tagsinput" class="form-control" placeholder="" value="<?= $take['cel']; ?>"accept=" " id="cel" name="cel">
    </div>
  </div>
</div>
<div class="col-lg-3">
  <div class="input-group">
   <label for="url" class="col-md-12 control-label">Fecha de Nacimiento</label>
   <div class="col-md-12">
    <div class="input-group">
      <div class="input-group-prepend">
        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
      </div>
      <input type="text" class="form-control" value="<?= $take['fechanaci']; ?>" id="fechanaci" name="fechanaci" >
    </div>
  </div>
</div>
</div>
<div class="col-lg-3">
  <div class="input-group">
   <label for="urlp" class="col-md-12 control-label">Entidad Nacimiento</label>
   <div class="col-md-12">
    <input type="text" data-role="tagsinput" class="form-control" placeholder="" value="<?= $take['entidadnaci']; ?>" id="entidadnaci" name="entidadnaci">
  </div>
</div>
</div>
</div>
</div>

<div class="form-group">
   <div class="row">
    <div class="col-lg-3">
      <div class="input-group">
       <label for="urlp" class="col-md-12 control-label">Actividad</label>
       <div class="col-md-12">
        <input type="text" class="form-control" placeholder="" value="<?= $take['actividad']; ?>" id="actividad" name="actividad">
      </div>
    </div>
  </div>
  <div class="col-lg-3">
    <div class="input-group">
     <label for="urlp" class="col-md-12 control-label">CURP</label>
     <div class="col-md-12">
      <input type="text" data-role="tagsinput" class="form-control" placeholder="" value="<?= $take['curp']; ?>" id="curp" name="curp">
    </div>
  </div>
</div>
<div class="col-lg-3">
  <div class="input-group">
   <label for="urlp" class="col-md-12 control-label">País</label>
   <div class="col-md-12">
    <input type="text" data-role="tagsinput" class="form-control" placeholder="" value="<?= $take['pais']; ?>" id="pais" name="pais">
  </div>
</div>
</div>
</div>
</div>

</div>
</div>

<div class="card card-default">

 <div class="card-header">
  <div class="d-inline-block">
    <h3 class="card-title"> Solicitud de Cheque</h3>
  </div>
</div>

<div class="card-body">

  <div class="form-group">
   <div class="row">
    <div class="col-lg-3">
      <div class="input-group">
       <label for="urlp" class="col-md-12 control-label">Banco</label>
       <div class="col-md-12">
        <input type="text" class="form-control" placeholder="" value="<?= $take['banco']; ?>" id="banco" name="banco">
      </div>
    </div>
  </div>
  <div class="col-lg-3">
    <div class="input-group">
     <label for="urlp" class="col-md-12 control-label">Cuenta</label>
     <div class="col-md-12">
      <input type="text" data-role="tagsinput" class="form-control" placeholder="" value="<?= $take['cuenta']; ?>" id="cuenta" name="cuenta">
    </div>
  </div>
</div>
<div class="col-lg-3">
  <div class="input-group">
   <label for="urlp" class="col-md-12 control-label">Clabe</label>
   <div class="col-md-12">
    <input type="text" data-role="tagsinput" class="form-control" placeholder="" value="<?= $take['clabe']; ?>" id="clabe" name="clabe">
  </div>
</div>
</div>
<div class="col-lg-3">
  <div class="input-group">
   <label for="urlp" class="col-md-12 control-label">Sucursal</label>
   <div class="col-md-12">
    <input type="text" data-role="tagsinput" class="form-control" placeholder="" value="<?= $take['sucursal']; ?>" id="sucursal" name="sucursal">
  </div>
</div>
</div>
</div>
</div>

  <div class="form-group">
   <div class="row">
    <div class="col-lg-3">
      <div class="input-group">
       <label for="urlp" class="col-md-12 control-label">Convenio</label>
       <div class="col-md-12">
        <input type="text" class="form-control" placeholder="" value="<?= $take['convenio']; ?>" id="convenio" name="convenio">
      </div>
    </div>
  </div>
  <div class="col-lg-3">
    <div class="input-group">
     <label for="urlp" class="col-md-12 control-label">Referencia</label>
     <div class="col-md-12">
      <input type="text" class="form-control" placeholder="" value="<?= $take['referencia']; ?>" id="referencia" name="referencia">
    </div>
  </div>
</div>
<div class="col-lg-3">
  <div class="input-group">
   <label for="urlp" class="col-md-12 control-label">Folio CFDI</label>
   <div class="col-md-12">
    <input type="text" class="form-control" placeholder="" value="<?= $take['foliocfdi']; ?>" id="foliocfdi" name="foliocfdi">
  </div>
</div>
</div>
 
</div>
</div>

</div>
</div>




  


<div class="card card-default">
  <div class="card-body">
    <div class="form-group">
      <div class="col-md-12">
        <input type="submit" name="submit" value="<?= trans('update_take') ?>" class="btn btn-primary pull-right">
      </div>
    </div>
    <?php echo form_close( ); ?>
  </div>
</div>

</section> 
</div>




<!-- Select2 -->
<script src="<?= base_url() ?>assets/plugins/select2/select2.full.min.js"></script>
<!-- InputMask -->
<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src="<?= base_url() ?>assets/plugins/daterangepicker/daterangepicker.js"></script>
<!-- bootstrap color picker -->
<script src="<?= base_url() ?>assets/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="<?= base_url() ?>assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- Sweet Alert -->
<script src="<?= base_url() ?>assets/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Page script -->
<!-- iCheck 1.0.1 -->
<script src="<?= base_url() ?>assets/plugins/iCheck/icheck.min.js"></script>

<script language="javascript">

  jQuery('#fechaidenti, #fechafac, #fechabaja, #fechafacfinal, #fechatoma, #fechacontrato, #fechanaci').datepicker({
    format: 'yyyy-mm-dd'
  });

</script>

<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker         : true,
      timePickerIncrement: 30,
      format             : 'MM/DD/YYYY h:mm A'
    })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
    {
      ranges   : {
        'Today'       : [moment(), moment()],
        'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month'  : [moment().startOf('month'), moment().endOf('month')],
        'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      },
      startDate: moment().subtract(29, 'days'),
      endDate  : moment()
    },
    function (start, end) {
      $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
    }
    )

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>

<script>
  $("#forms").addClass('active');
  $("#advanced").addClass('active');
</script> 

