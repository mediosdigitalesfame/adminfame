<?php

date_default_timezone_set('America/Mexico_City');
   
   $fecha = $take['fechacontrato']; 

     $month = date('M', strtotime($fecha));
     $year = date('Y', strtotime($fecha));
     $day = date('d', strtotime($fecha));

    $partes = explode('-', $month); 
    $_fecha = "$year.$month.$day";


$html = '
<table border="0" style="width:100%">
	<thead>
		<tr class="headerrow">
		<th></th>
		<th></th>
		<th></th>
		</tr>
	</thead>
	<tbody>';

	$subtable = '
		<table border="0" cellspacing="6" cellpadding="4">
		<tr>
			<td>a</td>
			<td>b</td>
			</tr><tr>
			<td>c</td>
			<td>d</td>
		</tr>
		</table>';

		$html = ' 
		<table border="0" cellspacing="3" cellpadding="4">

		<tr>
    <th></th>
    
    <th align="right">'.$take['agciudad'].', ' .$take['agestado'].' a: '.$day.' de ' .$month.' del ' .$year.'</th>

  </tr>

		<tr>
		<td colspan="2">
			A QUIEN CORRESPONDA:<br><br><br><br><br>

			

			Por medio de la presente le hago constar que los documentos recibidos de este vehiculo son autenticos y legales ya que se realizó una investigación de dicha unidad. Cualquier duda o alaración, quedo a sus órdenes.
			<br><br><br><br><br><br><br><br><br>

			<table>
				<tr>
					<td></td>
					<td align="right">
						<b>
							Marca vehiculo:<br>
							Modelo:<br>
							Año modelo:<br>
							Tipo:<br>
							Numero de serie:<br>
							No. Motor:
						</b>
					</td>
					<td align="left">																						
						<font color="#0A6ACF">'.$take['brname'].'</font><br>																	
						<font color="#0A6ACF">'.$take['modelotxt'].'</font><br>																	
						<font color="#0A6ACF">'.$take['anname'].'</font><br>
						<font color="#0A6ACF">'.$take['versiontxt'].'</font><br>									
						<font color="#0A6ACF">'.$take['nserie'].'</font><br>
						<font color="#0A6ACF">'.$take['nmotor'].'</font></td>
					<td></td>
				</tr>
		    </table>
				<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>

			<table align="center">
				<tr>
					<td></td>
					<td>
						________________________________
						<b><font color="#0A6ACF">'.$take['ciadfirstnamegs'].' '.$take['ciadlastnamegs'].'</font><br>Gerente de Seminuevos</b><br><br>
					</td>
					<td></td>
				</tr>
			</table>

		</td>
		</tr>';
		
	$html .='</tbody> </table>';

tcpdf();
$obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
header('Content-type: application/pdf');
$obj_pdf->SetCreator(PDF_CREATOR);
$title = "Autentificación de Documentos";
$obj_pdf->SetTitle($title);
$obj_pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $title, PDF_HEADER_STRING);
$obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$obj_pdf->SetDefaultMonospacedFont('helvetica');
$obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$obj_pdf->SetFont('helvetica', '', 9);
$obj_pdf->setFontSubsetting(false);
$obj_pdf->AddPage();
ob_start();
    // podemos tener cualquier parte de la vista aquí como HTML, PHP, etc.
$content = ob_get_contents();
ob_end_clean();
$obj_pdf->writeHTML($content, true, false, true, false, '');
$obj_pdf->writeHTML($html, true, false, true, false, '');

$filename = 'autentificacion_documentos';
$obj_pdf->Output($filename . '.pdf', 'D');
?>