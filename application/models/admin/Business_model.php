<?php
class Business_model extends CI_Model{

	public function add_business($data){
		$this->db->insert('business', $data);
		return true;
	}

		//---------------------------------------------------
		// obtener todas los Negocios para el procesamiento de tablas de datos del lado del servidor (basado en ajax)
		//-----------------------------------------------------
	public function get_all_business_brands(){
		$this->db->select('m.*, b.name as bname');
		$this->db->join('brands b','b.id=m.brands_id', 'LEFT');
			//$this->db->where('status',1);
		return $this->db->get('business m')->result_array();
	}

		//---------------------------------------------------
		// obtener todas los Negocios con sus marcas para el procesamiento de tablas de datos del lado del servidor (basado en ajax)
		//-----------------------------------------------------
	public function get_all_business(){
		$this->db->select('*');
			//$this->db->where('status',1);
		return $this->db->get('business')->result_array();
	}

		//---------------------------------------------------
		// Obtener información de Negocio por ID
		//-----------------------------------------------------
	public function get_business_by_id($id){
		$query = $this->db->get_where('business', array('id' => $id));
		return $result = $query->row_array();
	}

		//---------------------------------------------------
		// Editar registro de Negocio
		//-----------------------------------------------------
	public function edit_business($data, $id){
		$this->db->where('id', $id);
		$this->db->update('business', $data);
		return true;
	}

		//---------------------------------------------------
		// Cambiar status de Negocio
		//-----------------------------------------------------
	function change_status()
	{		
		$this->db->set('status', $this->input->post('status'));
		$this->db->where('id', $this->input->post('id'));
		$this->db->update('business');
	} 

		//---------------------------------------------------
		// obtener marcas para exportar a csv
		//-----------------------------------------------------
	public function get_business_for_csv(){
		$this->db->where('status', 1);
		$this->db->select('id, name, created_at');
		$this->db->from('business');
		$query = $this->db->get();
		return $result = $query->result_array();
	}

		//---------------------------------------------------
		// obtener todos los registros de marcas
		//-----------------------------------------------------
	public function get_all_simple_brands(){
		$this->db->where('status', 1);
		$this->db->order_by('id', 'desc');
		$query = $this->db->get('brands');
		return $result = $query->result_array();
	}

	//---------------------------------------------------
		// obtener todos los registros de años
		//-----------------------------------------------------
	public function get_all_simple_years(){
		$this->db->where('status', 1);
		$this->db->order_by('id', 'desc');
		$query = $this->db->get('anios');
		return $result = $query->result_array();
	}

	//---------------------------------------------------
		// obtener todos los registros de Negocios
		//-----------------------------------------------------
	public function get_all_simple_business(){
		$this->db->where('status', 1);
		$this->db->order_by('id', 'desc');
		$query = $this->db->get('business');
		return $result = $query->result_array();
	}

		//---------------------------------------------------
		// obtener todos los registros de marcas 
		//-----------------------------------------------------
	function get_admin_roles()
	{
		$this->db->from('ci_admin_roles');
		$this->db->where('admin_role_status',1);
		$query=$this->db->get();
		return $query->result_array();
	}

}

?>