  <!-- daterange picker -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/daterangepicker/daterangepicker-bs3.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/colorpicker/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/select2/select2.min.css">

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      <div class="card card-default">

        <div class="card-header">
          <div class="d-inline-block">
            <h3 class="card-title"> <i class="fa fa-plus"></i>
             <?= trans('add_new_guide') ?> </h3>
           </div>
           <div class="d-inline-block float-right">
            <a href="<?= base_url('admin/guides'); ?>" class="btn btn-success"><i class="fa fa-list"></i>  <?= trans('guides_list') ?></a>
          </div>
        </div>

        <div class="card-body">

         <!-- For Messages -->
         <?php $this->load->view('admin/includes/_messages.php') ?>
         <?php echo form_open_multipart(base_url('admin/guides/add'), 'class="form-horizontal"','id="formgu"','name="formgu"');  ?> 
         <?php $permitted_chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'; $iden = '-'.date('dmY').'-'.substr(str_shuffle($permitted_chars), 0, 8); ?>

         <input type="hidden" id="identi" name="identi" value="<?php echo html_escape($iden); ?>">
         <input type="hidden" id="id" name="id" value="">

         <div class="form-group">
           <div class="row">
            <div class="col-lg-3"></div>

            <div class="col-lg-2">
              <div class="input-group">
                <label for="firstname" alling="right" class="pull-right col-md-12 control-label">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Agencia:</label>  
              </div>
            </div>

            <div class="col-lg-3">
              <div class="input-group">
               <div class="col-md-12">
                <select name="agencia" id="agencia" class="form-control select2" onChange="cambiafolio();">
                  <option value=""><?= trans('select_agencys') ?></option>
                  <?php foreach($agencys as $agency): ?>
                    <option value="<?= $agency['id']; ?>"><?= $agency['name']; ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>
          </div>

          <div class="col-lg-1">
            <div class="input-group">
              <label for="firstname" alling="right" class="col-md-12 control-label">&nbsp;&nbsp;&nbsp;&nbsp;Folio:</label>
            </div>
          </div>
          <div class="col-lg-3">
            <div class="input-group">
              <div class="col-md-12">
                <input type="text" id="folio" name="folio" class="form-control" placeholder="" readonly required>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="form-group">
       <div class="row">

        <div class="col-lg-2">
          <div class="input-group">
           <label for="pdudlv7" class="col-md-12 control-label">% <?= trans('utility') ?>*</label>
           <div class="col-md-12">
            <select id="pdudlv7" name="pdudlv7" class="form-control select2">
              <!--<option value=""><?= trans('utility') ?></option>-->
              <?php foreach($utilitys as $utility): ?>
                <option value="<?= $utility['valor']; ?>"><?= html_escape($utility['valor']); ?></option>
              <?php endforeach; ?>
            </select>
          </div>
        </div>
      </div>

      <div class="col-lg-2">
        <div class="input-group">
         <label for="anio" class="col-md-12 control-label">Año*</label>
         <div class="col-md-12">
          <select id="anio" name="anio" class="form-control select2">
            <!--<option value=""><?= trans('utility') ?></option>-->
            <?php foreach($years as $year): ?>
              <option value="<?= $year['id']; ?>"><?= html_escape($year['name']); ?></option>
            <?php endforeach; ?>
          </select>
        </div>
      </div>
    </div>

    <div class="col-lg-2">
      <div class="input-group">
       <label for="marca" class="col-md-12 control-label">Marca*</label>
       <div class="col-md-12">
        <select id="marca" name="marca" class="form-control select2">
          <!--<option value=""><?= trans('utility') ?></option>-->
          <?php foreach($brands as $brand): ?>
            <option value="<?= $brand['id']; ?>"><?= html_escape($brand['name']); ?></option>
          <?php endforeach; ?>
        </select>
      </div>
    </div>
  </div>

  <div class="col-lg-6">
    <div class="input-group">
      <label for="modelo" class="col-md-12 control-label">Modelo</label>
      <div class="col-md-12">
        <input onkeyup="javascript:this.value=this.value.toUpperCase();" type="text" value="<?= old("modelo"); ?>" class="form-control " name="modelo" id="modelo">
        <div class="input-group">

        </div>
      </div>
    </div>
  </div>


      <!--
      <div class="col-lg-2">
        <div class="input-group">
          <label for="marca" class="col-md-12 control-label">Marca <?php echo $ultimo; ?> </label>
          <div class="col-md-12">
            <?php  
            $options = array('' => 'Sel. Marca')+array_column($brands, 'name','id');
            echo form_dropdown('marca',$options,'','class="form-control country select2" ');
            ?>
          </div>
        </div>
      </div>
      <div class="col-lg-2">
        <div class="input-group">
          <label for="anio" class="col-md-12 control-label">Año</label>
          <div class="col-md-12">
            <select class="form-control state select2" id="anio" name="anio">
              <option value="">Sel. Año</option>
            </select>
          </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="input-group">
          <label for="modelo" class="col-md-12 control-label">Modelo</label>
          <div class="col-md-12">
            <select class="form-control city select2" id="modelo" name="modelo">
              <option value="">Selecciona Modelo</option>
            </select>
          </div>
        </div>
      </div>
    -->

  </div>
</div>

<div class="form-group">
 <div class="row">
  <div class="col-lg-4">
    <div class="input-group">
     <label for="vin" class="col-md-12 control-label"><?= trans('vin') ?></label>
     <div class="col-md-12">
      <input onkeyup="javascript:this.value=this.value.toUpperCase();" type="text" value="<?= old("vin"); ?>" class="form-control " name="vin" id="vin">
      <div class="input-group">
       <h4 id="mensajevin" style="color: red;"></h4>
     </div>
   </div>
 </div>
</div>
<div class="col-lg-2">
  <div class="input-group">
   <label for="url" class="col-md-12 control-label"><?= trans('km') ?></label>
   <div class="col-md-12">
    <input type="text" value="<?= old("km"); ?>" class="form-control" placeholder="" id="km" name="km">
  </div>
</div>
</div>
<div class="col-lg-3">
  <div class="input-group">
   <label for="urlp" class="col-md-12 control-label">Compra Guía</label>
   <div class="col-md-12">
    <div class="input-group">
      <div class="input-group-prepend">
        <span class="input-group-text">$</span>
      </div>
      <input type="text" value="<?= old("compraautometrica"); ?>" class="form-control" placeholder="" name="compraautometrica" id="compraautometrica">
    </div>
  </div>
</div>
</div>
<div class="col-lg-3">
  <div class="input-group">
   <label for="urlp" class="col-md-12 control-label">Venta Guía</label>
   <div class="col-md-12">
    <div class="input-group">
      <div class="input-group-prepend">
        <span class="input-group-text">$</span>
      </div>
      <input type="text" value="<?= old("ventaautometrica"); ?>"class="form-control" placeholder="" id="ventaautometrica" name="ventaautometrica">
    </div>
  </div>
</div>
</div>
</div>
</div>

<div class="form-group">
 <div class="row">
  <div class="col-lg-3">
    <div class="input-group">
     <label for="urlp" class="col-md-12 control-label">Equipamiento</label>
     <div class="col-md-12">
      <div class="input-group">
        <div class="input-group-prepend">
          <span class="input-group-text">$</span>
        </div>
        <input type="text" value="<?= old("equipamiento"); ?>" id="equipamiento" name="equipamiento" class="form-control" placeholder="">
      </div>
    </div>
  </div>
</div>
<div class="col-lg-3">
  <div class="input-group">
    <label for="tel" class="col-md-12 control-label">Premio/castigo</label>
    <div class="col-md-12">
      <div class="input-group">
        <div class="input-group-prepend">
          <span class="input-group-text">$</span>
        </div>
        <input type="text" value="<?= old("premio"); ?>" id="premio" name="premio" class="form-control" placeholder="">
      </div>
    </div>
  </div>
</div>
<div class="col-lg-3">
  <div class="input-group">
    <label for="calle" class="col-md-12 control-label">Reacondicionamiento</label>
    <div class="col-md-12">
      <div class="input-group">
        <div class="input-group-prepend">
          <span class="input-group-text">$</span>
        </div>
        <input type="text" value="<?= old("reacondicionamiento"); ?>"name="reacondicionamiento" id="reacondicionamiento" class="form-control" placeholder="">
      </div>
    </div>
  </div>
</div>
<div class="col-lg-3">
  <div class="input-group">
   <label for="int" class="col-md-12 control-label">Valor Coche</label>
   <div class="col-md-12">
    <div class="input-group">
      <div class="input-group-prepend">
        <span class="input-group-text">$</span>
      </div>
      <input type="text" value="<?= old("valor"); ?>" name="valor" id="valor" class="form-control" placeholder="" onblur="cambiaValores()">
    </div>
  </div>
</div>
</div>
</div>
</div>

<div class="form-group">
 <div class="row">
  <div class="col-lg-3">
    <div class="input-group">
     <label for="agency" class="col-md-12 control-label">Versión de Guía*</label>
     <div class="col-md-12">
      <select id="tipoguia" name="tipoguia" onChange="mostrar(this.value),cambiaValores();" class="form-control select2" >
        <option value="">Tipo de Guía</option>
        <?php foreach($tguias as $tguia): ?>
          <option value="<?= $tguia['id']; ?>"> <?= $tguia['version'].' '.$tguia['persona']; ?></option>
        <?php endforeach; ?>
      </select>
    </div>
  </div>
</div>

<div class="col-lg-5">
  <div class="input-group">
    <label class="col-md-12 control-label">Imágen Autometrica</label> 
    <div class="form-group">
      <td>
       <input type="file" name="image" accept=".png, .jpg, .jpeg, .gif, .svg">
       <p><small class="text-success"><?= trans('allowed_types') ?>: gif, jpg, png, jpeg</small></p>

     </td>
   </div>
 </div>
</div>

<div class="col-lg-3">
  <div class="input-group">
    <!--<label class="col-md-12 control-label">Imágen Autometrica</label> -->
    <?php if(!empty($guide['image'])): ?>
     <p><img src="<?= base_url($guide['image']); ?>" class="logo" width="150"></p>
   <?php endif; ?>
 </div>
</div>

</div>
</div>

<div class="row m-t-30">
  <!-- <h4>Costos</h4> -->
  <div class="row m-t-30" id="x1" style="display: flex;">
    <div class="col-sm-6" >
      <div class="form-group">
       <!-- <label>Precio de Venta sin Reacondicionamiento</label>-->
       <input type="hidden" class="form-control" name="pdvapsr1" id="pdvapsr1" value="" readonly ><p class="help-block text-danger"></p>
       <input type="hidden" id="pdvapsr1a" name="pdvapsr1a" value="">
     </div>
   </div>
   <div class="col-sm-4">
    <div class="form-group">
      <!--<label>Precio de la Toma tope al cliente con IVA</label>-->
      <input type="hidden" class="form-control" name="pdlttfac2" id="pdlttfac2" readonly value="" ><p class="help-block text-danger"></p>
      <input type="hidden" id="pdlttfac2a" name="pdlttfac2a" value="">
    </div>
  </div>
  <div class="col-sm-4">
    <div class="form-group">
      <!--<label>Utilidad de la Venta</label>-->
      <input type="hidden" class="form-control" name="udlv6" id="udlv6" readonly value=""><p class="help-block text-danger"></p>
      <input type="hidden" id="udlv6a" name="udlv6a" value="">
    </div>
  </div>
</div>
<div class="row m-t-30" id="x2" style="display: 'flex';">
  <div class="col-sm-3">
    <div class="form-group">
     <!-- <label>Precio de Venta al Publico sin IVA</label>-->
     <input type="hidden" class="form-control" name="pdvapsi4" id="pdvapsi4" readonly value="" ><p class="help-block text-danger"></p>
     <input type="hidden" id="pdvapsi4a" name="pdvapsi4a" value="">
   </div>
 </div>
 <div class="col-sm-6">
  <div class="form-group">
   <!-- <label>Precio de Venta al Publico con IVA y Reacondicionamiento</label>-->
   <input type="hidden" class="form-control" name="pdvapciyr5" id="pdvapciyr5" readonly value="" ><p class="help-block text-danger"></p>
   <input type="hidden" id="pdvapciyr5a" name="pdvapciyr5a" value="">
 </div>
</div>
<div class="col-sm-3">
  <div class="form-group">
    <!--<label>Costo Real</label>-->
    <input type="hidden" class="form-control" name="crpdltsi3" id="crpdltsi3" readonly value="" ><p class="help-block text-danger"></p>
    <input type="hidden" id="crpdltsi3a" name="crpdltsi3a" value="">
  </div>
</div>
</div>
<div class="row m-t-30" id="x3" style="display: 'flex';">
  <div class="col-sm-4">
    <div class="form-group">
     <!-- <label>Precio Minimo de Venta al Publico Reacondicionado</label>-->
     <input type="hidden" class="form-control" name="pmdvap8" id="pmdvap8" readonly value="" ><p class="help-block text-danger"></p>
     <input type="hidden" id="pmdvap8a" name="pmdvap8a" value="">
   </div>
 </div>
 <div class="col-sm-4">
  <div class="form-group">
    <!--<label>Costo Compra con Reacondicionamiento</label>-->
    <input type="hidden" class="form-control" name="cccr9" id="cccr9" readonly value="" ><p class="help-block text-danger"></p>
    <input type="hidden" id="cccr9a" name="cccr9a" value="">
  </div>
</div>
<div class="col-sm-4">
  <div class="form-group">
    <!--<label>Precio de Venta al Publico</label>-->
    <input type="hidden" class="form-control" name="pdvap10" id="pdvap10" readonly value="" ><p class="help-block text-danger"></p>
    <input type="hidden" id="pdvap10a" name="pdvap10a" value="">
  </div>
</div>
</div>
<div class="row m-t-30" id="x4" style="display:'flex';">
  <div class="col-sm-4">
    <div class="form-group">
      <!--<label>Utilidad con IVA de la Utilidad</label>-->
      <input type="hidden" class="form-control" name="ucidlu11" id="ucidlu11" readonly value=""><p class="help-block text-danger"></p>
      <input type="hidden" id="ucidlu11a" name="ucidlu11a" value="">
    </div>
  </div>
  <div class="col-sm-3">
    <div class="form-group">
     <!-- <label>IVA de la Utilidad</label>-->
     <input type="hidden" class="form-control" name="idlu12" id="idlu12" readonly value="" ><p class="help-block text-danger"></p>
     <input type="hidden" id="idlu12a" name="idlu12a" value="">
   </div>
 </div>
 <div class="col-sm-3">
  <div class="form-group">
    <!--<label>Factor de Utilidad Minima Esperada</label>-->
    <input type="hidden" class="form-control" name="fdume13" id="fdume13" readonly value="" ><p class="help-block text-danger"></p>
    <input type="hidden" id="fdume13a" name="fdume13a" value="">
  </div>
</div>
</div>
<input type="hidden" id="id" name="id" value="">
<input type="hidden" id="idmarca" name="idmarca" value="">
<input type="hidden" id="idanio" name="idanio" value="">
<input type="hidden" id="idmodelo" name="idmodelo" value="">
<!-- csrf token -->
<input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
</div> <!-- div class row 30 -->


</div>
<!-- /.box-body -->
</div>

<div class="card card-default">
  <div class="card-body">
    <div class="form-group">
     <div class="row">

      <div class="col-md-1"></div>

      <div class="col-md-10">
        <div class="card-body" >

          <div class="row" style="display: 'flex';">
            <div class="col-lg-3"></div>
            <div class="col-lg-6">
              <div class="input-group">
               <h4 id="msjr" style="color: green;"></h4>
             </div>
           </div>
         </div>

         <div class="callout callout-danger" >
          <div class="row" id="xc1" style="display: 'flex';">
            <div class="col-lg-9">
              <div class="input-group">
               <label class="col-md-12 control-label">Precio Minimo de Venta al Publico Reacondicionado</label>
             </div>
           </div>
           <div class="col-lg-3">
            <div class="input-group">
             <h3> <p id="premindeven" style="color: black;">$</p></h3>
           </div>
         </div>
       </div>
     </div>
     <div class="callout callout-success">
      <div class="row" id="xa2" style="display: flex;">
        <div class="col-lg-9">

          <div class="input-group">
           <label class="col-md-12 control-label">Precio de la toma tope al cliente con iva</label>
         </div>
       </div>
       <div class="col-lg-3">
        <div class="input-group">
         <h3> <p id="predetoma" style="color: black;">$</p></h3>
       </div>
     </div>
   </div>
 </div>
 <div class="callout callout-success">
  <div class="row" id="xb2" style="display:'flex';">
    <div class="col-lg-9">
      <div class="input-group">
       <label class="col-md-12 control-label">Precio de venta al publico con iva y reacondicionamiento</label>
     </div>
   </div>
   <div class="col-lg-3">
    <div class="input-group">
     <h3> <p id="predevencon" style="color: black;">$</p></h3>
   </div>
 </div>
</div>
</div>
</div>
</div>

<div class="col-md-1"></div>
<div class="col-md-2"></div>

<div class="col-md-8">
  <div class="card-body">
    <div class="callout callout-info">

      <div class="form-group">
        <div class="row" id="xa1" style="display: flex;">
          <div class="col-lg-9">
            <div class="input-group">
              <p>Precio de venta sin reacondicionamiento:</p>
            </div>
          </div>
          <div class="col-lg-3">
            <div class="input-group">
              <p id="predeventa" style="color: black;">$</p>
            </div>
          </div>
        </div>
        <div class="row" id="xc6" style="display: 'flex';">
          <div class="col-lg-9">
            <div class="input-group">
              <p>Factor de utilidad minima esperada:</p>
            </div>
          </div>
          <div class="col-lg-3">
            <div class="input-group">
              <p id="facdeu" style="color: black;">$</p>
            </div>
          </div>
        </div>
        <div class="row" id="xb3" style="display: 'flex';">
          <div class="col-lg-9">
            <div class="input-group">
              <p>Costo real:</p>
            </div>
          </div>
          <div class="col-lg-3">
            <div class="input-group">
              <p id="ctoreal" style="color: black;">$</p>
            </div>
          </div>
        </div>
        <div class="row" id="xb1" style="display: 'flex';">
          <div class="col-lg-9">
            <div class="input-group">
              <p>Precio de venta al publico sin iva:</p>
            </div>
          </div>
          <div class="col-lg-3">
            <div class="input-group">
              <p id="predevensin" style="color: black;">$</p>
            </div>
          </div>
        </div>
        <div class="row" id="xc2" style="display:'flex';">
          <div class="col-lg-9">
            <div class="input-group">
              <p>Costo compra con reacondicionamiento:</p>
            </div>
          </div>
          <div class="col-lg-3">
            <div class="input-group">
              <p id="coconrea" style="color: black;">$</p>
            </div>
          </div>
        </div>
        <div class="row" id="xc3" style="display: 'flex';">
          <div class="col-lg-9">
            <div class="input-group">
              <p>Precio de venta al publico:</p>
            </div>
          </div>
          <div class="col-lg-3">
            <div class="input-group">
              <p id="predevenpu" style="color: black;">$</p>
            </div>
          </div>
        </div>
        <div class="row" id="xc4" style="display: 'flex';">
          <div class="col-lg-9">
            <div class="input-group">
              <p>Utilidad con IVA de la utilidad:</p>
            </div>
          </div>
          <div class="col-lg-3">
            <div class="input-group">
              <p id="uconi" style="color: black;">$</p>
            </div>
          </div>
        </div>
        <div class="row" id="xc5" style="display: 'flex';">
          <div class="col-lg-9">
            <div class="input-group">
              <p>IVA de la utilidad:</p>
            </div>
          </div>
          <div class="col-lg-3">
            <div class="input-group">
              <p id="idelau" style="color: black;">$</p>
            </div>
          </div>
        </div>
        <div class="row" id="xa3" style="display: flex;">
          <div class="col-lg-9">
            <div class="input-group">
              <p>Utilidad de la venta:</p>
            </div>
          </div>
          <div class="col-lg-3">
            <div class="input-group">
              <p id="udelaven" style="color: black;">$</p>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>
</div>

<div class="card card-default">
  <div class="card-body">
    <div class="form-group">
      <div class="col-md-12" name="botones" id="botones" style="display: flex;">
        <input type="submit" name="submit" id="submit" value="<?= trans('add_guide') ?>" class="btn btn-primary pull-right">
      </div>
    </div>
    <?php echo form_close( ); ?>
  </div>
</div>

</section> 
</div>

<!-- Select2 -->
<script src="<?= base_url() ?>assets/plugins/select2/select2.full.min.js"></script>
<!-- InputMask -->
<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.js"></script>

<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>

<script src="<?= base_url() ?>assets/plugins/input-mask/inputmask.js"></script>
<script src="<?= base_url() ?>assets/plugins/input-mask/inputmask.min.js"></script>
<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.min.js"></script>

<!-- date-range-picker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src="<?= base_url() ?>assets/plugins/daterangepicker/daterangepicker.js"></script>
<!-- bootstrap color picker -->
<script src="<?= base_url() ?>assets/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="<?= base_url() ?>assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- Page script -->
<!-- iCheck 1.0.1 -->
<script src="<?= base_url() ?>assets/plugins/iCheck/icheck.min.js"></script>

<script>
 // ! function(window, document, $) {
  //  "use strict";
 //   $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
 // }(window, document, jQuery);
</script>

<script>

 $("#compraautometrica,#ventaautometrica,#reacondicionamiento,#equipamiento,#premio,#valor").inputmask("currency",{
   numericInput: true,
   rightAlign: true,
   digitsOptional: true,
   prefix:'',
   inputFormat: "999,999,999.99",
   outputFormat: "999999999.99",
   inputMode:"numeric",
   inputEventOnly: true,
   removeMaskOnSubmit: true,
   }); //default


 $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker         : true,
      timePickerIncrement: 30,
      format             : 'MM/DD/YYYY h:mm A'
    })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
    {
      ranges   : {
        'Today'       : [moment(), moment()],
        'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month'  : [moment().startOf('month'), moment().endOf('month')],
        'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      },
      startDate: moment().subtract(29, 'days'),
      endDate  : moment()
    },
    function (start, end) {
      $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
    }
    )

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>

<script>
  $("#forms").addClass('active');
  $("#advanced").addClass('active');
</script>  

<script>
//-------------------------------------------------------------------
// Scrip para cambiar el año y los modelos 
//-------------------------------------------------------------------
var csfr_token_name = '<?php echo $this->security->get_csrf_token_name(); ?>';
var csfr_token_value = '<?php echo $this->security->get_csrf_hash(); ?>';


$(function(){

  $(document).on('change','.country',function()
  {
    if(this.value == '')
    {
      $('.state').html('<option value="">Select Option</option>');
      $('.city').html('<option value="">Select Option</option>');
      return false;
    }

    var data =  {
      country : this.value,
    }
    data[csfr_token_name] = csfr_token_value;
    $.ajax({
      type: "POST",
      url: "<?= base_url('admin/auth/get_brands_years') ?>",
      data: data,
      dataType: "json",
      success: function(obj) {
        $('.state').html(obj.msg);
      },
    });
  });

  $(document).on('change','.state',function()
  {
    var data =  {
      state : this.value,
    }

    data[csfr_token_name] = csfr_token_value;
    $.ajax({
      type: "POST",
      url: "<?= base_url('admin/auth/get_years_models') ?>",
      data: data,
      dataType: "json",
      success: function(obj) {
        $('.city').html(obj.msg);
      },

    });
  });
});
</script>

<script>
  function cambiafolio() {

    var a1 = document.getElementById("agencia").value;
    var a2 = document.getElementById("identi").value;
    var folio = document.getElementById("folio");

    switch (a1)
    {
      case '1': 
      folio.value = "GPF"+a2;
      break;
      case '2':
      folio.value = "NIA"+a2;
      break;
      case '3':
      folio.value = "NAL"+a2;
      break;
      case '4':
      folio.value = "HMN"+a2;
      break;
      case '5':
      folio.value = "HAT"+a2;
      break;
      case '6':
      folio.value = "HDF"+a2;
      break;
      case '7':
      folio.value = "HAL"+a2;
      break;
      case '8':
      folio.value = "HUR"+a2;
      break;
      case '9':
      folio.value = "HMM"+a2;
      break;
      case '10':
      folio.value = "HCO"+a2;
      break;
      case '11':
      folio.value = "HMR"+a2;
      break;
      case '12':
      folio.value = "HSJ"+a2;
      break;
      case '13':
      folio.value = "KPE"+a2;
      break;
      case '14':
      folio.value = "KUR"+a2;
      break;
      case '15':
      folio.value = "KMC"+a2;
      break;
      case '16':
      folio.value = "KDU"+a2;
      break;
      case '17':
      folio.value = "CMM"+a2;
      break;
      case '18':
      folio.value = "CLZ"+a2;
      break;
      case '19':
      folio.value = "CUR"+a2;
      break;
      case '20':
      folio.value = "CAP"+a2;
      break;
      case '21':
      folio.value = "CZA"+a2;
      break;
      case '22':
      folio.value = "TUR"+a2;
      break;
      case '23':
      folio.value = "TAL"+a2;
      break;
      case '24':
      folio.value = "TVA"+a2;
      break;
      case '25':
      folio.value = "TPE"+a2;
      break;
      case '26':
      folio.value = "MQR"+a2;
      break;
      case '27':
      folio.value = "MUR"+a2;
      break;
      case '28':
      folio.value = "GMC"+a2;
      break;
      case '29':
      folio.value = "CAU"+a2;
      break;
      case '30':
      folio.value = "BMW"+a2;
      break;
      case '31':
      folio.value = "MOT"+a2;
      break;
      case '32':
      folio.value = "MIN"+a2;
      break;
      case '33':
      folio.value = "CHQ"+a2;
      break;
      case '34':
      folio.value = "CHU"+a2;
      break;
      case '35':
      folio.value = "ACJ"+a2;
      break;
      case '36':
      folio.value = "ISU"+a2;
      break;
      case '37':
      folio.value = "VW"+a2;
      break;
      case '38':
      folio.value = "DWA"+a2;
      break;
      case '39':
      folio.value = "SDA"+a2;
      break;
      case '40':
      folio.value = "MM"+a2;
      break;
      case '41':
      folio.value = "NTF"+a2;
      break;
      case '42':
      folio.value = "ACQ"+a2;
      break;
    }
  }

  function cambiaValores() {

    var ver = document.getElementById("ver");

    var input1 = document.getElementById("pdvapsr1");
    var input2 = document.getElementById("pdlttfac2");
    var input3 = document.getElementById("crpdltsi3");
    var input4 = document.getElementById("pdvapsi4");
    var input5 = document.getElementById("pdvapciyr5");
    var input6 = document.getElementById("udlv6");
    var input8 = document.getElementById("pmdvap8");
    var input9 = document.getElementById("cccr9");
    var input10 = document.getElementById("pdvap10");
    var input11 = document.getElementById("ucidlu11");
    var input12 = document.getElementById("idlu12");
    var input13 = document.getElementById("fdume13");

    var input1a = document.getElementById("pdvapsr1a");
    var input2a = document.getElementById("pdlttfac2a");
    var input3a = document.getElementById("crpdltsi3a");
    var input4a = document.getElementById("pdvapsi4a");
    var input5a = document.getElementById("pdvapciyr5a");
    var input6a = document.getElementById("udlv6a");
    var input8a = document.getElementById("pmdvap8a");
    var input9a = document.getElementById("cccr9a");
    var input10a = document.getElementById("pdvap10a");
    var input11a = document.getElementById("ucidlu11a");
    var input12a = document.getElementById("idlu12a");
    var input13a = document.getElementById("fdume13a");

    Tguia = document.getElementById("tipoguia").value;

    var vala = document.getElementById("valor").value;
    var compa = document.getElementById("compraautometrica").value;
    var venta = document.getElementById("ventaautometrica").value;
    var equia = document.getElementById("equipamiento").value;
    var prema = document.getElementById("premio").value;
    var reaa = document.getElementById("reacondicionamiento").value;

    var vala = vala.replace(",", "");
    var compa = compa.replace(",", "");
    var venta = venta.replace(",", "");
    var equia = equia.replace(",", "");
    var prema = prema.replace(",", "");
    var reaa = reaa.replace(",", "");
    var vala = vala.replace("$", "");
    var compa = compa.replace("$", "");
    var venta = venta.replace("$", "");
    var equia = equia.replace("$", "");
    var prema = prema.replace("$", "");
    var reaa = reaa.replace("$", "");

    var val =  parseFloat(vala);
    var comp =  parseFloat(compa);
    var vent =  parseFloat(venta);
    var equi =  parseFloat(equia);
    var prem =  parseFloat(prema);
    var rea =  parseFloat(reaa);

    var d16 = parseFloat(comp);
    var d17 = parseFloat(vent);
    var d18 = parseFloat(equi);
    var d19 = parseFloat(prem);
    var d20 = parseFloat(rea);

    acucom = d16 + d18 + d19 + d20;
    acuven = d17 + d18 + d19 + d20;
    acupno = ((d16+d17)/2) + d18 + d19 + d20;
    acupsi = ((d16+d17)/2) + d18 + d19;

    pdudlv7 = document.getElementById("pdudlv7").value;
    //porutilidad2 = $("#pdudlv7 option:selected").text();
    //var xguia = $('#tipoguia').val();
    //if(val === ''){ swal("Detalle!", "Introduce el Valor del Vehiculo!", "warning") }  
    //if(rea > 1){ swal("Detalle!", "Introduce un valor negativo en Reacondicionamiento!", "warning") }

    switch (Tguia)
    {
      case '1':  
      pdvapsr1 = val-(rea*-1); //val-rea;
      pdlttfac2 = pdvapsr1*(1-(pdudlv7/100));
      crpdltsi3 = pdlttfac2/(1+(16/100));
      pdvapsi4 = pdvapsr1/(1+(16/100));
      pdvapciyr5 =  pdvapsi4*(1+(16/100))-rea;
      udlv6 = pdvapsi4-crpdltsi3;
      // pdudlv77 = (udlv6/pdvapsi4)*100;
      pmdvap8 = 0;
      cccr9 = 0;
      pdvap10 = 0;
      ucidlu11 = 0;
      idlu12 = 0;
      fdume13 = 0;

      if(isNaN(pdvapsr1)){pdvapsr1=0}
        if(isNaN(pdlttfac2)){pdlttfac2=0}
          if(isNaN(crpdltsi3)){crpdltsi3=0}
            if(isNaN(pdvapsi4)){pdvapsi4=0}
              if(isNaN(pdvapciyr5)){pdvapciyr5=0}
                if(isNaN(udlv6)){udlv6=0}
                  if(isNaN(pmdvap8)){pmdvap8=0}
                    if(isNaN(cccr9)){cccr9=0}
                      if(isNaN(pdvap10)){pdvap10=0}
                        if(isNaN(ucidlu11)){ucidlu11=0}
                          if(isNaN(idlu12)){idlu12=0}
                            if(isNaN(fdume13)){fdume13=0}

                              if(pdlttfac2 > acucom ){ 
                                alert("El precio de la Toma no puede ser superior al PRECIO DE COMPRA de la Guía Autometrica!")
        //swal("Error!", "El precio de la Toma no puede ser superior al precio de la Guía Autometrica");
        msjerror = "Incorrecto, Favor de Corregir"; 
        $("#botones").hide();
      }  else{msjerror = ""; $("#botones").show();}
      break;  
      case '2': 
      pdvapsr1 = val-(rea*-1);
      pdlttfac2 = pdvapsr1*(1-(pdudlv7/100));
      crpdltsi3 = pdlttfac2/(1+(16/100));
      pdvapsi4 = pdvapsr1/(1+(16/100));
      pdvapciyr5 =  pdvapsi4*(1+(16/100))-rea;
      udlv6 = pdvapsi4-crpdltsi3;
      // pdudlv77 = (udlv6/pdvapsi4)*100; 
      pmdvap8 = 0;
      cccr9 = 0;
      pdvap10 = 0;
      ucidlu11 = 0;
      idlu12 = 0;
      fdume13 = 0;

      if(pdlttfac2 > acuven ){ 
        alert("El precio de la Toma no puede ser superior al precio de la Guía Autometrica!")
        //swal("Error!", "El precio de la Toma no puede ser superior al precio de la Guía Autometrica");
        msjerror = "Incorrecto, Favor de Corregir"; 
        $("#botones").hide();
      }  else{msjerror = ""; $("#botones").show();}
      break;  
      case '3': 
      pdvapsr1 = val-(rea*-1); //val+rea;
      pdlttfac2 = pdvapsr1*(1-(pdudlv7/100));
      crpdltsi3 = pdlttfac2/(1+(16/100));
      pdvapsi4 = pdvapsr1/(1+(16/100));
      pdvapciyr5 =  pdvapsi4*(1+(16/100))-rea;
      udlv6 = pdvapsi4-crpdltsi3;
      // pdudlv77 = (udlv6/pdvapsi4)*100; 
      pmdvap8 = 0;
      cccr9 = 0;
      pdvap10 = 0;
      ucidlu11 = 0;
      idlu12 = 0;
      fdume13 = 0;

      if(pdlttfac2 > acupsi ){ 
        alert("El precio de la Toma no puede ser superior al precio de la Guía Autometrica!")
        //swal("Error!", "El precio de la Toma no puede ser superior al precio de la Guía Autometrica");
        msjerror = "Incorrecto, Favor de Corregir"; 
        $("#botones").hide();
      }  else{msjerror = ""; $("#botones").show();}

      break;  
      case '4': 
      pdvapsr1 = val-(rea*-1);
      fdume13 = 100/(1-(pdudlv7/100))-100;
      pdlttfac2 = pdvapsr1*(1-(fdume13/100));
      crpdltsi3 = pdlttfac2/(1+(16/100));
      pdvapsi4 = pdvapsr1/(1+(16/100));
      pdvapciyr5 =  pdvapsi4*(1+(16/100))-rea;
      udlv6 = pdvapsi4-crpdltsi3;
      // pdudlv77 = (udlv6/pdvapsi4)*100; 
      pmdvap8 = pdvapsr1-rea;
      cccr9 = pdlttfac2-(rea/1.16);
      pdvap10 = pmdvap8;
      ucidlu11 = pmdvap8-cccr9;
      idlu12 = ucidlu11-(ucidlu11/1.16);

      if(pdlttfac2 > acucom ){ 
        alert("El precio de la Toma no puede ser superior al precio de la Guía Autometrica!")
        //swal("Error!", "El precio de la Toma no puede ser superior al precio de la Guía Autometrica");
        msjerror = "Incorrecto, Favor de Corregir"; 
        $("#botones").hide();
      }  else{msjerror = ""; $("#botones").show();}
      break;  
      case '5': 
      pdvapsr1 = val-(rea*-1);
      fdume13 = 100/(1-(pdudlv7/100))-100;
      pdlttfac2 = pdvapsr1*(1-(fdume13/100));
      crpdltsi3 = pdlttfac2/(1+(16/100));
      pdvapsi4 = pdvapsr1/(1+(16/100));
      pdvapciyr5 =  pdvapsi4*(1+(16/100))-rea;
      udlv6 = pdvapsi4-crpdltsi3;
      // pdudlv77 = (udlv6/pdvapsi4)*100; 
      pmdvap8 = pdvapsr1-rea;
      cccr9 = pdlttfac2-(rea/1.16);
      pdvap10 = pmdvap8;
      ucidlu11 = pmdvap8-cccr9;
      idlu12 = ucidlu11-(ucidlu11/1.16);

      if(pdlttfac2 > acuven ){ 
        alert("El precio de la Toma no puede ser superior al precio de la Guía Autometrica!")
        //swal("Error!", "El precio de la Toma no puede ser superior al precio de la Guía Autometrica");
        msjerror = "Incorrecto, Favor de Corregir"; 
        $("#botones").hide();
      }  else{msjerror = ""; $("#botones").show();}
      break;
      case '6': 
      pdvapsr1 = val-(rea*-1);
      fdume13 = 100/(1-(pdudlv7/100))-100;
      pdlttfac2 = pdvapsr1*(1-(fdume13/100));
      crpdltsi3 = pdlttfac2/(1+(16/100));
      pdvapsi4 = pdvapsr1/(1+(16/100));
      pdvapciyr5 =  pdvapsi4*(1+(16/100))-rea;
      udlv6 = pdvapsi4-crpdltsi3;
      // pdudlv77 = (udlv6/pdvapsi4)*100; 
      pmdvap8 = pdvapsr1-rea;
      cccr9 = pdlttfac2-(rea/1.16);
      pdvap10 = pmdvap8;
      ucidlu11 = pmdvap8-cccr9;
      idlu12 = ucidlu11-(ucidlu11/1.16);

      if(pdlttfac2 > acupno ){  
        alert("El precio de la Toma no puede ser superior al precio de la Guía Autometrica!")
        //swal("Error!", "El precio de la Toma no puede ser superior al precio de la Guía Autometrica");
        msjerror = "Incorrecto, Favor de Corregir"; 
        $("#botones").hide();
      }  else{msjerror = ""; $("#botones").show();}
      break;    
    } 

    const usCurrencyFormat = new Intl.NumberFormat('en-US', {style: 'currency', currency: 'USD'});
    document.getElementById('predeventa').innerHTML="<span style='color: black;'>"+usCurrencyFormat.format(pdvapsr1)+"</span>";
    document.getElementById('predetoma').innerHTML="<span style='color: green;'>"+usCurrencyFormat.format(pdlttfac2)+"</span>";
    document.getElementById('udelaven').innerHTML="<span style='color: black;'>"+usCurrencyFormat.format(udlv6)+"</span>";
    document.getElementById('predevensin').innerHTML="<span style='color: black;'>"+usCurrencyFormat.format(pdvapsi4)+"</span>";
    document.getElementById('predevencon').innerHTML="<span style='color: black;'>"+usCurrencyFormat.format(pdvapciyr5)+"</span>";
    document.getElementById('ctoreal').innerHTML="<span style='color: black;'>"+usCurrencyFormat.format(crpdltsi3)+"</span>";
    document.getElementById('premindeven').innerHTML="<span style='color: green;'>"+usCurrencyFormat.format(pmdvap8)+"</span>";
    document.getElementById('coconrea').innerHTML="<span style='color: black;'>"+usCurrencyFormat.format(cccr9)+"</span>";
    document.getElementById('predevenpu').innerHTML="<span style='color: black;'>"+usCurrencyFormat.format(pdvap10)+"</span>";
    document.getElementById('uconi').innerHTML="<span style='color: black;'>"+usCurrencyFormat.format(ucidlu11)+"</span>";
    document.getElementById('idelau').innerHTML="<span style='color: black;'>"+usCurrencyFormat.format(idlu12)+"</span>";
    document.getElementById('facdeu').innerHTML="<span style='color: black;'>"+fdume13.toFixed(2)+"</span>";
    document.getElementById('msjr').innerHTML="<span style='color: red;'>"+msjerror+"</span>";

    input1.value = pdvapsr1.toFixed(2);
    input2.value = pdlttfac2.toFixed(2); 
    input3.value = crpdltsi3.toFixed(2);
    input4.value = pdvapsi4.toFixed(2);
    input5.value = pdvapciyr5.toFixed(2);
    input6.value = udlv6.toFixed(2);
    input8.value = pmdvap8.toFixed(2);
    input9.value = cccr9.toFixed(2);
    input10.value = pdvap10.toFixed(2);
    input11.value = ucidlu11.toFixed(2);
    input12.value = idlu12.toFixed(2);
    input13.value = fdume13.toFixed(2);
    input1a.value = pdvapsr1.toFixed(2);
    input2a.value = pdlttfac2.toFixed(2); 
    input3a.value = crpdltsi3.toFixed(2);
    input4a.value = pdvapsi4.toFixed(2);
    input5a.value = pdvapciyr5.toFixed(2);
    input6a.value = udlv6.toFixed(2);
    input8a.value = pmdvap8.toFixed(2);
    input9a.value = cccr9.toFixed(2);
    input10a.value = pdvap10.toFixed(2);
    input11a.value = ucidlu11.toFixed(2);
    input12a.value = idlu12.toFixed(2);
    input13a.value = fdume13.toFixed(2);

  }

  function mostrar(id) {
    if (id == 1) {
      //$("#x1").show();
      $("#xa1").show();
      $("#xa2").show();
      $("#xa3").show();

     // $("#x2").show();
     $("#xb1").show();
     $("#xb2").show();
     $("#xb3").show();

      //$("#x3").hide();
      $("#xc1").hide();
      $("#xc2").hide();
      $("#xc3").hide();
      $("#xc4").hide();
      $("#xc5").hide();
      $("#xc6").hide();
     // $("#x4").hide();
   }

   if (id == 2) {
      //$("#x1").show();
      $("#xa1").show();
      $("#xa2").show();
      $("#xa3").show();

     // $("#x2").show();
     $("#xb1").show();
     $("#xb2").show();
     $("#xb3").show();

     // $("#x3").hide();
     $("#xc1").hide();
     $("#xc2").hide();
     $("#xc3").hide();
     $("#xc4").hide();
     $("#xc5").hide();
     $("#xc6").hide();
      //$("#x4").hide();
    }

    if (id == 3) {
     // $("#x1").show();
     $("#xa1").show();
     $("#xa2").show();
     $("#xa3").show();

     // $("#x2").show();
     $("#xb1").show();
     $("#xb2").show();
     $("#xb3").show();

     // $("#x3").hide();
     $("#xc1").hide();
     $("#xc2").hide();
     $("#xc3").hide();
     $("#xc4").hide();
     $("#xc5").hide();
     $("#xc6").hide();
     // $("#x4").hide();
   }

   if (id == 4) {
     // $("#x1").show();
     $("#xa1").show();
     $("#xa2").show();
     $("#xa3").show();

      //$("#x2").hide();
      $("#xb1").hide();
      $("#xb2").hide();
      $("#xb3").hide();

      //$("#x3").show();
      $("#xc1").show();
      $("#xc2").show();
      $("#xc3").show();
      $("#xc4").show();
      $("#xc5").show();
      $("#xc6").show();
     // $("#x4").show();
   }

   if (id == 5) {
     // $("#x1").show();
     $("#xa1").show();
     $("#xa2").show();
     $("#xa3").show();

     // $("#x2").hide();
     $("#xb1").hide();
     $("#xb2").hide();
     $("#xb3").hide();

     // $("#x3").show();
     $("#xc1").show();
     $("#xc2").show();
     $("#xc3").show();
     $("#xc4").show();
     $("#xc5").show();
     $("#xc6").show();
      //$("#x4").show();
    }

    if (id == 6) {
     // $("#x1").show();
     $("#xa1").show();
     $("#xa2").show();
     $("#xa3").show();

     // $("#x2").hide();
     $("#xb1").hide();
     $("#xb2").hide();
     $("#xb3").hide();

     // $("#x3").show();
     $("#xc1").show();
     $("#xc2").show();
     $("#xc3").show();
     $("#xc4").show();
     $("#xc5").show();
     $("#xc6").show();

     // $("#x4").show();
   }
 }

 $(document).ready(function(){

  $('#km').keyup(function(){
    var id=$(this).val();
    if (id.length > 1){ document.getElementById("compraautometrica").disabled = false;} 
    else { document.getElementById("compraautometrica").disabled = true; }
      //if(id > 80000){swal("Detalle!", "El Kilometraje No Puede ser superior a 80,000", "error")
     //   document.getElementById("km").value = "";
   // }
 });

  $('#compraautometrica').change(function(){
    var id=$(this).val();
    if (id.length > 2){ document.getElementById("ventaautometrica").disabled = false;} 
    else { document.getElementById("ventaautometrica").disabled = true;}
    if (document.getElementById("ventaautometrica").value>1){cambiaValores();}
  });

  $('#ventaautometrica').keyup(function(){
    var id=$(this).val();
    if (id.length > 3){ document.getElementById("premio").disabled = false;} 
    else { document.getElementById("premio").disabled = true;}
  });

  $('#equipamiento').keyup(function(){
    var id=$(this).val();
    if (id.length > 3){ document.getElementById("premio").disabled = false;} 
    else { document.getElementById("premio").disabled = true;}
  });

  $('#premio').keyup(function(){
    var id=$(this).val();
    if (id.length > 3){ document.getElementById("reacondicionamiento").disabled = false;} 
    else { document.getElementById("reacondicionamiento").disabled = true;}
  });

  $('#reacondicionamiento').keyup(function(){
    var id=$(this).val();
    if (id.length > 3){ document.getElementById("valor").disabled = false;} 
    else { document.getElementById("valor").disabled = true;}
  });

  $('#pdudlv7').change(function(){
   if(document.getElementById("valor").disabled === ''){
    alert("Introduce el Valor del Vehiculo!")}
    else { cambiaValores();}
  });

  $('#reacondicionamiento').blur(function(){
    if(document.getElementById("valor").value !== ''){cambiaValores();}
  });


  $('#vin').blur(function(){
    var id=$(this).val();
    msjerrorvin = "El VIN no puede contener mas de 17 Caracteres";
    if (id.length > 17)
    { 
     $("#vin").addClass('is-invalid');
     document.getElementById('mensajevin').innerHTML="<span style='font-size: 0.7em; color: red;'>"+msjerrorvin+"</span>";
   } 
 });

  return false;
});


</script>