<?php
$html = ' 
<table border="0" cellspacing="3" cellpadding="4">

<tr>
<td colspan="2">
Asunto:<br>
<label align="justify">Autorización para la expedición del Comprobante Fiscal Digital (CFDI) por la enajenación de mi vehículo usado para solicitar en su caso
mi inscripción al RFC.</label><br>
<label align="justify">Por este conducto y bajo protesta de decir verdad les informo que No me encuentro dado de alta bajo el régimen de Actividad
Empresarial, profesional o Régimen Incorporación Fiscal autorizo expresamente a:</label>
<br><br>
<label align="center"><font color="#0A6ACF">'.$take['bursocial'].'</font></label>
<br><br>
<label align="justify">Para que sus funcionarios facultados lleven a cabo el tramite de expedición del Comprobante Fiscal Digital ( CFDI) de acuerdo a la
reforma del Código Fiscal de la Federación que entro en vigor a partir del 1° de Enero de 2014, en lo concerniente a la enajenación de mi
vehículo usado con las siguientes características:</label>
<br>

<label align="justify">Marca Vehiculo: <font color="#0A6ACF">'.$take['brname'].'</font> </label>, Modelo: <font color="#0A6ACF">'.$take['modelotxt'].'</font>, Version: <font color="#0A6ACF">'.$take['versiontxt'].'</font>,  ID Vehicular: <font color="#0A6ACF">'.$take['nidenti'].'</font>
<br>
y en su caso tramitar mi inscripción ante el SAT para la obtención de mi RFC
<br>

<h4>Por tal motivo proporciono la siguiente información y documentación.</h4>

*CURP o copia del Acta de Nacimiento.
<br>
*Comprobante de domiciliario.
<br>
*Copia del Registro Federal de Contribuyentes.
<br>
*Identificación Oficial (IFE o Pasaporte vigente.
<br>
<br>
Datos del proveedor (Persona Física sin actividad empresarial, profesional o en régimen de Incorporación)<br><br>

<table border="0" cellspacing="3" cellpadding="4">
<tr>
<td>
<h4>Datos proveedor:</h4>
</td>
<td>
</td>
<td>
<h4>Domicilio:</h4>
</td>
<td>
</td>
</tr>

<tr>
	<td>
	Nombre(s): 			
	<br>
	Primer Apellido: 
	<br>
	Segundo Apellido: 		
	<br>
	Fecha Nacimiento: 
	<br>
	Curp: 
	<br>
	Actividad Preponderante: 
	<br>
	RFC: 
	<br>
	Telefono Fijo: 
	<br>			
	Telefono Movil: 
	</td>

	<td>
	<font color="#0A6ACF">'.$take['nombrescfdi'].'</font>			
	<br>
	<font color="#0A6ACF">'.$take['apellido1'].'</font>
	<br>
	<font color="#0A6ACF">'.$take['apellido2'].'</font>		
	<br>
	<font color="#0A6ACF">'.$take['fechanaci'].'</font>
	<br>
	<font color="#0A6ACF">'.$take['curp'].'</font>
	<br>
	<font color="#0A6ACF">'.$take['actividad'].'</font>
	<br>
	<font color="#0A6ACF">'.$take['rfc'].'</font>
	<br>
	<font color="#0A6ACF">'.$take['tel'].'</font>
	<br>			
	<font color="#0A6ACF">'.$take['cel'].'</font>
	</td>

	<td>
	Calle: 
	<br>
	Numero Ext: 
	<br>
	Colonia: 
	<br>													
	Codigo Postal: 
	<br>		
	Localidad o municipio: 
	<br>						
	Entidad Federativa: 
	<br>
	Pais:
	<br>
	Correo Electronico: 
	<br>
	</td>

	<td>
	<font color="#0A6ACF">'.$take['calle'].'</font>
	<br>
	<font color="#0A6ACF">'.$take['numext'].'</font>
	<br>
	<font color="#0A6ACF">'.$take['colonia'].'</font>
	<br>													
	<font color="#0A6ACF">'.$take['cp'].'</font>
	<br>		
	<font color="#0A6ACF">'.$take['municipio'].'</font>
	<br>						
	<font color="#0A6ACF">'.$take['entidadnaci'].'</font>
	<br>
	<font color="#0A6ACF">'.$take['pais'].'</font>
	<br>
	<font color="#0A6ACF">'.$take['email'].'</font>
	<br>
	</td>

</tr>
</table>

<h6>Todos los datos son obligatorios.</h6>
<br>

<label align="justify">Firmo de conformidad y declaro bajo protesta de decir verdad que los datos y documentación proporcionada son verdaderos.
</label>

<br><br><br><br>

<label align="center">
_________________________________________
<br>
<font color="#0A6ACF">'.$take['nombre'].'</font>
<br>
*La firma deberá ser igual a la de la Identificación.
</label>

<br><br>
<table border="1">
<H5>RMF 2020 2.4.3; Para los efectos del articulo 27 del CFF, podrán inscribirse en el RFC a través de los adquirientes de sus productos o de los
contribuyentes a los que les otorgue el uso o gose, de conformidad con el procedimiento que se señala en la pagina del internet del SAT, los
contribuyentes personas fisicas que: IV. Enajenen vehiculos usados, con excepción de aquellas que contribuyen en los términos de las
secciones I y II, del Capitulo II del Titulo IV de la Ley del ISR (en los Regímenes de las Personas Físicas con Actividades Empresariales y
Profesionales y el de Incorporación Fiscal).
<br>
...Los contribuyentes que opten por aplicar lo dispuesto en esta regla, deberán proporcionar a dichos adquirientes o a sus arrendatarios, así como a las
agricolas, pecuarios, acuicolas o pesqueros de un sistema Producto según sea el caso, lo siguiente: <br>
A) Nombre.
<br>
B) Curp o copia del acta de nacimiento.
<br>
C) Actividad preponderante que realiza.
<br>
D) Domicilio Fiscal.
<br>
</H5>
</table>

</td>
</tr>';
	


$html .=	'</tbody>
</table>			
';



tcpdf();
$obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
header('Content-type: application/pdf');
$obj_pdf->SetCreator(PDF_CREATOR);
$title = "Solicitud CFDI";
$obj_pdf->SetTitle($title);
$obj_pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $title, PDF_HEADER_STRING);
$obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$obj_pdf->SetDefaultMonospacedFont('helvetica');
$obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$obj_pdf->SetFont('helvetica', '', 9);
$obj_pdf->setFontSubsetting(false);
$obj_pdf->AddPage();
ob_start();
    // podemos tener cualquier parte de la vista aquí como HTML, PHP, etc.
$content = ob_get_contents();
ob_end_clean();
$obj_pdf->writeHTML($content, true, false, true, false, '');
$obj_pdf->writeHTML($html, true, false, true, false, '');


$filename = 'solicitud_cfdi'.'-'.$take['folio'];

$obj_pdf->Output($filename . '.pdf', 'D');




?>