 
 
 //-------------------------------------------------------------------

// Validaciones del Formulario

//-------------------------------------------------------------------

const formulario = document.getElementById('formulario');
const inputs = document.querySelectorAll('#formulario input');

msjerrorvin = "El VIN debe contener de 10 a 16 Caracteres sin simbolos";
 

const expresiones = {
	usuario: /^[a-zA-Z0-9\_\-]{4,16}$/, // Letras, numeros, guion y guion_bajo
	nombre: /^[a-zA-ZÀ-ÿ\s]{1,40}$/, // Letras y espacios, pueden llevar acentos.
	password: /^.{4,12}$/, // 4 a 12 digitos.
	correo: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/,
	telefono: /^\d{7,14}$/, // 7 a 14 numeros.
	vin: /^[a-zA-Z0-9\s]{10,16}$/, // de 10 a 16 Letras o numeros
	km: /^\d{2,9}$/, // 7 a 14 numeros.
	
 
	prueba: /^([0-9])*$/

}

const validarFormulario = (e) => {
	switch (e.target.name) {
	    
		case "vin":
			if(expresiones.vin.test(e.target.value)){
			    document.getElementById(`vin`).classList.remove('is-invalid');
			    document.getElementById(`vin`).classList.add('is-valid');
			    document.getElementById(`mensajevin`).classList.remove('forminput-error-activo');
			}else{
		        document.getElementById(`vin`).classList.add('is-invalid');
		        document.getElementById(`vin`).classList.remove('is-valid');
		        document.getElementById(`mensajevin`).classList.add('forminput-error-activo');
			}
		break;
		
		case "km":
			if(expresiones.km.test(e.target.value)){
			    document.getElementById(`km`).classList.remove('is-invalid');
			    document.getElementById(`km`).classList.add('is-valid');
			    document.getElementById(`mensajekm`).classList.remove('forminput-error-activo');
			}else{
		        document.getElementById(`km`).classList.add('is-invalid');
		        document.getElementById(`km`).classList.remove('is-valid');
		        document.getElementById(`mensajekm`).classList.add('forminput-error-activo');
			}
		break;
		
	}
}

inputs.forEach((input) => {
	input.addEventListener('keyup', validarFormulario);
	input.addEventListener('blur', validarFormulario);
});


formulario.addEventListener('submit', (e) => {
	e.preventDefault();

	const terminos = document.getElementById('terminos');
	if(campos.vin && campos.modelo && campos.version ){
		formulario.reset();

		document.getElementById('formulario__mensaje-exito').classList.add('formulario__mensaje-exito-activo');
		setTimeout(() => {
			document.getElementById('formulario__mensaje-exito').classList.remove('formulario__mensaje-exito-activo');
		}, 5000);

		document.querySelectorAll('.formulario__grupo-correcto').forEach((icono) => {
			icono.classList.remove('formulario__grupo-correcto');
		});
	} else {
		document.getElementById('formulario__mensaje').classList.add('formulario__mensaje-activo');
	}
	
});




 document.addEventListener('DOMContentLoaded', () => {
    document.querySelectorAll('input[type=text]').forEach( node => node.addEventListener('keypress', e => {
      if(e.keyCode == 13) {
        e.preventDefault();
      }
    }))
  });
 

 // ! function(window, document, $) {

  //  "use strict";

 //   $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();

 // }(window, document, jQuery);

 




function obtener(){
  //para obtener el texto

  var year = new Date().getFullYear();
  var numero = document.getElementById("anio").value;

  if( (year-numero) <= 5 ) {
        // Hacer algo si el checkbox ha sido seleccionado
        alert("El Auto es menor a 5 años");
        //status22.value = 1;
        document.getElementById('prebasestado3').innerHTML=1;
    } else {
        // Hacer algo si el checkbox ha sido deseleccionado
        alert("El Auto es mayor a 5 años");
        //status22.value = 0;
        document.getElementById('prebasestado3').innerHTML=0;
    }
   
}






 $("#compraautometrica,#ventaautometrica,#reacondicionamiento,#equipamiento,#premio,#valor").inputmask("currency",{

   numericInput: true,

   rightAlign: true,

   digitsOptional: true,

   prefix:'',

   inputFormat: "999,999,999.99",

   outputFormat: "999999999.99",

   inputMode:"numeric",

   inputEventOnly: true,

   removeMaskOnSubmit: true,

   }); //default





 $(function () {

    //Initialize Select2 Elements

    $('.select2').select2()



    //Datemask dd/mm/yyyy

    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })

    //Datemask2 mm/dd/yyyy

    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })

    //Money Euro

    $('[data-mask]').inputmask()



    //Date range picker

    $('#reservation').daterangepicker()

    //Date range picker with time picker

    $('#reservationtime').daterangepicker({

      timePicker         : true,

      timePickerIncrement: 30,

      format             : 'MM/DD/YYYY h:mm A'

    })

    //Date range as a button

    $('#daterange-btn').daterangepicker(

    {

      ranges   : {

        'Today'       : [moment(), moment()],

        'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],

        'Last 7 Days' : [moment().subtract(6, 'days'), moment()],

        'Last 30 Days': [moment().subtract(29, 'days'), moment()],

        'This Month'  : [moment().startOf('month'), moment().endOf('month')],

        'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]

      },

      startDate: moment().subtract(29, 'days'),

      endDate  : moment()

    },

    function (start, end) {

      $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))

    }

    )



    //iCheck for checkbox and radio inputs

    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({

      checkboxClass: 'icheckbox_minimal-blue',

      radioClass   : 'iradio_minimal-blue'

    })

    //Red color scheme for iCheck

    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({

      checkboxClass: 'icheckbox_minimal-red',

      radioClass   : 'iradio_minimal-red'

    })

    //Flat red color scheme for iCheck

    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({

      checkboxClass: 'icheckbox_flat-green',

      radioClass   : 'iradio_flat-green'

    })



    //Colorpicker

    $('.my-colorpicker1').colorpicker()

    //color picker with addon

    $('.my-colorpicker2').colorpicker()



    //Timepicker

    $('.timepicker').timepicker({

      showInputs: false

    })

  })

 

  $("#forms").addClass('active');

  $("#advanced").addClass('active');

 


 

//-------------------------------------------------------------------

// Scrip para cambiar el año y los modelos 

//-------------------------------------------------------------------

var csfr_token_name = '<?php echo $this->security->get_csrf_token_name(); ?>';

var csfr_token_value = '<?php echo $this->security->get_csrf_hash(); ?>';





$(function(){



  $(document).on('change','.country',function()

  {

    if(this.value === '')

    {

      $('.state').html('<option value="">Select Option</option>');

      $('.city').html('<option value="">Select Option</option>');

      return false;

    }



    var data =  {

      country : this.value,

    }

    data[csfr_token_name] = csfr_token_value;

    $.ajax({

      type: "POST",

      url: "<?= base_url('admin/auth/get_brands_years') ?>",

      data: data,

      dataType: "json",

      success: function(obj) {

        $('.state').html(obj.msg);

      },

    });

  });



  $(document).on('change','.state',function()

  {

    var data =  {

      state : this.value,

    }



    data[csfr_token_name] = csfr_token_value;

    $.ajax({

      type: "POST",

      url: "<?= base_url('admin/auth/get_years_models') ?>",

      data: data,

      dataType: "json",

      success: function(obj) {

        $('.city').html(obj.msg);

      },



    });

  });

});

 

  function cambiafolio() {



    var a1 = document.getElementById("agencia").value;

    var a2 = document.getElementById("identi").value;

    var folio = document.getElementById("folio");



    switch (a1)

    {

      case '1': 

      folio.value = "GPF"+a2;

      break;

      case '2':

      folio.value = "NIA"+a2;

      break;

      case '3':

      folio.value = "NAL"+a2;

      break;

      case '4':

      folio.value = "HMN"+a2;

      break;

      case '5':

      folio.value = "HAT"+a2;

      break;

      case '6':

      folio.value = "HDF"+a2;

      break;

      case '7':

      folio.value = "HAL"+a2;

      break;

      case '8':

      folio.value = "HUR"+a2;

      break;

      case '9':

      folio.value = "HMM"+a2;

      break;

      case '10':

      folio.value = "HCO"+a2;

      break;

      case '11':

      folio.value = "HMR"+a2;

      break;

      case '12':

      folio.value = "HSJ"+a2;

      break;

      case '13':

      folio.value = "KPE"+a2;

      break;

      case '14':

      folio.value = "KUR"+a2;

      break;

      case '15':

      folio.value = "KMC"+a2;

      break;

      case '16':

      folio.value = "KDU"+a2;

      break;

      case '17':

      folio.value = "CMM"+a2;

      break;

      case '18':

      folio.value = "CLZ"+a2;

      break;

      case '19':

      folio.value = "CUR"+a2;

      break;

      case '20':

      folio.value = "CAP"+a2;

      break;

      case '21':

      folio.value = "CZA"+a2;

      break;

      case '22':

      folio.value = "TUR"+a2;

      break;

      case '23':

      folio.value = "TAL"+a2;

      break;

      case '24':

      folio.value = "TVA"+a2;

      break;

      case '25':

      folio.value = "TPE"+a2;

      break;

      case '26':

      folio.value = "MQR"+a2;

      break;

      case '27':

      folio.value = "MUR"+a2;

      break;

      case '28':

      folio.value = "GMC"+a2;

      break;

      case '29':

      folio.value = "CAU"+a2;

      break;

      case '30':

      folio.value = "BMW"+a2;

      break;

      case '31':

      folio.value = "MOT"+a2;

      break;

      case '32':

      folio.value = "MIN"+a2;

      break;

      case '33':

      folio.value = "CHQ"+a2;

      break;

      case '34':

      folio.value = "CHU"+a2;

      break;

      case '35':

      folio.value = "ACJ"+a2;

      break;

      case '36':

      folio.value = "ISU"+a2;

      break;

      case '37':

      folio.value = "VW"+a2;

      break;

      case '38':

      folio.value = "DWA"+a2;

      break;

      case '39':

      folio.value = "SDA"+a2;

      break;

      case '40':

      folio.value = "MM"+a2;

      break;

      case '41':

      folio.value = "NTF"+a2;

      break;

      case '42':

      folio.value = "ACQ"+a2;

      break;

    }

  }





  function cambiaValores2() {
    var inputNombre = document.getElementById("nombre");
    inputNombre.value = "DYP";
}



  function cambiaValores() {



    var ver = document.getElementById("ver");
    var input1 = document.getElementById("pdvapsr1");
    var input2 = document.getElementById("pdlttfac2");
    var input3 = document.getElementById("crpdltsi3");
    var input4 = document.getElementById("pdvapsi4");
    var input5 = document.getElementById("pdvapciyr5");
    var input6 = document.getElementById("udlv6");
    var input8 = document.getElementById("pmdvap8");
    var input9 = document.getElementById("cccr9");
    var input10 = document.getElementById("pdvap10");
    var input11 = document.getElementById("ucidlu11");
    var input12 = document.getElementById("idlu12");
    var input13 = document.getElementById("fdume13");
    var input1a = document.getElementById("pdvapsr1a");
    var input2a = document.getElementById("pdlttfac2a");
    var input3a = document.getElementById("crpdltsi3a");
    var input4a = document.getElementById("pdvapsi4a");
    var input5a = document.getElementById("pdvapciyr5a");
    var input6a = document.getElementById("udlv6a");
    var input8a = document.getElementById("pmdvap8a");
    var input9a = document.getElementById("cccr9a");
    var input10a = document.getElementById("pdvap10a");
    var input11a = document.getElementById("ucidlu11a");
    var input12a = document.getElementById("idlu12a");
    var input13a = document.getElementById("fdume13a");

    Tguia = document.getElementById("tipoguia").value;

    var vala = document.getElementById("valor").value;
    var compa = document.getElementById("compraautometrica").value;
    var venta = document.getElementById("ventaautometrica").value;
    var equia = document.getElementById("equipamiento").value;
    var prema = document.getElementById("premio").value;
    var reaa = document.getElementById("reacondicionamiento").value;
    var vala = vala.replace(",", "");
    var compa = compa.replace(",", "");
    var venta = venta.replace(",", "");
    var equia = equia.replace(",", "");
    var prema = prema.replace(",", "");
    var reaa = reaa.replace(",", "");
    var vala = vala.replace("$", "");
    var compa = compa.replace("$", "");
    var venta = venta.replace("$", "");
    var equia = equia.replace("$", "");
    var prema = prema.replace("$", "");
    var reaa = reaa.replace("$", "");
    var val =  parseFloat(vala);
    var comp =  parseFloat(compa);
    var vent =  parseFloat(venta);
    var equi =  parseFloat(equia);
    var prem =  parseFloat(prema);
    var rea =  parseFloat(reaa);
    var d16 = parseFloat(comp);
    var d17 = parseFloat(vent);
    var d18 = parseFloat(equi);
    var d19 = parseFloat(prem);
    var d20 = parseFloat(rea);
    
    var  msjerror = ""

    acucom = d16 + d18 + d19 + d20;
    acuven = d17 + d18 + d19 + d20;
    acupno = ((d16+d17)/2) + d18 + d19 + d20;
    acupsi = ((d16+d17)/2) + d18 + d19;


    comprasi = d16 + d18 + d19 + d20;
    promediosi = ((d16+d17)/2) + d18 + d19;
    ventasi = d17 + d18 + d19 + d20;

    comprano = d16 + d18 + d19 + d20;
    promediono = ((d16+d17)/2) + d18 + d19 + d20;
    ventano = d17 + d18 + d19 + d20;

    pdudlv7 = document.getElementById("pdudlv7").value;

    //porutilidad2 = $("#pdudlv7 option:selected").text();
    //var xguia = $('#tipoguia').val();
    //if(val === ''){ swal("Detalle!", "Introduce el Valor del Vehiculo!", "warning") }  
    //if(rea > 1){ swal("Detalle!", "Introduce un valor negativo en Reacondicionamiento!", "warning") }

    switch (Tguia)

    {

      case '1':  
      pdvapsr1 = val-(rea*-1); //val-rea;
      pdlttfac2 = pdvapsr1*(1-(pdudlv7/100));
      crpdltsi3 = pdlttfac2/(1+(16/100));
      pdvapsi4 = pdvapsr1/(1+(16/100));
      pdvapciyr5 =  pdvapsi4*(1+(16/100))-rea;
      udlv6 = pdvapsi4-crpdltsi3;
      // pdudlv77 = (udlv6/pdvapsi4)*100;
      pmdvap8 = 0;
      cccr9 = 0;
      pdvap10 = 0;
      ucidlu11 = 0;
      idlu12 = 0;
      fdume13 = 100/(1-(pdudlv7/100))-100;

      if(isNaN(pdvapsr1)){pdvapsr1=0}
        if(isNaN(pdlttfac2)){pdlttfac2=0}
          if(isNaN(crpdltsi3)){crpdltsi3=0}
            if(isNaN(pdvapsi4)){pdvapsi4=0}
              if(isNaN(pdvapciyr5)){pdvapciyr5=0}
                if(isNaN(udlv6)){udlv6=0}
                  if(isNaN(pmdvap8)){pmdvap8=0}
                    if(isNaN(cccr9)){cccr9=0}
                      if(isNaN(pdvap10)){pdvap10=0}
                        if(isNaN(ucidlu11)){ucidlu11=0}
                          if(isNaN(idlu12)){idlu12=0}
                            if(isNaN(fdume13)){fdume13=0}

                              if(pdlttfac2 > comprasi )
                              {
                                alert("El precio de la TOMA TOPE FINAL no puede ser superior al PRECIO DE VENTA de la Guía Autometrica!")
                                  //swal("Error!", "El precio de la Toma no puede ser superior al precio de la Guía Autometrica");
                                  msjerror = "Incorrecto, Favor de Corregir"; 
                                  $("#botones").hide();
                                }  
                                else{msjerror = ""; $("#botones").show();}

                                break;  

                                case '2': 

                                pdvapsr1 = val-(rea*-1);

                                pdlttfac2 = pdvapsr1*(1-(pdudlv7/100));

                                crpdltsi3 = pdlttfac2/(1+(16/100));

                                pdvapsi4 = pdvapsr1/(1+(16/100));

                                pdvapciyr5 =  pdvapsi4*(1+(16/100))-rea;

                                udlv6 = pdvapsi4-crpdltsi3;

      // pdudlv77 = (udlv6/pdvapsi4)*100; 

      pmdvap8 = 0;

      cccr9 = 0;

      pdvap10 = 0;

      ucidlu11 = 0;

      idlu12 = 0;

      fdume13 = 100/(1-(pdudlv7/100))-100;



      if(pdlttfac2 > promediosi ){ 

        alert("El precio de la Toma no puede ser superior al precio de la Guía Autometrica!")

        //swal("Error!", "El precio de la Toma no puede ser superior al precio de la Guía Autometrica");

        msjerror = "Incorrecto, Favor de Corregir"; 

        $("#botones").hide();

      }  else{msjerror = ""; $("#botones").show();}

      break;  

      case '3': 

      pdvapsr1 = val-(rea*-1); //val+rea;

      pdlttfac2 = pdvapsr1*(1-(pdudlv7/100));

      crpdltsi3 = pdlttfac2/(1+(16/100));

      pdvapsi4 = pdvapsr1/(1+(16/100));

      pdvapciyr5 =  pdvapsi4*(1+(16/100))-rea;

      udlv6 = pdvapsi4-crpdltsi3;

      // pdudlv77 = (udlv6/pdvapsi4)*100; 

      pmdvap8 = 0;

      cccr9 = 0;

      pdvap10 = 0;

      ucidlu11 = 0;

      idlu12 = 0;

      fdume13 = 100/(1-(pdudlv7/100))-100;



      if(pdlttfac2 > ventasi ){ 

        alert("El precio de la Toma no puede ser superior al precio de la Guía Autometrica!")

        //swal("Error!", "El precio de la Toma no puede ser superior al precio de la Guía Autometrica");

        msjerror = "Incorrecto, Favor de Corregir"; 

        $("#botones").hide();

      }  else{msjerror = ""; $("#botones").show();}



      break;  

      case '4': 

      pdvapsr1 = val-(rea*-1);

      fdume13 = 100/(1-(pdudlv7/100))-100;

      pdlttfac2 = pdvapsr1*(1-(fdume13/100));

      crpdltsi3 = pdlttfac2/(1+(16/100));

      pdvapsi4 = pdvapsr1/(1+(16/100));

      pdvapciyr5 =  pdvapsi4*(1+(16/100))-rea;

      udlv6 = pdvapsi4-crpdltsi3;

      // pdudlv77 = (udlv6/pdvapsi4)*100; 

      pmdvap8 = pdvapsr1-rea;

      cccr9 = pdlttfac2-(rea/1.16);

      pdvap10 = pmdvap8;

      ucidlu11 = pmdvap8-cccr9;

      idlu12 = ucidlu11-(ucidlu11/1.16);



      if(pdlttfac2 > comprano ){ 

        alert("El precio de la Toma no puede ser superior al precio de la Guía Autometrica!")

        //swal("Error!", "El precio de la Toma no puede ser superior al precio de la Guía Autometrica");

        msjerror = "Incorrecto, Favor de Corregir"; 

        $("#botones").hide();

      }  else{msjerror = ""; $("#botones").show();}

      break;  

      case '5': 

      pdvapsr1 = val-(rea*-1);

      fdume13 = 100/(1-(pdudlv7/100))-100;

      pdlttfac2 = pdvapsr1*(1-(fdume13/100));

      crpdltsi3 = pdlttfac2/(1+(16/100));

      pdvapsi4 = pdvapsr1/(1+(16/100));

      pdvapciyr5 =  pdvapsi4*(1+(16/100))-rea;

      udlv6 = pdvapsi4-crpdltsi3;

      // pdudlv77 = (udlv6/pdvapsi4)*100; 

      pmdvap8 = pdvapsr1-rea;

      cccr9 = pdlttfac2-(rea/1.16);

      pdvap10 = pmdvap8;

      ucidlu11 = pmdvap8-cccr9;

      idlu12 = ucidlu11-(ucidlu11/1.16);



      if(pdlttfac2 > promediono ){ 

        alert("El precio de la Toma no puede ser superior al precio de la Guía Autometrica!")

        //swal("Error!", "El precio de la Toma no puede ser superior al precio de la Guía Autometrica");

        msjerror = "Incorrecto, Favor de Corregir"; 

        $("#botones").hide();

      }  else{msjerror = ""; $("#botones").show();}

      break;

      case '6': 

      pdvapsr1 = val-(rea*-1);

      fdume13 = 100/(1-(pdudlv7/100))-100;

      pdlttfac2 = pdvapsr1*(1-(fdume13/100));

      crpdltsi3 = pdlttfac2/(1+(16/100));

      pdvapsi4 = pdvapsr1/(1+(16/100));

      pdvapciyr5 =  pdvapsi4*(1+(16/100))-rea;

      udlv6 = pdvapsi4-crpdltsi3;

      // pdudlv77 = (udlv6/pdvapsi4)*100; 

      pmdvap8 = pdvapsr1-rea;

      cccr9 = pdlttfac2-(rea/1.16);

      pdvap10 = pmdvap8;

      ucidlu11 = pmdvap8-cccr9;

      idlu12 = ucidlu11-(ucidlu11/1.16);



      if(pdlttfac2 > ventano ){  

        alert("El precio de la Toma no puede ser superior al precio de la Guía Autometrica!")

        //swal("Error!", "El precio de la Toma no puede ser superior al precio de la Guía Autometrica");

        msjerror = "Incorrecto, Favor de Corregir"; 

        $("#botones").hide();

      }  else{msjerror = ""; $("#botones").show();}

      break;    

    } 



    const usCurrencyFormat = new Intl.NumberFormat('en-US', {style: 'currency', currency: 'USD'});

    document.getElementById('predeventa').innerHTML="<span style='color: black;'>"+usCurrencyFormat.format(pdvapsr1)+"</span>";

    document.getElementById('predetoma').innerHTML="<span style='color: green;'>"+usCurrencyFormat.format(pdlttfac2)+"</span>";

    document.getElementById('udelaven').innerHTML="<span style='color: black;'>"+usCurrencyFormat.format(udlv6)+"</span>";

    document.getElementById('predevensin').innerHTML="<span style='color: black;'>"+usCurrencyFormat.format(pdvapsi4)+"</span>";

    document.getElementById('predevencon').innerHTML="<span style='color: black;'>"+usCurrencyFormat.format(pdvapciyr5)+"</span>";

    document.getElementById('ctoreal').innerHTML="<span style='color: black;'>"+usCurrencyFormat.format(crpdltsi3)+"</span>";

    document.getElementById('premindeven').innerHTML="<span style='color: green;'>"+usCurrencyFormat.format(pmdvap8)+"</span>";

    document.getElementById('coconrea').innerHTML="<span style='color: black;'>"+usCurrencyFormat.format(cccr9)+"</span>";

    document.getElementById('predevenpu').innerHTML="<span style='color: black;'>"+usCurrencyFormat.format(pdvap10)+"</span>";

    document.getElementById('uconi').innerHTML="<span style='color: black;'>"+usCurrencyFormat.format(ucidlu11)+"</span>";

    document.getElementById('idelau').innerHTML="<span style='color: black;'>"+usCurrencyFormat.format(idlu12)+"</span>";

    document.getElementById('facdeu').innerHTML="<span style='color: black;'>"+Number(fdume13).toFixed(2)+"</span>";

    document.getElementById('msjr').innerHTML="<span style='color: red;'>"+msjerror+"</span>";



    input1.value = Number(pdvapsr1).toFixed(2);

    input2.value = Number(pdlttfac2).toFixed(2); 

    input3.value = Number(crpdltsi3).toFixed(2);

    input4.value = Number(pdvapsi4).toFixed(2);

    input5.value = Number(pdvapciyr5).toFixed(2);

    input6.value = Number(udlv6).toFixed(2);

    input8.value = Number(pmdvap8).toFixed(2);

    input9.value = Number(cccr9).toFixed(2);

    input10.value = Number(pdvap10).toFixed(2);

    input11.value = Number(ucidlu11).toFixed(2);

    input12.value = Number(idlu12).toFixed(2);

    input13.value = Number(fdume13).toFixed(2);

    input1a.value = Number(pdvapsr1).toFixed(2);

    input2a.value = Number(pdlttfac2).toFixed(2); 

    input3a.value = Number(crpdltsi3).toFixed(2);

    input4a.value = Number(pdvapsi4).toFixed(2);

    input5a.value = Number(pdvapciyr5).toFixed(2);

    input6a.value = Number(udlv6).toFixed(2);

    input8a.value = Number(pmdvap8).toFixed(2);

    input9a.value = Number(cccr9).toFixed(2);

    input10a.value = Number(pdvap10).toFixed(2);

    input11a.value = Number(ucidlu11).toFixed(2);

    input12a.value = Number(idlu12).toFixed(2);

    input13a.value = Number(fdume13).toFixed(2);



  }



  function mostrar(id) {

    if (id == 1) {

      //$("#x1").show();

      $("#xa1").show();

      $("#xa2").show();

      $("#xa3").show();



     // $("#x2").show();

     $("#xb1").show();

     $("#xb2").show();

     $("#xb3").show();



      //$("#x3").hide();

      $("#xc1").hide();

      $("#xc2").hide();

      $("#xc3").hide();

      $("#xc4").hide();

      $("#xc5").hide();

      $("#xc6").hide();

     // $("#x4").hide();

   }



   if (id == 2) {

      //$("#x1").show();

      $("#xa1").show();

      $("#xa2").show();

      $("#xa3").show();



     // $("#x2").show();

     $("#xb1").show();

     $("#xb2").show();

     $("#xb3").show();



     // $("#x3").hide();

     $("#xc1").hide();

     $("#xc2").hide();

     $("#xc3").hide();

     $("#xc4").hide();

     $("#xc5").hide();

     $("#xc6").hide();

      //$("#x4").hide();

    }



    if (id == 3) {

     // $("#x1").show();

     $("#xa1").show();

     $("#xa2").show();

     $("#xa3").show();



     // $("#x2").show();

     $("#xb1").show();

     $("#xb2").show();

     $("#xb3").show();



     // $("#x3").hide();

     $("#xc1").hide();

     $("#xc2").hide();

     $("#xc3").hide();

     $("#xc4").hide();

     $("#xc5").hide();

     $("#xc6").hide();

     // $("#x4").hide();

   }



   if (id == 4) {

     // $("#x1").show();

     $("#xa1").show();

     $("#xa2").show();

     $("#xa3").show();



      //$("#x2").hide();

      $("#xb1").hide();

      $("#xb2").hide();

      $("#xb3").hide();



      //$("#x3").show();

      $("#xc1").show();

      $("#xc2").show();

      $("#xc3").show();

      $("#xc4").show();

      $("#xc5").show();

      $("#xc6").show();

     // $("#x4").show();

   }



   if (id == 5) {

     // $("#x1").show();

     $("#xa1").show();

     $("#xa2").show();

     $("#xa3").show();



     // $("#x2").hide();

     $("#xb1").hide();

     $("#xb2").hide();

     $("#xb3").hide();



     // $("#x3").show();

     $("#xc1").show();

     $("#xc2").show();

     $("#xc3").show();

     $("#xc4").show();

     $("#xc5").show();

     $("#xc6").show();

      //$("#x4").show();

    }



    if (id == 6) {

     // $("#x1").show();

     $("#xa1").show();

     $("#xa2").show();

     $("#xa3").show();



     // $("#x2").hide();

     $("#xb1").hide();

     $("#xb2").hide();

     $("#xb3").hide();



     // $("#x3").show();

     $("#xc1").show();

     $("#xc2").show();

     $("#xc3").show();

     $("#xc4").show();

     $("#xc5").show();

     $("#xc6").show();



     // $("#x4").show();

   }

 }



 $(document).ready(function(){


  $("#siniestros").on('change', function() {

     var s2 = document.getElementById("prebasestado2");
    
    if( $(this).is(':checked') ) {
        // Hacer algo si el checkbox ha sido seleccionado
       // alert("El Auto SI Tiene Siniestros");
        //status22.value = 1;
        //document.getElementById('prebasestado2').innerHTML=0;
        //document.getElementById('prebasestado222').innerHTML=0;
        s2.value = 0;
    } else {
        // Hacer algo si el checkbox ha sido deseleccionado
       // alert("El Auto NO Tiene Siniestros");
        //status22.value = 0;
        //document.getElementById('prebasestado2').innerHTML=1;
        //document.getElementById('prebasestado222').innerHTML=1;
        s2.value = 1;
    }
});

$("#anio").on('change', function() {

  var year = new Date().getFullYear();
  var numero = document.getElementById("anio").value;
  
  var s3 = document.getElementById("prebasestado3");

  if( (year-numero) <= 5 ) {
        // Hacer algo si el checkbox ha sido seleccionado
        //alert("El Auto es menor a 5 años");
        //status22.value = 1;
        //document.getElementById('prebasestado3').innerHTML=1;
        //document.getElementById('prebasestado33').innerHTML=1;
        s3.value = 1;
    } else {
        // Hacer algo si el checkbox ha sido deseleccionado
        // alert("El Auto es mayor a 5 años");
        //status22.value = 0;
        //document.getElementById('prebasestado3').innerHTML=0;
        //document.getElementById('prebasestado33').innerHTML=0;
        s3.value = 0;
    }
});


$("#km").on('change', function() {

  var km=$(this).val();
  var s4 = document.getElementById("prebasestado4");

  if( km <= 80000 ) {
        // Hacer algo si el checkbox ha sido seleccionado
        // alert("El Auto es menor a 80000km");
        //status22.value = 1;
        //document.getElementById('prebasestado4').innerHTML=1;
        //document.getElementById('prebasestado44').innerHTML=1;
        s4.value = 1;
    } else {
        // Hacer algo si el checkbox ha sido deseleccionado
        // alert("El Auto es mayor a 80000km");
        //status22.value = 0;
        //document.getElementById('prebasestado4').innerHTML=0;
        //document.getElementById('prebasestado44').innerHTML=0;
        s4.value = 0;
    }

    if (km.length > 2){ document.getElementById("compraautometrica").disabled = false;} 
    else { document.getElementById("compraautometrica").disabled = true; }
});


  //$('#km').keyup(function(){

  //  var id=$(this).val();

  //  if (id.length > 3){ document.getElementById("compraautometrica").disabled = false;} 

  //  else { document.getElementById("compraautometrica").disabled = true; }

  //  if(id<=)

      //if(id > 80000){swal("Detalle!", "El Kilometraje No Puede ser superior a 80,000", "error")

     //   document.getElementById("km").value = "";

   // }

// }); 

    $('#compraautometrica').change(function(){
    var id=$(this).val();
    if (id.length > 2){ document.getElementById("ventaautometrica").disabled = false;} 
    else { document.getElementById("ventaautometrica").disabled = true;}
    if (document.getElementById("ventaautometrica").value>1){cambiaValores();}
  });

  $('#ventaautometrica').keyup(function(){
    var id=$(this).val();
    if (id.length > 3){ document.getElementById("premio").disabled = false;} 
    else { document.getElementById("premio").disabled = true;}
  });

  $('#equipamiento').keyup(function(){

    var id=$(this).val();

    if (id.length > 3){ document.getElementById("premio").disabled = false;} 

    else { document.getElementById("premio").disabled = true;}

  });

  $('#premio').keyup(function(){

    var id=$(this).val();

    if (id.length > 3){ document.getElementById("reacondicionamiento").disabled = false;} 

    else { document.getElementById("reacondicionamiento").disabled = true;}

  });

  $('#reacondicionamiento').keyup(function(){

    var id=$(this).val();

    if (id.length > 3){ document.getElementById("valor").disabled = false;} 

    else { document.getElementById("valor").disabled = true;}

  });

  $('#pdudlv7').change(function(){

   if(document.getElementById("valor").disabled === ''){

    alert("Introduce el Valor del Vehiculo!")}

    else { cambiaValores();}

  });

  $('#reacondicionamiento').blur(function(){

    if(document.getElementById("valor").value !== ''){cambiaValores();}

  });

  //$('#vin').blur(function(){

//var id=$(this).val();

  // msjerrorvin = "El VIN no puede contener mas de 17 Caracteres";

  // if (id.length > 17)

  // { 

  //  $("#vin").addClass('is-invalid');

  //  document.getElementById('mensajevin').innerHTML="<span style='font-size: 0.7em; color: red;'>"+msjerrorvin+"</span>";

 // } 

//});



  return false;

});







 

 

 

  
 
 




















